package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.repository.*;
import com.hr.hpital.manager.service.impl.ShiftServiceImpl;
import com.hr.hpital.manager.service.mapper.ShiftMapper;
import com.hr.hpital.manager.service.mapper.UserMapper;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * Unit test class for the {@link ShiftService} utility class.
 */
public class ShiftServiceImplUT {

    private final ShiftRepository shiftRepository = mock(ShiftRepository.class);
    private final ShiftTypeRepository shiftTypeRepository = mock(ShiftTypeRepository.class);
    private final DayConfigShiftTypeRepository dayConfigShiftTypeRepository = mock(DayConfigShiftTypeRepository.class);
    private final DayConfigShiftTypeService dayConfigShiftTypeService = mock(DayConfigShiftTypeService.class);
    private final WorkerAvailabilityService workerAvailabilityService = mock(WorkerAvailabilityService.class);
    private final UserShiftTypeRepository userShiftTypeRepository = mock(UserShiftTypeRepository.class);
    private final ShiftMapper shiftMapper = mock(ShiftMapper.class);
    private final UserService userService = mock(UserService.class);
    private final RequestContext requestContext = mock(RequestContext.class);
    private final TeamUserService teamUserService = mock(TeamUserService.class);
    private final ShiftWorkerService shiftWorkerService = mock(ShiftWorkerService.class);
    private final SpecializationService specializationService = mock(SpecializationService.class);
    private final UserMapper userMapper = mock(UserMapper.class);
    private final ShiftServiceImpl shiftServiceMock = mock(ShiftServiceImpl.class);

    private final ShiftServiceImpl shiftService = new ShiftServiceImpl(
        shiftRepository, shiftTypeRepository, dayConfigShiftTypeService, dayConfigShiftTypeRepository, workerAvailabilityService, userShiftTypeRepository,
        shiftMapper, userService, requestContext, teamUserService, shiftWorkerService, specializationService, userMapper);


    @Test
    public void testCreateShift() {
        testCreateShift(null, false);
        testCreateShift(null, true);

        final User user = new User();
        user.setId(1003L);
        testCreateShift(user, false);
        testCreateShift(user, true);
    }

    private void testCreateShift(User systemUser, boolean isGenerated) {
        final ShiftType shiftType = new ShiftType();
        shiftType.setId(1001L);
        shiftType.setStartHour(3);
        shiftType.setStartMinute(50);
        shiftType.setDuration(Duration.ofHours(8));
        final LocalDate date = LocalDate.of(2020, 3, 15);
        final long nowInMillis = Instant.now().toEpochMilli();
        when(userService.getUserWithAuthoritiesByLogin("system")).thenReturn(Optional.ofNullable(systemUser));

        Shift shift = shiftService.create(shiftType, date, isGenerated);

        assertThat(shift.getCreateDate().toEpochMilli() - nowInMillis).isLessThan(100L);
        assertThat(shift.getCreatedBy()).isEqualTo(systemUser);
        assertThat(shift.getStart()).isEqualTo(
            DateTimeUtil.getInstantAtStartOfDay(date)
                .plus(shiftType.getStartHour(), ChronoUnit.HOURS)
                .plus(shiftType.getStartMinute(), ChronoUnit.MINUTES)
        );
        assertThat(shift.getEnd()).isEqualTo(
            DateTimeUtil.getInstantAtStartOfDay(date)
                .plus(shiftType.getStartHour(), ChronoUnit.HOURS)
                .plus(shiftType.getStartMinute(), ChronoUnit.MINUTES)
                .plus(shiftType.getDuration())
        );
        assertThat(shift.getShiftType()).isEqualTo(shiftType);
        assertThat(shift.isGenerated()).isEqualTo(isGenerated);
        assertThat(shift.getUpdatedBy()).isEqualTo(systemUser);
        assertThat(shift.getUpdateDate().toEpochMilli() - nowInMillis).isLessThan(100L);
    }

    @Test
    public void testGenerateShiftsWithEmptyParams() {
        assertThat(shiftService.generateShifts(new HashSet<>())).isNotNull().hasSize(0);
    }

    @Test
    public void testPopulateShift() {
        final ShiftType shiftType = new ShiftType();
        final Team team = new Team();
        team.setMaximumWorkingWeekends(2);
        shiftType.setStartHour(4);
        shiftType.setStartMinute(30);
        shiftType.setDuration(Duration.ofHours(8L));
        shiftType.setTeam(new Team());
        final LocalDate date = LocalDate.of(2020, 10, 4);
        final Integer noOfWorkers = 3;
        final DayConfigShiftType dayConfigShiftType = new DayConfigShiftType(noOfWorkers, shiftType).dayConfig(new DayConfig().date(date));
        shiftType.setTeam(team);
        dayConfigShiftType.setSpecializations(new HashSet<>());

        final User user1 = new User();
        final User user2 = new User();
        final User user3 = new User();
        final User user4 = new User();
        final List<User> emptyUserList = new ArrayList<>();
        final List<User> possibleUsers = Arrays.asList(user1, user2, user3, user4);
        final List<User> users124 = Arrays.asList(user1, user2, user4);
        final List<User> users12 = Arrays.asList(user1, user2);
        List<UserShiftType> emptyUserShiftTypeList = new ArrayList<>();
        List<UserShiftType> userShiftTypeList = Arrays.asList(
            new UserShiftType(user1, shiftType), new UserShiftType(user2, shiftType), new UserShiftType(user3, shiftType), new UserShiftType(user4, shiftType));

        //no users for that shift type
        populateShift(dayConfigShiftType, emptyUserShiftTypeList, emptyUserList, emptyUserList);

        //no users pass hard rules
        populateShift(dayConfigShiftType, userShiftTypeList, possibleUsers, emptyUserList);

        //some users pass hard rules
        populateShift(dayConfigShiftType, userShiftTypeList, possibleUsers, users124);

        //when the number of workers needed or less
        populateShift(dayConfigShiftType, userShiftTypeList, users12, users12);

        //when the number of workers is more than needed
        populateShift(dayConfigShiftType, userShiftTypeList, possibleUsers, possibleUsers);
    }

    private void populateShift(DayConfigShiftType dayConfigShiftType,
                               List<UserShiftType> userShiftTypeList,
                               List<User> users,
                               List<User> usersFilteredByHardRules) {

        when(userShiftTypeRepository.findByShiftType(eq(dayConfigShiftType.getShiftType()))).thenReturn(userShiftTypeList);

        when(shiftServiceMock.filterWorkersByHardRules(any(), eq(dayConfigShiftType))).thenReturn(usersFilteredByHardRules);

        doCallRealMethod().when(shiftServiceMock).setUserShiftTypeRepository(eq(userShiftTypeRepository));
        doCallRealMethod().when(shiftServiceMock).setUserService(eq(userService));
        doCallRealMethod().when(shiftServiceMock).populateShift(eq(dayConfigShiftType));
        doCallRealMethod().when(shiftServiceMock).create(eq(dayConfigShiftType.getShiftType()), eq(dayConfigShiftType.getDayConfig().getDate()), eq(true));
        doCallRealMethod().when(shiftServiceMock).assignWorkersToShift(eq(usersFilteredByHardRules), any(Shift.class), eq(dayConfigShiftType.getNoOfWorkers()));
        shiftServiceMock.setUserShiftTypeRepository(userShiftTypeRepository);
        shiftServiceMock.setUserService(userService);
        Shift returnedShift = shiftServiceMock.populateShift(dayConfigShiftType);
        if (users.size() == 0) {
            assertThat(returnedShift.getUsers()).containsExactlyInAnyOrderElementsOf(usersFilteredByHardRules);
        } else {
            assertThat(returnedShift.getUsers().size()).isLessThanOrEqualTo(dayConfigShiftType.getNoOfWorkers());
            assertThat(returnedShift.getUsers()).containsAnyElementsOf(usersFilteredByHardRules);
        }
    }

    @Test
    public void testPopulateShiftBySpecializations() {
        doCallRealMethod().when(shiftServiceMock).setSpecializationService(eq(specializationService));
        shiftServiceMock.setSpecializationService(specializationService);

        final User user1 = new User().id(1L).login("1");
        final User user2 = new User().id(2L).login("2");
        final User user3 = new User().id(3L).login("3");
        final User user4 = new User().id(4L).login("4");
        final User user5 = new User().id(5L).login("5");
        final List<User> possibleUsers = Arrays.asList(user1, user2, user3, user4, user5);

        final Specialization specialization1 = new Specialization().id(1L);
        final Specialization specialization2 = new Specialization().id(2L);
        final Specialization specialization3 = new Specialization().id(3L);

        final DayConfigShiftTypeSpec dayConfigShiftTypeSpec1 = new DayConfigShiftTypeSpec().specialization(specialization1).noOfWorkers(1);
        final DayConfigShiftTypeSpec dayConfigShiftTypeSpec2 = new DayConfigShiftTypeSpec().specialization(specialization2).noOfWorkers(2);
        final DayConfigShiftTypeSpec dayConfigShiftTypeSpec3 = new DayConfigShiftTypeSpec().specialization(specialization3).noOfWorkers(3);

        // empty lists
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(),
            Map.of(specialization1, new ArrayList<>(), specialization2, new ArrayList<>(), specialization3, new ArrayList<>()),
            Map.of(specialization1, new ArrayList<>(), specialization2, new ArrayList<>(), specialization3, new ArrayList<>())
        );

        // one user tests
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1), specialization2, new ArrayList<>(), specialization3, new ArrayList<>()),
            Map.of(specialization1, new ArrayList<>(), specialization2, new ArrayList<>(), specialization3, new ArrayList<>())
        );
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1), specialization2, new ArrayList<>(), specialization3, new ArrayList<>()),
            Map.of(specialization1, Arrays.asList(user1), specialization2, new ArrayList<>(), specialization3, new ArrayList<>())
        );

        // same user can do multiple specializations
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1), specialization2, Arrays.asList(user1), specialization3, new ArrayList<>()),
            Map.of(specialization1, Arrays.asList(user1), specialization2, new ArrayList<>(), specialization3, new ArrayList<>())
        );
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1, user2), specialization2, Arrays.asList(user1, user3), specialization3, new ArrayList<>()),
            Map.of(specialization1, Arrays.asList(user2), specialization2, Arrays.asList(user1), specialization3, new ArrayList<>())
        );

        // multiple users
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1), specialization2, Arrays.asList(user2, user3), specialization3, Arrays.asList(user2)),
            Map.of(specialization1, Arrays.asList(user1), specialization2, Arrays.asList(user2), specialization3, new ArrayList<>())
        );
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1), specialization2, Arrays.asList(user2, user3), specialization3, Arrays.asList(user2)),
            Map.of(specialization1, Arrays.asList(user1), specialization2, Arrays.asList(user2, user3), specialization3, new ArrayList<>())
        );
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1), specialization2, Arrays.asList(user2, user3), specialization3, Arrays.asList(user2)),
            Map.of(specialization1, Arrays.asList(user1), specialization2, Arrays.asList(user3), specialization3, Arrays.asList(user2))
        );
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1, user2, user3), specialization2, Arrays.asList(user4), specialization3, Arrays.asList(user5)),
            Map.of(specialization1, Arrays.asList(user2), specialization2, Arrays.asList(user4), specialization3, Arrays.asList(user5))
        );
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1, user2, user3), specialization2, Arrays.asList(user4), specialization3, Arrays.asList(user5)),
            Map.of(specialization1, Arrays.asList(user1, user2), specialization2, Arrays.asList(user4), specialization3, Arrays.asList(user5))
        );
        testPopulateShiftBySpecializations(
            new DayConfigShiftType().specializations(Set.of(dayConfigShiftTypeSpec1, dayConfigShiftTypeSpec2, dayConfigShiftTypeSpec3)),
            new ArrayList<>(possibleUsers),
            Map.of(specialization1, Arrays.asList(user1, user2, user3), specialization2, Arrays.asList(user4), specialization3, Arrays.asList(user5)),
            Map.of(specialization1, Arrays.asList(user1, user2, user3), specialization2, Arrays.asList(user4), specialization3, Arrays.asList(user5))
        );
    }

    private void testPopulateShiftBySpecializations(DayConfigShiftType dayConfigShiftType,
                                                    List<User> possibleWorkers,
                                                    Map<Specialization, List<User>> availableWorkersMap,
                                                    Map<Specialization, List<User>> expectedAssignedWorkersMap) {
        Shift shift = new Shift();
        shift.setUsers(new HashSet<>());

        for (DayConfigShiftTypeSpec dayConfigShiftTypeSpec : dayConfigShiftType.getSpecializations()) {
            Specialization specialization = dayConfigShiftTypeSpec.getSpecialization();
            List<User> availableWorkers = availableWorkersMap.get(specialization);
            List<User> expectedAssignedWorkers = expectedAssignedWorkersMap.get(specialization);

            when(specializationService.findUsersWhoCanWorkSpecialization(
                eq(specialization),
                argThat((List<User> availableWorkersParam) -> possibleWorkers.size() == availableWorkersParam.size() && possibleWorkers.containsAll(availableWorkersParam))
            )).thenReturn(availableWorkers);
            when(shiftServiceMock.assignWorkersToShift(
                argThat((List<User> availableWorkersParam) -> availableWorkersParam != null && availableWorkers.size() == availableWorkersParam.size() && availableWorkers.containsAll(availableWorkersParam)),
                eq(shift),
                eq(dayConfigShiftTypeSpec.getNoOfWorkers())
            )).thenReturn(expectedAssignedWorkers);
            possibleWorkers.removeAll(expectedAssignedWorkers);
        }

        doCallRealMethod().when(shiftServiceMock).populateShiftBySpecializations(dayConfigShiftType, possibleWorkers, shift);
        shiftServiceMock.populateShiftBySpecializations(dayConfigShiftType, possibleWorkers, shift);
    }

    @Test
    public void testAddWorkersInShift() {
        final User user1 = new User().id(1L).login("User 1");
        final User user2 = new User().id(2L).login("User 2");
        final User user3 = new User().id(3L).login("User 3");
        final User user4 = new User().id(4L).login("User 4");
        final List<User> someUsers = Arrays.asList(user1, user2, user3, user4);
        final List<User> noUsers = new ArrayList<>();

        // test with no available users
        testAddWorkersInShift(noUsers, 0);
        testAddWorkersInShift(noUsers, 1);

        // test with actual users
        testAddWorkersInShift(someUsers, 0);
        testAddWorkersInShift(someUsers, 1);
        testAddWorkersInShift(someUsers, 2);
        testAddWorkersInShift(someUsers, 3);
        testAddWorkersInShift(someUsers, 4);
        testAddWorkersInShift(someUsers, 5);
    }

    private void testAddWorkersInShift(List<User> availableWorkers, Integer noOfWorkersNeeded) {
        final Shift shift = new Shift();
        shift.setUsers(new HashSet<>());

        List<User> assignedWorkers = shiftService.assignWorkersToShift(availableWorkers, shift, noOfWorkersNeeded);

        assertThat(shift.getUsers()).containsExactlyInAnyOrderElementsOf(assignedWorkers);
        if (availableWorkers.size() <= noOfWorkersNeeded) {
            assertThat(shift.getUsers()).hasSize(availableWorkers.size());
            assertThat(shift.getUsers()).containsExactlyInAnyOrderElementsOf(availableWorkers);
        } else {
            assertThat(shift.getUsers()).hasSize(noOfWorkersNeeded);
            if (noOfWorkersNeeded > 0) {
                assertThat(availableWorkers).containsAll(shift.getUsers());
            }
        }
    }

    @Test
    public void testFilterWorkersForWhomPopulateShiftHadPassed() {
        final User user1 = new User();
        user1.setId(1L);
        user1.setLogin("User 1");
        final User user2 = new User();
        user2.setId(2L);
        user2.setLogin("User 2");
        final User user3 = new User();
        user3.setId(3L);
        user3.setLogin("User 3");
        final User user4 = new User();
        user3.setId(4L);
        user3.setLogin("User 4");
        final List<User> allUsers = Arrays.asList(user1, user2, user3, user4);
        final List<User> users124 = Arrays.asList(user1, user2, user4);
        final List<User> users14 = Arrays.asList(user1, user4);
        final List<User> users34 = Arrays.asList(user3, user4);
        final List<User> users3 = Arrays.asList(user3);
        final List<User> users2 = Arrays.asList(user2);
        final List<User> empty = new ArrayList<>();

        /*-------shift is not intersecting weekend & shift is not night shift--------should not enter first if statement*/
        //1 2 3 4 Users are unassigned and available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, allUsers, allUsers, allUsers, allUsers);
        //1 2 3 4 Users are unassigned and 1 2 4 available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, allUsers, users124, users124, users124);
        //1 2 3 4 Users are unassigned and 3 4 available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, allUsers, users34, users34, users34);
        //1 2 4 Users are unassigned and 1 4 available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, users124, users14, users14, users14);
        //3 4 Users are unassigned and 3 available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, users34, users3, users3, users3);
        //3 User is unassigned and 3 available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, users3, users3, users3, users3);
        //none are unassigned
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, empty, empty, empty, empty);
        //3 4 Users are unassigned and none are available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, allUsers, users34, empty, empty, empty);
        //no users
        testFilterWorkersForWhomPopulateShiftHadPassed(false, false, empty, empty, empty, empty, empty);

        /*-------shift is intersecting weekend & shift is not night shift-------should enter both if statements*/
        //1 2 3 4 Users are unassigned and available
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, allUsers, allUsers, allUsers, allUsers, empty);
        //1 2 3 4 Users are unassigned and 1 2 4 available and 1 4 do not reach max weekends
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, allUsers, allUsers, users124, users14, users14);
        //1 2 3 4 Users are unassigned and available and 3 4 do not reach max weekends
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, allUsers, allUsers, allUsers, users34, users34);
        //1 2 4 Users are unassigned and 1 2 4 available and 2 does not reach max weekends
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, allUsers, users124, users124, users2, users2);
        //1 2 4 Users are unassigned and 1 2 4 available and all reach max weekends
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, allUsers, users124, users124, empty, empty);
        //none are unassigned
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, allUsers, empty, empty, empty, empty);
        //3 4 Users are unassigned and none are available
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, allUsers, users34, empty, empty, empty);
        //no users
        testFilterWorkersForWhomPopulateShiftHadPassed(true, false, empty, empty, empty, empty, empty);

        /*--------shift is not intersecting weekend & shift is night shift----------should not enter both if statements*/
        //1 2 3 4 Users are unassigned and available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, true, allUsers, allUsers, allUsers, allUsers, allUsers);
        //1 2 3 4 Users are unassigned and 1 2 4 available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, true, allUsers, allUsers, users124, users124, users124);
        //1 2 3 4 Users are unassigned and available and 3 4 do not reach max weekends
        testFilterWorkersForWhomPopulateShiftHadPassed(false, true, allUsers, allUsers, users34, users34, users34);
        //1 2 4 Users are unassigned and 1 2 4 available, should not be influenced by list of intersecting weekends user list
        testFilterWorkersForWhomPopulateShiftHadPassed(false, true, allUsers, users124, users124, users124, users124);
        //none are unassigned
        testFilterWorkersForWhomPopulateShiftHadPassed(false, true, allUsers, empty, empty, empty, empty);
        //3 4 Users are unassigned and none are available
        testFilterWorkersForWhomPopulateShiftHadPassed(false, true, allUsers, users34, empty, empty, empty);
        //no users
        testFilterWorkersForWhomPopulateShiftHadPassed(false, true, empty, empty, empty, empty, empty);

        /*--------shift is intersecting weekend & shift is night shift----------should not enter second if statement*/
        //1 2 3 4 Users are unassigned and available
        testFilterWorkersForWhomPopulateShiftHadPassed(true, true, allUsers, allUsers, allUsers, allUsers, allUsers);
        //1 2 3 4 Users are unassigned and 1 2 4 available
        testFilterWorkersForWhomPopulateShiftHadPassed(true, true, allUsers, allUsers, users124, users124, users124);
        //1 2 3 4 Users are unassigned and available and 3 4 do not reach max weekends
        testFilterWorkersForWhomPopulateShiftHadPassed(true, true, allUsers, allUsers, allUsers, users34, users34);
        //1 2 4 Users are unassigned and 1 2 4 available, should not be influenced by list of intersecting weekends user list
        testFilterWorkersForWhomPopulateShiftHadPassed(true, true, allUsers, users124, users124, users124, users124);
        //none are unassigned
        testFilterWorkersForWhomPopulateShiftHadPassed(true, true, allUsers, empty, empty, empty, empty);
        //3 4 Users are unassigned and none are available
        testFilterWorkersForWhomPopulateShiftHadPassed(true, true, allUsers, users34, empty, empty, empty);
        //no users
        testFilterWorkersForWhomPopulateShiftHadPassed(true, true, empty, empty, empty, empty, empty);
    }

    private void testFilterWorkersForWhomPopulateShiftHadPassed(boolean isIntersectingWeekend, boolean isNightShift, List<User> users, List<User> unasignedUsers, List<User> availableUsers, List<User> usersWithMaxWorkingWeekendsNotReached, List<User> usersForWhomMiniFreeDaysAfterNightShiftsHadPassed) {
        LocalDate date = LocalDate.of(2020, 10, 15);
        if (isIntersectingWeekend) {
            date = LocalDate.of(2020, 10, 17);
        }

        final Integer noOfWorkers = 4;
        final ShiftType shiftType = new ShiftType();
        shiftType.setStartHour(4);
        shiftType.setStartMinute(30);
        shiftType.setDuration(Duration.ofHours(8L));
        if (isNightShift) {
            shiftType.setStartHour(20);
            shiftType.setStartMinute(30);
            shiftType.setDuration(Duration.ofHours(8L));
        }
        final DayConfigShiftType dayConfigShiftType = new DayConfigShiftType(noOfWorkers, shiftType).dayConfig(new DayConfig().date(date));
        Team team = new Team();
        team.setId(1L);
        team.setMaximumWorkingWeekends(2);
        team.setMinimumFreeDaysAfterNightShifts(1);
        dayConfigShiftType.setShiftType(shiftType);
        shiftType.setTeam(team);

        when(shiftWorkerService.filterUnassignedWorkers(
            eq(users), any(Instant.class), any(Instant.class)
        )).thenReturn(unasignedUsers);
        when(workerAvailabilityService.filterAvailableUsers(
            eq(unasignedUsers), any(Instant.class), any(Instant.class)
        )).thenReturn(availableUsers);

        if (!isIntersectingWeekend) {
            when(shiftWorkerService.filterWorkersWithMaximumWorkingWeekendsNotReached(
                eq(availableUsers), eq(team.getMaximumWorkingWeekends()), eq(date.getYear()), eq(date.getMonth()))
            ).thenThrow(new RuntimeException("Should not enter when is not intersecting weekend"));
        } else {
            when(shiftWorkerService.filterWorkersWithMaximumWorkingWeekendsNotReached(
                eq(availableUsers), eq(team.getMaximumWorkingWeekends()), eq(date.getYear()), eq(date.getMonth()))
            ).thenReturn(usersWithMaxWorkingWeekendsNotReached);
        }

        if (!isNightShift) {
            when(shiftWorkerService.filterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
                eq(usersWithMaxWorkingWeekendsNotReached), eq(team.getMinimumFreeDaysAfterNightShifts()), any(Instant.class))
            ).thenReturn(usersForWhomMiniFreeDaysAfterNightShiftsHadPassed);
        } else {
            when(shiftWorkerService.filterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
                eq(usersWithMaxWorkingWeekendsNotReached), eq(team.getMinimumFreeDaysAfterNightShifts()), any(Instant.class))
            ).thenThrow(new RuntimeException("Should not enter when is night shift"));
        }

        List<User> returnedUsers = shiftService.filterWorkersByHardRules(users, dayConfigShiftType);
        assertThat(returnedUsers).containsExactlyInAnyOrderElementsOf(usersForWhomMiniFreeDaysAfterNightShiftsHadPassed);
    }

}
