package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.repository.DayConfigShiftTypeRepository;
import com.hr.hpital.manager.service.impl.DayConfigShiftTypeServiceImpl;
import com.hr.hpital.manager.service.mapper.DayConfigShiftTypeMapper;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Unit test class for the {@link ShiftService} utility class.
 */
public class DayConfigShiftTypeServiceImplUT {

    private final DayConfigShiftTypeRepository dayConfigShiftTypeRepository = mock(DayConfigShiftTypeRepository.class);

    private final DayConfigShiftTypeMapper dayConfigShiftTypeMapper = mock(DayConfigShiftTypeMapper.class);

    private final DayConfigShiftTypeService dayConfigShiftTypeService = new DayConfigShiftTypeServiceImpl(
        dayConfigShiftTypeRepository, null, null, dayConfigShiftTypeMapper
    );

    @Test
    public void testFilterMandatory() {
        final LocalDate today = LocalDate.now();
        final LocalDate yesterday = today.minusDays(1L);

        final ShiftType mandatoryShiftType1 = new ShiftType().mandatory(true).name("Mandatory shift type");
        final ShiftType optionalShiftType1 = new ShiftType().mandatory(false).name("Optional shift type");

        final DayConfigShiftType mandatoryCfg1 = new DayConfigShiftType().dayConfig(new DayConfig().date(today)).shiftType(mandatoryShiftType1);
        final DayConfigShiftType mandatoryCfg2 = new DayConfigShiftType().dayConfig(new DayConfig().date(yesterday)).shiftType(mandatoryShiftType1);
        final DayConfigShiftType optionalCfg1 = new DayConfigShiftType().dayConfig(new DayConfig().date(today)).shiftType(optionalShiftType1);
        final DayConfigShiftType optionalCfg2 = new DayConfigShiftType().dayConfig(new DayConfig().date(yesterday)).shiftType(optionalShiftType1);

        testFilterMandatory(new HashSet<>(), new HashSet<>());
        testFilterMandatory(Set.of(optionalCfg1), new HashSet<>());
        testFilterMandatory(Set.of(optionalCfg2), new HashSet<>());
        testFilterMandatory(Set.of(optionalCfg1, optionalCfg2), new HashSet<>());

        testFilterMandatory(Set.of(mandatoryCfg1), Set.of(mandatoryCfg1));
        testFilterMandatory(Set.of(mandatoryCfg2), Set.of(mandatoryCfg2));
        testFilterMandatory(Set.of(mandatoryCfg1, optionalCfg1), Set.of(mandatoryCfg1));
        testFilterMandatory(Set.of(mandatoryCfg1, mandatoryCfg2), Set.of(mandatoryCfg1, mandatoryCfg2));
        testFilterMandatory(Set.of(mandatoryCfg1, mandatoryCfg2, optionalCfg1), Set.of(mandatoryCfg1, mandatoryCfg2));
        testFilterMandatory(Set.of(mandatoryCfg1, mandatoryCfg2, optionalCfg1, optionalCfg2), Set.of(mandatoryCfg1, mandatoryCfg2));
        testFilterMandatory(Set.of(mandatoryCfg2, optionalCfg1, optionalCfg2), Set.of(mandatoryCfg2));
    }

    private void testFilterMandatory(Set<DayConfigShiftType> input, Set<DayConfigShiftType> output) {
        final Set<DayConfigShiftType> result = dayConfigShiftTypeService.filter(input, dayConfigShiftType -> dayConfigShiftType.getShiftType().isMandatory());
        assertThat(result).isNotNull();
        assertThat(result).containsExactlyInAnyOrderElementsOf(output);
    }

    @Test
    public void testFilterWeekends() {
        final LocalDate weekendDate1 = LocalDate.of(2020, 10, 24);
        final LocalDate weekendDate2 = LocalDate.of(2020, 10, 25);
        final LocalDate nonWeekendDate1 = LocalDate.of(2020, 10, 21);
        final LocalDate nonWeekendDate2 = LocalDate.of(2020, 10, 22);

        final ShiftType shiftType1 = new ShiftType().name("Shift type 1");
        final ShiftType shiftType2 = new ShiftType().name("Shift type 2");

        final DayConfigShiftType weekendCfg1 = new DayConfigShiftType().dayConfig(new DayConfig().date(weekendDate1)).shiftType(shiftType1);
        final DayConfigShiftType weekendCfg2 = new DayConfigShiftType().dayConfig(new DayConfig().date(weekendDate2)).shiftType(shiftType2);
        final DayConfigShiftType nonWeekendCfg1 = new DayConfigShiftType().dayConfig(new DayConfig().date(nonWeekendDate1)).shiftType(shiftType1);
        final DayConfigShiftType nonWeekendCfg2 = new DayConfigShiftType().dayConfig(new DayConfig().date(nonWeekendDate2)).shiftType(shiftType2);

        testFilterWeekends(new HashSet<>(), new HashSet<>());
        testFilterWeekends(Set.of(nonWeekendCfg1), new HashSet<>());
        testFilterWeekends(Set.of(nonWeekendCfg2), new HashSet<>());
        testFilterWeekends(Set.of(nonWeekendCfg1, nonWeekendCfg2), new HashSet<>());

        testFilterWeekends(Set.of(weekendCfg1), Set.of(weekendCfg1));
        testFilterWeekends(Set.of(weekendCfg2), Set.of(weekendCfg2));
        testFilterWeekends(Set.of(weekendCfg1, nonWeekendCfg1), Set.of(weekendCfg1));
        testFilterWeekends(Set.of(weekendCfg1, nonWeekendCfg2), Set.of(weekendCfg1));
        testFilterWeekends(Set.of(weekendCfg1, nonWeekendCfg1, nonWeekendCfg2), Set.of(weekendCfg1));
        testFilterWeekends(Set.of(weekendCfg2, nonWeekendCfg1), Set.of(weekendCfg2));
        testFilterWeekends(Set.of(weekendCfg2, nonWeekendCfg2), Set.of(weekendCfg2));
        testFilterWeekends(Set.of(weekendCfg2, nonWeekendCfg1, nonWeekendCfg2), Set.of(weekendCfg2));
        testFilterWeekends(Set.of(weekendCfg1, weekendCfg2), Set.of(weekendCfg1, weekendCfg2));
        testFilterWeekends(Set.of(weekendCfg1, weekendCfg2, nonWeekendCfg1), Set.of(weekendCfg1, weekendCfg2));
        testFilterWeekends(Set.of(weekendCfg1, weekendCfg2, nonWeekendCfg2), Set.of(weekendCfg1, weekendCfg2));
        testFilterWeekends(Set.of(weekendCfg1, weekendCfg2, nonWeekendCfg1, nonWeekendCfg2), Set.of(weekendCfg1, weekendCfg2));
    }

    private void testFilterWeekends(Set<DayConfigShiftType> input, Set<DayConfigShiftType> output) {
        final Set<DayConfigShiftType> result = dayConfigShiftTypeService.filter(input, dayConfigShiftType -> DateTimeUtil.isWeekendDay(dayConfigShiftType.getDayConfig().getDate()));
        assertThat(result).isNotNull();
        assertThat(result).containsExactlyInAnyOrderElementsOf(output);
    }

    @Test
    public void testFilterBankHolidays() {
        final LocalDate today = LocalDate.now();
        final LocalDate yesterday = today.minusDays(1L);

        final ShiftType shiftType1 = new ShiftType().name("Shift type 1");
        final ShiftType shiftType2 = new ShiftType().name("Shift type 2");

        final DayConfigShiftType bankHolidayCfg1 = new DayConfigShiftType().dayConfig(new DayConfig().date(today).isBankHoliday(true)).shiftType(shiftType1);
        final DayConfigShiftType bankHolidayCfg2 = new DayConfigShiftType().dayConfig(new DayConfig().date(yesterday).isBankHoliday(true)).shiftType(shiftType2);
        final DayConfigShiftType nonBankHolidayCfg1 = new DayConfigShiftType().dayConfig(new DayConfig().date(today).isBankHoliday(false)).shiftType(shiftType1);
        final DayConfigShiftType nonBankHolidayCfg2 = new DayConfigShiftType().dayConfig(new DayConfig().date(yesterday).isBankHoliday(false)).shiftType(shiftType2);

        testFilterBankHolidays(new HashSet<>(), new HashSet<>());
        testFilterBankHolidays(Set.of(nonBankHolidayCfg1), new HashSet<>());
        testFilterBankHolidays(Set.of(nonBankHolidayCfg2), new HashSet<>());
        testFilterBankHolidays(Set.of(nonBankHolidayCfg1, nonBankHolidayCfg2), new HashSet<>());

        testFilterBankHolidays(Set.of(bankHolidayCfg1), Set.of(bankHolidayCfg1));
        testFilterBankHolidays(Set.of(bankHolidayCfg2), Set.of(bankHolidayCfg2));
        testFilterBankHolidays(Set.of(bankHolidayCfg1, nonBankHolidayCfg1), Set.of(bankHolidayCfg1));
        testFilterBankHolidays(Set.of(bankHolidayCfg1, nonBankHolidayCfg2), Set.of(bankHolidayCfg1));
        testFilterBankHolidays(Set.of(bankHolidayCfg1, nonBankHolidayCfg1, nonBankHolidayCfg2), Set.of(bankHolidayCfg1));
        testFilterBankHolidays(Set.of(bankHolidayCfg2, nonBankHolidayCfg1), Set.of(bankHolidayCfg2));
        testFilterBankHolidays(Set.of(bankHolidayCfg2, nonBankHolidayCfg2), Set.of(bankHolidayCfg2));
        testFilterBankHolidays(Set.of(bankHolidayCfg2, nonBankHolidayCfg1, nonBankHolidayCfg2), Set.of(bankHolidayCfg2));
        testFilterBankHolidays(Set.of(bankHolidayCfg1, bankHolidayCfg2), Set.of(bankHolidayCfg1, bankHolidayCfg2));
        testFilterBankHolidays(Set.of(bankHolidayCfg1, bankHolidayCfg2, nonBankHolidayCfg1), Set.of(bankHolidayCfg1, bankHolidayCfg2));
        testFilterBankHolidays(Set.of(bankHolidayCfg1, bankHolidayCfg2, nonBankHolidayCfg2), Set.of(bankHolidayCfg1, bankHolidayCfg2));
        testFilterBankHolidays(Set.of(bankHolidayCfg1, bankHolidayCfg2, nonBankHolidayCfg1, nonBankHolidayCfg2), Set.of(bankHolidayCfg1, bankHolidayCfg2));
    }

    private void testFilterBankHolidays(Set<DayConfigShiftType> input, Set<DayConfigShiftType> output) {
        final Set<DayConfigShiftType> result = dayConfigShiftTypeService.filter(input, dayConfigShiftType -> dayConfigShiftType.getDayConfig().isIsBankHoliday());
        assertThat(result).isNotNull();
        assertThat(result).containsExactlyInAnyOrderElementsOf(output);
    }

}
