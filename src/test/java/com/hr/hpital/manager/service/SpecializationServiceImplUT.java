package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.Specialization;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.repository.SpecializationRepository;
import com.hr.hpital.manager.service.impl.SpecializationServiceImpl;
import com.hr.hpital.manager.service.mapper.SpecializationMapper;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Unit test class for the {@link ShiftService} utility class.
 */
public class SpecializationServiceImplUT {
    private final SpecializationRepository specializationRepository = mock(SpecializationRepository.class);
    private final SpecializationMapper specializationMapper = mock(SpecializationMapper.class);

    private final SpecializationService specializationService = new SpecializationServiceImpl(specializationRepository, specializationMapper);

    @Test
    public void testFilterUsersWhoCanWorkSpecialization() {
        final User user1 = new User().id(1L).login("1");
        final User user2 = new User().id(2L).login("2");
        final User user3 = new User().id(3L).login("3");
        final User user4 = new User().id(4L).login("4");

        testFilterUsersWhoCanWorkSpecialization(new HashSet<>(), new ArrayList<>(), new ArrayList<>());
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1), new ArrayList<>(), new ArrayList<>());
        testFilterUsersWhoCanWorkSpecialization(new HashSet<>(), Arrays.asList(user1), new ArrayList<>());
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1), Arrays.asList(user2), new ArrayList<>());
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1), Arrays.asList(user2, user3), new ArrayList<>());
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1), Arrays.asList(user1), Arrays.asList(user1));
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1), Arrays.asList(user1, user2), Arrays.asList(user1));
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1, user2), Arrays.asList(user1), Arrays.asList(user1));
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1, user2), Arrays.asList(user1, user2), Arrays.asList(user1, user2));
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1, user2, user3), Arrays.asList(user1, user2), Arrays.asList(user1, user2));
        testFilterUsersWhoCanWorkSpecialization(Set.of(user1, user2, user3), Arrays.asList(user1, user2, user4), Arrays.asList(user1, user2));
    }

    private void testFilterUsersWhoCanWorkSpecialization(Set<User> userWhoCanWorkSpecialization, List<User> possibleUsers, List<User> expectedUsers) {
        Specialization specialization = new Specialization().users(userWhoCanWorkSpecialization);
        List<User> result = specializationService.findUsersWhoCanWorkSpecialization(specialization, possibleUsers);
        assertThat(result).containsExactlyInAnyOrderElementsOf(expectedUsers);
    }
}
