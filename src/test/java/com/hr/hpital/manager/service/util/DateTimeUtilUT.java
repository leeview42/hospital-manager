package com.hr.hpital.manager.service.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.util.Pair;

import java.time.*;
import java.util.Arrays;
import java.util.List;

import static java.time.temporal.ChronoUnit.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Unit test class for {@link DateTimeUtil}.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(DateTimeUtil.class)
public class DateTimeUtilUT {

    @Test
    public void testGetWeekendsInMonthForSeptember2020() {
        final List<Pair<LocalDate, LocalDate>> expectedResult = Arrays.asList(
            Pair.of(LocalDate.of(2020, 9, 5), LocalDate.of(2020, 9, 6)),
            Pair.of(LocalDate.of(2020, 9, 12), LocalDate.of(2020, 9, 13)),
            Pair.of(LocalDate.of(2020, 9, 19), LocalDate.of(2020, 9, 20)),
            Pair.of(LocalDate.of(2020, 9, 26), LocalDate.of(2020, 9, 27))
        );

        testGetWeekendsInMonth(2020, Month.SEPTEMBER, expectedResult);
    }

    @Test
    public void testGetWeekendsInMonthForOctober2020() {
        final List<Pair<LocalDate, LocalDate>> expectedResult = Arrays.asList(
            Pair.of(LocalDate.of(2020, 10, 3), LocalDate.of(2020, 10, 4)),
            Pair.of(LocalDate.of(2020, 10, 10), LocalDate.of(2020, 10, 11)),
            Pair.of(LocalDate.of(2020, 10, 17), LocalDate.of(2020, 10, 18)),
            Pair.of(LocalDate.of(2020, 10, 24), LocalDate.of(2020, 10, 25)),
            Pair.of(LocalDate.of(2020, 10, 31), LocalDate.of(2020, 11, 1))
        );

        testGetWeekendsInMonth(2020, Month.OCTOBER, expectedResult);
    }

    private void testGetWeekendsInMonth(Integer year, Month month, List<Pair<LocalDate, LocalDate>> expectedResult) {
        PowerMockito.mockStatic(DateTimeUtil.class);
        when(DateTimeUtil.getWeekendsInMonth(eq(year), any(Month.class))).thenCallRealMethod();
        final List<Pair<LocalDate, LocalDate>> result = DateTimeUtil.getWeekendsInMonth(year, month);
        assertThat(result).containsExactlyInAnyOrderElementsOf(expectedResult);
    }

    @Test
    public void testGetInstantAtStartOfDay() {
        assertThat(DateTimeUtil.getInstantAtStartOfDay(null)).isNull();

        assertThat(DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 1, 1)))
            .isEqualTo(LocalDateTime.of(2020, 1, 1, 0, 0).toInstant(ZoneOffset.UTC));

        assertThat(DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 4, 13)))
            .isEqualTo(LocalDateTime.of(2020, 4, 13, 0, 0).toInstant(ZoneOffset.UTC));
    }

    @Test
    public void testGetInstantAtEndOfDay() {
        assertThat(DateTimeUtil.getInstantAtEndOfDay(null)).isNull();

        assertThat(DateTimeUtil.getInstantAtEndOfDay(LocalDate.of(2020, 1, 1)))
            .isEqualTo(LocalDateTime.of(2020, 1, 1, 23, 59, 59).toInstant(ZoneOffset.UTC));

        assertThat(DateTimeUtil.getInstantAtEndOfDay(LocalDate.of(2020, 4, 13)))
            .isEqualTo(LocalDateTime.of(2020, 4, 13, 23, 59, 59).toInstant(ZoneOffset.UTC));
    }

    @Test
    public void testGetInstantAtTime() {
        assertThat(DateTimeUtil.getInstantAtTime(null, 1, 1, 1)).isNull();
        assertThat(DateTimeUtil.getInstantAtTime(LocalDate.of(2020, 10, 4), 1, 2, 3))
            .isEqualTo(LocalDateTime.of(2020, 10, 4, 1, 2, 3).toInstant(ZoneOffset.UTC));
        assertThat(DateTimeUtil.getInstantAtTime(LocalDate.of(2021, 3, 14), 10, 20, 31))
            .isEqualTo(LocalDateTime.of(2021, 3, 14, 10, 20, 31).toInstant(ZoneOffset.UTC));
    }

    @Test
    public void testGetDate() {
        assertThat(DateTimeUtil.getDate(Instant.ofEpochMilli(1141452525252L))).isEqualTo(LocalDate.of(2006, 3, 4));
        assertThat(DateTimeUtil.getDate(Instant.ofEpochMilli(1599689218311L))).isEqualTo(LocalDate.of(2020, 9, 9));
    }

    @Test
    public void testIntersectsWeekend() {
        final Instant startOfThursday = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 8));
        final Instant startOfFriday = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 9));

        // both ends of interval are before weekend
        testIntersectsWeekend(startOfThursday, startOfFriday, false);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(1, HOURS), false);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(11, HOURS), false);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(23, HOURS), false);

        // left end of the interval is before weekend, right end is during weekend
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(1, DAYS), true);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(1, DAYS).plus(1, SECONDS), true);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(2, DAYS), true);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(2, DAYS).plus(1, HOURS), true);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(2, DAYS).plus(10, HOURS), true);

        // left end of the interval is before weekend, right end after the weekend
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(2, DAYS).plus(24, HOURS), true);
        testIntersectsWeekend(startOfThursday, startOfFriday.plus(2, DAYS).plus(24, HOURS).plus(1, SECONDS), true);

        final Instant startOfMonday = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 12));
        final Instant startOfTuesday = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 13));

        // both ends of the interval are after weekend
        testIntersectsWeekend(startOfMonday, startOfTuesday, false);
        testIntersectsWeekend(startOfMonday, startOfTuesday.plus(1, HOURS), false);
        testIntersectsWeekend(startOfMonday, startOfTuesday.plus(24, HOURS), false);
        testIntersectsWeekend(startOfMonday.plus(1, SECONDS), startOfTuesday.plus(24, HOURS), false);
        testIntersectsWeekend(startOfMonday.plus(1, HOURS), startOfTuesday.plus(24, HOURS), false);

        // left end of the interval is in weekend, right end is not
        testIntersectsWeekend(startOfMonday.minus(1, SECONDS), startOfTuesday, true);
        testIntersectsWeekend(startOfMonday.minus(1, HOURS), startOfTuesday, true);
        testIntersectsWeekend(startOfMonday.minus(10, HOURS), startOfTuesday, true);
        testIntersectsWeekend(startOfMonday.minus(1, DAYS), startOfTuesday, true);
        testIntersectsWeekend(startOfMonday.minus(2, DAYS), startOfTuesday, true);
        testIntersectsWeekend(startOfMonday.minus(1, DAYS).minus(23, HOURS), startOfTuesday, true);

        final Instant startOfSaturday = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 10));
        final Instant startOfSunday = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 11));

        // both ends of the interval is in weekend
        testIntersectsWeekend(startOfSaturday, startOfSunday, true);
        testIntersectsWeekend(startOfSaturday.plus(1, HOURS), startOfSunday, true);
        testIntersectsWeekend(startOfSaturday.plus(23, HOURS), startOfSunday, true);
        testIntersectsWeekend(startOfSaturday, startOfSunday.plus(1, HOURS), true);
        testIntersectsWeekend(startOfSaturday.plus(1, HOURS), startOfSunday.plus(1, HOURS), true);
        testIntersectsWeekend(startOfSaturday, startOfSunday.plus(23, HOURS), true);
        testIntersectsWeekend(startOfSaturday, startOfSunday.plus(23, HOURS).plus(59, SECONDS), true);
        testIntersectsWeekend(startOfSaturday, startOfSaturday.plus(1, HOURS), true);
        testIntersectsWeekend(startOfSaturday.plus(1, HOURS), startOfSaturday.plus(2, HOURS), true);
        testIntersectsWeekend(startOfSaturday.plus(10, HOURS), startOfSaturday.plus(22, HOURS), true);
        testIntersectsWeekend(startOfSunday, startOfSunday.plus(1, HOURS), true);
        testIntersectsWeekend(startOfSunday.plus(1, HOURS), startOfSunday.plus(2, HOURS), true);
        testIntersectsWeekend(startOfSunday.plus(10, HOURS), startOfSunday.plus(22, HOURS), true);


        final Instant from = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 5));
        final Instant to = DateTimeUtil.getInstantAtStartOfDay(LocalDate.of(2020, 10, 20));
        testIntersectsWeekend(from, to, true);
    }

    private void testIntersectsWeekend(Instant from, Instant to, boolean weekendIsIntersected) {
        assertThat(DateTimeUtil.intersectsWeekend(from, to)).isEqualTo(weekendIsIntersected);
    }

    @Test
    public void testIsWeekendDay() {
        final List<LocalDate> weekendDays = Arrays.asList(
            LocalDate.of(2020, 10, 24),
            LocalDate.of(2020, 10, 25),
            LocalDate.of(2021, 1, 2),
            LocalDate.of(2021, 1, 3)
        );
        final List<LocalDate> nonWeekendDays = Arrays.asList(
            LocalDate.of(2020, 10, 12),
            LocalDate.of(2020, 10, 13),
            LocalDate.of(2020, 10, 14),
            LocalDate.of(2020, 10, 15),
            LocalDate.of(2020, 10, 16),
            LocalDate.of(2020, 12, 28),
            LocalDate.of(2020, 12, 29),
            LocalDate.of(2020, 12, 30),
            LocalDate.of(2021, 1, 1)
        );

        for (LocalDate weekendDay : weekendDays) {
            assertThat(DateTimeUtil.isWeekendDay(weekendDay)).isEqualTo(true);
        }

        for (LocalDate weekendDay : nonWeekendDays) {
            assertThat(DateTimeUtil.isWeekendDay(weekendDay)).isEqualTo(false);
        }

    }
}
