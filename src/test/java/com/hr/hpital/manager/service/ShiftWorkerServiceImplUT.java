package com.hr.hpital.manager.service;

import com.hr.hpital.manager.config.ApplicationProperties;
import com.hr.hpital.manager.domain.Shift;
import com.hr.hpital.manager.domain.ShiftWorker;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.repository.ShiftWorkerRepository;
import com.hr.hpital.manager.repository.TeamRepository;
import com.hr.hpital.manager.service.impl.ShiftWorkerServiceImpl;
import com.hr.hpital.manager.service.mapper.ShiftWorkerMapper;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;

import static com.hr.hpital.manager.service.util.DateTimeUtil.getWeekendsInMonth;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * Unit test class for the {@link ShiftWorkerService} utility class.
 */
public class ShiftWorkerServiceImplUT {

    private final ShiftWorkerRepository shiftWorkerRepository = mock(ShiftWorkerRepository.class);

    private final ShiftWorkerMapper shiftWorkerMapper = mock(ShiftWorkerMapper.class);

    private final TeamRepository teamRepository = mock(TeamRepository.class);

    private final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

    private final ShiftWorkerService shiftWorkerService = new ShiftWorkerServiceImpl(shiftWorkerRepository, shiftWorkerMapper, teamRepository, applicationProperties);

    @Test
    public void testFilterWorkersWithMaximumWorkingWeekendsReached() {
        testFilterWorkersWithMaximumWorkingWeekendsReached(0);
        testFilterWorkersWithMaximumWorkingWeekendsReached(1);
        testFilterWorkersWithMaximumWorkingWeekendsReached(2);
        testFilterWorkersWithMaximumWorkingWeekendsReached(3);
        testFilterWorkersWithMaximumWorkingWeekendsReached(4);
    }

    private void testFilterWorkersWithMaximumWorkingWeekendsReached(Integer maximumWorkingWeekendsAllowed) {
        final User user = new User();
        final User user2 = new User();
        final User user3 = new User();
        final List<User> users = Arrays.asList(user, user2, user3);
        final List<ShiftWorker> emptyList = new ArrayList<>();

        when(applicationProperties.getSqlBatchSizeForInConditions()).thenReturn(100);
        when(shiftWorkerRepository.findAllByWorkerInAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(
            eq(users), any(Instant.class), any(Instant.class)
        )).thenReturn(
            Arrays.asList(new ShiftWorker(null, user), new ShiftWorker(null, user2)), // user, user2 have worked the first weekend of month
            Arrays.asList(new ShiftWorker(null, user), new ShiftWorker(null, user2)), // user, user2 have worked the second weekend of month
            Collections.singletonList(new ShiftWorker(null, user)),  // user have worked the third weekend of month
            emptyList, // no user have worked the forth weekend of month
            emptyList // no user have worked the fifth weekend of month
        );

        final List<User> availableWorkers = shiftWorkerService.filterWorkersWithMaximumWorkingWeekendsNotReached(users, maximumWorkingWeekendsAllowed, 2020, Month.JANUARY);

        switch (maximumWorkingWeekendsAllowed) {
            case 0:
                assertThat(availableWorkers.size()).isEqualTo(0);
                break;
            case 1:
            case 2:
                assertThat(availableWorkers).containsOnly(user3);
                break;
            case 3:
                assertThat(availableWorkers).containsOnly(user2, user3);
                break;
            default:
                assertThat(availableWorkers).containsOnly(user, user2, user3);
        }
    }

    @Test
    public void testFilterWorkersWithMaximumWorkingWeekendsReachedWhenEmptyUserList() {
        // init test fields
        final LocalDate now = LocalDate.now();
        final Integer year = now.getYear();
        final Month month = Month.of(now.getMonthValue());
        final List<User> emptyUserList = mock(ArrayList.class);
        final Integer maximumWorkingWeekendsAllowed = 1;

        // call the method
        shiftWorkerService.filterWorkersWithMaximumWorkingWeekendsNotReached(emptyUserList, maximumWorkingWeekendsAllowed, year, month);

        // check no method is called
        getWeekendsInMonth(year, month).forEach(pair -> {
            final Instant start = DateTimeUtil.getInstantAtStartOfDay(pair.getFirst());
            final Instant end = DateTimeUtil.getInstantAtEndOfDay(pair.getSecond());
            verify(shiftWorkerRepository, times(0)).findAllByWorkerInAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(emptyUserList, start, end);
        });
        verify(emptyUserList, times(0)).stream();
    }

    @Test
    public void testAlreadyHasShiftAssigned() {
        testAlreadyHasShiftAssigned(0L, false);
        testAlreadyHasShiftAssigned(1L, true);
        testAlreadyHasShiftAssigned(2L, true);
        testAlreadyHasShiftAssigned(3L, true);
    }

    private void testAlreadyHasShiftAssigned(Long plannedShiftOnThatDay, Boolean alreadyHasShiftPlannedExpectedValue) {
        final User user = new User();
        when(shiftWorkerRepository.countByWorkerAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(
            eq(user), any(Instant.class), any(Instant.class)
        )).thenReturn(plannedShiftOnThatDay);

        final boolean alreadyHasShiftAssigned = shiftWorkerService.alreadyHasShiftAssigned(user, Instant.now(), Instant.now());
        assertThat(alreadyHasShiftAssigned).isEqualTo(alreadyHasShiftPlannedExpectedValue);
    }

    @Test
    public void testMinimumFreeDaysAfterNightShiftsHadPassed() {
        final Instant instant = Instant.ofEpochMilli(1599642212311L); // Sep 09 2020 09:03:32 UTC
        // last shift is not night shift because shift start is the same day as shift end
        testMinimumFreeDaysAfterNightShiftsHadPassed(0, instant, instant, instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(1, instant, instant, instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(3, instant, instant, instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(1, instant.minus(2L, DAYS), instant.minus(2L, DAYS), instant, true);
        // last shift is a night shift because shift start is the day before of shift end
        testMinimumFreeDaysAfterNightShiftsHadPassed(0, instant.minus(1L, DAYS), instant, instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(1, instant.minus(1L, DAYS), instant, instant, false);
        testMinimumFreeDaysAfterNightShiftsHadPassed(1, instant.minus(2L, DAYS), instant.minus(1L, DAYS), instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(1, instant.minus(3L, DAYS), instant.minus(2L, DAYS), instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(3, instant.minus(5L, DAYS), instant.minus(4L, DAYS), instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(4, instant.minus(5L, DAYS), instant.minus(4L, DAYS), instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(4, instant.minus(6L, DAYS), instant.minus(5L, DAYS), instant, true);
        testMinimumFreeDaysAfterNightShiftsHadPassed(5, instant.minus(5L, DAYS), instant.minus(4L, DAYS), instant, false);
        // treats case when worker has no shift planned
        testMinimumFreeDaysAfterNightShiftsHadPassed(5, null, null, instant, true);
    }

    private void testMinimumFreeDaysAfterNightShiftsHadPassed(Integer minimumFreeDaysAfterNightShifts,
                                                              Instant lastPlannedShiftStart,
                                                              Instant lastPlannedShiftEnd,
                                                              Instant dateToTestWith,
                                                              boolean expectedMinimumFreeDaysAfterNightShiftsHadPassedValue) {
        final User user = new User();
        ShiftWorker lastPlannedShiftWorker = null;
        if (lastPlannedShiftStart != null && lastPlannedShiftEnd != null) {
            lastPlannedShiftWorker = new ShiftWorker();
            final Shift shift = new Shift();
            lastPlannedShiftWorker.setWorker(user);
            lastPlannedShiftWorker.setShift(shift);
            shift.setStart(lastPlannedShiftStart);
            shift.setEnd(lastPlannedShiftEnd);
        }

        when(shiftWorkerRepository.findFirstByWorkerAndShiftEndLessThanEqualOrderByShiftEndDesc(
            eq(user), eq(dateToTestWith)
        )).thenReturn(Optional.ofNullable(lastPlannedShiftWorker));

        final boolean minimumFreeDaysAfterNightShiftsHadPassed =
            shiftWorkerService.minimumFreeDaysAfterNightShiftsHadPassed(user, minimumFreeDaysAfterNightShifts, dateToTestWith);
        assertThat(minimumFreeDaysAfterNightShiftsHadPassed).isEqualTo(expectedMinimumFreeDaysAfterNightShiftsHadPassedValue);
    }

    @Test
    public void testFilterAlreadyAssignedWorkers() {
        final User user = new User();
        final User user2 = new User();
        final User user3 = new User();
        final List<User> users = Arrays.asList(user, user2, user3);
        final List<User> emptyList = new ArrayList<>();
        when(applicationProperties.getSqlBatchSizeForInConditions()).thenReturn(100);

        testFilterAlreadyAssignedWorkers(emptyList, emptyList); // no user
        testFilterAlreadyAssignedWorkers(users, emptyList); // no users are already assigned
        testFilterAlreadyAssignedWorkers(users, users); // all users are already assigned
        testFilterAlreadyAssignedWorkers(users, Collections.singletonList(user)); // only user is already assigned
        testFilterAlreadyAssignedWorkers(users, Collections.singletonList(user2)); // only user2 is already assigned
        testFilterAlreadyAssignedWorkers(users, Collections.singletonList(user3)); // only user2 is already assigned
        testFilterAlreadyAssignedWorkers(users, Arrays.asList(user, user2)); // only user and user2 are already assigned
        testFilterAlreadyAssignedWorkers(users, Arrays.asList(user, user3)); // only user and user3 are already assigned
        testFilterAlreadyAssignedWorkers(users, Arrays.asList(user2, user3)); // only user2 and user3 are already assigned
    }

    private void testFilterAlreadyAssignedWorkers(List<User> users, List<User> assignedUsers) {
        when(shiftWorkerRepository.findAllByWorkerInAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(
            eq(users), any(Instant.class), any(Instant.class)
        )).thenReturn(
            assignedUsers.stream().map(user -> new ShiftWorker(null, user)).collect(Collectors.toList())
        );

        List<User> availableUsers = shiftWorkerService.filterUnassignedWorkers(users, Instant.now(), Instant.now());
        if (users.size() == 0) {
            assertThat(availableUsers.size()).isEqualTo(0);
        } else {
            assertThat(availableUsers).containsExactlyInAnyOrderElementsOf(
                users.stream().filter(user -> !assignedUsers.contains(user)).collect(Collectors.toList())
            );
        }
    }

    @Test
    public void testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed() {
        final User user = new User();
        user.setLogin("User 1");
        final User user2 = new User();
        user2.setLogin("User 2");
        final User user3 = new User();
        user3.setLogin("User 3");
        final User user4 = new User();
        user4.setLogin("User 4");

        // no users as input
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(Collections.emptyList(), null, Collections.emptyList());

        // one user as input
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Collections.singletonList(user), Collections.emptyList(), Collections.singletonList(user)
        );
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Collections.singletonList(user), Collections.singletonList(user), Collections.emptyList()
        );

        // two users as input
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2), Collections.emptyList(), Arrays.asList(user, user2)
        );
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2), Collections.singletonList(user), Collections.singletonList(user2)
        );
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2), Collections.singletonList(user2), Collections.singletonList(user)
        );
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2), Arrays.asList(user, user2), Collections.emptyList()
        );

        // other cases
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2, user3, user4), Collections.emptyList(), Arrays.asList(user, user2, user3, user4)
        );
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2, user3, user4), Arrays.asList(user, user2), Arrays.asList(user3, user4)
        );
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2, user3, user4), Collections.singletonList(user), Arrays.asList(user2, user3, user4)
        );
        testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
            Arrays.asList(user, user2, user3, user4), Arrays.asList(user, user2, user3, user4), Collections.emptyList()
        );
    }

    private void testFilterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(List<User> input, List<User> limitIsReached, List<User> output) {
        final Instant now = Instant.now();
        final ShiftWorkerServiceImpl shiftWorkerService = mock(ShiftWorkerServiceImpl.class);
        final Integer minimumFreeDaysAfterNightShifts = 2;

        doCallRealMethod().when(shiftWorkerService).setTeamRepository(eq(teamRepository));
        shiftWorkerService.setTeamRepository(teamRepository);

        input.forEach(user ->
            when(
                shiftWorkerService.minimumFreeDaysAfterNightShiftsHadPassed(eq(user), eq(minimumFreeDaysAfterNightShifts), eq(now))
            ).thenReturn(!limitIsReached.contains(user))
        );
        when(shiftWorkerService.filterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(any(), any(), any())).thenCallRealMethod();
        List<User> result = shiftWorkerService.filterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(input, minimumFreeDaysAfterNightShifts, now);
        assertThat(result).containsExactlyInAnyOrderElementsOf(output);
    }

}
