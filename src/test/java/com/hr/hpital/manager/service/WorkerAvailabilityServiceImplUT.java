package com.hr.hpital.manager.service;

import com.hr.hpital.manager.config.ApplicationProperties;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.WorkerAvailability;
import com.hr.hpital.manager.repository.WorkerAvailabilityRepository;
import com.hr.hpital.manager.service.impl.WorkerAvailabilityServiceImpl;
import com.hr.hpital.manager.service.util.CollectionsUtilKt;
import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test class for the {@link ShiftWorkerService} utility class.
 */
public class WorkerAvailabilityServiceImplUT {

    private final WorkerAvailabilityRepository workerAvailabilityRepository = mock(WorkerAvailabilityRepository.class);

    private final ApplicationProperties appProps = mock(ApplicationProperties.class);

    private final WorkerAvailabilityService workerAvailabilityService = new WorkerAvailabilityServiceImpl(
        workerAvailabilityRepository,
        null,
        null,
        null,
        null,
        null,
        null,
        appProps
    );

    @Test
    public void testIsAvailable() {
        testIsAvailable(false, true);
        testIsAvailable(true, false);
    }

    private void testIsAvailable(boolean unavailableOnADay, boolean expectedAvailabilityValue) {
        when(workerAvailabilityRepository.countByUserAndDateFromLessThanAndDateToGreaterThanAndIsAvailableIsFalse(
            any(User.class), any(Instant.class), any(Instant.class)
        )).thenReturn(unavailableOnADay ? 1L : 0L);
        assertThat(workerAvailabilityService.isAvailable(new User(), Instant.now(), Instant.now())).isEqualTo(expectedAvailabilityValue);
    }

    @Test
    public void testFilterAvailableUsers() {
        final User user = new User();
        user.setId(1L);
        user.setLogin("User 1");
        final User user2 = new User();
        user2.setId(2L);
        user2.setLogin("User 2");
        final User user3 = new User();
        user3.setId(3L);
        user3.setLogin("User 3");
        final List<User> emptyList = new ArrayList<>();

        when(appProps.getSqlBatchSizeForInConditions()).thenReturn(1);

        testFilterAvailableUsers(emptyList, emptyList, emptyList);
        testFilterAvailableUsers(Collections.singletonList(user), emptyList, Collections.singletonList(user));
        testFilterAvailableUsers(Collections.singletonList(user), Collections.singletonList(user), emptyList);

        testFilterAvailableUsers(Arrays.asList(user, user2), emptyList, Arrays.asList(user, user2));
        testFilterAvailableUsers(Arrays.asList(user, user2), Collections.singletonList(user), Collections.singletonList(user2));
        testFilterAvailableUsers(Arrays.asList(user, user2), Collections.singletonList(user2), Collections.singletonList(user));
        testFilterAvailableUsers(Arrays.asList(user, user2), Arrays.asList(user, user2), emptyList);

        testFilterAvailableUsers(Arrays.asList(user, user2, user3), emptyList, Arrays.asList(user, user2, user3));
        testFilterAvailableUsers(Arrays.asList(user, user2, user3), Collections.singletonList(user3), Arrays.asList(user, user2));
        testFilterAvailableUsers(Arrays.asList(user, user2, user3), Collections.singletonList(user2), Arrays.asList(user, user3));
        testFilterAvailableUsers(Arrays.asList(user, user2, user3), Collections.singletonList(user), Arrays.asList(user2, user3));
        testFilterAvailableUsers(Arrays.asList(user, user2, user3), Arrays.asList(user, user2), Collections.singletonList(user3));
        testFilterAvailableUsers(Arrays.asList(user, user2, user3), Arrays.asList(user2, user3), Collections.singletonList(user));
        testFilterAvailableUsers(Arrays.asList(user, user2, user3), Arrays.asList(user, user3), Collections.singletonList(user2));
        testFilterAvailableUsers(Arrays.asList(user, user2, user3), Arrays.asList(user, user2, user3), emptyList);
    }

    private void testFilterAvailableUsers(List<User> inputUser, List<User> unavailableUsers, List<User> expectedAvailableUsers) {
        when(
            workerAvailabilityRepository.findAllByUserInAndDateToGreaterThanAndDateFromLessThanAndIsAvailableIsFalse(any(), any(Instant.class), any(Instant.class))
        ).thenThrow(new RuntimeException("Should call with actual params"));

        CollectionsUtilKt.applyInBatches(
            inputUser,
            appProps.getSqlBatchSizeForInConditions(),
            (subList) -> when(
                workerAvailabilityRepository.findAllByUserInAndDateToGreaterThanAndDateFromLessThanAndIsAvailableIsFalse(eq(subList), any(Instant.class), any(Instant.class))
            ).thenReturn(
                unavailableUsers.stream().map(WorkerAvailability::new).collect(Collectors.toList())
            )
        );

        final Instant now = Instant.now();
        List<User> availableUsers = workerAvailabilityService.filterAvailableUsers(inputUser, now, now);
        assertThat(availableUsers).containsExactlyInAnyOrderElementsOf(expectedAvailableUsers);
    }

}
