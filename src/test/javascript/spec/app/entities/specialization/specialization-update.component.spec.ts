/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { HospitalmanagerTestModule } from '../../../test.module';
import { SpecializationUpdateComponent } from 'app/entities/specialization/specialization-update.component';
import { SpecializationService } from 'app/entities/specialization/specialization.service';
import { Specialization } from 'app/shared/model/specialization.model';

describe('Component Tests', () => {
  describe('Specialization Management Update Component', () => {
    let comp: SpecializationUpdateComponent;
    let fixture: ComponentFixture<SpecializationUpdateComponent>;
    let service: SpecializationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HospitalmanagerTestModule],
        declarations: [SpecializationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SpecializationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpecializationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpecializationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Specialization(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Specialization();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
