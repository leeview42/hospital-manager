/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HospitalmanagerTestModule } from '../../../test.module';
import { SpecializationDetailComponent } from 'app/entities/specialization/specialization-detail.component';
import { Specialization } from 'app/shared/model/specialization.model';

describe('Component Tests', () => {
  describe('Specialization Management Detail Component', () => {
    let comp: SpecializationDetailComponent;
    let fixture: ComponentFixture<SpecializationDetailComponent>;
    const route = ({ data: of({ specialization: new Specialization(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HospitalmanagerTestModule],
        declarations: [SpecializationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SpecializationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpecializationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.specialization).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
