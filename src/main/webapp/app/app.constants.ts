// These constants are injected via webpack environment variables.
// You can add more variables in webpack.common.js or in profile specific webpack.<dev|prod>.js files.
// If you change the values in the webpack config files, you need to re run webpack to update the application

export const VERSION = process.env.VERSION;
export const DEBUG_INFO_ENABLED: boolean = !!process.env.DEBUG_INFO_ENABLED;
export const SERVER_API_URL = process.env.SERVER_API_URL;
export const BUILD_TIMESTAMP = process.env.BUILD_TIMESTAMP;
export const HOUR_TO_MILLIS = 60 * 60 * 1000;
export const DURATONS = [
  {
    label: '1h',
    value: 1 * HOUR_TO_MILLIS
  },
  {
    label: '2h',
    value: 2 * HOUR_TO_MILLIS
  },
  {
    label: '4h',
    value: 4 * HOUR_TO_MILLIS
  },
  {
    label: '6h',
    value: 6 * HOUR_TO_MILLIS
  },
  {
    label: '7h',
    value: 7 * HOUR_TO_MILLIS
  },
  {
    label: '8h',
    value: 8 * HOUR_TO_MILLIS
  },
  {
    label: '12h',
    value: 12 * HOUR_TO_MILLIS
  },
  {
    label: '16h',
    value: 16 * HOUR_TO_MILLIS
  },
  {
    label: '24h',
    value: 24 * HOUR_TO_MILLIS
  },
  {
    label: '36h',
    value: 36 * HOUR_TO_MILLIS
  },
  {
    label: '48h',
    value: 48 * HOUR_TO_MILLIS
  }
];
