import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DayConfig } from 'app/shared/model/day-config.model';
import { DayConfigService } from './day-config.service';
import { DayConfigComponent } from './day-config.component';
import { DayConfigDetailComponent } from './day-config-detail.component';
import { DayConfigUpdateComponent } from './day-config-update.component';
import { DayConfigDeletePopupComponent } from './day-config-delete-dialog.component';
import { IDayConfig } from 'app/shared/model/day-config.model';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

@Injectable({ providedIn: 'root' })
export class DayConfigResolve implements Resolve<IDayConfig> {
  constructor(private service: DayConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDayConfig> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<DayConfig>) => response.ok),
        map((dayConfig: HttpResponse<DayConfig>) => dayConfig.body)
      );
    }
    const date = route.queryParams['date'] ? route.queryParams['date'] : null;
    if (date) {
      const newDayConfig = new DayConfig();
      newDayConfig.date = moment(moment(date).format(DATE_FORMAT));
      return of(newDayConfig);
    }

    return of(new DayConfig());
  }
}

export const dayConfigRoute: Routes = [
  {
    path: '',
    component: DayConfigComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.dayConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DayConfigDetailComponent,
    resolve: {
      dayConfig: DayConfigResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DayConfigUpdateComponent,
    resolve: {
      dayConfig: DayConfigResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfig.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DayConfigUpdateComponent,
    resolve: {
      dayConfig: DayConfigResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfig.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const dayConfigPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DayConfigDeletePopupComponent,
    resolve: {
      dayConfig: DayConfigResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfig.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
