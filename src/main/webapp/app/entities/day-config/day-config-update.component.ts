import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { DayConfig, IDayConfig } from 'app/shared/model/day-config.model';
import { DayConfigService } from './day-config.service';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { MAX_ITEMS_PER_PAGE } from 'app/shared';
import { select } from '@angular-redux/store';
import { IWeekDay } from '../../shared/model/week-day.model';
import { ISpecialization } from 'app/shared/model/specialization.model';
import { filter, map } from 'rxjs/operators';
import { SpecializationService } from '../specialization';
import { JhiAlertService } from 'ng-jhipster';
import { IDayConfigShiftType, DayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';
import { DayConfigShiftTypeSpec, IDayConfigShiftTypeSpec } from 'app/shared/model/day-config-shift-type-spec.model';

@Component({
  selector: 'jhi-day-config-update',
  templateUrl: './day-config-update.component.html'
})
export class DayConfigUpdateComponent implements OnInit, OnDestroy {
  dayConfig: IDayConfig;
  isSaving: boolean;
  hasSpecializations: boolean;
  shiftTypes: IShiftType[];
  weekDays: IWeekDay[];
  specializations: ISpecialization[];

  subscriptions: Subscription[] = [];

  @select(['translate', 'weekDays'])
  weekDays$: Observable<IWeekDay[]>;

  @select(['team', 'shiftTypes'])
  shiftTypes$: Observable<IShiftType[]>;

  @select(['user', 'hasSpecializations'])
  hasSpecializations$: Observable<boolean>;

  editForm = this.fb.group({
    id: [],
    date: [],
    isBankHoliday: [false],
    createWholeMonth: [false],
    weekDays: [],
    dayConfigShiftTypes: this.fb.array([], [Validators.required]),
    shiftTypeSelect: [[], Validators.required]
  });

  constructor(
    protected dayConfigService: DayConfigService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected specializationService: SpecializationService,
    protected jhiAlertService: JhiAlertService
  ) {}

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  ngOnInit() {
    this.isSaving = false;
    this.subscriptions.push(
      this.shiftTypes$.subscribe(shiftTypes => {
        this.shiftTypes = shiftTypes;
        this.subscriptions.push(
          this.hasSpecializations$.subscribe(hasSpecializations => {
            this.hasSpecializations = hasSpecializations;
            this.subscriptions.push(
              this.activatedRoute.data.subscribe(({ dayConfig }) => {
                this.dayConfig = dayConfig;
                if (!this.dayConfig.dayConfigShiftTypes) {
                  this.dayConfig.dayConfigShiftTypes = [];
                }
                this.updateForm(dayConfig);
                this.editForm.get('createWholeMonth').valueChanges.subscribe(createWholeMonth => {
                  if (createWholeMonth) {
                    this.editForm.get('weekDays').enable();
                  } else {
                    this.editForm.get('weekDays').disable();
                  }
                });
                this.editForm.get('weekDays').disable();
              })
            );
          })
        );
      })
    );
    this.subscriptions.push(
      this.weekDays$.subscribe(weekDays => {
        this.weekDays = weekDays;
      })
    );
    this.specializationService
      .query({
        size: MAX_ITEMS_PER_PAGE
      })
      .pipe(
        filter((mayBeOk: HttpResponse<ISpecialization[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISpecialization[]>) => response.body)
      )
      .subscribe((res: ISpecialization[]) => (this.specializations = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(dayConfig: IDayConfig) {
    this.editForm.patchValue({
      id: dayConfig.id,
      date: dayConfig.date,
      isBankHoliday: dayConfig.isBankHoliday,
      shiftTypeSelect:
        dayConfig == null || !dayConfig.dayConfigShiftTypes ? [] : dayConfig.dayConfigShiftTypes.map(shiftType => shiftType.shiftTypeId),
      weekDays: dayConfig.date ? [dayConfig.date.day() - 1] : []
    });
    if (dayConfig && dayConfig.dayConfigShiftTypes) {
      dayConfig.dayConfigShiftTypes.forEach(dayConfiShiftType => {
        this.dayConfigShiftTypes.push(
          this.newDayConfShiftTypesGroup(dayConfiShiftType.shiftType, dayConfiShiftType.noOfWorkers, dayConfiShiftType.specializations)
        );
      });
    }
  }

  shiftTypeSelectChanged(selectedShiftTypes: any[]) {
    if (selectedShiftTypes.length > this.dayConfigShiftTypes.controls.length) {
      this.dayConfigShiftTypes.push(
        this.newDayConfShiftTypesGroup(
          selectedShiftTypes.find(
            shiftType =>
              !this.dayConfigShiftTypes.controls
                .map(dConfShiftTypeControl => dConfShiftTypeControl.get('shiftTypeId').value)
                .includes(shiftType.id)
          )
        )
      );
    } else if (selectedShiftTypes.length < this.dayConfigShiftTypes.controls.length) {
      const shiftTypeIds = selectedShiftTypes.map(shiftType => shiftType.id);
      this.dayConfigShiftTypes.removeAt(
        this.dayConfigShiftTypes.controls.findIndex(dayConfShiftType => !shiftTypeIds.includes(dayConfShiftType.get('shiftTypeId').value))
      );
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const dayConfig = this.createFromForm();
    const createWholeMonth = this.editForm.get(['createWholeMonth']).value;
    if (dayConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.dayConfigService.update(dayConfig, createWholeMonth, this.editForm.get('weekDays').value));
    } else {
      this.subscribeToSaveResponse(this.dayConfigService.create(dayConfig, createWholeMonth, this.editForm.get('weekDays').value));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDayConfig>>) {
    result.subscribe((res: HttpResponse<IDayConfig>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IDayConfig {
    const entity = {
      ...new DayConfig(),
      id: this.editForm.get(['id']).value,
      date: this.editForm.get(['date']).value,
      isBankHoliday: this.editForm.get(['isBankHoliday']).value,
      dayConfigShiftTypes: this.mapDayConfShiftTypes()
    };
    return entity;
  }

  private mapDayConfShiftTypes(): IDayConfigShiftType[] {
    return this.dayConfigShiftTypes.controls.map(dayConfShiftType => {
      return {
        ...new DayConfigShiftType(),
        shiftTypeId: dayConfShiftType.get('shiftTypeId').value,
        noOfWorkers: dayConfShiftType.get('noOfWorkers').value,
        specializations: this.hasSpecializations
          ? this.mapDayConfShiftTypeSpecs(<FormArray>dayConfShiftType.get('specializationsArray'))
          : null
      };
    });
  }

  private mapDayConfShiftTypeSpecs(specializationArray: FormArray): IDayConfigShiftTypeSpec[] {
    return specializationArray.controls.map(specialization => {
      return {
        ...new DayConfigShiftTypeSpec(),
        specializationId: specialization.get('specializationId').value,
        noOfWorkers: specialization.get('noOfWorkers').value
      };
    });
  }

  addWeekDay() {
    this.editForm.patchValue({
      weekDays: this.dayConfig.date ? [this.dayConfig.date.day() - 1] : []
    });
  }

  private newDayConfShiftTypesGroup(shiftType: IShiftType, noOfWorkers?, specializations?): FormGroup {
    let noOfWorkersValue = noOfWorkers;
    if (!noOfWorkersValue) {
      noOfWorkersValue = shiftType.noOfWorkers && shiftType.noOfWorkers > 0 ? shiftType.noOfWorkers : 1;
    }
    let specializationsArray = this.fb.array([]);
    if (this.hasSpecializations) {
      specializationsArray = this.fb.array([], [Validators.required, this.uniqueSpecializationValidator]);
      if (specializations) {
        specializations.forEach(specialization => {
          specializationsArray.push(this.newShiftTypeSpecializationGroup(specialization.specializationId, specialization.noOfWorkers));
        });
      }
    }
    return this.fb.group({
      noOfWorkers: [noOfWorkersValue, [Validators.required, Validators.min(1)]],
      shiftTypeName: shiftType.name,
      shiftTypeId: shiftType.id,
      shiftType,
      specializationsArray
    });
  }

  newShiftTypeSpecializationGroup(specializationId?, noOfworkers?): FormGroup {
    const noOfWorkersValue = noOfworkers ? noOfworkers : 1;
    return this.fb.group({
      specializationId: [specializationId, [Validators.required]],
      noOfWorkers: [noOfWorkersValue, [Validators.required, Validators.min(1)]]
    });
  }

  public get dayConfigShiftTypes() {
    return this.editForm.get('dayConfigShiftTypes') as FormArray;
  }

  public getSpecializationsConfig = dayConfShiftTypeIndex =>
    <FormArray>this.dayConfigShiftTypes.at(dayConfShiftTypeIndex).get('specializationsArray');

  public addSpecialization(index) {
    this.getSpecializationsConfig(index).push(this.newShiftTypeSpecializationGroup());
  }

  public uniqueSpecializationValidator(formArray: FormArray): { [key: string]: boolean } | null {
    const specIdSet = new Set();
    if (formArray && formArray.controls && formArray.controls.length > 1) {
      formArray.controls.forEach(group => {
        specIdSet.add(group.get('specializationId').value);
      });
      if (specIdSet.size !== formArray.controls.length) {
        return { notUnique: true };
      }
    }
    return null;
  }

  public countNoOfWorkers(index: number): number {
    const specializationsArray = (<FormArray>this.dayConfigShiftTypes.at(index).get('specializationsArray')).controls;
    return specializationsArray.map(specializationGroup => specializationGroup.get('noOfWorkers').value).reduce((a, b) => a + b, 0);
  }
}
