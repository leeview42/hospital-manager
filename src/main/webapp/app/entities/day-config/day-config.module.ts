import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarModule, DateAdapter } from 'angular-calendar';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  DayConfigComponent,
  DayConfigDetailComponent,
  DayConfigUpdateComponent,
  DayConfigDeletePopupComponent,
  DayConfigDeleteDialogComponent,
  dayConfigRoute,
  dayConfigPopupRoute
} from './';

const ENTITY_STATES = [...dayConfigRoute, ...dayConfigPopupRoute];

@NgModule({
  imports: [
    HospitalManagerSharedModule,
    RouterModule.forChild(ENTITY_STATES),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  declarations: [
    DayConfigComponent,
    DayConfigDetailComponent,
    DayConfigUpdateComponent,
    DayConfigDeleteDialogComponent,
    DayConfigDeletePopupComponent
  ],
  entryComponents: [DayConfigComponent, DayConfigUpdateComponent, DayConfigDeleteDialogComponent, DayConfigDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerDayConfigModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
