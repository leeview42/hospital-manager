import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDayConfig } from 'app/shared/model/day-config.model';

@Component({
  selector: 'jhi-day-config-detail',
  templateUrl: './day-config-detail.component.html'
})
export class DayConfigDetailComponent implements OnInit {
  dayConfig: IDayConfig;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ dayConfig }) => {
      this.dayConfig = dayConfig;
    });
  }

  previousState() {
    window.history.back();
  }
}
