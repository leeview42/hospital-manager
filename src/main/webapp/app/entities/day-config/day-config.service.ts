import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDayConfig } from 'app/shared/model/day-config.model';

type EntityResponseType = HttpResponse<IDayConfig>;
type EntityArrayResponseType = HttpResponse<IDayConfig[]>;

@Injectable({ providedIn: 'root' })
export class DayConfigService {
  public updateResourceUrl = SERVER_API_URL + 'api/team-management/day-configs';
  public getResourceUrl = SERVER_API_URL + 'api/team-member/day-configs';

  public currentView: any = {};

  constructor(protected http: HttpClient) {}

  create(dayConfig: IDayConfig, createWholeMonth: boolean, weekDays: [] = []): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dayConfig);
    const req = createRequestOption({ createWholeMonth, weekDays });
    return this.http
      .post<IDayConfig>(this.updateResourceUrl, copy, { observe: 'response', params: req })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(dayConfig: IDayConfig, createWholeMonth?: boolean, weekDays: [] = []): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dayConfig);
    const req = createRequestOption({ createWholeMonth, weekDays });
    return this.http
      .put<IDayConfig>(this.updateResourceUrl, copy, { observe: 'response', params: req })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDayConfig>(`${this.getResourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findForInterval(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDayConfig[]>(`${this.getResourceUrl}/interval`, { params: options, observe: 'response' });
  }

  deleteForInterval(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.delete<IDayConfig[]>(`${this.updateResourceUrl}/interval`, { params: options, observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDayConfig[]>(this.getResourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.updateResourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(dayConfig: IDayConfig): IDayConfig {
    const copy: IDayConfig = Object.assign({}, dayConfig, {
      date: dayConfig.date != null && dayConfig.date.isValid() ? dayConfig.date.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date != null ? moment(res.body.date) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((dayConfig: IDayConfig) => {
        dayConfig.date = dayConfig.date != null ? moment(dayConfig.date) : null;
      });
    }
    return res;
  }
}
