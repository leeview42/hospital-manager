import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDayConfig } from 'app/shared/model/day-config.model';
import { DayConfigService } from './day-config.service';

@Component({
  selector: 'jhi-day-config-delete-dialog',
  templateUrl: './day-config-delete-dialog.component.html'
})
export class DayConfigDeleteDialogComponent {
  dayConfig: IDayConfig;

  constructor(protected dayConfigService: DayConfigService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.dayConfigService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'dayConfigListModification',
        content: 'Deleted an dayConfig'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-day-config-delete-popup',
  template: ''
})
export class DayConfigDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ dayConfig }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DayConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.dayConfig = dayConfig;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/day-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/day-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
