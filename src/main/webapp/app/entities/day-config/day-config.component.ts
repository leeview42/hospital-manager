import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, Subject, Subscription } from 'rxjs';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IDayConfig } from 'app/shared/model/day-config.model';
import { AccountService } from 'app/core';

import { DayConfigService } from './day-config.service';
import { CalendarEvent, CalendarMonthViewComponent, CalendarView, CalendarMonthViewDay } from 'angular-calendar';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { isSameDay, isSameMonth } from 'date-fns';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { ConfirmModalComponent } from '../../shared/util/confirm-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { calculateWeekNumbers } from 'app/shared/util/moment-util';
import { select } from '@angular-redux/store';
import { Moment } from 'moment';

@Component({
  selector: 'jhi-day-config',
  templateUrl: './day-config.component.html',
  styleUrls: ['day-config.component.scss']
})
export class DayConfigComponent implements OnInit, OnDestroy {
  currentAccount: any;

  activeDayIsOpen = true;

  viewDate: Date = new Date();
  fromDate: any;
  toDate: any;
  weekNumbers: number[] = [];

  @ViewChild('calendarMonthView', { static: false }) calendarMonthView: CalendarMonthViewComponent;
  CalendarView = CalendarView;
  view = CalendarView.Month;
  refresh: Subject<any> = new Subject();
  @select(['team', 'id'])
  teamId: Observable<number>;
  teamIdSubscription: Subscription;
  events: CalendarEvent[] = [];
  dayConfigs: IDayConfig[] = [];
  @select(['user', 'hasSpecializations'])
  hasSpecializations$: Observable<boolean>;

  constructor(
    protected dayConfigService: DayConfigService,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected router: Router,
    private modalService: NgbModal,
    protected eventManager: JhiEventManager
  ) {}

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    if (this.dayConfigService.currentView.viewDate) {
      this.viewDate = this.dayConfigService.currentView.viewDate;
    }
    this.getEventsForDate(this.viewDate);
    if (this.activeDayIsOpen === true) {
      this.toggleEventsPreview({ date: this.viewDate, events: this.events });
    }
    this.teamIdSubscription = this.teamId.subscribe(value => this.getEventsForDate(this.viewDate));
  }

  getEventsForDate(date: Date) {
    this.fromDate = moment(date).startOf('month');
    this.toDate = moment(date).endOf('month');
    this.loadAll(this.fromDate, this.toDate);
  }

  loadAll(from: Moment, to: Moment) {
    this.dayConfigService
      .findForInterval({
        fromDate: from.format(DATE_FORMAT),
        toDate: to.format(DATE_FORMAT)
      })
      .subscribe(
        (res: HttpResponse<IDayConfig[]>) => this.convertEventsResponse(res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  convertEventsResponse(dayConfigs: IDayConfig[]) {
    this.events = [];
    this.dayConfigs = [];
    dayConfigs.forEach(dayConfig => this.events.push(this.convertDayConfigToEvent(dayConfig)));
    this.refresh.next();
  }

  convertDayConfigToEvent(dayConfig: IDayConfig): any {
    this.dayConfigs.push(dayConfig);

    return {
      id: dayConfig.id,
      title: this.getEventTitle(dayConfig),
      start: this.getEventStartTime(dayConfig),
      end: this.getEventEndTime(dayConfig),
      dayConfig,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      color: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
      },
      actions: [
        {
          label: `<span style="color: #1e90ff!important"> View </span>`,
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventAction('View', event);
          }
        },
        {
          label: `<span style="color: #1e90ff!important"> Edit </span>`,
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventAction('Edit', event);
          }
        },
        {
          label: `<span style="color: #1e90ff!important"> Remove </span> <br>`,
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventAction('Remove', event);
          }
        }
      ]
    };
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    const viewDateMoment = moment(this.viewDate);
    if (
      events.length === 0 &&
      !viewDateMoment.isBefore(moment(date).startOf('month')) &&
      !viewDateMoment.isAfter(moment(date).endOf('month'))
    ) {
      this.router.navigate(['/day-config/new'], { queryParams: { date }, queryParamsHandling: 'merge' });
    }
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  toggleEventsPreview({ date, events }: { date: Date; events: CalendarEvent[] }) {
    if (isSameMonth(date, this.viewDate)) {
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0);
      this.viewDate = date;
    }
  }

  eventAction(actionType: string, event: CalendarEvent) {
    switch (actionType) {
      case 'View':
        this.router.navigate(['/day-config', event.id, 'view']);
        break;
      case 'Edit':
        this.router.navigate(['/day-config', event.id, 'edit']);
        break;
      case 'Remove':
        this.removeEvent(event);
        break;
    }
  }

  removeEvent(event: CalendarEvent) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'hospitalmanagerApp.dayConfig.delete.title';
    modalRef.componentInstance.content = 'hospitalmanagerApp.dayConfig.delete.questionDetailed';
    modalRef.componentInstance.contentParams = {
      date: moment(event.start).format('DD-MM-YYYY'),
      details: event.title
    };

    modalRef.result.then(result => {
      if (result === 'OK') {
        this.dayConfigService.delete(Number(event.id)).subscribe(() => {
          this.events.splice(this.events.indexOf(event), 1);
          this.refresh.next();
        });
      }
    });
  }

  eventTimesChanged({ event, newStart, newEnd, shiftType }: any): void {
    const dayConfig = this.getDayConfig(Number(event.id));
    dayConfig.date = moment(moment(newStart).format(DATE_FORMAT));
    dayConfig.dayConfigShiftTypes.map(aShiftType => (aShiftType.noOfWorkers = this.getNoOfWorkers(dayConfig, aShiftType)));
    this.dayConfigService.update(dayConfig).subscribe((res: HttpResponse<IDayConfig>) => {
      this.events.splice(this.events.indexOf(event), 1);
      this.dayConfigs.splice(this.dayConfigs.indexOf(dayConfig), 1);
      this.refresh.next();

      const newDayConfig = res.body;
      const newEvent = {
        ...event,
        title: this.getEventTitle(newDayConfig),
        start: this.getEventStartTime(newDayConfig),
        end: this.getEventEndTime(newDayConfig)
      };

      this.events.push(newEvent);
      this.dayConfigs.push(newDayConfig);
      this.refresh.next();

      if (this.activeDayIsOpen === true) {
        this.toggleEventsPreview({ date: this.viewDate, events: this.events });
      }
    });
  }

  public getNoOfWorkers(dayConfig: IDayConfig, shiftType: IShiftType): number {
    const result = dayConfig.dayConfigShiftTypes
      .filter(dayCfgShiftType => dayCfgShiftType.shiftTypeId === shiftType.id)
      .map(dayConfigShiftType => dayConfigShiftType.noOfWorkers)
      .reduce((a, b) => a + b);
    return result ? result : 1;
  }

  viewDateChanged($event: Date) {
    this.closeOpenMonthViewDay();
    this.getEventsForDate($event);
    this.dayConfigService.currentView.viewDate = $event;
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    this.weekNumbers = calculateWeekNumbers(body.map(day => day.date));
  }

  clear() {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'hospitalmanagerApp.dayConfig.delete.all.title';
    modalRef.componentInstance.content = 'hospitalmanagerApp.dayConfig.delete.all.questionDetailed';
    modalRef.componentInstance.contentParams = {
      dateFrom: moment(this.fromDate).format('DD-MM-YYYY'),
      dateTo: moment(this.toDate).format('DD-MM-YYYY')
    };

    modalRef.result.then(result => {
      if (result === 'OK') {
        this.dayConfigService
          .deleteForInterval({
            fromDate: moment(this.fromDate).format(DATE_FORMAT),
            toDate: moment(this.toDate).format(DATE_FORMAT)
          })
          .subscribe(
            next => {
              this.events = [];
            },
            (res: HttpErrorResponse) => this.onError(res.message)
          );
      }
    });
  }

  getWeekNumbers(): string {
    let result = '';
    this.weekNumbers.forEach(weekNumber => {
      result += (result === '' ? '' : ', ') + weekNumber;
    });
    return result !== '' ? '(' + result + ')' : '';
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.teamIdSubscription);
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private getEventTitle(dayConfig: IDayConfig): string {
    let title = dayConfig.isBankHoliday ? 'Bank holiday<br>' : '';
    if (dayConfig.dayConfigShiftTypes) {
      dayConfig.dayConfigShiftTypes.forEach(dayConfigShiftType => {
        const startTime = moment(`${dayConfig.date} ${dayConfigShiftType.shiftType.startHour}:${dayConfigShiftType.shiftType.startMinute}`);
        const endTime = this.getEndTime(startTime, dayConfigShiftType.shiftType);
        const time = isSameDay(startTime.toDate(), endTime.toDate())
          ? `${startTime.format('hh:mm')} and ${endTime.format('hh:mm')}`
          : `${startTime.format('DD-MM-YYYY hh:mm')} and ${endTime.format('DD-MM-YYYY hh:mm')}`;
        title = title.concat(` between ${time}<br>`);
      });
    }

    return title.search('<br>') > -1 ? title.slice(0, title.lastIndexOf('<br>')) : title;
  }

  private getEventStartTime(dayConfig: IDayConfig): Date {
    const date = moment(dayConfig.date);
    return moment(`${date.format(DATE_FORMAT)} 00:00`).toDate();
  }

  private getEventEndTime(dayConfig: IDayConfig): Date {
    const date = moment(dayConfig.date);
    return moment(`${date.format(DATE_FORMAT)} 01:00`).toDate();
  }

  private getEndTime(startTime: Moment, shiftType: IShiftType): Moment {
    return startTime.clone().add(shiftType.durationMillis, 'milliseconds');
  }

  private getDayConfig(id: number): IDayConfig {
    return this.dayConfigs.find(dayConfig => dayConfig.id === id);
  }
}
