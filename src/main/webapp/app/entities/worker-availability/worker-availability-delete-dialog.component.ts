import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWorkerAvailability } from 'app/shared/model/worker-availability.model';
import { WorkerAvailabilityService } from './worker-availability.service';

@Component({
  selector: 'jhi-worker-availability-delete-dialog',
  templateUrl: './worker-availability-delete-dialog.component.html'
})
export class WorkerAvailabilityDeleteDialogComponent {
  workerAvailability: IWorkerAvailability;

  constructor(
    protected workerAvailabilityService: WorkerAvailabilityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.workerAvailabilityService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'workerAvailabilityListModification',
        content: 'Deleted an workerAvailability'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-worker-availability-delete-popup',
  template: ''
})
export class WorkerAvailabilityDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ workerAvailability }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(WorkerAvailabilityDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.workerAvailability = workerAvailability;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/worker-availability', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/worker-availability', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
