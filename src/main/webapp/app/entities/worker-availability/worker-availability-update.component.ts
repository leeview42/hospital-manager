import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IWorkerAvailability, WorkerAvailability } from 'app/shared/model/worker-availability.model';
import { WorkerAvailabilityService } from './worker-availability.service';
import { IUser, UserService } from 'app/core';
import { MAX_ITEMS_PER_PAGE } from 'app/shared';

@Component({
  selector: 'jhi-worker-availability-update',
  templateUrl: './worker-availability-update.component.html'
})
export class WorkerAvailabilityUpdateComponent implements OnInit {
  workerAvailability: IWorkerAvailability;
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    dateFrom: [],
    dateTo: [],
    isAvailable: [],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected workerAvailabilityService: WorkerAvailabilityService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ workerAvailability }) => {
      this.updateForm(workerAvailability);
      this.workerAvailability = workerAvailability;
    });
    this.userService
      .query({
        size: MAX_ITEMS_PER_PAGE
      })
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(workerAvailability: IWorkerAvailability) {
    this.editForm.patchValue({
      id: workerAvailability.id,
      dateFrom: workerAvailability.dateFrom != null ? workerAvailability.dateFrom.format(DATE_TIME_FORMAT) : null,
      dateTo: workerAvailability.dateTo != null ? workerAvailability.dateTo.format(DATE_TIME_FORMAT) : null,
      isAvailable: workerAvailability.isAvailable,
      userId: workerAvailability.userId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const workerAvailability = this.createFromForm();
    if (workerAvailability.id !== undefined) {
      this.subscribeToSaveResponse(this.workerAvailabilityService.update(workerAvailability));
    } else {
      this.subscribeToSaveResponse(this.workerAvailabilityService.create(workerAvailability));
    }
  }

  private createFromForm(): IWorkerAvailability {
    const entity = {
      ...new WorkerAvailability(),
      id: this.editForm.get(['id']).value,
      dateFrom: this.editForm.get(['dateFrom']).value != null ? moment(this.editForm.get(['dateFrom']).value, DATE_TIME_FORMAT) : undefined,
      dateTo: this.editForm.get(['dateTo']).value != null ? moment(this.editForm.get(['dateTo']).value, DATE_TIME_FORMAT) : undefined,
      isAvailable: this.editForm.get(['isAvailable']).value,
      userId: this.editForm.get(['userId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkerAvailability>>) {
    result.subscribe((res: HttpResponse<IWorkerAvailability>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
