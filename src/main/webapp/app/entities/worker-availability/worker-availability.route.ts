import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { WorkerAvailability } from 'app/shared/model/worker-availability.model';
import { WorkerAvailabilityService } from './worker-availability.service';
import { WorkerAvailabilityComponent } from './worker-availability.component';
import { WorkerAvailabilityDetailComponent } from './worker-availability-detail.component';
import { WorkerAvailabilityUpdateComponent } from './worker-availability-update.component';
import { WorkerAvailabilityDeletePopupComponent } from './worker-availability-delete-dialog.component';
import { IWorkerAvailability } from 'app/shared/model/worker-availability.model';

@Injectable({ providedIn: 'root' })
export class WorkerAvailabilityResolve implements Resolve<IWorkerAvailability> {
  constructor(private service: WorkerAvailabilityService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWorkerAvailability> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<WorkerAvailability>) => response.ok),
        map((workerAvailability: HttpResponse<WorkerAvailability>) => workerAvailability.body)
      );
    }
    return of(new WorkerAvailability());
  }
}

export const workerAvailabilityRoute: Routes = [
  {
    path: '',
    component: WorkerAvailabilityComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.workerAvailability.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WorkerAvailabilityDetailComponent,
    resolve: {
      workerAvailability: WorkerAvailabilityResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.workerAvailability.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WorkerAvailabilityUpdateComponent,
    resolve: {
      workerAvailability: WorkerAvailabilityResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.workerAvailability.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WorkerAvailabilityUpdateComponent,
    resolve: {
      workerAvailability: WorkerAvailabilityResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.workerAvailability.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const workerAvailabilityPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: WorkerAvailabilityDeletePopupComponent,
    resolve: {
      workerAvailability: WorkerAvailabilityResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.workerAvailability.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
