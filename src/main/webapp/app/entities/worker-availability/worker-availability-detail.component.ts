import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWorkerAvailability } from 'app/shared/model/worker-availability.model';

@Component({
  selector: 'jhi-worker-availability-detail',
  templateUrl: './worker-availability-detail.component.html'
})
export class WorkerAvailabilityDetailComponent implements OnInit {
  workerAvailability: IWorkerAvailability;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ workerAvailability }) => {
      this.workerAvailability = workerAvailability;
    });
  }

  previousState() {
    window.history.back();
  }
}
