import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IWorkerAvailability } from 'app/shared/model/worker-availability.model';
import { Moment } from 'moment';

type EntityResponseType = HttpResponse<IWorkerAvailability>;
type EntityArrayResponseType = HttpResponse<IWorkerAvailability[]>;

@Injectable({ providedIn: 'root' })
export class WorkerAvailabilityService {
  public updateResourceUrl = SERVER_API_URL + 'api/team-management/worker-availabilities';
  public getResourceUrl = SERVER_API_URL + 'api/team-member/worker-availabilities';
  public currentView: any = {};

  constructor(protected http: HttpClient) {}

  create(workerAvailability: IWorkerAvailability): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workerAvailability);
    return this.http
      .post<IWorkerAvailability>(this.updateResourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(workerAvailability: IWorkerAvailability): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workerAvailability);
    return this.http
      .put<IWorkerAvailability>(this.updateResourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  updateUnavailabilities(
    userId: number,
    startDateInterval: string,
    stopDateInterval: string,
    unavailableDays: string[]
  ): Observable<HttpResponse<any>> {
    return this.http.put<IWorkerAvailability>(
      `${this.updateResourceUrl}/unavailableDays`,
      {
        userId,
        startDateInterval,
        stopDateInterval,
        unavailableDays
      },
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWorkerAvailability>(`${this.getResourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findAvailabilities(req?: any): Observable<HttpResponse<Moment[]>> {
    return this.http.get<Moment[]>(`${this.getResourceUrl}/unavailableDays`, { params: req, observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWorkerAvailability[]>(this.getResourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.updateResourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(workerAvailability: IWorkerAvailability): IWorkerAvailability {
    const copy: IWorkerAvailability = Object.assign({}, workerAvailability, {
      dateFrom: workerAvailability.dateFrom != null && workerAvailability.dateFrom.isValid() ? workerAvailability.dateFrom.toJSON() : null,
      dateTo: workerAvailability.dateTo != null && workerAvailability.dateTo.isValid() ? workerAvailability.dateTo.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateFrom = res.body.dateFrom != null ? moment(res.body.dateFrom) : null;
      res.body.dateTo = res.body.dateTo != null ? moment(res.body.dateTo) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((workerAvailability: IWorkerAvailability) => {
        workerAvailability.dateFrom = workerAvailability.dateFrom != null ? moment(workerAvailability.dateFrom) : null;
        workerAvailability.dateTo = workerAvailability.dateTo != null ? moment(workerAvailability.dateTo) : null;
      });
    }
    return res;
  }
}
