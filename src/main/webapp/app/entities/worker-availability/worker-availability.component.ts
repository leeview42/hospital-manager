import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { AccountService } from 'app/core';

import { WorkerAvailabilityService } from './worker-availability.service';
import { CalendarMonthViewComponent, CalendarView, CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { WishService } from 'app/entities/wish';
import { IWish } from 'app/shared/model/wish.model';
import { ConfirmModalComponent, formatUserName } from 'app/shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { calculateWeekNumbers } from 'app/shared/util/moment-util';
import { select } from '@angular-redux/store';

export const SELECTED_DAY_CLASS = 'cal-day-selected';

@Component({
  selector: 'jhi-worker-availability',
  templateUrl: './worker-availability.component.html',
  styleUrls: ['./worker-availability.component.scss']
})
export class WorkerAvailabilityComponent implements OnInit, OnDestroy {
  currentAccount: any;

  @select(['team', 'users'])
  users$: Observable<any>;
  users: IUser[] = [];
  allUsers: IUser[] = [];
  selectedUserId: number;

  userSubscription: Subscription;

  showEditButton = false;
  showClearButton = false;
  editMode = false;

  viewDate: Date = new Date();
  fromDate: any;
  toDate: any;
  weekNumbers: number[] = [];
  userFilter = '';

  wishes: IWish[] = [];

  @ViewChild('calendarMonthView', { static: false }) calendarMonthView: CalendarMonthViewComponent;
  CalendarView = CalendarView;
  view = CalendarView.Month;
  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  selectedDays = [];

  constructor(
    protected workerAvailabilityService: WorkerAvailabilityService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected userService: UserService,
    protected wishService: WishService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.loadUsers();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  loadUsers() {
    this.userSubscription = this.users$.subscribe(
      (res: IUser[]) => this.convertUsersToSelectArr(res),
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  getEventsForDate(date: Date) {
    this.fromDate = moment(date).startOf('month');
    this.toDate = moment(date).endOf('month');
    const selectedUser = this.getSelectedUser();
    if (selectedUser) {
      this.loadAvailabilities(this.fromDate, this.toDate, selectedUser);
      this.loadUserWishes(selectedUser.id, this.fromDate, this.toDate);
    }
  }

  loadAvailabilities(from: Moment, to: Moment, selectedUser: any) {
    if (selectedUser) {
      this.selectedDays = [];
      this.showEditButton = true;
      this.showClearButton = true;
      this.editMode = false;
      this.workerAvailabilityService
        .findAvailabilities({
          userId: selectedUser.id,
          dateFrom: from.toJSON(),
          dateTo: to.toJSON()
        })
        .subscribe(
          (res: HttpResponse<Moment[]>) => this.drawUnavailabilities(res.body),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  loadUserWishes(userId: number, from: Moment, to: Moment) {
    if (userId && from && to) {
      this.wishService
        .query({
          'fromDate.greaterOrEqualThan': from.format(DATE_FORMAT),
          'toDate.lessOrEqualThan': to.format(DATE_FORMAT),
          'userId.equals': userId
        })
        .subscribe(
          (res: HttpResponse<IWish[]>) => this.convertEventsResponse(res.body),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  convertEventsResponse(wishes: IWish[]) {
    this.events = [];
    this.wishes = [];
    wishes.forEach(dayConfig => this.events.push(this.convertWishToEvent(dayConfig)));
    this.refresh.next();
  }

  convertWishToEvent(wish: IWish): any {
    this.wishes.push(wish);

    return {
      id: wish.id,
      title: this.getEventTitle(wish),
      start: this.getEventStartTime(wish),
      end: this.getEventEndTime(wish),
      wish,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      color: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
      },
      actions: [
        {
          label: `<span style='color: #1e90ff!important'> View </span>`,
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventAction('View', event);
          }
        },
        {
          label: `<span style='color: #1e90ff!important'> Edit </span>`,
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventAction('Edit', event);
          }
        },
        {
          label: `<span style='color: #1e90ff!important'> Remove </span> <br>`,
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventAction('Remove', event);
          }
        }
      ]
    };
  }

  public eventAction(actionType: string, event: CalendarEvent) {
    if (!this.editMode) {
      switch (actionType) {
        case 'View':
          this.router.navigate(['/wish', event.id, 'view']);
          break;
        case 'Edit':
          this.router.navigate(['/wish', event.id, 'edit']);
          break;
        case 'Remove':
          this.removeEvent(event);
          break;
      }
    }
  }

  removeEvent(event: CalendarEvent) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'hospitalmanagerApp.wish.delete.title';
    modalRef.componentInstance.content = 'hospitalmanagerApp.wish.delete.questionDetailed';
    modalRef.componentInstance.contentParams = {
      date: moment(event.start).format('DD-MM-YYYY')
    };

    modalRef.result.then(result => {
      if (result === 'OK') {
        this.wishService.delete(Number(event.id)).subscribe(() => {
          this.events.splice(this.events.indexOf(event), 1);
          this.refresh.next();
        });
      }
    });
  }

  drawUnavailabilities(unavailableDays: Moment[]) {
    unavailableDays
      .map(day => moment(day).format(DATE_FORMAT))
      .filter(day => this.selectedDays.filter(aDay => aDay === day).length === 0)
      .forEach(day => this.selectedDays.push(day));
    this.refresh.next();
  }

  convertUsersToSelectArr(res: IUser[]) {
    this.users = res
      .map(resUser => {
        resUser.name = formatUserName(resUser);
        return resUser;
      })
      .sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0));
    this.allUsers = [...this.users];

    if (this.workerAvailabilityService.currentView.viewDate) {
      this.viewDate = this.workerAvailabilityService.currentView.viewDate;
    }
    if (this.workerAvailabilityService.currentView.userId) {
      this.userWasSelected(this.workerAvailabilityService.currentView.userId);
    }
  }

  getSelectedUser(): IUser {
    return this.selectedUserId ? this.users.find(user => user.id === this.selectedUserId) : null;
  }

  userWasSelected(userId: any) {
    this.selectedDays = [];
    this.events = [];
    this.wishes = [];

    const userWasUnselected = this.selectedUserId === userId;
    this.showEditButton = !userWasUnselected;
    this.showClearButton = !this.showClearButton;
    if (userWasUnselected) {
      this.selectedUserId = null;
      this.refresh.next();
    } else {
      this.selectedUserId = userId;
      this.workerAvailabilityService.currentView.userId = userId;
      this.getEventsForDate(this.viewDate);
    }
  }

  userWasRemoved() {
    this.selectedUserId = null;
    this.selectedDays = [];
    this.showEditButton = false;
    this.editMode = false;
    this.refresh.next();
  }

  dayClicked(day: CalendarMonthViewDay): void {
    const viewDateMoment = moment(this.viewDate);
    const dayMoment = moment(day.date);
    if (!viewDateMoment.isBefore(moment(day.date).startOf('month')) && !viewDateMoment.isAfter(moment(day.date).endOf('month'))) {
      if (this.editMode && day.events.length === 0) {
        const selectedDayString = dayMoment.format(DATE_FORMAT);
        const dateIndex = this.selectedDays.findIndex(selectedDay => selectedDay === selectedDayString);
        if (dateIndex > -1) {
          delete day.cssClass;
          this.selectedDays.splice(dateIndex, 1);
        } else {
          this.selectedDays.push(dayMoment.format(DATE_FORMAT));
          day.cssClass = SELECTED_DAY_CLASS;
        }
      } else if (!this.editMode && day.events.length === 0 && day.cssClass !== SELECTED_DAY_CLASS) {
        const selectedUser = this.getSelectedUser();
        if (selectedUser) {
          this.router.navigate(['/wish/new'], {
            queryParams: { date: day.date, userId: this.getSelectedUser().id },
            queryParamsHandling: 'merge'
          });
        }
      }
    }
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(calendarDay => {
      const calendarDayMoment = moment(calendarDay.date);
      const calendarDayString = calendarDayMoment.format(DATE_FORMAT);
      if (this.selectedDays.filter(aDay => aDay === calendarDayString).length > 0) {
        calendarDay.cssClass = SELECTED_DAY_CLASS;
      }
    });

    this.weekNumbers = calculateWeekNumbers(body.map(day => day.date));
  }

  enableEditButton(show: boolean) {
    this.showEditButton = show;
    this.editMode = !show;
    this.showClearButton = false;

    if (show) {
      this.getEventsForDate(this.viewDate);
    }
  }

  saveWorkerAvailability() {
    const selectedUser = this.getSelectedUser();
    if (selectedUser) {
      this.workerAvailabilityService
        .updateUnavailabilities(
          selectedUser.id,
          moment(this.viewDate)
            .startOf('month')
            .format(DATE_FORMAT),
          moment(this.viewDate)
            .endOf('month')
            .format(DATE_FORMAT),
          this.selectedDays
        )
        .subscribe(
          () => {
            this.showEditButton = true;
            this.editMode = false;
          },
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  viewDateChanged($event: Date) {
    this.getEventsForDate($event);
    this.workerAvailabilityService.currentView.viewDate = $event;
  }

  filterUsers() {
    this.users = this.allUsers.filter(user =>
      `${user.firstName ? user.firstName : ''}${user.lastName ? user.lastName : ''}${user.email ? user.email : ''}`
        .toLowerCase()
        .includes(this.userFilter)
    );
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private getEventTitle(wish: IWish): string {
    return wish.wishType;
  }

  private getEventStartTime(dayConfig: IWish): Date {
    const date = moment(dayConfig.date);
    return moment(`${date.format(DATE_FORMAT)} 00:00`).toDate();
  }

  private getEventEndTime(dayConfig: IWish): Date {
    const date = moment(dayConfig.date);
    return moment(`${date.format(DATE_FORMAT)} 01:00`).toDate();
  }
  clearWish() {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'hospitalmanagerApp.wish.delete.all.title';
    modalRef.componentInstance.content = 'hospitalmanagerApp.wish.delete.all.questionDetailed';
    modalRef.componentInstance.contentParams = {
      dateFrom: moment(this.fromDate).format('DD-MM-YYYY'),
      dateTo: moment(this.toDate).format('DD-MM-YYYY')
    };

    modalRef.result.then(result => {
      if (result === 'OK') {
        this.wishService
          .deleteForInterval({
            fromDate: moment(this.fromDate).format(DATE_FORMAT),
            toDate: moment(this.toDate).format(DATE_FORMAT)
          })
          .subscribe(
            next => {
              this.events = [];
            },
            (res: HttpErrorResponse) => this.onError(res.message)
          );
      }
    });
  }

  getWeekNumbers(): string {
    let result = '';
    this.weekNumbers.forEach(weekNumber => {
      result += (result === '' ? '' : ', ') + weekNumber;
    });
    return result !== '' ? '(' + result + ')' : '';
  }
}
