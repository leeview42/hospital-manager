import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  WorkerAvailabilityComponent,
  WorkerAvailabilityDetailComponent,
  WorkerAvailabilityUpdateComponent,
  WorkerAvailabilityDeletePopupComponent,
  WorkerAvailabilityDeleteDialogComponent,
  workerAvailabilityRoute,
  workerAvailabilityPopupRoute
} from './';
import { CalendarModule, DateAdapter } from 'angular-calendar';

const ENTITY_STATES = [...workerAvailabilityRoute, ...workerAvailabilityPopupRoute];

@NgModule({
  imports: [
    HospitalManagerSharedModule,
    RouterModule.forChild(ENTITY_STATES),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  declarations: [
    WorkerAvailabilityComponent,
    WorkerAvailabilityDetailComponent,
    WorkerAvailabilityUpdateComponent,
    WorkerAvailabilityDeleteDialogComponent,
    WorkerAvailabilityDeletePopupComponent
  ],
  entryComponents: [
    WorkerAvailabilityComponent,
    WorkerAvailabilityUpdateComponent,
    WorkerAvailabilityDeleteDialogComponent,
    WorkerAvailabilityDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerWorkerAvailabilityModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
