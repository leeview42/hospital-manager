import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShiftType } from 'app/shared/model/shift-type.model';
import { ShiftTypeService } from './shift-type.service';
import { ShiftTypeComponent } from './shift-type.component';
import { ShiftTypeDetailComponent } from './shift-type-detail.component';
import { ShiftTypeUpdateComponent } from './shift-type-update.component';
import { ShiftTypeDeletePopupComponent } from './shift-type-delete-dialog.component';
import { IShiftType } from 'app/shared/model/shift-type.model';

@Injectable({ providedIn: 'root' })
export class ShiftTypeResolve implements Resolve<IShiftType> {
  constructor(private service: ShiftTypeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShiftType> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShiftType>) => response.ok),
        map((shiftType: HttpResponse<ShiftType>) => shiftType.body)
      );
    }
    return of(new ShiftType());
  }
}

export const shiftTypeRoute: Routes = [
  {
    path: '',
    component: ShiftTypeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.shiftType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShiftTypeDetailComponent,
    resolve: {
      shiftType: ShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShiftTypeUpdateComponent,
    resolve: {
      shiftType: ShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftType.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShiftTypeUpdateComponent,
    resolve: {
      shiftType: ShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftType.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shiftTypePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShiftTypeDeletePopupComponent,
    resolve: {
      shiftType: ShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftType.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
