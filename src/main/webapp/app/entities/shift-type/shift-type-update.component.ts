import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShiftType, ShiftType } from 'app/shared/model/shift-type.model';
import { ShiftTypeService } from './shift-type.service';
import { IPriority } from 'app/shared/model/priority.model';
import { PriorityService } from 'app/entities/priority';
import { HOUR_TO_MILLIS, DURATONS } from 'app/app.constants';

@Component({
  selector: 'jhi-shift-type-update',
  templateUrl: './shift-type-update.component.html'
})
export class ShiftTypeUpdateComponent implements OnInit {
  shiftType: IShiftType;
  isSaving: boolean;

  defaultStartTime: { hour: number; minute: number };
  defaultDuration: number;
  durations = DURATONS;

  priorities: IPriority[];

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
    active: [true],
    description: [],
    canRepeat: [true],
    startHour: [null, [Validators.min(0), Validators.max(23)]],
    startMinute: [null, [Validators.min(0), Validators.max(59)]],
    durationMillis: [],
    fillWithRemaining: [false],
    noOfWorkers: [1],
    priorityId: [1],
    startTime: [],
    displayCode: [null, [Validators.maxLength(3)]],
    mandatory: [false]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shiftTypeService: ShiftTypeService,
    protected priorityService: PriorityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.defaultStartTime = { hour: 8, minute: 0 };
    this.defaultDuration = 8 * HOUR_TO_MILLIS;
  }

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shiftType }) => {
      this.updateForm(shiftType);
      this.shiftType = shiftType;
    });
    this.priorityService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPriority[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPriority[]>) => response.body)
      )
      .subscribe((res: IPriority[]) => (this.priorities = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shiftType: IShiftType) {
    const startTime = this.defaultStartTime;
    if (shiftType && shiftType.startHour) {
      startTime.hour = shiftType.startHour;
      startTime.minute = shiftType.startMinute ? shiftType.startMinute : 0;
    }

    this.editForm.patchValue({
      id: shiftType.id,
      name: shiftType.name,
      code: shiftType.code,
      active: shiftType.active === undefined ? true : shiftType.active,
      description: shiftType.description,
      canRepeat: shiftType.canRepeat === undefined ? true : shiftType.canRepeat,
      startTime,
      durationMillis: shiftType.durationMillis,
      fillWithRemaining: shiftType.fillWithRemaining,
      noOfWorkers: shiftType.noOfWorkers ? shiftType.noOfWorkers : 1,
      priorityId: shiftType.priorityId,
      displayCode: shiftType.displayCode,
      mandatory: shiftType.mandatory
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shiftType = this.createFromForm();
    if (shiftType.id !== undefined) {
      this.subscribeToSaveResponse(this.shiftTypeService.update(shiftType));
    } else {
      this.subscribeToSaveResponse(this.shiftTypeService.create(shiftType));
    }
  }

  private createFromForm(): IShiftType {
    let startTime = this.editForm.get(['startTime']).value;
    if (!startTime) {
      startTime = this.defaultStartTime;
    }

    const entity = {
      ...new ShiftType(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      code: this.editForm.get(['code']).value,
      active: this.editForm.get(['active']).value,
      description: this.editForm.get(['description']).value,
      canRepeat: this.editForm.get(['canRepeat']).value,
      startHour: startTime.hour,
      startMinute: startTime.minute,
      durationMillis: this.editForm.get(['durationMillis']).value,
      fillWithRemaining: this.editForm.get(['fillWithRemaining']).value,
      noOfWorkers: this.editForm.get(['noOfWorkers']).value,
      priorityId: this.editForm.get(['priorityId']).value,
      mandatory: this.editForm.get(['mandatory']).value,
      displayCode: this.editForm.get(['displayCode']).value ? this.editForm.get(['displayCode']).value.toUpperCase() : null
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShiftType>>) {
    result.subscribe((res: HttpResponse<IShiftType>) => this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess(shiftType) {
    this.isSaving = false;
    this.shiftTypeService.addShiftTypeToRedux(shiftType);
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPriorityById(index: number, item: IPriority) {
    return item.id;
  }
}
