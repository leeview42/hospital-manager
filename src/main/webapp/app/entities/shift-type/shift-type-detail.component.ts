import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShiftType } from 'app/shared/model/shift-type.model';

@Component({
  selector: 'jhi-shift-type-detail',
  templateUrl: './shift-type-detail.component.html'
})
export class ShiftTypeDetailComponent implements OnInit {
  shiftType: IShiftType;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftType }) => {
      this.shiftType = shiftType;
    });
  }

  previousState() {
    window.history.back();
  }
}
