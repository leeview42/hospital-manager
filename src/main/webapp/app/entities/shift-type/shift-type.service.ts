import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from 'app/redux/store';
import { ADD_SHIFT_TYPE } from 'app/redux/team/teamActions';

type EntityResponseType = HttpResponse<IShiftType>;
type EntityArrayResponseType = HttpResponse<IShiftType[]>;

@Injectable({ providedIn: 'root' })
export class ShiftTypeService {
  public updateResourceUrl = SERVER_API_URL + 'api/team-management/shift-types';

  public getResourceUrl = SERVER_API_URL + 'api/team-member/shift-types';

  constructor(protected http: HttpClient, protected ngRedux: NgRedux<IAppState>) {}

  create(shiftType: IShiftType): Observable<EntityResponseType> {
    return this.http.post<IShiftType>(this.updateResourceUrl, shiftType, { observe: 'response' });
  }

  update(shiftType: IShiftType): Observable<EntityResponseType> {
    return this.http.put<IShiftType>(this.updateResourceUrl, shiftType, { observe: 'response' });
  }

  addShiftTypeToRedux(shiftType) {
    this.ngRedux.dispatch({ type: ADD_SHIFT_TYPE, shiftType });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShiftType>(`${this.getResourceUrl}/${id}`, { observe: 'response' });
  }

  findAll(): Observable<EntityArrayResponseType> {
    return this.http.get<IShiftType[]>(`${this.getResourceUrl}/all`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShiftType[]>(this.getResourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.updateResourceUrl}/${id}`, { observe: 'response' });
  }
}
