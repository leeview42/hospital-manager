import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IShiftType } from 'app/shared/model/shift-type.model';
import { AccountService } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ShiftTypeService } from './shift-type.service';
import { select } from '@angular-redux/store';

@Component({
  selector: 'jhi-shift-type',
  templateUrl: './shift-type.component.html'
})
export class ShiftTypeComponent implements OnInit, OnDestroy {
  currentAccount: any;
  shiftTypes: IShiftType[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  mandatory: boolean;
  nameFilter = '';
  descriptionFilter = '';
  mandatoryFilter = false;
  filterResetTimestamp: number;
  @select(['team', 'id'])
  teamId: Observable<number>;
  teamIdSubscription: Subscription;

  constructor(
    protected shiftTypeService: ShiftTypeService,
    protected parseLinks: JhiParseLinks,
    protected jhiAlertService: JhiAlertService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.teamIdSubscription = this.teamId.subscribe(value => this.loadAll());
  }
  loadAll() {
    this.shiftTypeService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        ...(this.nameFilter && { 'name.contains': this.nameFilter }),
        ...(this.descriptionFilter && { 'description.contains': this.descriptionFilter }),
        'mandatory.equals': this.mandatoryFilter
      })
      .subscribe(
        (res: HttpResponse<IShiftType[]>) => this.paginateShiftTypes(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  filterChanged() {
    this.filterResetTimestamp = new Date().getTime();
    setTimeout(() => {
      const currentMillis = new Date().getTime();
      if (currentMillis - this.filterResetTimestamp >= 1000) {
        this.loadAll();
      }
    }, 1000);
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/shift-type'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/shift-type',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInShiftTypes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
    this.eventManager.destroy(this.teamIdSubscription);
  }

  trackId(index: number, item: IShiftType) {
    return item.id;
  }

  registerChangeInShiftTypes() {
    this.eventSubscriber = this.eventManager.subscribe('shiftTypeListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateShiftTypes(data: IShiftType[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.shiftTypes = data;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
