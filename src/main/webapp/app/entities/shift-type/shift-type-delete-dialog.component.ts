import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShiftType } from 'app/shared/model/shift-type.model';
import { ShiftTypeService } from './shift-type.service';

@Component({
  selector: 'jhi-shift-type-delete-dialog',
  templateUrl: './shift-type-delete-dialog.component.html'
})
export class ShiftTypeDeleteDialogComponent {
  shiftType: IShiftType;

  constructor(protected shiftTypeService: ShiftTypeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shiftTypeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shiftTypeListModification',
        content: 'Deleted an shiftType'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shift-type-delete-popup',
  template: ''
})
export class ShiftTypeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftType }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShiftTypeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shiftType = shiftType;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shift-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shift-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
