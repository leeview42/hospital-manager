import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  ShiftTypeComponent,
  ShiftTypeDetailComponent,
  ShiftTypeUpdateComponent,
  ShiftTypeDeletePopupComponent,
  ShiftTypeDeleteDialogComponent,
  shiftTypeRoute,
  shiftTypePopupRoute
} from './';

const ENTITY_STATES = [...shiftTypeRoute, ...shiftTypePopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShiftTypeComponent,
    ShiftTypeDetailComponent,
    ShiftTypeUpdateComponent,
    ShiftTypeDeleteDialogComponent,
    ShiftTypeDeletePopupComponent
  ],
  entryComponents: [ShiftTypeComponent, ShiftTypeUpdateComponent, ShiftTypeDeleteDialogComponent, ShiftTypeDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerShiftTypeModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
