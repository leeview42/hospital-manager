import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import { TeamComponent, TeamDetailComponent, TeamUpdateComponent, teamRoute, TeamSelectionComponent } from './';

const ENTITY_STATES = [...teamRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [TeamComponent, TeamDetailComponent, TeamUpdateComponent, TeamSelectionComponent],
  entryComponents: [TeamComponent, TeamUpdateComponent, TeamSelectionComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  exports: [TeamSelectionComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalManagerTeamModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
