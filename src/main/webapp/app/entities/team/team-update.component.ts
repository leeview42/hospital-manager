import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { ITeam, Team } from 'app/shared/model/team.model';
import { TeamService } from './team.service';
import { AccountService, IUser, UserService } from 'app/core';
import { select } from '@angular-redux/store';
import { compareAlphabetical, DEFAULT_MULTI_SELECT_SETTINGS, formatUserName, MAX_ITEMS_PER_PAGE } from 'app/shared';
import { filter, map } from 'rxjs/operators';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { MultiSelectOptionModel } from 'app/shared/model/multi-select-option.model';

@Component({
  selector: 'jhi-team-update',
  templateUrl: './team-update.component.html'
})
export class TeamUpdateComponent implements OnInit, OnDestroy {
  team: ITeam;
  isSaving: boolean;
  allUsers: IUser[];
  teamMembers: IMultiSelectOption[];
  teamManagers: IMultiSelectOption[];
  @select(['user', 'organizationId'])
  organizationId$: Observable<number>;

  organizationSubscription: Subscription;

  private _organizationId: any;
  refresh: Subject<any> = new Subject();

  multiSelectSettings = DEFAULT_MULTI_SELECT_SETTINGS;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: [],
    managers: [[], [Validators.required]],
    members: [[], [Validators.required]],
    maximumWorkingWeekends: [5, [Validators.required, Validators.min(0), Validators.max(5)]],
    minimumFreeDaysAfterNightShifts: [0, [Validators.required, Validators.min(0)]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected teamService: TeamService,
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService,
    protected userService: UserService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ team }) => {
      this.updateForm(team);
      this.team = team;
    });
    this.allUsers = [];
    this.teamMembers = [];
    this.teamManagers = [];
    this.organizationSubscription = this.organizationId$.subscribe(value => {
      this._organizationId = value;
    });
    this.userService
      .query({
        size: MAX_ITEMS_PER_PAGE
      })
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => this.convertUsersToSelectArr(res), (res: HttpErrorResponse) => this.onError(res.message));
    this.refresh.subscribe(next => {
      if (next === 'users') {
        if (this.team && this.team.managers) {
          this.team.managers.forEach(this.removeFromTeamMembers);
        }
        if (this.team && this.team.members) {
          this.team.managers.forEach(this.removeFromTeamManagers);
        }
      }
    });
  }

  convertUsersToSelectArr(res: IUser[]) {
    this.allUsers = res.map(resUser => {
      resUser.name = formatUserName(resUser);
      return resUser;
    });
    this.teamManagers = this.convertUsersToMultiSelectOptions(this.allUsers).sort((a, b) => compareAlphabetical(a.name, b.name));
    this.teamMembers = this.convertUsersToMultiSelectOptions(this.allUsers).sort((a, b) => compareAlphabetical(a.name, b.name));
    this.refresh.next('users');
  }

  convertUsersToMultiSelectOptions(users: IUser[]): IMultiSelectOption[] {
    const options = [];
    if (users != null && users.length > 0) {
      users.forEach(user => {
        options.push(this.convertUserToSelectOption(user));
      });
    }
    return options;
  }

  convertUserToSelectOption(user: IUser): IMultiSelectOption {
    if (user) {
      return new MultiSelectOptionModel(user.id, user.name);
    }
    return null;
  }

  updateForm(team: ITeam) {
    this.editForm.patchValue({
      id: team.id,
      name: team.name,
      description: team.description,
      managers: team.managers,
      members: team.members,
      maximumWorkingWeekends: team.maximumWorkingWeekends,
      minimumFreeDaysAfterNightShifts: team.minimumFreeDaysAfterNightShifts
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.accountService.identity().then(account => {
      this.isSaving = true;
      let team = { ...this.createFromForm() };
      if (team.id !== undefined) {
        team = { ...this.createFromForm(), organizationId: this.team.organizationId };
        this.subscribeToSaveResponse(this.teamService.update(team));
      } else {
        team = { ...this.createFromForm(), organizationId: this._organizationId };
        this.subscribeToSaveResponse(this.teamService.create(team));
      }
    });
  }

  private createFromForm(): ITeam {
    const entity = {
      ...new Team(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value,
      managers: this.editForm.get(['managers']).value,
      members: this.editForm.get(['members']).value,
      maximumWorkingWeekends: this.editForm.get(['maximumWorkingWeekends']).value,
      minimumFreeDaysAfterNightShifts: this.editForm.get(['minimumFreeDaysAfterNightShifts']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITeam>>) {
    result.subscribe((res: HttpResponse<ITeam>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  removeFromTeamMembers(id: any) {
    if (this.teamMembers) {
      const teamMembersOptions = [...this.teamMembers];
      teamMembersOptions.splice(teamMembersOptions.findIndex(option => option.id === id), 1);
      this.teamMembers = teamMembersOptions.sort((a, b) => compareAlphabetical(a.name, b.name));
    }
  }

  addToTeamMembers(id: any) {
    if (this.teamMembers) {
      const teamMembersOptions = [...this.teamMembers];
      teamMembersOptions.push(this.convertUserToSelectOption(this.allUsers.find(user => user.id === id)));
      this.teamMembers = teamMembersOptions.sort((a, b) => compareAlphabetical(a.name, b.name));
    }
  }

  removeFromTeamManagers(id: any) {
    if (this.teamManagers) {
      const teamManagersOptions = [...this.teamManagers];
      teamManagersOptions.splice(teamManagersOptions.findIndex(option => option.id === id), 1);
      this.teamManagers = teamManagersOptions.sort((a, b) => compareAlphabetical(a.name, b.name));
    }
  }

  addToTeamManagers(id: any) {
    if (this.teamManagers) {
      const teamManagersOptions = [...this.teamManagers];
      teamManagersOptions.push(this.convertUserToSelectOption(this.allUsers.find(user => user.id === id)));
      this.teamManagers = teamManagersOptions.sort((a, b) => compareAlphabetical(a.name, b.name));
    }
  }

  ngOnDestroy(): void {
    this.organizationSubscription.unsubscribe();
  }
}
