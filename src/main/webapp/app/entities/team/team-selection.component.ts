import { ITeam } from '../../shared/model/team.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { TeamService } from 'app/entities/team/index';
import { NgRedux, select } from '@angular-redux/store';
import { Observable, Subject, Subscription } from 'rxjs';
import { compareAlphabetical, MAX_ITEMS_PER_PAGE } from 'app/shared';
import { IAppState } from 'app/redux/store';
import { TEAM_SELECTED } from 'app/redux/team/teamActions';
import { Router, ActivationStart } from '@angular/router';

@Component({
  selector: 'team-selection',
  templateUrl: './team-selection.component.html',
  styleUrls: ['./team-selection.component.scss']
})
export class TeamSelectionComponent implements OnInit, OnDestroy {
  disableTeamSelection = false;
  teamList: ITeam[];
  selectedTeamList: number[];
  selectedTeamId: number;

  @select(['user', 'id'])
  currentUserId$: Observable<number>;

  isManager = false;

  currentUserId: number;

  refresh: Subject<any> = new Subject();

  refreshSubscription: Subscription;

  currentUserIdSubscription: Subscription;
  routerEventsSubscription: Subscription;

  constructor(
    private jhiAlertService: JhiAlertService,
    private teamService: TeamService,
    private ngRedux: NgRedux<IAppState>,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {}

  ngOnInit() {
    this.createRoutesListener();
    this.currentUserIdSubscription = this.currentUserId$.subscribe(value => {
      this.currentUserId = value;
      this.teamService
        .query({
          size: MAX_ITEMS_PER_PAGE,
          'userId.equals': this.currentUserId
        })
        .subscribe(
          (res: HttpResponse<ITeam[]>) => {
            this.teamsLoaded(res.body);
            this.refresh.next();
          },
          (res: HttpErrorResponse) => this.onError(res.message)
        );
      this.refresh.next();
    });
    this.refreshSubscription = this.refresh.subscribe(value => {
      if (this.currentUserId && this.selectedTeamId) {
        this.changedSelection();
      }
    });
  }

  private createRoutesListener() {
    this.routerEventsSubscription = this.router.events.subscribe(event => {
      if (event instanceof ActivationStart) {
        const disable = event.snapshot.data['disableTeamSelection'];
        this.disableTeamSelection = disable !== undefined && disable;
      }
    });
  }

  teamsLoaded(teamList: ITeam[]) {
    if (teamList && teamList.length > 0) {
      this.selectedTeamList = [];
      this.teamList = teamList.sort((a, b) => compareAlphabetical(a.name, b.name));
      this.teamList.forEach(this.formatAndMapTeam);
      this.selectedTeamList.push(this.teamList[0].id);
      this.selectedTeamId = this.teamList[0].id;
    }
  }

  formatAndMapTeam(team: ITeam) {
    team.users = team.users.sort((a, b) => compareAlphabetical(a.name, b.name));
    const shiftTypeIds = team.shiftTypes.map(shiftType => shiftType.id);
    team.users.forEach(user => {
      const userShiftTypesCopy = [...user.shiftTypes];
      userShiftTypesCopy
        .filter(shiftTypeId => shiftTypeIds.indexOf(shiftTypeId) < 0)
        .forEach(shiftTypeId => user.shiftTypes.splice(user.shiftTypes.indexOf(shiftTypeId), 1));
    });
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  changedSelection() {
    const selectedTeam = this.teamList.find(team => team.id === this.selectedTeamId);
    const isManager = selectedTeam.managers.indexOf(this.currentUserId) >= 0;
    const users = selectedTeam.users;
    this.ngRedux.dispatch({ type: TEAM_SELECTED, isManager, id: selectedTeam.id, users, shiftTypes: selectedTeam.shiftTypes });
    this.isManager = isManager;
  }

  currentUserIsManagerInTeam(team: ITeam): boolean {
    return team.managers && team.managers.findIndex(manager => manager === this.currentUserId) !== -1;
  }

  ngOnDestroy(): void {
    this.eventManager.destroy(this.currentUserIdSubscription);
    this.eventManager.destroy(this.refreshSubscription);
    this.eventManager.destroy(this.routerEventsSubscription);
  }
}
