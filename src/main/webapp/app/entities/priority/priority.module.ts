import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  PriorityComponent,
  PriorityDetailComponent,
  PriorityUpdateComponent,
  PriorityDeletePopupComponent,
  PriorityDeleteDialogComponent,
  priorityRoute,
  priorityPopupRoute
} from './';

const ENTITY_STATES = [...priorityRoute, ...priorityPopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PriorityComponent,
    PriorityDetailComponent,
    PriorityUpdateComponent,
    PriorityDeleteDialogComponent,
    PriorityDeletePopupComponent
  ],
  entryComponents: [PriorityComponent, PriorityUpdateComponent, PriorityDeleteDialogComponent, PriorityDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerPriorityModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
