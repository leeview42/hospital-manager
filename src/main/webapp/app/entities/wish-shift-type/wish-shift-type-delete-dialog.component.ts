import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWishShiftType } from 'app/shared/model/wish-shift-type.model';
import { WishShiftTypeService } from './wish-shift-type.service';

@Component({
  selector: 'jhi-wish-shift-type-delete-dialog',
  templateUrl: './wish-shift-type-delete-dialog.component.html'
})
export class WishShiftTypeDeleteDialogComponent {
  wishShiftType: IWishShiftType;

  constructor(
    protected wishShiftTypeService: WishShiftTypeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.wishShiftTypeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'wishShiftTypeListModification',
        content: 'Deleted an wishShiftType'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-wish-shift-type-delete-popup',
  template: ''
})
export class WishShiftTypeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ wishShiftType }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(WishShiftTypeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.wishShiftType = wishShiftType;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/wish-shift-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/wish-shift-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
