import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { WishShiftType } from 'app/shared/model/wish-shift-type.model';
import { WishShiftTypeService } from './wish-shift-type.service';
import { WishShiftTypeComponent } from './wish-shift-type.component';
import { WishShiftTypeDetailComponent } from './wish-shift-type-detail.component';
import { WishShiftTypeUpdateComponent } from './wish-shift-type-update.component';
import { WishShiftTypeDeletePopupComponent } from './wish-shift-type-delete-dialog.component';
import { IWishShiftType } from 'app/shared/model/wish-shift-type.model';

@Injectable({ providedIn: 'root' })
export class WishShiftTypeResolve implements Resolve<IWishShiftType> {
  constructor(private service: WishShiftTypeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWishShiftType> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<WishShiftType>) => response.ok),
        map((wishShiftType: HttpResponse<WishShiftType>) => wishShiftType.body)
      );
    }
    return of(new WishShiftType());
  }
}

export const wishShiftTypeRoute: Routes = [
  {
    path: '',
    component: WishShiftTypeComponent,
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wishShiftType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WishShiftTypeDetailComponent,
    resolve: {
      wishShiftType: WishShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wishShiftType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WishShiftTypeUpdateComponent,
    resolve: {
      wishShiftType: WishShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wishShiftType.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WishShiftTypeUpdateComponent,
    resolve: {
      wishShiftType: WishShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wishShiftType.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const wishShiftTypePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: WishShiftTypeDeletePopupComponent,
    resolve: {
      wishShiftType: WishShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wishShiftType.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
