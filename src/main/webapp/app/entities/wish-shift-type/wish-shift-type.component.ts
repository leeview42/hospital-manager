import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IWishShiftType } from 'app/shared/model/wish-shift-type.model';
import { AccountService } from 'app/core';
import { WishShiftTypeService } from './wish-shift-type.service';

@Component({
  selector: 'jhi-wish-shift-type',
  templateUrl: './wish-shift-type.component.html'
})
export class WishShiftTypeComponent implements OnInit, OnDestroy {
  wishShiftTypes: IWishShiftType[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected wishShiftTypeService: WishShiftTypeService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.wishShiftTypeService
      .query()
      .pipe(
        filter((res: HttpResponse<IWishShiftType[]>) => res.ok),
        map((res: HttpResponse<IWishShiftType[]>) => res.body)
      )
      .subscribe(
        (res: IWishShiftType[]) => {
          this.wishShiftTypes = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInWishShiftTypes();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IWishShiftType) {
    return item.id;
  }

  registerChangeInWishShiftTypes() {
    this.eventSubscriber = this.eventManager.subscribe('wishShiftTypeListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
