import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  WishShiftTypeComponent,
  WishShiftTypeDetailComponent,
  WishShiftTypeUpdateComponent,
  WishShiftTypeDeletePopupComponent,
  WishShiftTypeDeleteDialogComponent,
  wishShiftTypeRoute,
  wishShiftTypePopupRoute
} from './';

const ENTITY_STATES = [...wishShiftTypeRoute, ...wishShiftTypePopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    WishShiftTypeComponent,
    WishShiftTypeDetailComponent,
    WishShiftTypeUpdateComponent,
    WishShiftTypeDeleteDialogComponent,
    WishShiftTypeDeletePopupComponent
  ],
  entryComponents: [
    WishShiftTypeComponent,
    WishShiftTypeUpdateComponent,
    WishShiftTypeDeleteDialogComponent,
    WishShiftTypeDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerWishShiftTypeModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
