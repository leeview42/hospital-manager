import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWishShiftType } from 'app/shared/model/wish-shift-type.model';

@Component({
  selector: 'jhi-wish-shift-type-detail',
  templateUrl: './wish-shift-type-detail.component.html'
})
export class WishShiftTypeDetailComponent implements OnInit {
  wishShiftType: IWishShiftType;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ wishShiftType }) => {
      this.wishShiftType = wishShiftType;
    });
  }

  previousState() {
    window.history.back();
  }
}
