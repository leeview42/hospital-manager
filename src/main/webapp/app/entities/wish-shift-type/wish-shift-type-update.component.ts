import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IWishShiftType, WishShiftType } from 'app/shared/model/wish-shift-type.model';
import { WishShiftTypeService } from './wish-shift-type.service';
import { IWish } from 'app/shared/model/wish.model';
import { WishService } from 'app/entities/wish';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { ShiftTypeService } from 'app/entities/shift-type';

@Component({
  selector: 'jhi-wish-shift-type-update',
  templateUrl: './wish-shift-type-update.component.html'
})
export class WishShiftTypeUpdateComponent implements OnInit {
  wishShiftType: IWishShiftType;
  isSaving: boolean;

  wishes: IWish[];

  shifttypes: IShiftType[];

  editForm = this.fb.group({
    id: [],
    wishId: [],
    shiftTypeId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected wishShiftTypeService: WishShiftTypeService,
    protected wishService: WishService,
    protected shiftTypeService: ShiftTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ wishShiftType }) => {
      this.updateForm(wishShiftType);
      this.wishShiftType = wishShiftType;
    });
    this.wishService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IWish[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWish[]>) => response.body)
      )
      .subscribe((res: IWish[]) => (this.wishes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.shiftTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShiftType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShiftType[]>) => response.body)
      )
      .subscribe((res: IShiftType[]) => (this.shifttypes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(wishShiftType: IWishShiftType) {
    this.editForm.patchValue({
      id: wishShiftType.id,
      wishId: wishShiftType.wishId,
      shiftTypeId: wishShiftType.shiftTypeId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const wishShiftType = this.createFromForm();
    if (wishShiftType.id !== undefined) {
      this.subscribeToSaveResponse(this.wishShiftTypeService.update(wishShiftType));
    } else {
      this.subscribeToSaveResponse(this.wishShiftTypeService.create(wishShiftType));
    }
  }

  private createFromForm(): IWishShiftType {
    const entity = {
      ...new WishShiftType(),
      id: this.editForm.get(['id']).value,
      wishId: this.editForm.get(['wishId']).value,
      shiftTypeId: this.editForm.get(['shiftTypeId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWishShiftType>>) {
    result.subscribe((res: HttpResponse<IWishShiftType>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackWishById(index: number, item: IWish) {
    return item.id;
  }

  trackShiftTypeById(index: number, item: IShiftType) {
    return item.id;
  }
}
