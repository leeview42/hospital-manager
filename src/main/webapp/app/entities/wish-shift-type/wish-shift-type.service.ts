import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IWishShiftType } from 'app/shared/model/wish-shift-type.model';

type EntityResponseType = HttpResponse<IWishShiftType>;
type EntityArrayResponseType = HttpResponse<IWishShiftType[]>;

@Injectable({ providedIn: 'root' })
export class WishShiftTypeService {
  public resourceUrl = SERVER_API_URL + 'api/wish-shift-types';

  constructor(protected http: HttpClient) {}

  create(wishShiftType: IWishShiftType): Observable<EntityResponseType> {
    return this.http.post<IWishShiftType>(this.resourceUrl, wishShiftType, { observe: 'response' });
  }

  update(wishShiftType: IWishShiftType): Observable<EntityResponseType> {
    return this.http.put<IWishShiftType>(this.resourceUrl, wishShiftType, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWishShiftType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWishShiftType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
