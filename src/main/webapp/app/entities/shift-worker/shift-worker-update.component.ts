import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShiftWorker, ShiftWorker } from 'app/shared/model/shift-worker.model';
import { ShiftWorkerService } from './shift-worker.service';
import { IShift } from 'app/shared/model/shift.model';
import { ShiftService } from 'app/entities/shift';
import { IUser, UserService } from 'app/core';
import { MAX_ITEMS_PER_PAGE } from 'app/shared';

@Component({
  selector: 'jhi-shift-worker-update',
  templateUrl: './shift-worker-update.component.html'
})
export class ShiftWorkerUpdateComponent implements OnInit {
  isSaving: boolean;

  shifts: IShift[];

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    shiftId: [],
    workerId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shiftWorkerService: ShiftWorkerService,
    protected shiftService: ShiftService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shiftWorker }) => {
      this.updateForm(shiftWorker);
    });
    this.shiftService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShift[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShift[]>) => response.body)
      )
      .subscribe((res: IShift[]) => (this.shifts = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query({
        size: MAX_ITEMS_PER_PAGE
      })
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shiftWorker: IShiftWorker) {
    this.editForm.patchValue({
      id: shiftWorker.id,
      shiftId: shiftWorker.shiftId,
      workerId: shiftWorker.workerId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shiftWorker = this.createFromForm();
    if (shiftWorker.id !== undefined) {
      this.subscribeToSaveResponse(this.shiftWorkerService.update(shiftWorker));
    } else {
      this.subscribeToSaveResponse(this.shiftWorkerService.create(shiftWorker));
    }
  }

  private createFromForm(): IShiftWorker {
    return {
      ...new ShiftWorker(),
      id: this.editForm.get(['id']).value,
      shiftId: this.editForm.get(['shiftId']).value,
      workerId: this.editForm.get(['workerId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShiftWorker>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShiftById(index: number, item: IShift) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
