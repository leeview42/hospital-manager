import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShiftWorker } from 'app/shared/model/shift-worker.model';

@Component({
  selector: 'jhi-shift-worker-detail',
  templateUrl: './shift-worker-detail.component.html'
})
export class ShiftWorkerDetailComponent implements OnInit {
  shiftWorker: IShiftWorker;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftWorker }) => {
      this.shiftWorker = shiftWorker;
    });
  }

  previousState() {
    window.history.back();
  }
}
