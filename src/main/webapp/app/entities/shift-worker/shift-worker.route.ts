import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShiftWorker } from 'app/shared/model/shift-worker.model';
import { ShiftWorkerService } from './shift-worker.service';
import { ShiftWorkerComponent } from './shift-worker.component';
import { ShiftWorkerDetailComponent } from './shift-worker-detail.component';
import { ShiftWorkerUpdateComponent } from './shift-worker-update.component';
import { ShiftWorkerDeletePopupComponent } from './shift-worker-delete-dialog.component';
import { IShiftWorker } from 'app/shared/model/shift-worker.model';

@Injectable({ providedIn: 'root' })
export class ShiftWorkerResolve implements Resolve<IShiftWorker> {
  constructor(private service: ShiftWorkerService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShiftWorker> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShiftWorker>) => response.ok),
        map((shiftWorker: HttpResponse<ShiftWorker>) => shiftWorker.body)
      );
    }
    return of(new ShiftWorker());
  }
}

export const shiftWorkerRoute: Routes = [
  {
    path: '',
    component: ShiftWorkerComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.shiftWorker.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShiftWorkerDetailComponent,
    resolve: {
      shiftWorker: ShiftWorkerResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftWorker.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShiftWorkerUpdateComponent,
    resolve: {
      shiftWorker: ShiftWorkerResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftWorker.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShiftWorkerUpdateComponent,
    resolve: {
      shiftWorker: ShiftWorkerResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftWorker.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shiftWorkerPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShiftWorkerDeletePopupComponent,
    resolve: {
      shiftWorker: ShiftWorkerResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftWorker.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
