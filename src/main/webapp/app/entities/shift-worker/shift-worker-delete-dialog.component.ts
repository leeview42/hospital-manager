import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShiftWorker } from 'app/shared/model/shift-worker.model';
import { ShiftWorkerService } from './shift-worker.service';

@Component({
  selector: 'jhi-shift-worker-delete-dialog',
  templateUrl: './shift-worker-delete-dialog.component.html'
})
export class ShiftWorkerDeleteDialogComponent {
  shiftWorker: IShiftWorker;

  constructor(
    protected shiftWorkerService: ShiftWorkerService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shiftWorkerService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shiftWorkerListModification',
        content: 'Deleted an shiftWorker'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shift-worker-delete-popup',
  template: ''
})
export class ShiftWorkerDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftWorker }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShiftWorkerDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shiftWorker = shiftWorker;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shift-worker', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shift-worker', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
