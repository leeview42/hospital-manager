import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShiftWorker } from 'app/shared/model/shift-worker.model';

type EntityResponseType = HttpResponse<IShiftWorker>;
type EntityArrayResponseType = HttpResponse<IShiftWorker[]>;

@Injectable({ providedIn: 'root' })
export class ShiftWorkerService {
  public resourceUrl = SERVER_API_URL + 'api/team-management/shift-workers';

  constructor(protected http: HttpClient) {}

  create(shiftWorker: IShiftWorker): Observable<EntityResponseType> {
    return this.http.post<IShiftWorker>(this.resourceUrl, shiftWorker, { observe: 'response' });
  }

  update(shiftWorker: IShiftWorker): Observable<EntityResponseType> {
    return this.http.put<IShiftWorker>(this.resourceUrl, shiftWorker, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShiftWorker>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShiftWorker[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  generateShifts(req?: any): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${this.resourceUrl}/generate`, req, { observe: 'response' });
  }
}
