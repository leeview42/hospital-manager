import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  ShiftWorkerComponent,
  ShiftWorkerDetailComponent,
  ShiftWorkerUpdateComponent,
  ShiftWorkerDeletePopupComponent,
  ShiftWorkerDeleteDialogComponent,
  shiftWorkerRoute,
  shiftWorkerPopupRoute
} from './';

const ENTITY_STATES = [...shiftWorkerRoute, ...shiftWorkerPopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShiftWorkerComponent,
    ShiftWorkerDetailComponent,
    ShiftWorkerUpdateComponent,
    ShiftWorkerDeleteDialogComponent,
    ShiftWorkerDeletePopupComponent
  ],
  entryComponents: [ShiftWorkerComponent, ShiftWorkerUpdateComponent, ShiftWorkerDeleteDialogComponent, ShiftWorkerDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerShiftWorkerModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
