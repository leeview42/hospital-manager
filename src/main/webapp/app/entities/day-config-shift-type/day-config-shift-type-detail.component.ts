import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';

@Component({
  selector: 'jhi-day-config-shift-type-detail',
  templateUrl: './day-config-shift-type-detail.component.html'
})
export class DayConfigShiftTypeDetailComponent implements OnInit {
  dayConfigShiftType: IDayConfigShiftType;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ dayConfigShiftType }) => {
      this.dayConfigShiftType = dayConfigShiftType;
    });
  }

  previousState() {
    window.history.back();
  }
}
