import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';
import { DayConfigShiftTypeService } from './day-config-shift-type.service';
import { DayConfigShiftTypeComponent } from './day-config-shift-type.component';
import { DayConfigShiftTypeDetailComponent } from './day-config-shift-type-detail.component';
import { DayConfigShiftTypeUpdateComponent } from './day-config-shift-type-update.component';
import { DayConfigShiftTypeDeletePopupComponent } from './day-config-shift-type-delete-dialog.component';
import { IDayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';

@Injectable({ providedIn: 'root' })
export class DayConfigShiftTypeResolve implements Resolve<IDayConfigShiftType> {
  constructor(private service: DayConfigShiftTypeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDayConfigShiftType> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<DayConfigShiftType>) => response.ok),
        map((dayConfigShiftType: HttpResponse<DayConfigShiftType>) => dayConfigShiftType.body)
      );
    }
    return of(new DayConfigShiftType());
  }
}

export const dayConfigShiftTypeRoute: Routes = [
  {
    path: '',
    component: DayConfigShiftTypeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.dayConfigShiftType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DayConfigShiftTypeDetailComponent,
    resolve: {
      dayConfigShiftType: DayConfigShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfigShiftType.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DayConfigShiftTypeUpdateComponent,
    resolve: {
      dayConfigShiftType: DayConfigShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfigShiftType.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DayConfigShiftTypeUpdateComponent,
    resolve: {
      dayConfigShiftType: DayConfigShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfigShiftType.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const dayConfigShiftTypePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DayConfigShiftTypeDeletePopupComponent,
    resolve: {
      dayConfigShiftType: DayConfigShiftTypeResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.dayConfigShiftType.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
