import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';

type EntityResponseType = HttpResponse<IDayConfigShiftType>;
type EntityArrayResponseType = HttpResponse<IDayConfigShiftType[]>;

@Injectable({ providedIn: 'root' })
export class DayConfigShiftTypeService {
  public resourceUrl = SERVER_API_URL + 'api/day-config-shift-types';

  constructor(protected http: HttpClient) {}

  create(dayConfigShiftType: IDayConfigShiftType): Observable<EntityResponseType> {
    return this.http.post<IDayConfigShiftType>(this.resourceUrl, dayConfigShiftType, { observe: 'response' });
  }

  update(dayConfigShiftType: IDayConfigShiftType): Observable<EntityResponseType> {
    return this.http.put<IDayConfigShiftType>(this.resourceUrl, dayConfigShiftType, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDayConfigShiftType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDayConfigShiftType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
