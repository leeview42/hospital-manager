import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IDayConfigShiftType, DayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';
import { DayConfigShiftTypeService } from './day-config-shift-type.service';
import { IDayConfig } from 'app/shared/model/day-config.model';
import { DayConfigService } from 'app/entities/day-config';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { ShiftTypeService } from 'app/entities/shift-type';

@Component({
  selector: 'jhi-day-config-shift-type-update',
  templateUrl: './day-config-shift-type-update.component.html'
})
export class DayConfigShiftTypeUpdateComponent implements OnInit {
  dayConfigShiftType: IDayConfigShiftType;
  isSaving: boolean;

  dayconfigs: IDayConfig[];

  shifttypes: IShiftType[];

  editForm = this.fb.group({
    id: [],
    noOfWorkers: [],
    dayConfigId: [],
    shiftTypeId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected dayConfigShiftTypeService: DayConfigShiftTypeService,
    protected dayConfigService: DayConfigService,
    protected shiftTypeService: ShiftTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ dayConfigShiftType }) => {
      this.updateForm(dayConfigShiftType);
      this.dayConfigShiftType = dayConfigShiftType;
    });
    this.dayConfigService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IDayConfig[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDayConfig[]>) => response.body)
      )
      .subscribe((res: IDayConfig[]) => (this.dayconfigs = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.shiftTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShiftType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShiftType[]>) => response.body)
      )
      .subscribe((res: IShiftType[]) => (this.shifttypes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(dayConfigShiftType: IDayConfigShiftType) {
    this.editForm.patchValue({
      id: dayConfigShiftType.id,
      noOfWorkers: dayConfigShiftType.noOfWorkers,
      dayConfigId: dayConfigShiftType.dayConfigId,
      shiftTypeId: dayConfigShiftType.shiftTypeId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const dayConfigShiftType = this.createFromForm();
    if (dayConfigShiftType.id !== undefined) {
      this.subscribeToSaveResponse(this.dayConfigShiftTypeService.update(dayConfigShiftType));
    } else {
      this.subscribeToSaveResponse(this.dayConfigShiftTypeService.create(dayConfigShiftType));
    }
  }

  private createFromForm(): IDayConfigShiftType {
    const entity = {
      ...new DayConfigShiftType(),
      id: this.editForm.get(['id']).value,
      noOfWorkers: this.editForm.get(['noOfWorkers']).value,
      dayConfigId: this.editForm.get(['dayConfigId']).value,
      shiftTypeId: this.editForm.get(['shiftTypeId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDayConfigShiftType>>) {
    result.subscribe((res: HttpResponse<IDayConfigShiftType>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackDayConfigById(index: number, item: IDayConfig) {
    return item.id;
  }

  trackShiftTypeById(index: number, item: IShiftType) {
    return item.id;
  }
}
