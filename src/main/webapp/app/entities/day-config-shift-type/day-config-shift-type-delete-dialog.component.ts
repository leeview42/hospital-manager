import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';
import { DayConfigShiftTypeService } from './day-config-shift-type.service';

@Component({
  selector: 'jhi-day-config-shift-type-delete-dialog',
  templateUrl: './day-config-shift-type-delete-dialog.component.html'
})
export class DayConfigShiftTypeDeleteDialogComponent {
  dayConfigShiftType: IDayConfigShiftType;

  constructor(
    protected dayConfigShiftTypeService: DayConfigShiftTypeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.dayConfigShiftTypeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'dayConfigShiftTypeListModification',
        content: 'Deleted an dayConfigShiftType'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-day-config-shift-type-delete-popup',
  template: ''
})
export class DayConfigShiftTypeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ dayConfigShiftType }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DayConfigShiftTypeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.dayConfigShiftType = dayConfigShiftType;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/day-config-shift-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/day-config-shift-type', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
