import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  DayConfigShiftTypeComponent,
  DayConfigShiftTypeDetailComponent,
  DayConfigShiftTypeUpdateComponent,
  DayConfigShiftTypeDeletePopupComponent,
  DayConfigShiftTypeDeleteDialogComponent,
  dayConfigShiftTypeRoute,
  dayConfigShiftTypePopupRoute
} from './';

const ENTITY_STATES = [...dayConfigShiftTypeRoute, ...dayConfigShiftTypePopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DayConfigShiftTypeComponent,
    DayConfigShiftTypeDetailComponent,
    DayConfigShiftTypeUpdateComponent,
    DayConfigShiftTypeDeleteDialogComponent,
    DayConfigShiftTypeDeletePopupComponent
  ],
  entryComponents: [
    DayConfigShiftTypeComponent,
    DayConfigShiftTypeUpdateComponent,
    DayConfigShiftTypeDeleteDialogComponent,
    DayConfigShiftTypeDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerDayConfigShiftTypeModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
