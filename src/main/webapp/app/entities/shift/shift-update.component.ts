import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IShift, Shift } from 'app/shared/model/shift.model';
import { ShiftService } from './shift.service';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { IUser, UserService } from 'app/core';
import { formatUserName } from 'app/shared';
import { select } from '@angular-redux/store';

@Component({
  selector: 'jhi-shift-update',
  templateUrl: './shift-update.component.html'
})
export class ShiftUpdateComponent implements OnInit, OnDestroy {
  shift: IShift;
  isCreate = false;
  isSaving: boolean;
  @select(['team', 'users'])
  users$: Observable<IUser[]>;
  @select(['team', 'isManager'])
  isManager$: Observable<boolean>;
  @select(['team', 'shiftTypes'])
  shiftTypes$: Observable<IShiftType[]>;
  subscriptions: Subscription[] = [];
  users: IUser[];
  isManager = false;

  editForm = this.fb.group({
    id: [],
    date: [null, [Validators.required]],
    updateDate: [],
    createDate: [],
    generated: [],
    shiftTypeId: [null, [Validators.required]],
    createdById: [],
    updatedById: [],
    users: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shiftService: ShiftService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.editForm.disable();
    this.activatedRoute.data.subscribe(({ shift }) => {
      this.updateForm(shift);
    });
    const usersSubscription = this.users$.subscribe(
      (res: IUser[]) => {
        this.convertUsersToSelectArr(res);
        const teamUserIds = res.map(user => user.id);
        const users = this.editForm.get(['users']).value.filter(userId => teamUserIds.indexOf(userId) >= 0);
        this.editForm.patchValue({ users });
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
    const isManagerSubscription = this.isManager$.subscribe(next => {
      this.isManager = next;
      if (this.isManager === true) {
        this.editForm.enable();
      } else {
        this.editForm.disable();
      }
    });
    this.subscriptions.push(usersSubscription);
    this.subscriptions.push(isManagerSubscription);
  }

  convertUsersToSelectArr(res: IUser[]) {
    this.users = res.map(resUser => {
      resUser.name = formatUserName(resUser);
      return resUser;
    });
  }

  updateForm(shift: IShift) {
    this.shift = shift;
    this.isCreate = this.shift.id === null || this.shift.id === undefined;
    this.editForm.patchValue({
      id: shift.id,
      date: shift.date,
      updateDate: shift.updateDate != null ? shift.updateDate.format(DATE_TIME_FORMAT) : null,
      createDate: shift.createDate != null ? shift.createDate.format(DATE_TIME_FORMAT) : null,
      generated: shift.generated,
      shiftTypeId: shift.shiftTypeId,
      createdById: shift.createdById,
      updatedById: shift.updatedById,
      users: shift.users === null || shift.users === undefined ? [] : shift.users.map(user => user.id)
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shift = this.createFromForm();
    if (this.isCreate) {
      this.subscribeToSaveResponse(this.shiftService.create(shift));
    } else {
      this.subscribeToSaveResponse(this.shiftService.update(shift));
    }
  }

  trackShiftTypeById(index: number, item: IShiftType) {
    return item.id;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
    this.subscriptions = [];
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShift>>) {
    result.subscribe((res: HttpResponse<IShift>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IShift {
    const entity = {
      ...new Shift(),
      id: this.editForm.get(['id']).value,
      date: this.editForm.get(['date']).value,
      generated: false,
      shiftTypeId: this.editForm.get(['shiftTypeId']).value,
      users: this.editForm.get(['users']).value.map(id => this.users.find(user => user.id === id))
    };
    return entity;
  }
}
