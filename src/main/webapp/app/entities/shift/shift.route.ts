import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Shift } from 'app/shared/model/shift.model';
import { ShiftService } from './shift.service';
import { ShiftComponent } from './shift.component';
import { ShiftUpdateComponent } from './shift-update.component';
import { ShiftDeletePopupComponent } from './shift-delete-dialog.component';
import { IShift } from 'app/shared/model/shift.model';

@Injectable({ providedIn: 'root' })
export class ShiftResolve implements Resolve<IShift> {
  constructor(private service: ShiftService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShift> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Shift>) => response.ok),
        map((shift: HttpResponse<Shift>) => shift.body)
      );
    }
    return of(new Shift());
  }
}

export const shiftRoute: Routes = [
  {
    path: '',
    component: ShiftComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.shift.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShiftUpdateComponent,
    resolve: {
      shift: ShiftResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shift.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shiftPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShiftDeletePopupComponent,
    resolve: {
      shift: ShiftResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shift.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
