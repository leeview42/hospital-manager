import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { HospitalManagerSharedModule } from 'app/shared';
import {
  ShiftComponent,
  ShiftUpdateComponent,
  ShiftDeletePopupComponent,
  ShiftDeleteDialogComponent,
  shiftRoute,
  shiftPopupRoute
} from './';
import { ShiftUpdatePopupComponent } from './shift-update-popup.component';

const ENTITY_STATES = [...shiftRoute, ...shiftPopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ShiftComponent, ShiftUpdateComponent, ShiftUpdatePopupComponent, ShiftDeleteDialogComponent, ShiftDeletePopupComponent],
  entryComponents: [ShiftComponent, ShiftUpdateComponent, ShiftUpdatePopupComponent, ShiftDeleteDialogComponent, ShiftDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalManagerShiftModule {
  constructor(
    private languageService: JhiLanguageService,
    private languageHelper: JhiLanguageHelper,
    private dpConfig: NgbDatepickerConfig
  ) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
    const currentDate = new Date();
    this.dpConfig.minDate = { year: currentDate.getFullYear(), month: currentDate.getMonth() + 1, day: currentDate.getDate() };
    this.dpConfig.maxDate = { year: 2099, month: 12, day: 31 };
    this.dpConfig.outsideDays = 'hidden';
  }
}
