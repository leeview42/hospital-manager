import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IShift, Shift } from 'app/shared/model/shift.model';
import { ShiftService } from './shift.service';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { IUser, UserService } from 'app/core';
import { ConfirmModalComponent, formatUserName, getUsersNames } from 'app/shared';
import { select } from '@angular-redux/store';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-shift-update-popup',
  templateUrl: './shift-update-popup.component.html'
})
export class ShiftUpdatePopupComponent implements OnInit, OnDestroy {
  shift: IShift;
  isSaving: boolean;
  @select(['team', 'users'])
  users$: Observable<IUser[]>;
  @select(['team', 'isManager'])
  isManager$: Observable<boolean>;
  @select(['team', 'shiftTypes'])
  shiftTypes$: Observable<IShiftType[]>;
  subscriptions: Subscription[] = [];
  users: IUser[];
  isManager = false;

  editForm = this.fb.group({
    id: [],
    date: [],
    updateDate: [],
    createDate: [],
    generated: [],
    shiftTypeId: [],
    shiftTypeName: [],
    createdById: [],
    updatedById: [],
    users: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shiftService: ShiftService,
    protected userService: UserService,
    private fb: FormBuilder,
    private activeModal: NgbActiveModal,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.editForm.disable();
    this.updateForm(this.shift);
    const usersSubscription = this.users$.subscribe(
      (res: IUser[]) => {
        this.convertUsersToSelectArr(res);
        const teamUserIds = res.map(user => user.id);
        const users = this.editForm.get(['users']).value.filter(userId => teamUserIds.indexOf(userId) >= 0);
        this.editForm.patchValue({ users });
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
    const isManagerSubscription = this.isManager$.subscribe(next => {
      this.isManager = next;
      if (this.isManager === true) {
        this.editForm.enable();
      } else {
        this.editForm.disable();
      }
    });
    this.subscriptions.push(usersSubscription);
    this.subscriptions.push(isManagerSubscription);
  }

  convertUsersToSelectArr(res: IUser[]) {
    this.users = res.map(resUser => {
      resUser.name = formatUserName(resUser);
      return resUser;
    });
  }

  updateForm(shift: IShift) {
    this.editForm.patchValue({
      id: shift.id,
      date: shift.date,
      updateDate: shift.updateDate != null ? shift.updateDate.format(DATE_TIME_FORMAT) : null,
      createDate: shift.createDate != null ? shift.createDate.format(DATE_TIME_FORMAT) : null,
      generated: shift.generated,
      shiftTypeId: shift.shiftTypeId,
      shiftTypeName: shift.shiftType === null || shift.shiftType === undefined ? null : shift.shiftType.name,
      createdById: shift.createdById,
      updatedById: shift.updatedById,
      users: shift.users === null || shift.users === undefined ? [] : shift.users.map(user => user.id)
    });
  }

  close(reason: any[]) {
    this.activeModal.dismiss(reason);
  }

  save() {
    this.isSaving = true;
    const shift = this.createFromForm();
    this.subscribeToSaveResponse(this.shiftService.update(shift));
  }

  delete() {
    let modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'calendar.remove.shift.title';
    modalRef.componentInstance.titleParams = {
      shiftName: this.shift.shiftType.name,
      workers: getUsersNames(this.shift),
      shiftStart: this.shift.start,
      shiftEnd: this.shift.end
    };
    modalRef.componentInstance.content = 'calendar.remove.shift.content';
    modalRef.result.then(
      result => {
        if (result === 'OK') {
          this.shiftService.delete(this.shift.id).subscribe(
            () => {
              this.close(['delete']);
              modalRef = null;
            },
            () => {}
          );
        }
      },
      () => {}
    );
  }

  trackShiftTypeById(index: number, item: IShiftType) {
    return item.id;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
    this.subscriptions = [];
    this.activeModal = null;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShift>>) {
    result.subscribe((res: HttpResponse<IShift>) => this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess(shift: IShift) {
    this.isSaving = false;
    this.close(['save', shift]);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IShift {
    return {
      ...new Shift(),
      id: this.editForm.get(['id']).value,
      date: this.editForm.get(['date']).value,
      generated: false,
      shiftTypeId: this.editForm.get(['shiftTypeId']).value,
      users: this.editForm.get(['users']).value.map(id => this.users.find(user => user.id === id))
    };
  }
}
