import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, DATE_FORMAT, momentToUtc } from 'app/shared';
import { IShift, Shift } from 'app/shared/model/shift.model';
import { IUser } from 'app/core';
import { IShiftType } from 'app/shared/model/shift-type.model';
import * as moment from 'moment';
import { Moment } from 'moment';

type EntityResponseType = HttpResponse<IShift>;
type EntityArrayResponseType = HttpResponse<IShift[]>;

@Injectable({ providedIn: 'root' })
export class ShiftService {
  public updateResourceUrl = SERVER_API_URL + 'api/team-management/shifts';
  public getResourceUrl = SERVER_API_URL + 'api/team-member/shifts';

  constructor(protected http: HttpClient) {}

  create(shift: IShift): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shift);
    return this.http
      .post<IShift>(this.updateResourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shift: IShift): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shift);
    return this.http
      .put<IShift>(this.updateResourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  updateWithoutDateConversion(shift: IShift): Observable<EntityResponseType> {
    return this.http.put<IShift>(this.updateResourceUrl, shift, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IShift>(`${this.getResourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShift[]>(this.getResourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  findForInterval(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShift[]>(`${this.getResourceUrl}/interval`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  deleteForInterval(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.delete<IShift[]>(`${this.updateResourceUrl}/interval`, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.updateResourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shift: IShift): IShift {
    const copy: IShift = Object.assign({}, shift, {
      date: shift.date != null && moment(shift.date).isValid() ? moment(shift.date).format(DATE_FORMAT) : null,
      start: shift.start && shift.start.isValid() ? shift.start.toJSON() : null,
      end: shift.end && shift.end.isValid() ? shift.end.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date != null ? moment(res.body.date) : null;
      res.body.start = momentToUtc(res.body.start);
      res.body.end = momentToUtc(res.body.end);
      res.body.updateDate = momentToUtc(res.body.updateDate);
      res.body.createDate = momentToUtc(res.body.createDate);
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shift: IShift) => {
        shift.start = momentToUtc(shift.start);
        shift.end = momentToUtc(shift.end);
        shift.updateDate = momentToUtc(shift.updateDate);
        shift.createDate = momentToUtc(shift.createDate);
      });
    }
    return res;
  }

  createNewShift(user: IUser, date: Moment, shiftType: IShiftType): IShift {
    const entity = {
      ...new Shift(),
      id: null,
      date,
      startHour: shiftType.startHour,
      startMinute: shiftType.startMinute,
      duration: shiftType.duration,
      generated: false,
      shiftTypeId: shiftType.id,
      users: [user]
    };
    return entity;
  }
}
