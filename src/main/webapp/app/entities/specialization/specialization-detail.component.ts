import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpecialization } from 'app/shared/model/specialization.model';

@Component({
  selector: 'jhi-specialization-detail',
  templateUrl: './specialization-detail.component.html'
})
export class SpecializationDetailComponent implements OnInit {
  specialization: ISpecialization;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ specialization }) => {
      this.specialization = specialization;
    });
  }

  previousState() {
    window.history.back();
  }
}
