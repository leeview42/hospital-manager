import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISpecialization } from 'app/shared/model/specialization.model';

type EntityResponseType = HttpResponse<ISpecialization>;
type EntityArrayResponseType = HttpResponse<ISpecialization[]>;

@Injectable({ providedIn: 'root' })
export class SpecializationService {
  public resourceUrl = SERVER_API_URL + 'api/specializations';

  constructor(protected http: HttpClient) {}

  create(specialization: ISpecialization): Observable<EntityResponseType> {
    return this.http.post<ISpecialization>(this.resourceUrl, specialization, { observe: 'response' });
  }

  update(specialization: ISpecialization): Observable<EntityResponseType> {
    return this.http.put<ISpecialization>(this.resourceUrl, specialization, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISpecialization>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISpecialization[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
