export * from './specialization.service';
export * from './specialization-update.component';
export * from './specialization-delete-dialog.component';
export * from './specialization-detail.component';
export * from './specialization.component';
export * from './specialization.route';
