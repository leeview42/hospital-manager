import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Specialization } from 'app/shared/model/specialization.model';
import { SpecializationService } from './specialization.service';
import { SpecializationComponent } from './specialization.component';
import { SpecializationDetailComponent } from './specialization-detail.component';
import { SpecializationUpdateComponent } from './specialization-update.component';
import { SpecializationDeletePopupComponent } from './specialization-delete-dialog.component';
import { ISpecialization } from 'app/shared/model/specialization.model';

@Injectable({ providedIn: 'root' })
export class SpecializationResolve implements Resolve<ISpecialization> {
  constructor(private service: SpecializationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISpecialization> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Specialization>) => response.ok),
        map((specialization: HttpResponse<Specialization>) => specialization.body)
      );
    }
    return of(new Specialization());
  }
}

export const specializationRoute: Routes = [
  {
    path: '',
    component: SpecializationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.specialization.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpecializationDetailComponent,
    resolve: {
      specialization: SpecializationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hospitalmanagerApp.specialization.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpecializationUpdateComponent,
    resolve: {
      specialization: SpecializationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hospitalmanagerApp.specialization.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpecializationUpdateComponent,
    resolve: {
      specialization: SpecializationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hospitalmanagerApp.specialization.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const specializationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SpecializationDeletePopupComponent,
    resolve: {
      specialization: SpecializationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hospitalmanagerApp.specialization.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
