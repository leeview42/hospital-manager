import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISpecialization, Specialization } from 'app/shared/model/specialization.model';
import { SpecializationService } from './specialization.service';
import { IOrganization } from 'app/shared/model/organization.model';
import { OrganizationService } from 'app/entities/organization';

@Component({
  selector: 'jhi-specialization-update',
  templateUrl: './specialization-update.component.html'
})
export class SpecializationUpdateComponent implements OnInit {
  specialization: ISpecialization;
  isSaving: boolean;

  organizations: IOrganization[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: [],
    organizationId: [null]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected specializationService: SpecializationService,
    protected organizationService: OrganizationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ specialization }) => {
      this.updateForm(specialization);
      this.specialization = specialization;
    });
    this.organizationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IOrganization[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOrganization[]>) => response.body)
      )
      .subscribe((res: IOrganization[]) => (this.organizations = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(specialization: ISpecialization) {
    this.editForm.patchValue({
      id: specialization.id,
      name: specialization.name,
      description: specialization.description,
      organizationId: specialization.organizationId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const specialization = this.createFromForm();
    if (specialization.id !== undefined) {
      this.subscribeToSaveResponse(this.specializationService.update(specialization));
    } else {
      this.subscribeToSaveResponse(this.specializationService.create(specialization));
    }
  }

  private createFromForm(): ISpecialization {
    const entity = {
      ...new Specialization(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value,
      organizationId: this.editForm.get(['organizationId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpecialization>>) {
    result.subscribe((res: HttpResponse<ISpecialization>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackOrganizationById(index: number, item: IOrganization) {
    return item.id;
  }
}
