import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  SpecializationComponent,
  SpecializationDetailComponent,
  SpecializationUpdateComponent,
  SpecializationDeletePopupComponent,
  SpecializationDeleteDialogComponent,
  specializationRoute,
  specializationPopupRoute
} from './';

const ENTITY_STATES = [...specializationRoute, ...specializationPopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SpecializationComponent,
    SpecializationDetailComponent,
    SpecializationUpdateComponent,
    SpecializationDeleteDialogComponent,
    SpecializationDeletePopupComponent
  ],
  entryComponents: [
    SpecializationComponent,
    SpecializationUpdateComponent,
    SpecializationDeleteDialogComponent,
    SpecializationDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerSpecializationModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
