import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpecialization } from 'app/shared/model/specialization.model';
import { SpecializationService } from './specialization.service';

@Component({
  selector: 'jhi-specialization-delete-dialog',
  templateUrl: './specialization-delete-dialog.component.html'
})
export class SpecializationDeleteDialogComponent {
  specialization: ISpecialization;

  constructor(
    protected specializationService: SpecializationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.specializationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'specializationListModification',
        content: 'Deleted an specialization'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-specialization-delete-popup',
  template: ''
})
export class SpecializationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ specialization }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SpecializationDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.specialization = specialization;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/specialization', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/specialization', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
