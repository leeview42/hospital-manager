import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Wish } from 'app/shared/model/wish.model';
import { WishService } from './wish.service';
import { WishComponent } from './wish.component';
import { WishDetailComponent } from './wish-detail.component';
import { WishUpdateComponent } from './wish-update.component';
import { WishDeletePopupComponent } from './wish-delete-dialog.component';
import { IWish } from 'app/shared/model/wish.model';
import { DayConfig } from 'app/shared/model/day-config.model';
import { DATE_FORMAT } from 'app/shared';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class WishResolve implements Resolve<IWish> {
  constructor(private service: WishService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWish> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Wish>) => response.ok),
        map((wish: HttpResponse<Wish>) => wish.body)
      );
    }
    const date = route.queryParams['date'] ? route.queryParams['date'] : null;
    const userId = route.queryParams['userId'] ? route.queryParams['userId'] : null;
    if (date) {
      const newWish = new Wish();
      newWish.date = moment(moment(date).format(DATE_FORMAT));
      if (userId) {
        newWish.userId = userId;
      }
      return of(newWish);
    }

    throw new Error('No params provided in the route!');
  }
}

export const wishRoute: Routes = [
  {
    path: '',
    component: WishComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.wish.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WishDetailComponent,
    resolve: {
      wish: WishResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wish.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WishUpdateComponent,
    resolve: {
      wish: WishResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wish.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WishUpdateComponent,
    resolve: {
      wish: WishResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wish.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const wishPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: WishDeletePopupComponent,
    resolve: {
      wish: WishResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.wish.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
