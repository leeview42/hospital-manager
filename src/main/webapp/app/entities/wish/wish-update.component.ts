import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IWish, Wish } from 'app/shared/model/wish.model';
import { WishService } from './wish.service';
import { IUser } from 'app/core';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { select } from '@angular-redux/store';
import { IWeekDay } from '../../shared/model/week-day.model';

@Component({
  selector: 'jhi-wish-update',
  templateUrl: './wish-update.component.html'
})
export class WishUpdateComponent implements OnInit {
  wish: IWish;
  isSaving: boolean;
  weekDays: IWeekDay[];

  @select(['team', 'shiftTypes'])
  shiftTypes: Observable<IShiftType[]>;

  @select(['translate', 'weekDays'])
  weekDays$: Observable<IWeekDay[]>;

  @select(['team', 'users'])
  users: Observable<IUser[]>;

  editForm = this.fb.group({
    id: [],
    comment: [],
    wishType: [null, [Validators.required]],
    userId: [null, [Validators.required]],
    date: [],
    shiftTypes: [[], [Validators.required]],
    createWholeMonth: [false],
    weekDays: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected wishService: WishService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ wish }) => {
      this.updateForm(wish);
      this.wish = wish;
    });
    if (!this.wish.shiftTypes) {
      this.wish.shiftTypes = [];
    }
    this.weekDays$.subscribe(weekDays => {
      this.weekDays = weekDays;
    });
  }

  updateForm(wish: IWish) {
    this.editForm.patchValue({
      id: wish.id,
      comment: wish.comment,
      wishType: wish.wishType,
      userId: Number(wish.userId),
      date: wish.date,
      shiftTypes: wish.shiftTypes,
      weekDays: wish.date ? [wish.date.day() - 1] : []
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const wish = this.createFromForm();
    const createWholeMonth = this.editForm.get(['createWholeMonth']).value;
    if (wish.id !== undefined) {
      this.subscribeToSaveResponse(this.wishService.update(wish, createWholeMonth, this.editForm.get('weekDays').value));
    } else {
      this.subscribeToSaveResponse(this.wishService.create(wish, createWholeMonth, this.editForm.get('weekDays').value));
    }
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  loadUserShiftTypes() {
    const selectedUserId = this.editForm.get(['userId']).value;
    /*if (selectedUserId) {
      this.shiftTypeService
        .query({
          size: MAX_ITEMS_PER_PAGE,
          'userId.equals': selectedUserId
        })
        .subscribe(shiftTypesResp => {
          this.shiftTypes = shiftTypesResp.body;
        });
    }*/
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWish>>) {
    result.subscribe((res: HttpResponse<IWish>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IWish {
    const entity = {
      ...new Wish(),
      id: this.editForm.get(['id']).value,
      comment: this.editForm.get(['comment']).value,
      wishType: this.editForm.get(['wishType']).value,
      userId: this.editForm.get(['userId']).value,
      date: this.editForm.get(['date']).value,
      shiftTypes: this.editForm.get(['shiftTypes']).value
    };
    return entity;
  }

  addWeekDay() {
    this.editForm.patchValue({
      weekDays: this.wish.date ? [this.wish.date.day() - 1] : []
    });
  }
}
