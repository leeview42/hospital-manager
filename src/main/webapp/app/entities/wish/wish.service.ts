import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IWish } from 'app/shared/model/wish.model';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

type EntityResponseType = HttpResponse<IWish>;
type EntityArrayResponseType = HttpResponse<IWish[]>;

@Injectable({ providedIn: 'root' })
export class WishService {
  public resourceUrl = SERVER_API_URL + 'api/team-management/wishes';

  constructor(protected http: HttpClient) {}

  create(wish: IWish, createWholeMonth: false, weekDays: [] = []): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(wish);
    const req = createRequestOption({ createWholeMonth, weekDays });
    return this.http
      .post<IWish>(this.resourceUrl, copy, { observe: 'response', params: req })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(wish: IWish, createWholeMonth: false, weekDays: [] = []): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(wish);
    const req = createRequestOption({ createWholeMonth, weekDays });
    return this.http
      .put<IWish>(this.resourceUrl, copy, { observe: 'response', params: req })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  deleteForInterval(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.delete<IWish[]>(`${this.resourceUrl}/interval`, { params: options, observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWish>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWish[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(wish: IWish): IWish {
    const copy: IWish = Object.assign({}, wish, {
      dateFrom: wish.dateFrom != null && wish.dateFrom.isValid() ? wish.dateFrom.toJSON() : null,
      dateTo: wish.dateTo != null && wish.dateTo.isValid() ? wish.dateTo.toJSON() : null,
      date: wish.date != null && wish.date.isValid() ? wish.date.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateFrom = res.body.dateFrom != null ? moment(res.body.dateFrom) : null;
      res.body.dateTo = res.body.dateTo != null ? moment(res.body.dateTo) : null;
      res.body.date = res.body.date != null ? moment(res.body.date) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((wish: IWish) => {
        wish.dateFrom = wish.dateFrom != null ? moment(wish.dateFrom) : null;
        wish.dateTo = wish.dateTo != null ? moment(wish.dateTo) : null;
      });
    }
    return res;
  }
}
