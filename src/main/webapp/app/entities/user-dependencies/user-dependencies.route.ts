import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { UserShiftType } from 'app/shared/model/user-shift-type.model';
import { UserDependenciesService } from './user-dependencies.service';
import { UserDependenciesComponent } from './user-dependencies.component';
import { IUserShiftType } from 'app/shared/model/user-shift-type.model';

@Injectable({ providedIn: 'root' })
export class UserDependenciesResolve implements Resolve<IUserShiftType> {
  constructor(private service: UserDependenciesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserShiftType> {
    return of(new UserShiftType());
  }
}

export const userDependenciesRoute: Routes = [
  {
    path: '',
    component: UserDependenciesComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.userDependencies.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
