import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IUserShiftType } from 'app/shared/model/user-shift-type.model';
import { IUser } from 'app/core';

import { select } from '@angular-redux/store';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { formatUserName, MAX_ITEMS_PER_PAGE } from 'app/shared';
import { UserDependenciesService } from 'app/entities/user-dependencies/user-dependencies.service';
import { SpecializationService } from '../specialization';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';
import { ISpecialization } from 'app/shared/model/specialization.model';

@Component({
  selector: 'jhi-user-dependencies',
  templateUrl: './user-dependencies.component.html'
})
export class UserDependenciesComponent implements OnInit, OnDestroy {
  currentAccount: any;
  @select(['team', 'users'])
  users$: Observable<IUser[]>;
  users: IUser[] = [];
  allUsers: IUser[] = [];
  userFilter = '';
  @select(['team', 'shiftTypes'])
  shiftTypes$: Observable<IShiftType[]>;
  @select(['user', 'hasSpecializations'])
  hasSpecializations$: Observable<string>;
  specializations: ISpecialization[];
  error: any;
  success: any;
  userSubscription: Subscription;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected router: Router,
    protected userDependenciesService: UserDependenciesService,
    protected specializationService: SpecializationService
  ) {}

  ngOnInit(): void {
    this.loadUsers();
    this.specializationService
      .query({
        size: MAX_ITEMS_PER_PAGE
      })
      .pipe(
        filter((mayBeOk: HttpResponse<ISpecialization[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISpecialization[]>) => response.body)
      )
      .subscribe((res: ISpecialization[]) => (this.specializations = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  filterUsers() {
    this.users = this.allUsers.filter(user =>
      `${user.firstName ? user.firstName : ''}${user.lastName ? user.lastName : ''}${user.email ? user.email : ''}`
        .toLowerCase()
        .includes(this.userFilter)
    );
  }

  loadUsers() {
    this.userSubscription = this.users$.subscribe(
      (res: IUser[]) => this.convertUsersToSelectArr(res),
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  convertUsersToSelectArr(res: IUser[]) {
    this.users = res
      .map(resUser => {
        resUser.name = formatUserName(resUser);
        return resUser;
      })
      .sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0));
    this.allUsers = [...this.users];
    this.userFilter = '';
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  trackId(index: number, item: IUserShiftType) {
    return item.id;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  formatUserName(user: IUser): string {
    return formatUserName(user);
  }

  changedUserShiftTypes(user: IUser) {
    this.userDependenciesService
      .update(
        {
          userId: user.id,
          otherEntityIds: user.shiftTypes
        },
        'user-shift-types'
      )
      .subscribe(next => {}, this.onError);
  }

  changedUserSpecializations(user: IUser) {
    this.userDependenciesService
      .update(
        {
          userId: user.id,
          shiftTypeIds: user.shiftTypes
        },
        'user-specializations'
      )
      .subscribe(next => {}, this.onError);
  }
}
