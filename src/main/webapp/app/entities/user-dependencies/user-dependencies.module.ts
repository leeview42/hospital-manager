import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import { UserDependenciesComponent, userDependenciesRoute } from '.';

const ENTITY_STATES = [...userDependenciesRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [UserDependenciesComponent],
  entryComponents: [UserDependenciesComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerUserDependenciesModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
