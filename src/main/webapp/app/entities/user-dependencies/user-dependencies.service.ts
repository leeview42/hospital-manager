import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IUserShiftType } from 'app/shared/model/user-shift-type.model';

type EntityResponseType = HttpResponse<IUserShiftType>;

@Injectable({ providedIn: 'root' })
export class UserDependenciesService {
  public resourceUrl = SERVER_API_URL + 'api/team-management/user-dependencies';

  constructor(protected http: HttpClient) {}

  update(userDependencies: any, dependencyEndpoint: string): Observable<EntityResponseType> {
    return this.http.post<any>(`${this.resourceUrl}/${dependencyEndpoint}`, userDependencies, { observe: 'response' });
  }
}
