import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'shift',
        loadChildren: './shift/shift.module#HospitalManagerShiftModule'
      },
      {
        path: 'shift-type',
        loadChildren: './shift-type/shift-type.module#HospitalmanagerShiftTypeModule'
      },
      {
        path: 'shift-day',
        loadChildren: './shift-day/shift-day.module#HospitalmanagerShiftDayModule'
      },
      {
        path: 'shift-worker',
        loadChildren: './shift-worker/shift-worker.module#HospitalmanagerShiftWorkerModule'
      },
      {
        path: 'priority',
        loadChildren: './priority/priority.module#HospitalmanagerPriorityModule'
      },
      {
        path: 'user-dependencies',
        loadChildren: './user-dependencies/user-dependencies.module#HospitalmanagerUserDependenciesModule'
      },
      {
        path: 'day-config',
        loadChildren: './day-config/day-config.module#HospitalmanagerDayConfigModule'
      },
      {
        path: 'day-config-shift-type',
        loadChildren: './day-config-shift-type/day-config-shift-type.module#HospitalmanagerDayConfigShiftTypeModule'
      },
      {
        path: 'worker-availability',
        loadChildren: './worker-availability/worker-availability.module#HospitalmanagerWorkerAvailabilityModule'
      },
      {
        path: 'shift-type-override',
        loadChildren: './shift-type-override/shift-type-override.module#HospitalmanagerShiftTypeOverrideModule'
      },
      {
        path: 'wish-shift-type',
        loadChildren: './wish-shift-type/wish-shift-type.module#HospitalmanagerWishShiftTypeModule'
      },
      {
        path: 'wish',
        loadChildren: './wish/wish.module#HospitalmanagerWishModule'
      },
      {
        path: 'organization',
        loadChildren: './organization/organization.module#HospitalmanagerOrganizationModule'
      },
      {
        path: 'team',
        loadChildren: './team/team.module#HospitalManagerTeamModule'
      },
      {
        path: 'specialization',
        loadChildren: './specialization/specialization.module#HospitalmanagerSpecializationModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerEntityModule {}
