import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShiftDay } from 'app/shared/model/shift-day.model';

@Component({
  selector: 'jhi-shift-day-detail',
  templateUrl: './shift-day-detail.component.html'
})
export class ShiftDayDetailComponent implements OnInit {
  shiftDay: IShiftDay;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftDay }) => {
      this.shiftDay = shiftDay;
    });
  }

  previousState() {
    window.history.back();
  }
}
