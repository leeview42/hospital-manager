import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShiftDay } from 'app/shared/model/shift-day.model';

type EntityResponseType = HttpResponse<IShiftDay>;
type EntityArrayResponseType = HttpResponse<IShiftDay[]>;

@Injectable({ providedIn: 'root' })
export class ShiftDayService {
  public resourceUrl = SERVER_API_URL + 'api/shift-days';

  constructor(protected http: HttpClient) {}

  create(shiftDay: IShiftDay): Observable<EntityResponseType> {
    return this.http.post<IShiftDay>(this.resourceUrl, shiftDay, { observe: 'response' });
  }

  update(shiftDay: IShiftDay): Observable<EntityResponseType> {
    return this.http.put<IShiftDay>(this.resourceUrl, shiftDay, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShiftDay>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShiftDay[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
