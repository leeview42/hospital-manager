import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShiftDay, ShiftDay } from 'app/shared/model/shift-day.model';
import { ShiftDayService } from './shift-day.service';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { ShiftTypeService } from 'app/entities/shift-type';

@Component({
  selector: 'jhi-shift-day-update',
  templateUrl: './shift-day-update.component.html'
})
export class ShiftDayUpdateComponent implements OnInit {
  isSaving: boolean;

  shifttypes: IShiftType[];

  editForm = this.fb.group({
    id: [],
    dayOfWeek: [],
    shiftTypeId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shiftDayService: ShiftDayService,
    protected shiftTypeService: ShiftTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shiftDay }) => {
      this.updateForm(shiftDay);
    });
    this.shiftTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShiftType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShiftType[]>) => response.body)
      )
      .subscribe((res: IShiftType[]) => (this.shifttypes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shiftDay: IShiftDay) {
    this.editForm.patchValue({
      id: shiftDay.id,
      dayOfWeek: shiftDay.dayOfWeek,
      shiftTypeId: shiftDay.shiftTypeId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shiftDay = this.createFromForm();
    if (shiftDay.id !== undefined) {
      this.subscribeToSaveResponse(this.shiftDayService.update(shiftDay));
    } else {
      this.subscribeToSaveResponse(this.shiftDayService.create(shiftDay));
    }
  }

  private createFromForm(): IShiftDay {
    return {
      ...new ShiftDay(),
      id: this.editForm.get(['id']).value,
      dayOfWeek: this.editForm.get(['dayOfWeek']).value,
      shiftTypeId: this.editForm.get(['shiftTypeId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShiftDay>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShiftTypeById(index: number, item: IShiftType) {
    return item.id;
  }
}
