import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShiftDay } from 'app/shared/model/shift-day.model';
import { ShiftDayService } from './shift-day.service';
import { ShiftDayComponent } from './shift-day.component';
import { ShiftDayDetailComponent } from './shift-day-detail.component';
import { ShiftDayUpdateComponent } from './shift-day-update.component';
import { ShiftDayDeletePopupComponent } from './shift-day-delete-dialog.component';
import { IShiftDay } from 'app/shared/model/shift-day.model';

@Injectable({ providedIn: 'root' })
export class ShiftDayResolve implements Resolve<IShiftDay> {
  constructor(private service: ShiftDayService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShiftDay> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShiftDay>) => response.ok),
        map((shiftDay: HttpResponse<ShiftDay>) => shiftDay.body)
      );
    }
    return of(new ShiftDay());
  }
}

export const shiftDayRoute: Routes = [
  {
    path: '',
    component: ShiftDayComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.shiftDay.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShiftDayDetailComponent,
    resolve: {
      shiftDay: ShiftDayResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftDay.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShiftDayUpdateComponent,
    resolve: {
      shiftDay: ShiftDayResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftDay.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShiftDayUpdateComponent,
    resolve: {
      shiftDay: ShiftDayResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftDay.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shiftDayPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShiftDayDeletePopupComponent,
    resolve: {
      shiftDay: ShiftDayResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftDay.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
