import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShiftDay } from 'app/shared/model/shift-day.model';
import { ShiftDayService } from './shift-day.service';

@Component({
  selector: 'jhi-shift-day-delete-dialog',
  templateUrl: './shift-day-delete-dialog.component.html'
})
export class ShiftDayDeleteDialogComponent {
  shiftDay: IShiftDay;

  constructor(protected shiftDayService: ShiftDayService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shiftDayService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shiftDayListModification',
        content: 'Deleted an shiftDay'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shift-day-delete-popup',
  template: ''
})
export class ShiftDayDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftDay }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShiftDayDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shiftDay = shiftDay;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shift-day', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shift-day', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
