import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  ShiftDayComponent,
  ShiftDayDetailComponent,
  ShiftDayUpdateComponent,
  ShiftDayDeletePopupComponent,
  ShiftDayDeleteDialogComponent,
  shiftDayRoute,
  shiftDayPopupRoute
} from './';

const ENTITY_STATES = [...shiftDayRoute, ...shiftDayPopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShiftDayComponent,
    ShiftDayDetailComponent,
    ShiftDayUpdateComponent,
    ShiftDayDeleteDialogComponent,
    ShiftDayDeletePopupComponent
  ],
  entryComponents: [ShiftDayComponent, ShiftDayUpdateComponent, ShiftDayDeleteDialogComponent, ShiftDayDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerShiftDayModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
