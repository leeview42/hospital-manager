import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShiftTypeOverride } from 'app/shared/model/shift-type-override.model';
import { ShiftTypeOverrideService } from './shift-type-override.service';

@Component({
  selector: 'jhi-shift-type-override-delete-dialog',
  templateUrl: './shift-type-override-delete-dialog.component.html'
})
export class ShiftTypeOverrideDeleteDialogComponent {
  shiftTypeOverride: IShiftTypeOverride;

  constructor(
    protected shiftTypeOverrideService: ShiftTypeOverrideService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.shiftTypeOverrideService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'shiftTypeOverrideListModification',
        content: 'Deleted an shiftTypeOverride'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-shift-type-override-delete-popup',
  template: ''
})
export class ShiftTypeOverrideDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftTypeOverride }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ShiftTypeOverrideDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.shiftTypeOverride = shiftTypeOverride;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/shift-type-override', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/shift-type-override', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
