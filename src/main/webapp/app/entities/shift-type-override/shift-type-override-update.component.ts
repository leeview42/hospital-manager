import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShiftTypeOverride, ShiftTypeOverride } from 'app/shared/model/shift-type-override.model';
import { ShiftTypeOverrideService } from './shift-type-override.service';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { ShiftTypeService } from 'app/entities/shift-type';

@Component({
  selector: 'jhi-shift-type-override-update',
  templateUrl: './shift-type-override-update.component.html'
})
export class ShiftTypeOverrideUpdateComponent implements OnInit {
  shiftTypeOverride: IShiftTypeOverride;
  isSaving: boolean;

  shifttypes: IShiftType[];

  editForm = this.fb.group({
    id: [],
    shiftTypeId: [],
    canOverrideId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shiftTypeOverrideService: ShiftTypeOverrideService,
    protected shiftTypeService: ShiftTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shiftTypeOverride }) => {
      this.updateForm(shiftTypeOverride);
      this.shiftTypeOverride = shiftTypeOverride;
    });
    this.shiftTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShiftType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShiftType[]>) => response.body)
      )
      .subscribe((res: IShiftType[]) => (this.shifttypes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shiftTypeOverride: IShiftTypeOverride) {
    this.editForm.patchValue({
      id: shiftTypeOverride.id,
      shiftTypeId: shiftTypeOverride.shiftTypeId,
      canOverrideId: shiftTypeOverride.canOverrideId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shiftTypeOverride = this.createFromForm();
    if (shiftTypeOverride.id !== undefined) {
      this.subscribeToSaveResponse(this.shiftTypeOverrideService.update(shiftTypeOverride));
    } else {
      this.subscribeToSaveResponse(this.shiftTypeOverrideService.create(shiftTypeOverride));
    }
  }

  private createFromForm(): IShiftTypeOverride {
    const entity = {
      ...new ShiftTypeOverride(),
      id: this.editForm.get(['id']).value,
      shiftTypeId: this.editForm.get(['shiftTypeId']).value,
      canOverrideId: this.editForm.get(['canOverrideId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShiftTypeOverride>>) {
    result.subscribe((res: HttpResponse<IShiftTypeOverride>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShiftTypeById(index: number, item: IShiftType) {
    return item.id;
  }
}
