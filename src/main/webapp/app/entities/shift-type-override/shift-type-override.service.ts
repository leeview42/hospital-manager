import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShiftTypeOverride } from 'app/shared/model/shift-type-override.model';

type EntityResponseType = HttpResponse<IShiftTypeOverride>;
type EntityArrayResponseType = HttpResponse<IShiftTypeOverride[]>;

@Injectable({ providedIn: 'root' })
export class ShiftTypeOverrideService {
  public resourceUrl = SERVER_API_URL + 'api/shift-type-overrides';

  constructor(protected http: HttpClient) {}

  create(shiftTypeOverride: IShiftTypeOverride): Observable<EntityResponseType> {
    return this.http.post<IShiftTypeOverride>(this.resourceUrl, shiftTypeOverride, { observe: 'response' });
  }

  update(shiftTypeOverride: IShiftTypeOverride): Observable<EntityResponseType> {
    return this.http.put<IShiftTypeOverride>(this.resourceUrl, shiftTypeOverride, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShiftTypeOverride>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShiftTypeOverride[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
