import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HospitalManagerSharedModule } from 'app/shared';
import {
  ShiftTypeOverrideComponent,
  ShiftTypeOverrideDetailComponent,
  ShiftTypeOverrideUpdateComponent,
  ShiftTypeOverrideDeletePopupComponent,
  ShiftTypeOverrideDeleteDialogComponent,
  shiftTypeOverrideRoute,
  shiftTypeOverridePopupRoute
} from './';

const ENTITY_STATES = [...shiftTypeOverrideRoute, ...shiftTypeOverridePopupRoute];

@NgModule({
  imports: [HospitalManagerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShiftTypeOverrideComponent,
    ShiftTypeOverrideDetailComponent,
    ShiftTypeOverrideUpdateComponent,
    ShiftTypeOverrideDeleteDialogComponent,
    ShiftTypeOverrideDeletePopupComponent
  ],
  entryComponents: [
    ShiftTypeOverrideComponent,
    ShiftTypeOverrideUpdateComponent,
    ShiftTypeOverrideDeleteDialogComponent,
    ShiftTypeOverrideDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalmanagerShiftTypeOverrideModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
