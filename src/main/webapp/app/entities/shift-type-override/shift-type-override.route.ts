import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShiftTypeOverride } from 'app/shared/model/shift-type-override.model';
import { ShiftTypeOverrideService } from './shift-type-override.service';
import { ShiftTypeOverrideComponent } from './shift-type-override.component';
import { ShiftTypeOverrideDetailComponent } from './shift-type-override-detail.component';
import { ShiftTypeOverrideUpdateComponent } from './shift-type-override-update.component';
import { ShiftTypeOverrideDeletePopupComponent } from './shift-type-override-delete-dialog.component';
import { IShiftTypeOverride } from 'app/shared/model/shift-type-override.model';

@Injectable({ providedIn: 'root' })
export class ShiftTypeOverrideResolve implements Resolve<IShiftTypeOverride> {
  constructor(private service: ShiftTypeOverrideService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShiftTypeOverride> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShiftTypeOverride>) => response.ok),
        map((shiftTypeOverride: HttpResponse<ShiftTypeOverride>) => shiftTypeOverride.body)
      );
    }
    return of(new ShiftTypeOverride());
  }
}

export const shiftTypeOverrideRoute: Routes = [
  {
    path: '',
    component: ShiftTypeOverrideComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      defaultSort: 'id,asc',
      pageTitle: 'hospitalmanagerApp.shiftTypeOverride.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShiftTypeOverrideDetailComponent,
    resolve: {
      shiftTypeOverride: ShiftTypeOverrideResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftTypeOverride.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShiftTypeOverrideUpdateComponent,
    resolve: {
      shiftTypeOverride: ShiftTypeOverrideResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftTypeOverride.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShiftTypeOverrideUpdateComponent,
    resolve: {
      shiftTypeOverride: ShiftTypeOverrideResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftTypeOverride.home.title',
      disableTeamSelection: true
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shiftTypeOverridePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShiftTypeOverrideDeletePopupComponent,
    resolve: {
      shiftTypeOverride: ShiftTypeOverrideResolve
    },
    data: {
      authorities: ['ROLE_TEAM_MANAGER'],
      pageTitle: 'hospitalmanagerApp.shiftTypeOverride.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
