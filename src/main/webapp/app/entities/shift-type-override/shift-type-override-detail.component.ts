import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShiftTypeOverride } from 'app/shared/model/shift-type-override.model';

@Component({
  selector: 'jhi-shift-type-override-detail',
  templateUrl: './shift-type-override-detail.component.html'
})
export class ShiftTypeOverrideDetailComponent implements OnInit {
  shiftTypeOverride: IShiftTypeOverride;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shiftTypeOverride }) => {
      this.shiftTypeOverride = shiftTypeOverride;
    });
  }

  previousState() {
    window.history.back();
  }
}
