import './vendor.ts';

import { NgRedux } from '@angular-redux/store';
import { IAppState, rootReducer, INITIAL_STATE } from './redux/store';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { NgJhipsterModule } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { TeamInterceptor } from './blocks/interceptor/team.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { HospitalManagerSharedModule } from 'app/shared';
import { HospitalmanagerCoreModule } from 'app/core';
import { HospitalmanagerAppRoutingModule } from './app-routing.module';
import { HospitalmanagerHomeModule } from './home/home.module';
import { HospitalManagerCalendarModule } from 'app/pages/calendar';
import { HospitalmanagerAccountModule } from './account/account.module';
import { HospitalmanagerEntityModule } from './entities/entity.module';
import { HospitalManagerTeamModule } from 'app/entities/team';

import * as moment from 'moment';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { LoaderInterceptorService } from 'app/shared/loader/loader.interceptor.service';
import { createStore } from 'redux';

@NgModule({
  imports: [
    BrowserModule,
    NgxWebstorageModule.forRoot({ prefix: 'jhi', separator: '-' }),
    NgJhipsterModule.forRoot({
      // set below to true to make alerts look like toast
      alertAsToast: false,
      alertTimeout: 5000,
      i18nEnabled: true,
      defaultI18nLang: 'ro'
    }),
    HospitalManagerSharedModule.forRoot(),
    HospitalmanagerCoreModule,
    HospitalmanagerHomeModule,
    HospitalManagerCalendarModule,
    HospitalmanagerAccountModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    HospitalmanagerEntityModule,
    HospitalmanagerAppRoutingModule,
    HospitalManagerTeamModule
  ],
  declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TeamInterceptor,
      multi: true
    }
  ],
  bootstrap: [JhiMainComponent]
})
export class HospitalmanagerAppModule {
  constructor(private dpConfig: NgbDatepickerConfig, private ngRedux: NgRedux<IAppState>) {
    this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
    ngRedux.provideStore(createStore(rootReducer, INITIAL_STATE));
  }
}
