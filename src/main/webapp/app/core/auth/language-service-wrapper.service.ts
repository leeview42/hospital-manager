import { Injectable } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';
import { Subject } from 'rxjs';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from 'app/redux/store';
import { MultiSelectOptionModel } from 'app/shared/model/multi-select-option.model';
import { TranslateService } from '@ngx-translate/core';
import { TRANSLATE_WEEK_DAYS, TRANSLATE_MONTHS } from 'app/redux/translate/translateActions';

export const DAYS_OF_WEEK_KEYS = [
  'calendar.mo',
  'calendar.tu',
  'calendar.wed',
  'calendar.thu',
  'calendar.fri',
  'calendar.sat',
  'calendar.sun'
];

export const MONTHS_KEYS = [
  'calendar.jan',
  'calendar.feb',
  'calendar.mar',
  'calendar.apr',
  'calendar.may',
  'calendar.jun',
  'calendar.jul',
  'calendar.aug',
  'calendar.sep',
  'calendar.oct',
  'calendar.nov',
  'calendar.dec'
];

@Injectable({ providedIn: 'root' })
export class LanguageServiceWrapper {
  private refresh: Subject<any> = new Subject();
  private weekDays: MultiSelectOptionModel[];
  private months: object[];

  constructor(
    private languageService: JhiLanguageService,
    private translateService: TranslateService,
    private ngRedux: NgRedux<IAppState>
  ) {}

  changeLanguage(languageKey: string) {
    this.languageService.changeLanguage(languageKey);
    this.languageService.getCurrent().then(currentLanguage => {
      this.refresh.subscribe(subject => {
        if (subject === 'weekDays' && this.weekDays && this.weekDays.length === 7) {
          this.ngRedux.dispatch({ type: TRANSLATE_WEEK_DAYS, weekDays: this.weekDays });
        }
        if (subject === 'months' && this.months && this.months.length === 12) {
          this.ngRedux.dispatch({ type: TRANSLATE_MONTHS, months: this.months });
        }
      });
    });
    this.translateWeekDays();
    this.translateMonths();
  }

  translateWeekDays() {
    this.weekDays = [];
    DAYS_OF_WEEK_KEYS.forEach(dayKey => {
      this.translateService.get(dayKey).subscribe(translated => {
        this.weekDays.push(new MultiSelectOptionModel(DAYS_OF_WEEK_KEYS.indexOf(dayKey), translated));
        this.refresh.next('weekDays');
      });
    });
  }

  translateMonths() {
    this.months = [];
    MONTHS_KEYS.forEach(monthKey => {
      this.translateService.get(monthKey).subscribe(translated => {
        this.months.push({ id: MONTHS_KEYS.indexOf(monthKey), label: translated });
        this.refresh.next('months');
      });
    });
  }
}
