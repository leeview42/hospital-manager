import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ConfirmModalComponent, formatUserName, ITEMS_PER_PAGE, MAX_ITEMS_PER_PAGE } from 'app/shared';
import { AccountService, UserService, User } from 'app/core';
import { OrganizationService } from 'app/entities/organization';
import { IOrganization } from 'app/shared/model/organization.model';

@Component({
  selector: 'jhi-user-mgmt',
  templateUrl: './user-management.component.html'
})
export class UserMgmtComponent implements OnInit, OnDestroy {
  currentAccount: any;
  users: User[];
  organizations: IOrganization[];
  error: any;
  success: any;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  filterResetTimestamp: number;
  mandatoryFilter = false;
  firstNameFilter = '';
  lastNameFilter = '';
  emailFilter = '';

  constructor(
    private userService: UserService,
    private alertService: JhiAlertService,
    private accountService: AccountService,
    private parseLinks: JhiParseLinks,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventManager: JhiEventManager,
    private modalService: NgbModal,
    private organizationService: OrganizationService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data['pagingParams'].page;
      this.previousPage = data['pagingParams'].page;
      this.reverse = data['pagingParams'].ascending;
      this.predicate = data['pagingParams'].predicate;
    });
  }

  ngOnInit() {
    this.accountService.identity().then(account => {
      this.currentAccount = account;
      this.loadAll();
      this.loadOrganizations();
      this.registerChangeInUsers();
    });
  }

  ngOnDestroy() {
    this.routeData.unsubscribe();
  }

  registerChangeInUsers() {
    this.eventManager.subscribe('userListModification', this.loadAll());
  }

  setActive(user, isActivated) {
    user.activated = isActivated;
    if (this.hasAuthority('ROLE_ADMIN')) {
      this.updateUser(user);
    } else {
      const req = {
        active: isActivated,
        userId: user.id
      };
      this.userService.setActive(req).subscribe(response => {
        if (response.status === 204) {
          this.error = null;
          this.success = 'OK';
          this.loadAll();
        } else {
          this.success = null;
          this.error = 'ERROR';
        }
      });
    }
  }

  loadAll() {
    const req = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort(),
      ...(this.firstNameFilter && { 'firstName.contains': this.firstNameFilter }),
      ...(this.lastNameFilter && { 'lastName.contains': this.lastNameFilter }),
      ...(this.emailFilter && { 'email.contains': this.emailFilter }),
      'mandatory.equals': this.mandatoryFilter
    };
    if (this.hasAuthority('ROLE_ADMIN')) {
      this.userService
        .queryAll(req)
        .subscribe(
          (res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers),
          (res: HttpResponse<any>) => this.onError(res.body)
        );
    } else {
      this.userService
        .query(req)
        .subscribe(
          (res: HttpResponse<User[]>) => this.onSuccess(res.body, res.headers),
          (res: HttpResponse<any>) => this.onError(res.body)
        );
    }
  }

  filterChanged() {
    this.filterResetTimestamp = new Date().getTime();
    setTimeout(() => {
      const currentMillis = new Date().getTime();
      if (currentMillis - this.filterResetTimestamp >= 1000) {
        this.loadAll();
      }
    }, 1000);
  }

  trackIdentity(index, item: User) {
    return item.id;
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/admin/user-management'], {
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  private onSuccess(data, headers) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = headers.get('X-Total-Count');
    this.users = data;
  }

  private onError(error) {
    this.alertService.error(error.error, error.message, null);
  }

  private loadOrganizations() {
    this.organizationService
      .query({
        size: MAX_ITEMS_PER_PAGE
      })
      .subscribe(response => (this.organizations = response.body));
  }

  trackOrganizationById(index: number, item: IOrganization) {
    return item.id;
  }

  public hasAuthority(authority: string): boolean {
    return this.accountService.hasAnyAuthority([authority]);
  }

  changeOrganization(user: User) {
    const modalRef = this.modalService.open(ConfirmModalComponent);

    modalRef.componentInstance.title = 'userManagement.confirm.organization.change.title';
    modalRef.componentInstance.content = 'userManagement.confirm.organization.change.detail';
    const newOrganization = this.organizations.find(org => org.id === Number(user.organizationId));
    if (newOrganization) {
      modalRef.componentInstance.contentParams = {
        organization: newOrganization.name,
        user: formatUserName(user)
      };

      modalRef.result.then(
        result => {
          this.updateUser(user);
        },
        canceled => {
          this.loadAll();
        }
      );
    }
  }

  private updateUser(user: User) {
    this.userService.update(user).subscribe(response => {
      if (response.status === 200) {
        this.error = null;
        this.success = 'OK';
        this.loadAll();
      } else {
        this.success = null;
        this.error = 'ERROR';
      }
    });
  }
}
