import { ADD_SHIFT_TYPE, TEAM_SELECTED, TEAM_UNSELECTED, UPDATE_SHIFT_TYPE } from 'app/redux/team/teamActions';

export const TEAM_INITIAL_STATE = {
  users: [],
  isManager: false,
  id: null,
  shiftTypes: []
};

export function createTeamReducer() {
  return function userReducer(state = TEAM_INITIAL_STATE, action) {
    switch (action.type) {
      case TEAM_SELECTED:
        state = Object.assign({}, state, {
          users: [...action.users],
          isManager: action.isManager,
          id: action.id,
          shiftTypes: action.shiftTypes
        });
        break;
      case TEAM_UNSELECTED:
        state = TEAM_INITIAL_STATE;
        break;
      case ADD_SHIFT_TYPE:
        if (action.shiftType.id) {
          const shiftTypeIndex = state.shiftTypes.findIndex(shiftType => shiftType.id === action.shiftType.id);
          if (shiftTypeIndex >= 0) {
            state.shiftTypes[shiftTypeIndex] = action.shiftType;
          } else {
            state.shiftTypes.push(action.shiftType);
          }
        }
        break;
      case UPDATE_SHIFT_TYPE:
        break;
    }
    return state;
  };
}
