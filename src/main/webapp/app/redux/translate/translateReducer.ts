import { TRANSLATE_WEEK_DAYS, TRANSLATE_MONTHS } from 'app/redux/translate/translateActions';

export const TRANSLATE_INITIAL_STATE = {
  weekDays: [],
  months: []
};

export function createTranslateReducer() {
  return function translateReducer(state = TRANSLATE_INITIAL_STATE, action) {
    switch (action.type) {
      case TRANSLATE_WEEK_DAYS:
        state = Object.assign({}, state, {
          weekDays: action.weekDays
        });
        break;
      case TRANSLATE_MONTHS:
        state = Object.assign({}, state, {
          months: action.months
        });
        break;
    }
    return state;
  };
}
