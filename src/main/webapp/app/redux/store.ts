import { combineReducers } from 'redux';
import { createTranslateReducer, TRANSLATE_INITIAL_STATE } from 'app/redux/translate/translateReducer';
import { composeReducers } from '@angular-redux/form';
import { createUserReducer, USER_INITIAL_STATE } from 'app/redux/user/userReducer';
import { createTeamReducer, TEAM_INITIAL_STATE } from 'app/redux/team/teamReducer';

export interface IAppState {
  team: any;
  user: any;
  translate: any;
}
export const INITIAL_STATE: IAppState = {
  team: TEAM_INITIAL_STATE,
  user: USER_INITIAL_STATE,
  translate: TRANSLATE_INITIAL_STATE
};

export const rootReducer = composeReducers(
  combineReducers({
    translate: createTranslateReducer(),
    user: createUserReducer(),
    team: createTeamReducer()
  })
);
