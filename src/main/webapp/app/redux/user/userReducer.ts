import { UPDATE_CURRENT_USER_ID, UPDATE_ORGANIZATION } from 'app/redux/user/userActions';

export const USER_INITIAL_STATE = {
  organizationId: -1,
  organizationName: null,
  hasSpecializations: false,
  id: -1
};

export function createUserReducer() {
  return function userReducer(state = USER_INITIAL_STATE, action) {
    switch (action.type) {
      case UPDATE_ORGANIZATION:
        state = Object.assign({}, state, {
          organizationId: action.organizationId,
          organizationName: action.organizationName,
          hasSpecializations: action.hasSpecializations
        });
        break;
      case UPDATE_CURRENT_USER_ID:
        state = Object.assign({}, state, {
          id: action.userId
        });
        break;
    }
    return state;
  };
}
