import { CanActivate, Route } from '@angular/router';

import { CalendarComponent } from './';
import { Injectable } from '@angular/core';
import { AccountService } from 'app/core';

@Injectable({ providedIn: 'root' })
export class IsAuthenticated implements CanActivate {
  constructor(private accountService: AccountService) {}

  canActivate() {
    return this.accountService.identity().then(account => this.accountService.isAuthenticated());
  }
}

export const CALENDAR_ROUTE: Route = {
  path: 'calendar',
  component: CalendarComponent,
  data: {
    authorities: ['ROLE_USER', 'ROLE_ADMIN'],
    pageTitle: 'home.title'
  },
  canActivate: [IsAuthenticated]
};
