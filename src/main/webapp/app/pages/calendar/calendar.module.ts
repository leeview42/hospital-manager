import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HospitalManagerSharedModule } from 'app/shared';
import { HospitalManagerShiftModule } from 'app/entities/shift/shift.module';
import { CALENDAR_ROUTE, CalendarComponent, UserMonthViewComponent } from './';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    HospitalManagerSharedModule,
    HospitalManagerShiftModule,
    RouterModule.forChild([CALENDAR_ROUTE]),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    BrowserAnimationsModule
  ],
  declarations: [CalendarComponent, UserMonthViewComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalManagerCalendarModule {}
