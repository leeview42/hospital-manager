import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Account, AccountService, IUser } from 'app/core';
import * as moment from 'moment';
import { ShiftService } from 'app/entities/shift';
import { ShiftUpdatePopupComponent } from 'app/entities/shift/shift-update-popup.component';
import { ShiftWorkerService } from 'app/entities/shift-worker';
import { IShift } from 'app/shared/model/shift.model';
import {
  DATE_FORMAT,
  formatUserName,
  getDateAtEndOfMonth,
  getDateAtStartOfMonth,
  getPlanningBadgeColor,
  getUsersNames,
  MAX_ITEMS_PER_PAGE
} from 'app/shared';
import { isSameDay, isSameMonth } from 'date-fns';
import {
  CalendarDayViewComponent,
  CalendarEvent,
  CalendarEventTimesChangedEvent,
  CalendarMonthViewComponent,
  CalendarMonthViewDay,
  CalendarWeekViewComponent
} from 'angular-calendar';
import { Observable, Subject, Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ConfirmModalComponent } from 'app/shared/util/confirm-modal.component';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { calculateWeekNumbers } from 'app/shared/util/moment-util';
import { DayConfigService } from 'app/entities/day-config/day-config.service';
import { IDayConfig } from 'app/shared/model/day-config.model';
import { WorkerAvailabilityService } from 'app/entities/worker-availability';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IWorkerAvailability } from 'app/shared/model/worker-availability.model';
import { IUserAllocation, UserAllocation } from 'app/shared/model/user-allocation.model';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { select } from '@angular-redux/store';

enum CalendarView {
  Month = 'month',
  Week = 'week',
  Day = 'day',
  Users = 'users'
}

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'jhi-home',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.scss']
})
export class CalendarComponent implements OnInit, OnDestroy {
  account: Account;
  view: CalendarView = CalendarView.Month;
  headerView: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  users: IUser[] = [];
  userAllocations: IUserAllocation[] = null;
  workerAvailabilities: IWorkerAvailability[] = null;
  isManager = false;
  viewDate: Date = new Date();
  @ViewChild('calendarMonthView', { static: false }) calendarMonthView: CalendarMonthViewComponent;
  @ViewChild('calendarWeekView', { static: false }) calendarWeekView: CalendarWeekViewComponent;
  @ViewChild('calendarDayView', { static: false }) calendarDayView: CalendarDayViewComponent;
  activeDayIsOpen = true;
  fromDate: any;
  toDate: any;
  weekNumbers: number[] = [];
  refresh: Subject<any> = new Subject();
  @select(['team', 'users'])
  users$: Observable<IUser[]>;
  @select(['team', 'id'])
  teamId$: Observable<number>;
  @select(['team', 'isManager'])
  isTeamManager$: Observable<boolean>;
  usersSubscription: Subscription;
  teamIdSubscription: Subscription;
  isTeamManagerSubscription: Subscription;
  events: CalendarEvent[] = [];
  shifts: IShift[] = null;
  dayConfigs: IDayConfig[] = null;
  isDisabled: boolean = null;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private modalService: NgbModal,
    private shiftService: ShiftService,
    private dayConfigService: DayConfigService,
    private jhiAlertService: JhiAlertService,
    private shiftWorkerService: ShiftWorkerService,
    private workerAvailabilityService: WorkerAvailabilityService
  ) {}

  ngOnInit() {
    this.accountService.identity().then((account: Account) => {
      this.account = account;
    });
    this.teamIdSubscription = this.teamId$.subscribe(newValue => {
      this.loadData(this.viewDate);
    });
    this.isTeamManagerSubscription = this.isTeamManager$.subscribe(next => (this.isManager = next));
    this.refresh.subscribe(value => this.dataLoaded(value));
    this.loadUsers();
  }

  dataLoaded(value: string) {
    switch (value) {
      case 'shifts': {
        if (this.dayConfigs) {
          this.convertEventsResponse();
          this.createUserAllocation();
        }
        break;
      }
      case 'dayConfigs': {
        if (this.shifts) {
          this.convertEventsResponse();
        }
        break;
      }
      case 'users':
      case 'workerAvailability':
        this.createUserAllocation();
        break;
    }
  }

  public loadData(date: Date) {
    let start = moment(date).startOf('month');
    let end = moment(date).endOf('month');
    switch (this.view) {
      case CalendarView.Day:
        {
          start = moment(date).startOf('day');
          end = moment(date).endOf('day');
        }
        break;
      case CalendarView.Week:
        {
          start = moment(date).startOf('week');
          end = moment(date).endOf('week');
        }
        break;
    }
    this.fromDate = start;
    this.toDate = end;
    this.fetchShifts();
    this.fetchDayConfigs();
    this.fetchWorkerAvailability();
    if (this.view === CalendarView.Month && this.activeDayIsOpen === true) {
      this.dayClicked({ date: this.viewDate, events: this.events });
    }
  }

  fetchDayConfigs() {
    this.dayConfigService
      .findForInterval({
        fromDate: this.fromDate.format(DATE_FORMAT),
        toDate: this.toDate.format(DATE_FORMAT)
      })
      .subscribe(
        (res: HttpResponse<IDayConfig[]>) => {
          this.dayConfigs = res.body;
          this.refresh.next('dayConfigs');
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  fetchShifts() {
    this.shiftService
      .findForInterval({
        fromDate: this.fromDate.format(DATE_FORMAT),
        toDate: this.toDate.format(DATE_FORMAT)
      })
      .subscribe(response => {
        this.shifts = response.body;
        this.refresh.next('shifts');
      });
  }

  fetchWorkerAvailability() {
    this.workerAvailabilityService
      .query({
        size: MAX_ITEMS_PER_PAGE,
        'dateFrom.greaterOrEqualThan': moment(getDateAtStartOfMonth(this.viewDate)).toJSON(),
        'dateTo.lessOrEqualThan': moment(getDateAtEndOfMonth(this.viewDate)).toJSON()
      })
      .pipe(
        filter((mayBeOk: HttpResponse<IWorkerAvailability[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWorkerAvailability[]>) => response.body)
      )
      .subscribe(
        (res: IWorkerAvailability[]) => {
          this.workerAvailabilities = res;
          this.refresh.next('workerAvailability');
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadUsers() {
    this.usersSubscription = this.users$.subscribe(value => {
      this.users = this.convertUserNames(value);
      this.refresh.next('users');
    });
  }

  convertUserNames(res: IUser[]): IUser[] {
    return res
      .map(resUser => {
        resUser.name = formatUserName(resUser);
        return resUser;
      })
      .sort((a, b) => a.name.toUpperCase().localeCompare(b.name.toUpperCase()));
  }

  createUserAllocation() {
    if (this.workerAvailabilities && this.shifts) {
      this.userAllocations = [];
      this.users.forEach(user => {
        this.userAllocations.push(
          new UserAllocation(
            user,
            this.workerAvailabilities
              .filter(availability => availability.userId === user.id)
              .sort((a, b) => (a.dateFrom.isSameOrBefore(b.dateFrom) ? -1 : 1)),
            this.shifts
              .filter(shift => shift.users.map(shiftUser => shiftUser.id).indexOf(user.id) >= 0)
              .sort((a, b) => (moment(a.start).isSameOrBefore(moment(b.start)) ? -1 : 1))
          )
        );
      });
    }
  }

  convertEventsResponse() {
    this.events = [];
    if (this.shifts) {
      this.shifts.forEach(shift => this.events.push(this.convertShiftToEvent(shift)));
    }
  }

  public getNoOfWorkers(dayConfig: IDayConfig, shiftType: IShiftType): number {
    const dayConfigShiftType = !dayConfig
      ? null
      : dayConfig.dayConfigShiftTypes.find(dayCfgShiftType => dayCfgShiftType.shiftType.id === shiftType.id);
    if (dayConfig) {
      const result = dayConfig.dayConfigShiftTypes.find(dayCfgShiftType => dayCfgShiftType.shiftType.id === shiftType.id);
      if (result) {
        return result.noOfWorkers;
      }
    }

    return dayConfigShiftType ? dayConfigShiftType.noOfWorkers : shiftType.noOfWorkers ? shiftType.noOfWorkers : 0;
  }
  getDayConfig(shift: IShift): IDayConfig {
    return this.dayConfigs.find(dayConfig => shift.date === dayConfig.date);
  }

  convertShiftToEvent(shift: IShift): CalendarEvent {
    const userNames = getUsersNames(shift);
    const noOfWorkersAvailable = shift.users.length;
    const supposedNoOfWorkers = this.getNoOfWorkers(this.getDayConfig(shift), shift.shiftType);
    const start = moment(shift.start);
    const end = moment(shift.end);

    return {
      id: shift.id,
      title: `${start.format('HH.mm')} - ${end.format('HH.mm')} ${shift.shiftType.name}`,
      start: shift.start.toDate(),
      end: this.view === CalendarView.Month ? null : shift.end.toDate(),
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      color: colors.blue,
      meta: {
        numberAvailable: noOfWorkersAvailable,
        supposedWorkers: supposedNoOfWorkers,
        userNames,
        shiftName: shift.shiftType.name,
        users: shift.users
      }
    };
  }

  eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
    const start = moment(newStart);
    const end = moment(newEnd);

    const shift = this.getShiftById(Number(event.id));
    shift.start = start;
    shift.end = end;

    this.shiftService.updateWithoutDateConversion(shift).subscribe((res: HttpResponse<IShift>) => {
      this.events.splice(this.events.indexOf(event), 1);
      this.shifts.splice(this.shifts.indexOf(shift), 1);

      const newShift = res.body;
      const newEvent = {
        ...event,
        start: newShift.start.toDate(),
        end: newShift.end.toDate()
      };

      this.events.push(newEvent);
      this.shifts.push(newShift);
    });
  }

  confirmShiftsGeneration() {
    if (this.fromDate && this.toDate) {
      const modalRef = this.modalService.open(ConfirmModalComponent);
      modalRef.componentInstance.title = 'calendar.generate.title';
      modalRef.componentInstance.content = 'calendar.generate.question';
      modalRef.componentInstance.contentParams = {
        dateFrom: moment(this.fromDate).format('DD-MM-YYYY'),
        dateTo: moment(this.toDate).format('DD-MM-YYYY')
      };

      modalRef.result.then(result => {
        if (result === 'OK') {
          this.generateShifts();
        }
      });
    }
  }

  private generateShifts() {
    this.isDisabled = true;
    if (this.fromDate && this.toDate) {
      this.shiftWorkerService
        .generateShifts({
          fromDate: moment(this.fromDate.format(DATE_FORMAT))
            .utc(true)
            .toJSON(),
          toDate: this.toDate.utc(true).toJSON()
        })
        .subscribe(resp => {
          this.fetchShifts();
          this.isDisabled = false;
        });
    }
  }

  clear() {
    if (this.fromDate && this.toDate) {
      const modalRef = this.modalService.open(ConfirmModalComponent);
      modalRef.componentInstance.title = 'calendar.remove.all.title';
      modalRef.componentInstance.content = 'calendar.remove.all.questionDetailed';
      modalRef.componentInstance.contentParams = {
        dateFrom: moment(this.fromDate).format('DD-MM-YYYY'),
        dateTo: moment(this.toDate).format('DD-MM-YYYY')
      };

      modalRef.result.then(result => {
        if (result === 'OK') {
          this.shiftService
            .deleteForInterval({
              fromDate: this.fromDate.format(DATE_FORMAT),
              toDate: this.toDate.format(DATE_FORMAT)
            })
            .subscribe(resp => {
              this.events = [];
              this.refresh.next();
            });
        }
      });
    }
  }

  setView(view: CalendarView) {
    this.view = view;
    this.headerView = this.view;
    if (view === CalendarView.Users) {
      this.headerView = CalendarView.Month;
    }
    this.loadData(this.viewDate);
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0);
      this.viewDate = date;
    }
  }

  eventAction($event, actionType: string, event: CalendarEvent) {
    switch (actionType) {
      case 'Edit':
        this.editShiftEvent(event);
        break;
      case 'Remove':
        this.removeShiftEvent(event);
        break;
    }
    $event.stopPropagation();
  }

  editShiftEvent(event: CalendarEvent) {
    const eventId = Number(event.id);
    const shift = this.getShiftById(eventId);
    let ngbModalRef = this.modalService.open(ShiftUpdatePopupComponent as Component, { size: 'lg', backdrop: 'static' });
    ngbModalRef.componentInstance.shift = this.shifts.find(shiftIterator => shiftIterator.id === event.id);
    ngbModalRef.result.then(
      () => {},
      reason => {
        switch (reason[0]) {
          case 'cancel':
            break;
          case 'save':
            this.onShiftEditSuccess(reason[1]);
            break;
          case 'delete':
            this.onShiftDeleteSuccess(event, shift);
            break;
        }
        ngbModalRef = null;
      }
    );
  }

  removeShiftEvent(event: CalendarEvent) {
    const eventId = Number(event.id);
    const shift = this.getShiftById(eventId);
    const eventName = shift.shiftType.name;
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = 'calendar.remove.shift.title';
    modalRef.componentInstance.titleParams = {
      shiftName: eventName,
      workers: getUsersNames(shift),
      shiftStart: shift.start,
      shiftEnd: shift.end
    };
    modalRef.componentInstance.content = 'calendar.remove.shift.content';
    modalRef.result.then(result => {
      if (result === 'OK') {
        this.shiftService.delete(eventId).subscribe(() => {
          this.onShiftDeleteSuccess(event, shift);
        });
      }
    });
  }

  private onShiftEditSuccess(shift: IShift) {
    const index = this.shifts.findIndex(iterator => iterator.id === shift.id);
    this.shifts.splice(index, 1);
    this.shifts.push(shift);
    this.refresh.next('shifts');
  }

  private onShiftDeleteSuccess(event: CalendarEvent, shift: IShift) {
    this.shifts.splice(this.shifts.indexOf(shift), 1);
    this.refresh.next('shifts');
  }

  getShiftById(id: number): IShift {
    return this.shifts.find(shift => shift.id === id);
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    this.weekNumbers = calculateWeekNumbers(body.map(day => day.date));
  }

  getWeekNumbers(): string {
    let result = '';
    this.weekNumbers.forEach(weekNumber => {
      result += (result === '' ? '' : ', ') + weekNumber;
    });
    return result !== '' ? '(' + result + ')' : '';
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  public getEventBadgeColor(noOfUsers: number, actualNoOfUsers: number): string {
    return getPlanningBadgeColor(noOfUsers, actualNoOfUsers);
  }

  ngOnDestroy(): void {
    this.usersSubscription.unsubscribe();
    this.teamIdSubscription.unsubscribe();
    this.isTeamManagerSubscription.unsubscribe();
  }
}
