import { Component, Input, OnChanges, QueryList, SimpleChanges, ViewChildren, OnInit, OnDestroy } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import * as moment from 'moment';
import { IUserAllocation, UserAllocation } from 'app/shared/model/user-allocation.model';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { DATE_FORMAT, getPlanningBadgeColor } from 'app/shared';
import { CdkDropList } from '@angular/cdk/drag-drop';
import { IDayConfig } from 'app/shared/model/day-config.model';
import { IShift } from 'app/shared/model/shift.model';
import { ShiftService } from 'app/entities/shift';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { select } from '@angular-redux/store';
import { Observable, Subscription } from 'rxjs';
import { IUser } from 'app/core';

@Component({
  selector: 'user-month-view',
  templateUrl: './user.month.view.component.html',
  styleUrls: ['./user.month.view.component.scss']
})
export class UserMonthViewComponent implements OnChanges, OnInit, OnDestroy {
  @Input() public viewDate;
  @Input() public userAllocations: IUserAllocation[];
  @Input() public dayConfigs: IDayConfig[];
  @Input() public shifts: IShift[];
  dayConfigShiftTypes: any[] = [];
  show = false;
  filteredUserAllocations: IUserAllocation[] = [];
  allDataSource: any;
  datasource: any;
  userFilter = '';
  @ViewChildren(CdkDropList) cdkDrops: QueryList<CdkDropList>;
  drops: CdkDropList[] = [];
  @select(['user', 'id'])
  currentUserId$: Observable<number>;
  currentUserIdSubscription: Subscription;
  currentUserId: number;
  isManager = false;
  @select(['team', 'isManager'])
  isTeamManager$: Observable<boolean>;
  isTeamManagerSubscription: Subscription;
  selectedDayConfig: any = null;
  isShiftDeletionActive = false;
  shiftsSelectedForDeletion: any[] = [];

  constructor(protected jhiAlertService: JhiAlertService, protected shiftService: ShiftService) {}

  ngOnInit() {
    this.isTeamManagerSubscription = this.isTeamManager$.subscribe(next => (this.isManager = next));
    this.currentUserIdSubscription = this.currentUserId$.subscribe(next => (this.currentUserId = next));
  }

  ngOnDestroy() {
    this.isTeamManagerSubscription.unsubscribe();
    this.currentUserIdSubscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.resetPossibleActions();
    this.createTableDatasource();
    if (this.dayConfigs && this.dayConfigs.length > 0) {
      this.createDayConfigDataSource();
    }
  }

  private resetPossibleActions() {
    this.finishShiftsAssignment();
    this.resetShiftDeletion();
  }

  private resetShiftDeletion() {
    this.isShiftDeletionActive = false;
    this.shiftsSelectedForDeletion = [];
  }

  dayConfigClicked(newSelection: any) {
    if (this.dayConfigAlreadySelected(newSelection)) {
      this.finishShiftsAssignment();
    } else {
      this.selectedDayConfig = newSelection;
    }
  }

  getDayConfigFontSize(newSelection: any): string {
    return this.dayConfigAlreadySelected(newSelection) ? 'xxx-large' : '';
  }

  private dayConfigAlreadySelected(newSelection: any): boolean {
    return (
      !!this.selectedDayConfig &&
      this.selectedDayConfig.shiftType === newSelection.shiftType &&
      this.selectedDayConfig.day === newSelection.day
    );
  }

  public dayClicked(daySelection: any) {
    if (!this.selectedDayConfig || this.selectedDayConfig.day !== daySelection.day) {
      return;
    }
    const line = daySelection.line;

    if (
      -1 !== this.datasource[line][daySelection.day].shiftTypes.findIndex(shiftType => shiftType.id === this.selectedDayConfig.shiftType.id)
    ) {
      return;
    }

    const userAllocation: IUserAllocation = this.datasource[line][0];
    if (!userAllocation.user.shiftTypes.includes(this.selectedDayConfig.shiftType.id)) return;
    this.addUserToShift(userAllocation, daySelection.day, this.selectedDayConfig.shiftType).subscribe(
      (res: HttpResponse<IShift>) => {
        const shift = res.body;
        this.spliceShift(shift);
        this.shifts.push(shift);
        this.datasource[line][daySelection.day].shiftTypes.push(this.selectedDayConfig.shiftType);
        const dConfShiftType = this.dayConfigShiftTypes[daySelection.day].find(
          dayConfShiftType => dayConfShiftType.shiftType === this.selectedDayConfig.shiftType
        );
        if (dConfShiftType) {
          dConfShiftType.actualNoOfWorkers++;
        }
        const shiftIndex = userAllocation.shifts.findIndex(shiftIterator => shiftIterator.id === shift.id);
        if (shiftIndex === -1) {
          userAllocation.shifts.push(shift);
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  private spliceShift(shift: IShift) {
    const index = this.shifts.findIndex(shiftIterator => shiftIterator.id === shift.id);
    if (index !== -1) {
      this.shifts.splice(index, 1);
    }
  }

  finishShiftsAssignment() {
    this.selectedDayConfig = null;
  }

  shiftClicked(newSelection: any) {
    if (this.isShiftDeletionActive) {
      const shiftIndex = this.getShiftSelectedForDeletionIndex(newSelection);
      if (shiftIndex !== -1) {
        this.shiftsSelectedForDeletion.splice(shiftIndex, 1);
      } else {
        this.shiftsSelectedForDeletion.push(newSelection);
      }
    }
  }

  isShiftMarkedForDeletion(newSelection: any): boolean {
    return this.getShiftSelectedForDeletionIndex(newSelection) !== -1;
  }

  private getShiftSelectedForDeletionIndex(newSelection: any): number {
    return this.shiftsSelectedForDeletion.findIndex(
      shift => shift.shiftType === newSelection.shiftType && shift.line === newSelection.line && shift.day === newSelection.day
    );
  }

  public enableShiftDeletion(enable: boolean) {
    this.isShiftDeletionActive = enable;
    if (this.isShiftDeletionActive) {
      this.finishShiftsAssignment();
    } else {
      this.resetShiftDeletion();
    }
  }

  public deleteShifts() {
    if (this.isShiftDeletionActive) {
      this.shiftsSelectedForDeletion.forEach(selection => this.deleteShift(selection));
      this.resetShiftDeletion();
    }
  }

  private deleteShift(selection: any) {
    if (!this.isShiftDeletionActive) {
      return;
    }
    this.removeUserFromShift(this.datasource[selection.line][0], selection);
  }

  addUserToShift(userAllocation: IUserAllocation, day: number, shiftType: IShiftType): Observable<HttpResponse<IShift>> {
    let shift = this.findShiftForDayAndType(day, shiftType);
    if (shift) {
      shift = { ...shift };
      shift.users = [...shift.users];
      shift.users.push(userAllocation.user);
      shift.date = moment(shift.date);
      return this.shiftService.update(shift);
    } else {
      shift = this.shiftService.createNewShift(userAllocation.user, moment(this.viewDate).date(day), shiftType);
      return this.shiftService.create(shift);
    }
  }

  removeUserFromShift(userAllocation: IUserAllocation, selection: any) {
    let shift = this.findShiftByAllocationDayAndShiftType(userAllocation, selection.day, selection.shiftType);
    if (shift) {
      shift = { ...shift };
      shift.users = [...shift.users];
      shift.users = shift.users.filter(user => user.id !== userAllocation.user.id);
      shift.date = moment(shift.date);
      this.shiftService.update(shift).subscribe(
        res => {
          shift = res.body;
          this.spliceShift(shift);
          this.shifts.push(shift);
          const containerShiftTypes = this.datasource[selection.line][selection.day].shiftTypes;
          containerShiftTypes.splice(containerShiftTypes.indexOf(selection.shiftType), 1);
          const dConfShiftType = this.dayConfigShiftTypes[selection.day].find(
            dayConfigShiftType => dayConfigShiftType.shiftType.id === selection.shiftType.id
          );
          if (dConfShiftType) {
            dConfShiftType.actualNoOfWorkers--;
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    }
  }

  findShiftByAllocationDayAndShiftType(userAllocation: IUserAllocation, day: number, shiftType: IShiftType): IShift {
    return this.getShiftFromAllocation(day, userAllocation).find(shift => (shift.shiftType.id = shiftType.id));
  }

  findShiftForDayAndType(day: number, shiftType: IShiftType): IShift {
    if (day) {
      const compareDate = moment(this.viewDate)
        .date(day)
        .format(DATE_FORMAT);
      return this.shifts.find(shift => shift.shiftType.id === shiftType.id && shift.start.format(DATE_FORMAT) === compareDate);
    }
    return null;
  }

  createDayConfigDataSource() {
    this.dayConfigShiftTypes = [];
    this.dayConfigShiftTypes.push({});
    this.getDays().forEach(day => {
      const current = moment(this.viewDate).date(day);
      const shiftTypes = [];
      this.dayConfigs
        .filter(dayConfig => moment(dayConfig.date).isSame(current, 'day'))
        .forEach(dayConfig => {
          dayConfig.dayConfigShiftTypes.forEach(dConfST => {
            const shiftType = dConfST.shiftType;
            const noOfWorkers = dConfST.noOfWorkers;
            shiftTypes.push({
              shiftType,
              noOfWorkers,
              actualNoOfWorkers: this.findActualNoOfWorkers(day, shiftType)
            });
          });
        });
      this.dayConfigShiftTypes.push(shiftTypes);
    });
  }

  createTableDatasource() {
    if (this.userAllocations) {
      this.filteredUserAllocations = [...this.userAllocations];
      this.datasource = [];
      this.userAllocations.forEach(allocation => {
        const line = [];
        line.push(allocation);
        this.getDays().forEach(day => {
          line.push({
            holiday: this.hasHoliday(allocation, day),
            shiftTypes: this.getShiftFromAllocation(day, allocation).map(shift => shift.shiftType)
          });
        });
        this.datasource.push(line);
      });
      this.allDataSource = [...this.datasource];
    }
  }

  findActualNoOfWorkers(day: number, shiftType: IShiftType): number {
    let count = 0;
    if (day && shiftType && this.allDataSource) {
      this.allDataSource.forEach(userAllocArray => {
        if (userAllocArray[day] && userAllocArray[day].shiftTypes && userAllocArray[day].shiftTypes.length > 0) {
          count = count + userAllocArray[day].shiftTypes.filter(userShiftType => userShiftType.id === shiftType.id).length;
        }
      });
    }
    return count;
  }

  public getDays(): number[] {
    return Array.from(Array(moment(this.viewDate).daysInMonth()), (x, i) => i + 1);
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  public filterUsers() {
    this.datasource = this.allDataSource.filter(line => {
      const user = line[0].user;
      return `${user.firstName ? user.firstName : ''}${user.lastName ? user.lastName : ''}${user.email ? user.lastName : ''}`
        .toLowerCase()
        .includes(this.userFilter);
    });
  }

  public hasHoliday(allocation: UserAllocation, day: number): boolean {
    const compareDate = moment(this.viewDate).date(day);
    const idx = allocation.userAvailabilities.findIndex(availability => compareDate.isBetween(availability.dateFrom, availability.dateTo));
    return idx >= 0;
  }

  public isWeekend(day: number) {
    const dayOfWeek = moment(this.viewDate)
      .date(day)
      .toDate()
      .getDay();
    return [0, 6].indexOf(dayOfWeek) !== -1;
  }

  public isDayConfigPasteAllowed(day: number, currentUser: IUser): boolean {
    return (
      this.selectedDayConfig && this.selectedDayConfig.day === day && currentUser.shiftTypes.includes(this.selectedDayConfig.shiftType.id)
    );
  }

  public getShiftFromAllocation(day: number, allocation: UserAllocation): IShift[] {
    if (day && allocation && allocation.shifts) {
      const compareDate = moment(this.viewDate)
        .date(day)
        .format(DATE_FORMAT);
      return allocation.shifts.filter(shift => shift.date.toString() === compareDate);
    }
    return [];
  }

  public getDayConfigBadgeColor(dayConfigShiftType: any): string {
    return getPlanningBadgeColor(dayConfigShiftType.noOfWorkers, dayConfigShiftType.actualNoOfWorkers);
  }

  public getWorkerReport(dayConfigShiftType: any): string {
    return `${dayConfigShiftType.actualNoOfWorkers}/${dayConfigShiftType.noOfWorkers}`;
  }
}
