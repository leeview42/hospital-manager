import { NgModule } from '@angular/core';

import {
  HospitalmanagerSharedLibsModule,
  FindLanguageFromKeyPipe,
  JhiAlertComponent,
  JhiAlertErrorComponent,
  TrimTextPipe,
  FormatMillisToHoursMinutes,
  MonthAndYearSelectionComponent
} from './';
import { LoaderComponent } from 'app/shared/loader/loader.component';

@NgModule({
  imports: [HospitalmanagerSharedLibsModule],
  declarations: [
    FindLanguageFromKeyPipe,
    JhiAlertComponent,
    JhiAlertErrorComponent,
    LoaderComponent,
    MonthAndYearSelectionComponent,
    TrimTextPipe,
    FormatMillisToHoursMinutes
  ],
  exports: [
    HospitalmanagerSharedLibsModule,
    FindLanguageFromKeyPipe,
    JhiAlertComponent,
    JhiAlertErrorComponent,
    LoaderComponent,
    MonthAndYearSelectionComponent,
    TrimTextPipe,
    FormatMillisToHoursMinutes
  ]
})
export class HospitalManagerSharedCommonModule {}
