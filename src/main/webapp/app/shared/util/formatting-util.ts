import { IUser } from 'app/core';
import { Moment } from 'moment';
import { IShift } from 'app/shared/model/shift.model';

export const getUsersNames = (shift: IShift): string => {
  if (shift.users && shift.users.length > 0) {
    let names = '';
    shift.users.forEach(user => (names += `, ${user.firstName} ${user.lastName}`));
    return names.substring(2, names.length);
  }
  return '';
};

export const formatUserName = (user: IUser): string => {
  return `${user.firstName ? user.firstName + ' ' : ''}
      ${user.lastName ? user.lastName + ' ' : ''}
      ${user.email ? '(' + user.email + ')' : ''}`;
};

export const formatUsersFirstAndLastNames = (users: IUser[]): string => {
  return users
    .map(user => formatUserFirstAndLastNames(user))
    .sort()
    .reduce((accumulator, current) => accumulator + ', ' + current);
};

export const formatUserFirstAndLastNames = (user: IUser): string => {
  return `${user.firstName ? user.firstName + ' ' : ''}${user.lastName ? user.lastName : ''}`;
};

export const stringifyDates = (dates: Moment[]): string => {
  return dates
    .sort()
    .map(date => date.toString())
    .reduce((accumulator, current) => accumulator + ', ' + current);
};

export const getPlanningBadgeColor = (noOfUsers: number, actualNoOfUsers: number): string => {
  let badgeColor = 'badge-secondary';
  if (noOfUsers !== null && actualNoOfUsers !== null && noOfUsers !== undefined && actualNoOfUsers !== undefined) {
    if (noOfUsers === actualNoOfUsers) {
      badgeColor = 'badge-success';
    } else if (actualNoOfUsers > noOfUsers) {
      badgeColor = 'badge-info';
    } else if (actualNoOfUsers < noOfUsers && actualNoOfUsers !== 0) {
      badgeColor = 'badge-warning';
    } else if (actualNoOfUsers === 0) {
      badgeColor = 'badge-danger';
    }
  }
  return badgeColor;
};

export const compareAlphabetical = (a: string, b: string): number => {
  if (a && b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  }
  return 0;
};
