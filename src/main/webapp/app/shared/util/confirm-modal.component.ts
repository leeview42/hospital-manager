import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-modal-confirm',
  template: `
    <div class="modal-header">
      <h6 class="modal-title" id="modal-title" jhiTranslate="{{ title }}" [translateValues]="titleParams">Title</h6>
      <button type="button" class="close" aria-describedby="modal-title" (click)="modal.dismiss('dismiss')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>
        <strong jhiTranslate="{{ content }}" [translateValues]="contentParams"> Content </strong>
      </p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal" (click)="modal.dismiss('cancel click')">
        <fa-icon [icon]="'ban'"></fa-icon>&nbsp;<span jhiTranslate="entity.action.cancel">Cancel</span>
      </button>
      <button id="jhi-confirm-delete-invoice" type="button" class="btn btn-success" (click)="modal.close('OK')">
        <fa-icon [icon]="'check'"></fa-icon>&nbsp;<span>Yes</span>
      </button>
    </div>
  `
})
export class ConfirmModalComponent {
  @Input() public title;
  @Input() public titleParams;
  @Input() public content;
  @Input() public contentParams;

  constructor(public modal: NgbActiveModal) {}
}
