import { Moment } from 'moment';
import * as moment from 'moment';

export const calculateWeekNumbers = (days: Date[]): number[] => {
  let minDay = null;
  let maxDay = null;

  days.forEach(day => {
    const dayMoment = moment(day);

    minDay = getEarlierMoment(minDay, dayMoment);
    maxDay = getLaterMoment(maxDay, dayMoment);
  });

  return this.calculate(minDay, maxDay);
};

export const calculate = (startDay: Moment, endDay: Moment): number[] => {
  const weekNumbers = [];
  if (startDay && endDay && startDay.isBefore(endDay)) {
    let dateIterator = startDay.clone();
    while (dateIterator.isBefore(endDay)) {
      weekNumbers.push(dateIterator.week());
      dateIterator = dateIterator.add(7, 'days');
    }
  }
  return weekNumbers;
};

export const getEarlierMoment = (first: Moment, second: Moment): Moment => {
  return first && second ? (first.isBefore(second) ? first : second) : first ? first : second ? second : null;
};

export const getLaterMoment = (first: Moment, second: Moment): Moment => {
  return first && second ? (first.isAfter(second) ? first : second) : first ? first : second ? second : null;
};

export const getDateAtStartOfMonth = (date: Date): Date => {
  if (date) {
    return new Date(date.getFullYear(), date.getMonth(), 1);
  }
  return null;
};

export const getDateAtEndOfMonth = (date: Date): Date => {
  if (date) {
    return new Date(date.getFullYear(), date.getMonth() + 1, 0);
  }
  return null;
};

export const momentToUtc = (mom: Moment): Moment => {
  return mom != null ? moment(mom).utc() : null;
};
