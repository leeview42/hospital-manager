import { IMultiSelectSettings } from 'angular-2-dropdown-multiselect';

export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';
export const DEFAULT_MULTI_SELECT_SETTINGS: IMultiSelectSettings = {
  enableSearch: true,
  checkedStyle: 'checkboxes',
  buttonClasses: 'btn btn-block btn-outline-primary',
  dynamicTitleMaxItems: 5,
  showCheckAll: true,
  showUncheckAll: true,
  displayAllSelectedText: true
};
