import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HospitalmanagerSharedLibsModule, HospitalManagerSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { ConfirmModalComponent } from 'app/shared/util/confirm-modal.component';

@NgModule({
  imports: [HospitalmanagerSharedLibsModule, HospitalManagerSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective, ConfirmModalComponent],
  entryComponents: [JhiLoginModalComponent, ConfirmModalComponent],
  exports: [HospitalManagerSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HospitalManagerSharedModule {
  static forRoot() {
    return {
      ngModule: HospitalManagerSharedModule
    };
  }
}
