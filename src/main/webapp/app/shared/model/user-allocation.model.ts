import { IUser } from 'app/core';
import { IWorkerAvailability } from 'app/shared/model/worker-availability.model';
import { IShift } from 'app/shared/model/shift.model';

export interface IUserAllocation {
  user?: IUser;
  userAvailabilities?: IWorkerAvailability[];
  shifts?: IShift[];
}

export class UserAllocation implements IUserAllocation {
  constructor(public user?: IUser, public userAvailabilities?: IWorkerAvailability[], public shifts?: IShift[]) {}
}
