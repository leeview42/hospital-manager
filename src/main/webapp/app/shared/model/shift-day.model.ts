export interface IShiftDay {
  id?: number;
  dayOfWeek?: number;
  shiftTypeId?: number;
}

export class ShiftDay implements IShiftDay {
  constructor(public id?: number, public dayOfWeek?: number, public shiftTypeId?: number) {}
}
