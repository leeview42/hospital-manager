import { IUser } from 'app/core';
import { IShift } from 'app/shared/model/shift.model';

export interface IShiftWorker {
  id?: number;
  shiftId?: number;
  workerId?: number;
  worker?: IUser;
  shift?: IShift;
}

export class ShiftWorker implements IShiftWorker {
  constructor(public id?: number, public shiftId?: number, public workerId?: number, public worker?: IUser, public shift?: IShift) {}
}
