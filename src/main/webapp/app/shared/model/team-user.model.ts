import { ITeam } from './team.model';

export interface ITeamUser {
  id?: number;
  isManager?: boolean;
  teamId?: number;
  team?: ITeam;
  userId?: number;
}

export class TeamUser implements ITeamUser {
  constructor(public id?: number, public isManager?: boolean, public teamId?: number, team?: ITeam, public userId?: number) {
    this.isManager = this.isManager || false;
  }
}
