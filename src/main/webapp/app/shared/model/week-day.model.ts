export interface IWeekDay {
  id?: number;
  name?: string;
}
