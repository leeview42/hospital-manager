export interface IShiftTypeOverride {
  id?: number;
  shiftTypeId?: number;
  canOverrideId?: number;
}

export class ShiftTypeOverride implements IShiftTypeOverride {
  constructor(public id?: number, public shiftTypeId?: number, public canOverrideId?: number) {}
}
