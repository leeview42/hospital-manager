export interface IShiftType {
  id?: number;
  name?: string;
  code?: string;
  active?: boolean;
  description?: string;
  canRepeat?: boolean;
  startHour?: number;
  startMinute?: number;
  duration?: number;
  durationMillis?: number;
  fillWithRemaining?: boolean;
  noOfWorkers?: number;
  priorityId?: number;
  priorityCode?: string;
  displayCode?: string;
  mandatory?: boolean;
}

export class ShiftType implements IShiftType {
  constructor(
    public id?: number,
    public name?: string,
    public code?: string,
    public active?: boolean,
    public description?: string,
    public canRepeat?: boolean,
    public startHour?: number,
    public startMinute?: number,
    public duration?: number,
    public durationMillis?: number,
    public fillWithRemaining?: boolean,
    public noOfWorkers?: number,
    public priorityId?: number,
    public displayCode?: string,
    public priorityCode?: string,
    public mandatory?: boolean
  ) {
    this.active = this.active || true;
    this.canRepeat = this.canRepeat || true;
    this.fillWithRemaining = this.fillWithRemaining || false;
    this.mandatory = this.mandatory || false;
  }
}
