import { Moment } from 'moment';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { IUser } from 'app/core';

export interface IShift {
  id?: number;
  date?: Moment;
  start?: Moment;
  end?: Moment;
  updateDate?: Moment;
  createDate?: Moment;
  generated?: boolean;
  shiftTypeId?: number;
  createdById?: number;
  updatedById?: number;
  shiftType?: IShiftType;
  users?: IUser[];
  durationMillis?: number;
}

export class Shift implements IShift {
  constructor(
    public id?: number,
    public date?: Moment,
    public start?: Moment,
    public end?: Moment,
    public updateDate?: Moment,
    public createDate?: Moment,
    public generated?: boolean,
    public shiftTypeId?: number,
    public createdById?: number,
    public updatedById?: number,
    public shiftType?: IShiftType,
    public users?: IUser[],
    public durationMillis?: number
  ) {
    this.generated = this.generated || false;
  }
}
