export interface IOrganization {
  id?: number;
  name?: string;
  description?: string;
  hasSpecializations?: boolean;
}

export class Organization implements IOrganization {
  constructor(public id?: number, public name?: string, public description?: string, public hasSpecializations?: boolean) {}
}
