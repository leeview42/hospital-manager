import { IDayConfig } from 'app/shared/model/day-config.model';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { ISpecialization, Specialization } from './specialization.model';

export interface IDayConfigShiftTypeSpec {
  id?: number;
  noOfWorkers?: number;
  specializationId?: number;
  specialization?: ISpecialization;
}

export class DayConfigShiftTypeSpec implements IDayConfigShiftTypeSpec {
  constructor(public id?: number, public noOfWorkers?: number, public specializationId?: number, public specialization?: ISpecialization) {}
}
