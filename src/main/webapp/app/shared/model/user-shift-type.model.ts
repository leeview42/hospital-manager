export interface IUserShiftType {
  id?: number;
  ratioPercentage?: number;
  userId?: number;
  shiftTypeId?: number;
}

export class UserShiftType implements IUserShiftType {
  constructor(public id?: number, public ratioPercentage?: number, public userId?: number, public shiftTypeId?: number) {}
}
