export interface ISpecialization {
  id?: number;
  name?: string;
  description?: string;
  organizationId?: number;
  organizationName?: string;
}

export class Specialization implements ISpecialization {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public organizationId?: number,
    public organizationName?: string
  ) {}
}
