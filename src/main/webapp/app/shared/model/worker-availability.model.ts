import { Moment } from 'moment';

export interface IWorkerAvailability {
  id?: number;
  dateFrom?: Moment;
  dateTo?: Moment;
  isAvailable?: boolean;
  userId?: number;
}

export class WorkerAvailability implements IWorkerAvailability {
  constructor(public id?: number, public dateFrom?: Moment, public dateTo?: Moment, public isAvailable?: boolean, public userId?: number) {
    this.isAvailable = this.isAvailable || false;
  }
}
