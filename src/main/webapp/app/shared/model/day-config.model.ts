import { Moment } from 'moment';
import { IDayConfigShiftType } from 'app/shared/model/day-config-shift-type.model';

export interface IDayConfig {
  id?: number;
  date?: Moment;
  isBankHoliday?: boolean;
  dayConfigShiftTypes?: IDayConfigShiftType[];
}

export class DayConfig implements IDayConfig {
  constructor(
    public id?: number,
    public date?: Moment,
    public isBankHoliday?: boolean,
    public dayConfigShiftTypes?: IDayConfigShiftType[]
  ) {
    this.isBankHoliday = this.isBankHoliday || false;
  }
}
