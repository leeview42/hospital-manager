import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

export class MultiSelectOptionModel implements IMultiSelectOption {
  constructor(
    public id: number,
    public name: string,
    public disabled?: boolean,
    public isLabel?: boolean,
    public parentId?: any,
    public params?: any,
    public classes?: string,
    public image?: string
  ) {}
}
