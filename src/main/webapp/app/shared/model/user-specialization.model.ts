export interface IUserSpecialization {
  id?: number;
  userId?: number;
  specializationId?: number;
}

export class UserSpecialization implements IUserSpecialization {
  constructor(public id?: number, public userId?: number, public specializationId?: number) {}
}
