import { IUser } from 'app/core';
import { IShiftType } from 'app/shared/model/shift-type.model';

export interface ITeam {
  id?: number;
  name?: string;
  description?: string;
  organizationId?: number;
  members?: number[];
  managers?: number[];
  users?: IUser[];
  shiftTypes?: IShiftType[];
  maximumWorkingWeekends?: number;
  minimumFreeDaysAfterNightShifts?: number;
}

export class Team implements ITeam {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public organizationId?: number,
    public members?: number[],
    public managers?: number[],
    public users?: IUser[],
    public shiftTypes?: IShiftType[],
    public maximumWorkingWeekends?: number,
    public minimumFreeDaysAfterNightShifts?: number
  ) {}
}
