export interface IPriority {
  id?: number;
  code?: string;
  description?: string;
}

export class Priority implements IPriority {
  constructor(public id?: number, public code?: string, public description?: string) {}
}
