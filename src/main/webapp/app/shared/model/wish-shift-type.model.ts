export interface IWishShiftType {
  id?: number;
  wishId?: number;
  shiftTypeId?: number;
}

export class WishShiftType implements IWishShiftType {
  constructor(public id?: number, public wishId?: number, public shiftTypeId?: number) {}
}
