import { IDayConfig } from 'app/shared/model/day-config.model';
import { IShiftType } from 'app/shared/model/shift-type.model';
import { ISpecialization, Specialization } from './specialization.model';
import { IDayConfigShiftTypeSpec } from 'app/shared/model/day-config-shift-type-spec.model';

export interface IDayConfigShiftType {
  id?: number;
  noOfWorkers?: number;
  dayConfigId?: number;
  shiftTypeId?: number;
  dayConfig?: IDayConfig;
  shiftType?: IShiftType;
  specializations?: IDayConfigShiftTypeSpec[];
}

export class DayConfigShiftType implements IDayConfigShiftType {
  constructor(
    public id?: number,
    public noOfWorkers?: number,
    public dayConfigId?: number,
    public shiftTypeId?: number,
    public dayConfig?: IDayConfig,
    public shiftType?: IShiftType,
    public specializations?: IDayConfigShiftTypeSpec[]
  ) {}
}
