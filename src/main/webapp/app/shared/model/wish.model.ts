import { Moment } from 'moment';
import { IUser } from 'app/core';
import { IShiftType } from 'app/shared/model/shift-type.model';

export const enum WishType {
  ONLY = 'ONLY',
  EXCEPT = 'EXCEPT'
}

export interface IWish {
  id?: number;
  comment?: string;
  dateFrom?: Moment;
  dateTo?: Moment;
  wishType?: WishType;
  userId?: number;
  user?: IUser;
  shiftTypes?: number[];
  date?: Moment;
}

export class Wish implements IWish {
  constructor(
    public id?: number,
    public comment?: string,
    public dateFrom?: Moment,
    public dateTo?: Moment,
    public wishType?: WishType,
    public userId?: number,
    public user?: IUser,
    public shiftTypes?: number[],
    public date?: Moment
  ) {}
}
