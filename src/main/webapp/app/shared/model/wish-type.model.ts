export interface IWishType {
  id?: number;
  name?: string;
  description?: string;
}

export class WishType implements IWishType {
  constructor(public id?: number, public name?: string, public description?: string) {}
}
