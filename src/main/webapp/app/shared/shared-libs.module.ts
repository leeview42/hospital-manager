import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgJhipsterModule } from 'ng-jhipster';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CookieModule } from 'ngx-cookie';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgReduxModule } from '@angular-redux/store';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    NgbModule,
    InfiniteScrollModule,
    CookieModule.forRoot(),
    FontAwesomeModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    DragDropModule,
    NgReduxModule,
    NgSelectModule
  ],
  exports: [
    FormsModule,
    CommonModule,
    NgbModule,
    NgJhipsterModule,
    InfiniteScrollModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    DragDropModule,
    NgSelectModule
  ]
})
export class HospitalmanagerSharedLibsModule {
  static forRoot() {
    return {
      ngModule: HospitalmanagerSharedLibsModule
    };
  }
}
