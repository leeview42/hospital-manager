import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoaderService } from './loader.service';
import { JhiAlertService } from 'ng-jhipster';

@Injectable({
  providedIn: 'root'
})
export class LoaderInterceptorService implements HttpInterceptor {
  requestTimeMillis: number;
  requests = [];
  isShowing = false;

  constructor(private loaderService: LoaderService, protected jhiAlertService: JhiAlertService) {
    this.requestTimeMillis = 20;
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const reqTime = new Date().getTime();
    this.showLoader(reqTime);
    return next.handle(req).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            this.onEnd(reqTime);
          }
        },
        (err: any) => {
          this.showError(reqTime);
        }
      )
    );
  }
  private onEnd(reqTime): void {
    this.hideLoader(reqTime);
  }
  private showLoader(reqTime: number): void {
    this.requests.push(reqTime);
    setTimeout(() => {
      const currentTime = new Date().getTime();
      const reqIndex = this.requests.indexOf(reqTime);
      if (reqIndex > -1) {
        if (currentTime - reqTime > this.requestTimeMillis) {
          this.loaderService.show();
          this.isShowing = true;
          setTimeout(() => {
            this.showError(reqTime);
          }, 60000);
        }
      }
    }, this.requestTimeMillis);
  }

  private showError(reqTime: any) {
    if (this.requests.indexOf(reqTime) > -1) {
      this.requests.splice(this.requests.indexOf(reqTime));
      this.loaderService.hide();
      this.isShowing = false;
      this.jhiAlertService.error('error.timeout');
    }
  }

  private hideLoader(reqTime: number): void {
    const reqIndex = this.requests.indexOf(reqTime);
    if (reqIndex > -1) {
      this.requests.splice(reqIndex);
      if (this.requests.length === 0) {
        this.loaderService.hide();
        this.isShowing = false;
      }
    }
  }
}
