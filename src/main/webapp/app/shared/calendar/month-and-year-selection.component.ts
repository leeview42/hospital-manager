import { Component, OnInit, Input, Output } from '@angular/core';
import * as moment from 'moment';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'month-and-year-selection',
  templateUrl: './month-and-year-selection.component.html'
})
export class MonthAndYearSelectionComponent implements OnInit {
  @select(['translate', 'months'])
  months$: Observable<any[]>;

  years = [];
  selectedYear = moment().year();
  selectedMonth = moment().month();

  viewDateMoment: any;

  @Output() viewDateChanged = new EventEmitter();

  ngOnInit(): void {
    this.initYears();
  }

  initYears() {
    for (let i = 0; i < 5; i++) {
      this.years.push(this.selectedYear - i);
    }
  }

  onSelectionChange() {
    this.viewDateMoment = this.viewDateMoment.year(this.selectedYear);
    this.viewDateMoment = this.viewDateMoment.month(this.selectedMonth);
    this.viewDateChanged.emit(this.viewDateMoment.toDate());
  }

  @Input() set viewDate(value: string) {
    this.viewDateMoment = moment(value);
    this.selectedYear = this.viewDateMoment.year();
    this.selectedMonth = this.viewDateMoment.month();
  }
}
