import { Pipe, PipeTransform } from '@angular/core';

const DEFAULT_REPLACE = '...';

@Pipe({
  name: 'trim'
})
export class TrimTextPipe implements PipeTransform {
  transform(text: string, maxLength?: number, replace?: string): string {
    if (maxLength && text) {
      if (text.length > maxLength) {
        return `${text.substr(0, maxLength - 1)}${replace ? replace : DEFAULT_REPLACE}`;
      }
    }
    return text;
  }
}
