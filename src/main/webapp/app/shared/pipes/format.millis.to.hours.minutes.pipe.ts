import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatMillisToHourMinutes'
})
export class FormatMillisToHoursMinutes implements PipeTransform {
  transform(durationMillis: number): string {
    const hours = Math.floor(durationMillis / 3600000);
    const minutes = Math.floor((durationMillis % 3600000) / 1000 / 60);
    return (hours !== 0 ? `${hours}h` : '') + (minutes !== 0 ? ` ${minutes}m` : '');
  }
}
