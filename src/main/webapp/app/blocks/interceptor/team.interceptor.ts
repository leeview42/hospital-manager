import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from 'app/redux/store';

@Injectable()
export class TeamInterceptor implements HttpInterceptor {
  teamId: any;

  constructor(private ngRedux: NgRedux<IAppState>) {
    ngRedux.select(['team', 'id']).subscribe(value => (this.teamId = value));
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.teamId) {
      const clonedRequest = request.clone({
        params: request.params.append('teamId', `${this.teamId}`)
      });

      return next.handle(clonedRequest);
    }
    return next.handle(request);
  }
}
