package com.hr.hpital.manager.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Profile

@Profile("rm")
@ConfigurationProperties(prefix = "rm", ignoreUnknownFields = true)
class RmProperties {
    var organizationId: Long? = null
    var dataExpirationHours: Long? = null
}
