package com.hr.hpital.manager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Hospitalmanager.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private Integer weekendStartHour;

    private Integer weekendStartMinute;

    private Integer minimumPauseHours;

    private Integer sqlBatchSizeForInConditions = 100;

    public void setWeekendStartHour(Integer weekendStartHour) {
        this.weekendStartHour = weekendStartHour;
    }

    public void setWeekendStartMinute(Integer weekendStartMinute) {
        this.weekendStartMinute = weekendStartMinute;
    }

    public Integer getWeekendStartHour() {
        return weekendStartHour;
    }

    public Integer getWeekendStartMinute() {
        return weekendStartMinute;
    }

    public Integer getMinimumPauseHours() {
        return minimumPauseHours;
    }

    public void setMinimumPauseHours(Integer minimumPauseHours) {
        this.minimumPauseHours = minimumPauseHours;
    }

    public Integer getSqlBatchSizeForInConditions() {
        return sqlBatchSizeForInConditions;
    }

    public void setSqlBatchSizeForInConditions(Integer sqlBatchSizeForInConditions) {
        this.sqlBatchSizeForInConditions = sqlBatchSizeForInConditions;
    }
}
