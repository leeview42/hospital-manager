package com.hr.hpital.manager.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ShiftWorker.
 */
@Entity
@Table(name = "shift_worker")
public class ShiftWorker implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("shiftWorkers")
    private Shift shift;

    @ManyToOne
    @JsonIgnoreProperties("shiftWorkers")
    private User worker;

    public ShiftWorker() {
    }

    public ShiftWorker(Shift shift, User worker) {
        this.shift = shift;
        this.worker = worker;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Shift getShift() {
        return shift;
    }

    public ShiftWorker shift(Shift shift) {
        this.shift = shift;
        return this;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public User getWorker() {
        return worker;
    }

    public ShiftWorker worker(User user) {
        this.worker = user;
        return this;
    }

    public void setWorker(User user) {
        this.worker = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShiftWorker)) {
            return false;
        }
        return id != null && id.equals(((ShiftWorker) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (shift != null ? shift.hashCode() : 0);
        result = 31 * result + (worker != null ? worker.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ShiftWorker{" +
            "id=" + getId() +
            "}";
    }
}
