package com.hr.hpital.manager.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ShiftDay.
 */
@Entity
@Table(name = "shift_day")
public class ShiftDay implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "day_of_week")
    private Integer dayOfWeek;

    @ManyToOne
    @JsonIgnoreProperties("shiftDays")
    private ShiftType shiftType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public ShiftDay dayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public ShiftDay shiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
        return this;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShiftDay)) {
            return false;
        }
        return id != null && id.equals(((ShiftDay) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dayOfWeek != null ? dayOfWeek.hashCode() : 0);
        result = 31 * result + (shiftType != null ? shiftType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ShiftDay{" +
            "id=" + getId() +
            ", dayOfWeek=" + getDayOfWeek() +
            "}";
    }
}
