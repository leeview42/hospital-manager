package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DayConfigShiftType.
 */
@Entity
@Table(name = "day_config_shift_type")
public class DayConfigShiftType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "no_of_workers")
    private Integer noOfWorkers;

    @ManyToOne
    private DayConfig dayConfig;

    @ManyToOne
    private ShiftType shiftType;

    @JsonIgnore
    @OneToMany(mappedBy = "dayConfigShiftType", cascade = CascadeType.ALL)
    private Set<DayConfigShiftTypeSpec> specializations = new HashSet<>();

    public DayConfigShiftType() {
    }

    public DayConfigShiftType(Integer noOfWorkers, ShiftType shiftType) {
        this.noOfWorkers = noOfWorkers;
        this.shiftType = shiftType;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfWorkers() {
        return noOfWorkers;
    }

    public DayConfigShiftType noOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
        return this;
    }

    public void setNoOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public DayConfig getDayConfig() {
        return dayConfig;
    }

    public DayConfigShiftType dayConfig(DayConfig dayConfig) {
        this.dayConfig = dayConfig;
        return this;
    }

    public void setDayConfig(DayConfig dayConfig) {
        this.dayConfig = dayConfig;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public DayConfigShiftType shiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
        return this;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }

    public Set<DayConfigShiftTypeSpec> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(Set<DayConfigShiftTypeSpec> specializations) {
        this.specializations = specializations;
    }

    public DayConfigShiftType specializations(Set<DayConfigShiftTypeSpec> specializations) {
        setSpecializations(specializations);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DayConfigShiftType that = (DayConfigShiftType) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(noOfWorkers, that.noOfWorkers) &&
            Objects.equals(dayConfig, that.dayConfig) &&
            Objects.equals(shiftType, that.shiftType) &&
            Objects.equals(specializations, that.specializations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, noOfWorkers, dayConfig, shiftType, specializations);
    }
}
