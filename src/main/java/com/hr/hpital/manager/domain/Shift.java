package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

/**
 * A Shift.
 */
@Entity
@Table(name = "shift")
public class Shift implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private Instant start;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private Instant end;

    @Column(name = "update_date")
    private Instant updateDate;

    @Column(name = "create_date")
    private Instant createDate;

    @Column(name = "generated")
    private Boolean generated;

    @ManyToOne
    @JsonIgnoreProperties("shifts")
    private ShiftType shiftType;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "shift_worker",
        joinColumns = {@JoinColumn(name = "shift_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "worker_id", referencedColumnName = "id")})
    private Set<User> users = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("shifts")
    private User createdBy;

    @ManyToOne
    @JsonIgnoreProperties("shifts")
    private User updatedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getStart() {
        return start;
    }

    public void setStart(Instant start) {
        this.start = start;
    }

    public Shift start(Instant start) {
        this.start = start;
        return this;
    }

    public Instant getEnd() {
        return end;
    }

    public void setEnd(Instant end) {
        this.end = end;
    }

    public Shift end(Instant end) {
        this.end = end;
        return this;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public Shift updateDate(Instant updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public Instant getCreateDate() {
        return createDate;
    }

    public Shift createDate(Instant createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }

    public Boolean isGenerated() {
        return generated;
    }

    public Shift generated(Boolean generated) {
        this.generated = generated;
        return this;
    }

    public void setGenerated(Boolean generated) {
        this.generated = generated;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public Shift shiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
        return this;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public Shift createdBy(User user) {
        this.createdBy = user;
        return this;
    }

    public void setCreatedBy(User user) {
        this.createdBy = user;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Shift updatedBy(User user) {
        this.updatedBy = user;
        return this;
    }

    public void setUpdatedBy(User user) {
        this.updatedBy = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shift)) {
            return false;
        }
        return id != null && id.equals(((Shift) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (generated != null ? generated.hashCode() : 0);
        result = 31 * result + (shiftType != null ? shiftType.hashCode() : 0);
        result = 31 * result + (users != null ? users.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Shift{" +
            "id=" + getId() +
            ", start='" + getStart() + "'" +
            ", end='" + getEnd() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", generated='" + isGenerated() + "'" +
            "}";
    }
}
