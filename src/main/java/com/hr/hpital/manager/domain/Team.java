package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Team.
 */
@Entity
@Table(name = "team")
public class Team implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column
    private Long externalId;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumn(updatable = false)
    @JsonIgnoreProperties("teams")
    private Organization organization;

    @Column(name = "import_date")
    private Instant importDate = null;

    public Team(@NotNull String name, Long externalId, Organization org) {
        this.name = name;
        this.externalId = externalId;
        this.organization = org;
    }

    public Team() {
    }

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "team_user",
        joinColumns = {@JoinColumn(name = "team_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    private Set<User> users = new HashSet<>();

    @OneToMany(mappedBy = "team")
    private Set<ShiftType> shiftTypes;

    @NotNull
    @Min(0)
    @Max(5)
    @Column(name = "maximum_working_weekends", nullable = false)
    private Integer maximumWorkingWeekends = 5;

    @NotNull
    @Min(0)
    @Column(name = "minimum_free_days_after_night_shifts", nullable = false)
    private Integer minimumFreeDaysAfterNightShifts = 0;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Team name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Team description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Team organization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Team users(Set<User> users) {
        this.users = users;
        return this;
    }

    public Set<ShiftType> getShiftTypes() {
        return shiftTypes;
    }

    public void setShiftTypes(Set<ShiftType> shiftTypes) {
        this.shiftTypes = shiftTypes;
    }

    public Integer getMaximumWorkingWeekends() {
        return maximumWorkingWeekends;
    }

    public void setMaximumWorkingWeekends(Integer maximumWorkingWeekends) {
        this.maximumWorkingWeekends = maximumWorkingWeekends;
    }

    public Integer getMinimumFreeDaysAfterNightShifts() {
        return minimumFreeDaysAfterNightShifts;
    }

    public void setMinimumFreeDaysAfterNightShifts(Integer minimumFreeDaysAfterNightShifts) {
        this.minimumFreeDaysAfterNightShifts = minimumFreeDaysAfterNightShifts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Team)) {
            return false;
        }
        return id != null && id.equals(((Team) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Team{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public Instant getImportDate() {
        return importDate;
    }

    public void setImportDate(Instant importDate) {
        this.importDate = importDate;
    }
}
