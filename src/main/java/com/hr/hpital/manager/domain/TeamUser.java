package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A TeamUser.
 */
@Entity
@Table(name = "team_user")
public class TeamUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "is_manager", nullable = false)
    private Boolean isManager;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("teamUsers")
    private Team team;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("teamUsers")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsManager() {
        return isManager;
    }

    public TeamUser isManager(Boolean isManager) {
        this.isManager = isManager;
        return this;
    }

    public void setIsManager(Boolean isManager) {
        this.isManager = isManager;
    }

    public Team getTeam() {
        return team;
    }

    public TeamUser team(Team team) {
        this.team = team;
        return this;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public User getUser() {
        return user;
    }

    public TeamUser worker(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TeamUser)) {
            return false;
        }
        return id != null && id.equals(((TeamUser) o).id);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (isManager != null ? isManager.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TeamUser{" +
            "id=" + getId() +
            ", isManager='" + isIsManager() + "'" +
            "}";
    }
}
