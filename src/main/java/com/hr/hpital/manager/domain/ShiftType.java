package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A ShiftType.
 */
@Entity
@Table(name = "shift_type")
public class ShiftType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "description")
    private String description;

    private String displayCode;

    @Column(name = "can_repeat")
    private Boolean canRepeat;

    @Min(value = 0)
    @Max(value = 23)
    @Column(name = "start_hour")
    private Integer startHour;

    @Min(value = 0)
    @Max(value = 59)
    @Column(name = "start_minute")
    private Integer startMinute;

    @Column(name = "duration")
    private Duration duration;

    @Column(name = "fill_with_remaining")
    private Boolean fillWithRemaining;

    @Column(name = "no_of_workers")
    private Integer noOfWorkers;

    @ManyToOne
    @JsonIgnoreProperties("shiftTypes")
    private Priority priority;

    @Column(name = "mandatory")
    private Boolean mandatory;

    @OneToMany(mappedBy = "shiftType")
    private List<ShiftTypeOverride> shiftTypeOverrides;

    @OneToMany(mappedBy = "canOverride")
    private List<ShiftTypeOverride> canBeOverriden;


    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("shiftTypes")
    private Team team;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ShiftType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public ShiftType code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean isActive() {
        return active;
    }

    public ShiftType active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public ShiftType description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isCanRepeat() {
        return canRepeat;
    }

    public ShiftType canRepeat(Boolean canRepeat) {
        this.canRepeat = canRepeat;
        return this;
    }

    public void setCanRepeat(Boolean canRepeat) {
        this.canRepeat = canRepeat;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public ShiftType startHour(Integer startHour) {
        this.startHour = startHour;
        return this;
    }

    public void setStartHour(Integer startHour) {
        this.startHour = startHour;
    }

    public Integer getStartMinute() {
        return startMinute;
    }

    public ShiftType startMinute(Integer startMinute) {
        this.startMinute = startMinute;
        return this;
    }

    public void setStartMinute(Integer startMinute) {
        this.startMinute = startMinute;
    }

    public Duration getDuration() {
        return duration;
    }

    public ShiftType duration(Duration duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Boolean isFillWithRemaining() {
        return fillWithRemaining;
    }

    public ShiftType fillWithRemaining(Boolean fillWithRemaining) {
        this.fillWithRemaining = fillWithRemaining;
        return this;
    }

    public void setFillWithRemaining(Boolean fillWithRemaining) {
        this.fillWithRemaining = fillWithRemaining;
    }

    public Integer getNoOfWorkers() {
        return noOfWorkers;
    }

    public ShiftType noOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
        return this;
    }

    public void setNoOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public Priority getPriority() {
        return priority;
    }

    public ShiftType priority(Priority priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Boolean isMandatory() {
        return mandatory;
    }

    public ShiftType mandatory(Boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public ShiftType fillMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public List<ShiftTypeOverride> getShiftTypeOverrides() {
        return shiftTypeOverrides;
    }

    public void setShiftTypeOverrides(List<ShiftTypeOverride> shiftTypeOverrides) {
        this.shiftTypeOverrides = shiftTypeOverrides;
    }

    public List<ShiftTypeOverride> getCanBeOverriden() {
        return canBeOverriden;
    }

    public void setCanBeOverriden(List<ShiftTypeOverride> canBeOverriden) {
        this.canBeOverriden = canBeOverriden;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public ShiftType team(Team team) {
        this.team = team;
        return this;
    }

    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShiftType)) {
            return false;
        }
        return id != null && id.equals(((ShiftType) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code, active, description, canRepeat, startHour, startMinute, duration, fillWithRemaining, noOfWorkers, priority, mandatory);
    }

    @Override
    public String toString() {
        return "ShiftType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", active='" + isActive() + "'" +
            ", description='" + getDescription() + "'" +
            ", canRepeat='" + isCanRepeat() + "'" +
            ", startHour=" + getStartHour() +
            ", startMinute=" + getStartMinute() +
            ", duration='" + getDuration() + "'" +
            ", fillWithRemaining='" + isFillWithRemaining() + "'" +
            ", noOfWorkers=" + getNoOfWorkers() +
            ", mandatory='" + isMandatory() +
            "}";
    }
}
