package com.hr.hpital.manager.domain.enumeration;

/**
 * The WishType enumeration.
 */
public enum WishType {
    ONLY, EXCEPT
}
