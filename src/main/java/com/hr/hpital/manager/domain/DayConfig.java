package com.hr.hpital.manager.domain;



import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * A DayConfig.
 */
@Entity
@Table(name = "day_config")
public class DayConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_date")
    private LocalDate date;

    @Column(name = "is_bank_holiday")
    private Boolean isBankHoliday;

    @OneToMany(mappedBy = "dayConfig", cascade = CascadeType.REMOVE)
    List<DayConfigShiftType> dayConfigShiftTypes;

    @ManyToOne(optional = false)
    @NotNull
    private Team team;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public DayConfig date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Boolean isIsBankHoliday() {
        return isBankHoliday;
    }

    public DayConfig isBankHoliday(Boolean isBankHoliday) {
        this.isBankHoliday = isBankHoliday;
        return this;
    }

    public void setIsBankHoliday(Boolean isBankHoliday) {
        this.isBankHoliday = isBankHoliday;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<DayConfigShiftType> getDayConfigShiftTypes() {
        return dayConfigShiftTypes;
    }

    public void setDayConfigShiftTypes(List<DayConfigShiftType> shiftTypes) {
        this.dayConfigShiftTypes = shiftTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DayConfig)) {
            return false;
        }
        return id != null && id.equals(((DayConfig) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (isBankHoliday != null ? isBankHoliday.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DayConfig{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", isBankHoliday='" + isIsBankHoliday() + "'" +
            "}";
    }
}
