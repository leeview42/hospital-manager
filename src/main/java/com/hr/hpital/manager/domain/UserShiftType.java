package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A UserShiftType.
 */
@Entity
@Table(name = "user_shift_type")
public class UserShiftType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "ratio_percentage")
    private Integer ratioPercentage;

    @ManyToOne
    @JsonIgnoreProperties("userShiftTypes")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("userShiftTypes")
    private ShiftType shiftType;

    public UserShiftType() {
    }

    public UserShiftType(User user, ShiftType shiftType) {
        this.user = user;
        this.shiftType = shiftType;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRatioPercentage() {
        return ratioPercentage;
    }

    public UserShiftType ratioPercentage(Integer ratioPercentage) {
        this.ratioPercentage = ratioPercentage;
        return this;
    }

    public void setRatioPercentage(Integer ratioPercentage) {
        this.ratioPercentage = ratioPercentage;
    }

    public User getUser() {
        return user;
    }

    public UserShiftType user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public UserShiftType shiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
        return this;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserShiftType)) {
            return false;
        }
        return id != null && id.equals(((UserShiftType) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ratioPercentage != null ? ratioPercentage.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (shiftType != null ? shiftType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserShiftType{" +
            "id=" + getId() +
            ", ratioPercentage=" + getRatioPercentage() +
            "}";
    }
}
