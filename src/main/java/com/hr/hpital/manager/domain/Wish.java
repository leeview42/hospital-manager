package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import com.hr.hpital.manager.domain.enumeration.WishType;
import org.hibernate.annotations.BatchSize;

/**
 * A Wish.
 */
@Entity
@Table(name = "wish")
public class Wish implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_comment")
    private String comment;

    @Column(name = "date_from")
    private Instant dateFrom;

    @Column(name = "date_to")
    private Instant dateTo;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "wish_type", nullable = false)
    private WishType wishType;

    @ManyToOne
    @JsonIgnoreProperties("wishes")
    private User user;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "wish_shift_type",
        joinColumns = {@JoinColumn(name = "wish_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "shift_type_id", referencedColumnName = "id")})
    @BatchSize(size = 20)
    private Set<ShiftType> shiftTypes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public Wish comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getDateFrom() {
        return dateFrom;
    }

    public Wish dateFrom(Instant dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public void setDateFrom(Instant dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Instant getDateTo() {
        return dateTo;
    }

    public Wish dateTo(Instant dateTo) {
        this.dateTo = dateTo;
        return this;
    }

    public void setDateTo(Instant dateTo) {
        this.dateTo = dateTo;
    }

    public WishType getWishType() {
        return wishType;
    }

    public Wish wishType(WishType wishType) {
        this.wishType = wishType;
        return this;
    }

    public void setWishType(WishType wishType) {
        this.wishType = wishType;
    }

    public User getUser() {
        return user;
    }

    public Wish user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<ShiftType> getShiftTypes() {
        return shiftTypes;
    }

    public void setShiftTypes(Set<ShiftType> shiftTypes) {
        this.shiftTypes = shiftTypes;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Wish)) {
            return false;
        }
        return id != null && id.equals(((Wish) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (dateFrom != null ? dateFrom.hashCode() : 0);
        result = 31 * result + (dateTo != null ? dateTo.hashCode() : 0);
        result = 31 * result + (wishType != null ? wishType.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (shiftTypes != null ? shiftTypes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Wish{" +
            "id=" + getId() +
            ", comment='" + getComment() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateTo='" + getDateTo() + "'" +
            ", wishType='" + getWishType() + "'" +
            "}";
    }
}
