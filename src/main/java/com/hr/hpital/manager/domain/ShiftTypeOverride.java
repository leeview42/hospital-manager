package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ShiftTypeOverride.
 */
@Entity
@Table(name = "shift_type_override")
public class ShiftTypeOverride implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("shiftTypeOverrides")
    private ShiftType shiftType;

    @ManyToOne
    @JsonIgnoreProperties("shiftTypeOverrides")
    private ShiftType canOverride;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public ShiftTypeOverride shiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
        return this;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }

    public ShiftType getCanOverride() {
        return canOverride;
    }

    public ShiftTypeOverride canOverride(ShiftType shiftType) {
        this.canOverride = shiftType;
        return this;
    }

    public void setCanOverride(ShiftType shiftType) {
        this.canOverride = shiftType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShiftTypeOverride)) {
            return false;
        }
        return id != null && id.equals(((ShiftTypeOverride) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (shiftType != null ? shiftType.hashCode() : 0);
        result = 31 * result + (canOverride != null ? canOverride.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ShiftTypeOverride{" +
            "id=" + getId() +
            "}";
    }
}
