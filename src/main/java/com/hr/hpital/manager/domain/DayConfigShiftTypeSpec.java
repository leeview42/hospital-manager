package com.hr.hpital.manager.domain;


import javax.persistence.*;
import java.io.Serializable;

/**
 * A DayConfigShiftType.
 */
@Entity
@Table(name = "day_config_shift_type_specialization")
public class DayConfigShiftTypeSpec implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "no_of_workers")
    private Integer noOfWorkers;

    @ManyToOne
    private DayConfigShiftType dayConfigShiftType;

    @ManyToOne
    private Specialization specialization;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfWorkers() {
        return noOfWorkers;
    }

    public void setNoOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public DayConfigShiftTypeSpec noOfWorkers(Integer noOfWorkers) {
        setNoOfWorkers(noOfWorkers);
        return this;
    }

    public DayConfigShiftType getDayConfigShiftType() {
        return dayConfigShiftType;
    }

    public void setDayConfigShiftType(DayConfigShiftType dayConfigShiftType) {
        this.dayConfigShiftType = dayConfigShiftType;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public DayConfigShiftTypeSpec specialization(Specialization specialization) {
        setSpecialization(specialization);
        return this;
    }
}
