package com.hr.hpital.manager.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A WishShiftType.
 */
@Entity
@Table(name = "wish_shift_type")
public class WishShiftType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("wishShiftTypes")
    private Wish wish;

    @ManyToOne
    @JsonIgnoreProperties("wishShiftTypes")
    private ShiftType shiftType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Wish getWish() {
        return wish;
    }

    public WishShiftType wish(Wish wish) {
        this.wish = wish;
        return this;
    }

    public void setWish(Wish wish) {
        this.wish = wish;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public WishShiftType shiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
        return this;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WishShiftType)) {
            return false;
        }
        return id != null && id.equals(((WishShiftType) o).id);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (wish != null ? wish.hashCode() : 0);
        result = 31 * result + (shiftType != null ? shiftType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WishShiftType{" +
            "id=" + getId() +
            "}";
    }
}
