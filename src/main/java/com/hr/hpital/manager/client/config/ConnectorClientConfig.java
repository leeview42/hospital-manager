package com.hr.hpital.manager.client.config;

import com.hr.hpital.manager.config.ConnectorProperties;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("rm")
@Configuration
public class ConnectorClientConfig {

    private final ConnectorProperties connectorProperties;

    public ConnectorClientConfig(ConnectorProperties connectorProperties) {
        this.connectorProperties = connectorProperties;
    }

    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor(connectorProperties.getUsername(), connectorProperties.getPassword());
    }
}
