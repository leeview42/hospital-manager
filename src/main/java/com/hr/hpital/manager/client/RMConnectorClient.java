package com.hr.hpital.manager.client;

import com.hr.hpital.manager.client.config.ConnectorClientConfig;
import com.hr.hpital.manager.service.dto.RMUserDTO;
import com.hr.hpital.manager.service.dto.TimeSheetGroupDTO;
import com.hr.hpital.manager.service.dto.TimeSheetGroupEmployeeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Profile("rm")
@FeignClient(name="connectorClient", url = "${connector.url}", configuration = {ConnectorClientConfig.class})
public interface RMConnectorClient {
    @GetMapping("/api/user/{login}")
    Optional<RMUserDTO> getConnectorUser(@PathVariable("login") String login);

    @GetMapping("/api/user")
    Optional<RMUserDTO> getConnectorUserById(@RequestParam("userId") Long id);

    @GetMapping("/api/user/timekeepers")
    List<Long> getTimekeeperUserIds(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date);

    @GetMapping("/api/timeSheetGroups")
    List<TimeSheetGroupDTO> getTimeSheetGroupsForUser(@RequestParam("userId") Long id);

    @GetMapping("/api/timeSheetGroups/employees")
    Set<TimeSheetGroupEmployeeDTO> getTimeSheetGroupEmployeesForUser(@RequestParam("userId") Long id, @RequestParam("groupId") Long groupId);
}
