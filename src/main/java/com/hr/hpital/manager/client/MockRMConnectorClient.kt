package com.hr.hpital.manager.client

import com.hr.hpital.manager.service.dto.RMUserDTO
import com.hr.hpital.manager.service.dto.TimeSheetGroupDTO
import com.hr.hpital.manager.service.dto.TimeSheetGroupEmployeeDTO
import java.util.*
import kotlin.collections.ArrayList

class MockRMConnectorClient : RMConnectorClient {
    override fun getConnectorUser(login: String): Optional<RMUserDTO> {
        var rmUserDTO = RMUserDTO()
        when (login) {
            "manager" -> rmUserDTO.id = 1;
            else -> rmUserDTO.id = login.hashCode().toLong()
        }
        rmUserDTO.uniquePassword = "test"
        rmUserDTO.userName = "User $login"
        rmUserDTO.user = login
        rmUserDTO.domainUserAccount = login
        rmUserDTO.blocked = false
        rmUserDTO.status = true
        return Optional.of(rmUserDTO)
    }

    override fun getConnectorUserById(id: Long): Optional<RMUserDTO> {
        return Optional.empty()
    }

    override fun getTimekeeperUserIds(date: Date): List<Long> {
        return ArrayList()
    }

    override fun getTimeSheetGroupsForUser(id: Long): List<TimeSheetGroupDTO> {
        var timeSheetGroups = ArrayList<TimeSheetGroupDTO>()
        if (id.equals(1)) {
            timeSheetGroups.add(TimeSheetGroupDTO(1, "test group 1"))
            timeSheetGroups.add(TimeSheetGroupDTO(2, "test group 2"))
        }
        return timeSheetGroups
    }

    override fun getTimeSheetGroupEmployeesForUser(id: Long, groupId: Long): Set<TimeSheetGroupEmployeeDTO> {
        var timeSheetGroupEmployees = HashSet<TimeSheetGroupEmployeeDTO>()
        if(id.equals(1)) {
            when (groupId) {
                1L -> {
                    timeSheetGroupEmployees.add(this.getConnectorUser("test1").get() as TimeSheetGroupEmployeeDTO)
                    timeSheetGroupEmployees.add(this.getConnectorUser("test2").get() as TimeSheetGroupEmployeeDTO)
                }
                1L -> {
                    timeSheetGroupEmployees.add(this.getConnectorUser("test3").get() as TimeSheetGroupEmployeeDTO)
                    timeSheetGroupEmployees.add(this.getConnectorUser("test4").get() as TimeSheetGroupEmployeeDTO)
                }
            }

        }
        return timeSheetGroupEmployees
    }
}
