package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.repository.WishRepository;
import com.hr.hpital.manager.service.dto.WishCriteria;
import com.hr.hpital.manager.service.dto.WishDTO;
import com.hr.hpital.manager.service.mapper.WishMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link Wish} entities in the database.
 * The main input is a {@link WishCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WishDTO} or a {@link Page} of {@link WishDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WishQueryService extends QueryService<Wish> {

    private final Logger log = LoggerFactory.getLogger(WishQueryService.class);

    private final WishRepository wishRepository;

    private final WishMapper wishMapper;

    public WishQueryService(WishRepository wishRepository, WishMapper wishMapper) {
        this.wishRepository = wishRepository;
        this.wishMapper = wishMapper;
    }

    /**
     * Return a {@link List} of {@link WishDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WishDTO> findByCriteria(WishCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Wish> specification = createSpecification(criteria);
        return wishMapper.toDto(wishRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link WishDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WishDTO> findByCriteria(WishCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Wish> specification = createSpecification(criteria);
        return wishRepository.findAll(specification, page)
            .map(wishMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(WishCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Wish> specification = createSpecification(criteria);
        return wishRepository.count(specification);
    }

    /**
     * Function to convert WishCriteria to a {@link Specification}.
     */
    private Specification<Wish> createSpecification(WishCriteria criteria) {
        Specification<Wish> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Wish_.id));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), Wish_.comment));
            }
            if (criteria.getDateFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateFrom(), Wish_.dateFrom));
            }
            if (criteria.getDateTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateTo(), Wish_.dateTo));
            }
            if (criteria.getWishType() != null) {
                specification = specification.and(buildSpecification(criteria.getWishType(), Wish_.wishType));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(
                    criteria.getUserId(),
                    root -> root.join(Wish_.user, JoinType.INNER).get(User_.id)
                    )
                );
            }

            if (criteria.getTeamIdFilter() != null) {
                specification = specification.and(
                    buildSpecification(criteria.getTeamIdFilter(), root -> root.join(Wish_.shiftTypes, JoinType.INNER).join(ShiftType_.team).get(Team_.id))
                );
            }
        }
        return specification;
    }
}
