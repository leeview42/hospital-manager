package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.repository.UserRepository;
import com.hr.hpital.manager.security.SecurityUtils;
import com.hr.hpital.manager.service.OrganizationService;
import com.hr.hpital.manager.domain.Organization;
import com.hr.hpital.manager.repository.OrganizationRepository;
import com.hr.hpital.manager.service.dto.OrganizationDTO;
import com.hr.hpital.manager.service.mapper.OrganizationMapper;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Organization}.
 */
@Service
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    private final Logger log = LoggerFactory.getLogger(OrganizationServiceImpl.class);

    private final OrganizationRepository organizationRepository;

    private final UserRepository userRepository;

    private final OrganizationMapper organizationMapper;

    public OrganizationServiceImpl(OrganizationRepository organizationRepository,
                                   UserRepository userRepository,
                                   OrganizationMapper organizationMapper) {
        this.organizationRepository = organizationRepository;
        this.userRepository = userRepository;
        this.organizationMapper = organizationMapper;
    }

    /**
     * Save a organization.
     *
     * @param organizationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganizationDTO save(OrganizationDTO organizationDTO) {
        log.debug("Request to save Organization : {}", organizationDTO);
        Organization organization = organizationMapper.toEntity(organizationDTO);
        organization = organizationRepository.save(organization);
        return organizationMapper.toDto(organization);
    }

    /**
     * Get all the organizations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganizationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Organizations");
        return organizationRepository.findAll(pageable)
            .map(organizationMapper::toDto);
    }


    /**
     * Get one organization by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganizationDTO> findOne(Long id) {
        log.debug("Request to get Organization : {}", id);
        return organizationRepository.findById(id)
            .map(organizationMapper::toDto);
    }

    @Override
    public Optional<OrganizationDTO> findOneByUserLogin() {
        log.debug("Request to get Organization by user login");

        Optional<String> currentUserLogin = SecurityUtils.getCurrentUserLogin();

        if (!currentUserLogin.isPresent()) {
            throw new BadRequestAlertException("Cannot find logged-in username", "User", "usernameNotLoggedIn");
        }

        Optional<User> currentUser = userRepository.findOneByLogin(currentUserLogin.get());

        if (!currentUser.isPresent()) {
            throw new BadRequestAlertException("Cannot find logged-in user", "User", "userNotLoggedIn");
        }

        Organization organization = currentUser.get().getOrganization();
        return Optional.ofNullable(organizationMapper.toDto(organization));
    }

    /**
     * Delete the organization by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Organization : {}", id);
        organizationRepository.deleteById(id);
    }
}
