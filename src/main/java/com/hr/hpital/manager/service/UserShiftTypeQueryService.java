package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.ShiftType_;
import com.hr.hpital.manager.domain.UserShiftType;
import com.hr.hpital.manager.domain.UserShiftType_;
import com.hr.hpital.manager.domain.User_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.repository.UserShiftTypeRepository;
import com.hr.hpital.manager.service.dto.UserShiftTypeCriteria;
import com.hr.hpital.manager.service.dto.UserShiftTypeDTO;
import com.hr.hpital.manager.service.mapper.UserShiftTypeMapper;

/**
 * Service for executing complex queries for {@link UserShiftType} entities in the database.
 * The main input is a {@link UserShiftTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserShiftTypeDTO} or a {@link Page} of {@link UserShiftTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserShiftTypeQueryService extends QueryService<UserShiftType> {

    private final Logger log = LoggerFactory.getLogger(UserShiftTypeQueryService.class);

    private final UserShiftTypeRepository userShiftTypeRepository;

    private final UserShiftTypeMapper userShiftTypeMapper;

    public UserShiftTypeQueryService(UserShiftTypeRepository userShiftTypeRepository, UserShiftTypeMapper userShiftTypeMapper) {
        this.userShiftTypeRepository = userShiftTypeRepository;
        this.userShiftTypeMapper = userShiftTypeMapper;
    }

    /**
     * Return a {@link List} of {@link UserShiftTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserShiftTypeDTO> findByCriteria(UserShiftTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserShiftType> specification = createSpecification(criteria);
        return userShiftTypeMapper.toDto(userShiftTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserShiftTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserShiftTypeDTO> findByCriteria(UserShiftTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserShiftType> specification = createSpecification(criteria);
        return userShiftTypeRepository.findAll(specification, page)
            .map(userShiftTypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserShiftTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserShiftType> specification = createSpecification(criteria);
        return userShiftTypeRepository.count(specification);
    }

    /**
     * Function to convert UserShiftTypeCriteria to a {@link Specification}.
     */
    private Specification<UserShiftType> createSpecification(UserShiftTypeCriteria criteria) {
        Specification<UserShiftType> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserShiftType_.id));
            }
            if (criteria.getRatioPercentage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRatioPercentage(), UserShiftType_.ratioPercentage));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(UserShiftType_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getShiftTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getShiftTypeId(),
                    root -> root.join(UserShiftType_.shiftType, JoinType.LEFT).get(ShiftType_.id)));
            }
        }
        return specification;
    }
}
