package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.Authority;
import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.Specialization;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.service.dto.UserDTO;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link User} and its DTO called {@link UserDTO}.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class UserMapper {

    private final OrganizationMapper organizationMapper;

    public UserMapper(OrganizationMapper organizationMapper) {
        this.organizationMapper = organizationMapper;
    }

    public List<UserDTO> usersToUserDTOs(List<User> users) {
        return users.stream()
            .filter(Objects::nonNull)
            .map(this::userToUserDTO)
            .collect(Collectors.toList());
    }

    public UserDTO userToUserDTO(User user) {
        UserDTO dto = new UserDTO(user);
        dto.setShiftTypes(user.getShiftTypes().stream().map(ShiftType::getId).collect(Collectors.toSet()));
        dto.setSpecializations(user.getSpecializations().stream().map(Specialization::getId).collect(Collectors.toSet()));
        if (user.getOrganization() != null) {
            dto.setOrganization(organizationMapper.toDto(user.getOrganization()));
            dto.setOrganizationId(user.getOrganization().getId());
        }
        return dto;
    }

    public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
        return userDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::userDTOToUser)
            .collect(Collectors.toList());
    }

    public User userDTOToUser(UserDTO userDTO) {
        if (userDTO == null) {
            return null;
        } else {
            User user = new User();
            user.setId(userDTO.getId());
            user.setLogin(userDTO.getLogin());
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setEmail(userDTO.getEmail());
            user.setImageUrl(userDTO.getImageUrl());
            user.setActivated(userDTO.isActivated());
            user.setLangKey(userDTO.getLangKey());
            user.setExternalId(userDTO.getExternalId());
            Set<Authority> authorities = this.authoritiesFromStrings(userDTO.getAuthorities());
            user.setAuthorities(authorities);
            user.setOrganization(organizationMapper.fromId(userDTO.getOrganizationId()));
            return user;
        }
    }


    private Set<Authority> authoritiesFromStrings(Set<String> authoritiesAsString) {
        Set<Authority> authorities = new HashSet<>();

        if(authoritiesAsString != null){
            authorities = authoritiesAsString.stream().map(string -> {
                Authority auth = new Authority();
                auth.setName(string);
                return auth;
            }).collect(Collectors.toSet());
        }

        return authorities;
    }

    public User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
