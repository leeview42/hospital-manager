package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.DayConfig;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link DayConfig} entity.
 */
public class DayConfigDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate date;

    private Boolean isBankHoliday;

    private Long teamId;

    private List<DayConfigShiftTypeDTO> dayConfigShiftTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Boolean isIsBankHoliday() {
        return isBankHoliday;
    }

    public void setIsBankHoliday(Boolean isBankHoliday) {
        this.isBankHoliday = isBankHoliday;
    }

    public List<DayConfigShiftTypeDTO> getDayConfigShiftTypes() {
        return dayConfigShiftTypes;
    }

    public void setDayConfigShiftTypes(List<DayConfigShiftTypeDTO> dayConfigShiftTypes) {
        this.dayConfigShiftTypes = dayConfigShiftTypes;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DayConfigDTO dayConfigDTO = (DayConfigDTO) o;
        if (dayConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dayConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DayConfigDTO{" +
            "id=" + id +
            ", date=" + date +
            ", isBankHoliday=" + isBankHoliday +
            ", teamId=" + teamId +
            '}';
    }
}
