package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DayConfigShiftType} and its DTO {@link DayConfigShiftTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {DayConfigMapper.class, ShiftTypeMapper.class, DayConfigShiftTypeSpecMapper.class})
public interface DayConfigShiftTypeMapper extends EntityMapper<DayConfigShiftTypeDTO, DayConfigShiftType> {

    @Mapping(source = "dayConfig.id", target = "dayConfigId")
    @Mapping(source = "shiftType.id", target = "shiftTypeId")
    DayConfigShiftTypeDTO toDto(DayConfigShiftType dayConfigShiftType);

    @Mapping(source = "dayConfigId", target = "dayConfig")
    @Mapping(source = "shiftTypeId", target = "shiftType")
    DayConfigShiftType toEntity(DayConfigShiftTypeDTO dayConfigShiftTypeDTO);

    default DayConfigShiftType fromId(Long id) {
        if (id == null) {
            return null;
        }
        DayConfigShiftType dayConfigShiftType = new DayConfigShiftType();
        dayConfigShiftType.setId(id);
        return dayConfigShiftType;
    }
}
