package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.domain.UserSpecialization;
import com.hr.hpital.manager.domain.*; // for static metamodels
import com.hr.hpital.manager.repository.UserSpecializationRepository;
import com.hr.hpital.manager.service.dto.UserSpecializationCriteria;
import com.hr.hpital.manager.service.dto.UserSpecializationDTO;
import com.hr.hpital.manager.service.mapper.UserSpecializationMapper;

/**
 * Service for executing complex queries for {@link UserSpecialization} entities in the database.
 * The main input is a {@link UserSpecializationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserSpecializationDTO} or a {@link Page} of {@link UserSpecializationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserSpecializationQueryService extends QueryService<UserSpecialization> {

    private final Logger log = LoggerFactory.getLogger(UserSpecializationQueryService.class);

    private final UserSpecializationRepository userSpecializationRepository;

    private final UserSpecializationMapper userSpecializationMapper;

    public UserSpecializationQueryService(UserSpecializationRepository userSpecializationRepository, UserSpecializationMapper userSpecializationMapper) {
        this.userSpecializationRepository = userSpecializationRepository;
        this.userSpecializationMapper = userSpecializationMapper;
    }

    /**
     * Return a {@link List} of {@link UserSpecializationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserSpecializationDTO> findByCriteria(UserSpecializationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserSpecialization> specification = createSpecification(criteria);
        return userSpecializationMapper.toDto(userSpecializationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserSpecializationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserSpecializationDTO> findByCriteria(UserSpecializationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserSpecialization> specification = createSpecification(criteria);
        return userSpecializationRepository.findAll(specification, page)
            .map(userSpecializationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserSpecializationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserSpecialization> specification = createSpecification(criteria);
        return userSpecializationRepository.count(specification);
    }

    /**
     * Function to convert UserSpecializationCriteria to a {@link Specification}.
     */
    private Specification<UserSpecialization> createSpecification(UserSpecializationCriteria criteria) {
        Specification<UserSpecialization> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserSpecialization_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(UserSpecialization_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getSpecializationId() != null) {
                specification = specification.and(buildSpecification(criteria.getSpecializationId(),
                    root -> root.join(UserSpecialization_.specialization, JoinType.LEFT).get(Specialization_.id)));
            }
        }
        return specification;
    }
}
