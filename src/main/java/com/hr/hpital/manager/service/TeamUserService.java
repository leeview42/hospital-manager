package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.TeamDTO;
import com.hr.hpital.manager.service.dto.TeamUserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.Set;

/**
 * Service Interface for managing {@link com.hr.hpital.manager.domain.TeamUser}.
 */
public interface TeamUserService {

    /**
     * Save a teamUser.
     *
     * @param teamUserDTO the entity to save.
     * @return the persisted entity.
     */
    TeamUserDTO save(TeamUserDTO teamUserDTO);

    /**
     * Get all the teamUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TeamUserDTO> findAll(Pageable pageable);

    /**
     * Get the "id" teamUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TeamUserDTO> findOne(Long id);

    /**
     * Delete the "id" teamUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void updateTeamManagers(Long teamId, Set<Long> managerIds);

    void makeTeamManager(Long teamId, Long managerIds);

    Set<Long> getTeamMembers(Long teamId);

    Set<Long> getTeamManagers(Long teamId);

    TeamDTO mapTeamMembers(TeamDTO teamDTO);

    /**
     * Checks if a user is member of a team
     *
     * @param userId user's id
     * @param teamId team's id
     * @return true in case it user is part of the team, false otherwise
     */
    boolean userIsTeamMember(Long userId, Long teamId);

    /**
     * Checks if a user is member of a team and is also one of the team's managers
     *
     * @param userId user's id
     * @param teamId team's id
     * @return true in case it user is part of the team and is manager, false otherwise
     */
    boolean userIsTeamManager(Long userId, Long teamId);
}
