package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.service.dto.OrganizationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Organization} and its DTO {@link OrganizationDTO}.
 */
@Mapper(componentModel = "spring", uses = {SpecializationMapper.class})
public interface OrganizationMapper extends EntityMapper<OrganizationDTO, Organization> {

    default Organization fromId(Long id) {
        if (id == null) {
            return null;
        }
        Organization organization = new Organization();
        organization.setId(id);
        return organization;
    }
}
