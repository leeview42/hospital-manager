package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.Shift;
import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.service.dto.ShiftDTO;
import com.hr.hpital.manager.web.rest.errors.WorkersAlreadyAssignedException;
import com.hr.hpital.manager.web.rest.errors.WorkersUnavailableException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Shift}.
 */
public interface ShiftService {

    /**
     * Save a shift.
     *
     * @param shiftDTO the entity to save.
     * @return the persisted entity.
     */
    ShiftDTO save(ShiftDTO shiftDTO) throws WorkersUnavailableException, WorkersAlreadyAssignedException;

    ShiftDTO update(ShiftDTO shiftDto) throws WorkersUnavailableException, WorkersAlreadyAssignedException;

    Shift create(ShiftType shiftType, LocalDate date, boolean isGenerated);

    /**
     * Get all the shifts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShiftDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shift.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShiftDTO> findOne(Long id);

    @Transactional(readOnly = true)
    List<ShiftDTO> findForInterval(Instant fromDate, Instant toDate, Long teamId);

    /**
     * Delete the "id" shift.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void deleteAndGenerateShiftsForInterval(Instant from, Instant to, Long teamId);

    void deleteGeneratedShiftsForInterval(Instant from, Instant to, Long teamId);

    void deleteAllShiftsForInterval(Instant from, Instant to, Long teamId);

    void generateShiftsForInterval(LocalDate from, LocalDate to, Long teamId);

}
