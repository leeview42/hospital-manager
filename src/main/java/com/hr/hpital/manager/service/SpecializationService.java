package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.Specialization;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.service.dto.SpecializationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hr.hpital.manager.domain.Specialization}.
 */
public interface SpecializationService {

    /**
     * Save a specialization.
     *
     * @param specializationDTO the entity to save.
     * @return the persisted entity.
     */
    SpecializationDTO save(SpecializationDTO specializationDTO);

    /**
     * Get all the specializations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SpecializationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" specialization.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SpecializationDTO> findOne(Long id);

    /**
     * Delete the "id" specialization.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<User> findUsersWhoCanWorkSpecialization(Specialization specialization, List<User> possibleWorkers);
}
