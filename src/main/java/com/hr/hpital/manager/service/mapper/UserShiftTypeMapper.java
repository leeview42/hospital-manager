package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.service.dto.UserShiftTypeDTO;

import com.hr.hpital.manager.domain.UserShiftType;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserShiftType} and its DTO {@link UserShiftTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ShiftTypeMapper.class})
public interface UserShiftTypeMapper extends EntityMapper<UserShiftTypeDTO, UserShiftType> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "shiftType.id", target = "shiftTypeId")
    UserShiftTypeDTO toDto(UserShiftType userShiftType);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "shiftTypeId", target = "shiftType")
    UserShiftType toEntity(UserShiftTypeDTO userShiftTypeDTO);

    default UserShiftType fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserShiftType userShiftType = new UserShiftType();
        userShiftType.setId(id);
        return userShiftType;
    }
}
