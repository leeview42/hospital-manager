package com.hr.hpital.manager.service.dto;

import com.hr.hpital.manager.domain.Shift;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link Shift} entity.
 */
public class ShiftDTO implements Serializable {

    private Long id;

    @FutureOrPresent
    private LocalDate date;

    @FutureOrPresent
    private Instant start;

    @FutureOrPresent
    private Instant end;

    private Instant updateDate;

    private Instant createDate;

    private Boolean generated;

    @NotNull
    private Long shiftTypeId;

    private Long createdById;

    private Long updatedById;

    private ShiftTypeDTO shiftType;

    private Set<UserDTO> users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Instant getStart() {
        return start;
    }

    public void setStart(Instant start) {
        this.start = start;
    }

    public Instant getEnd() {
        return end;
    }

    public void setEnd(Instant end) {
        this.end = end;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public Instant getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }

    public Boolean isGenerated() {
        return generated;
    }

    public void setGenerated(Boolean generated) {
        this.generated = generated;
    }

    public Long getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(Long shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long userId) {
        this.createdById = userId;
    }

    public Long getUpdatedById() {
        return updatedById;
    }

    public void setUpdatedById(Long userId) {
        this.updatedById = userId;
    }

    public ShiftTypeDTO getShiftType() {
        return shiftType;
    }

    public void setShiftType(ShiftTypeDTO shiftType) {
        this.shiftType = shiftType;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShiftDTO shiftDTO = (ShiftDTO) o;
        if (shiftDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shiftDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShiftDTO{" +
            "id=" + getId() +
            ", start='" + getStart() + "'" +
            ", end='" + getEnd() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", generated='" + isGenerated() + "'" +
            ", shiftType=" + getShiftTypeId() +
            ", createdBy=" + getCreatedById() +
            ", updatedBy=" + getUpdatedById() +
            "}";
    }
}
