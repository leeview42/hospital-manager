package com.hr.hpital.manager.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hr.hpital.manager.domain.TeamUser} entity.
 */
public class TeamUserDTO implements Serializable {

    private Long id;

    @NotNull
    private Boolean isManager;

    private Long teamId;

    private Long userId;

    private TeamDTO team;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsManager() {
        return isManager;
    }

    public void setIsManager(Boolean isManager) {
        this.isManager = isManager;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeam(TeamDTO team) {
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TeamUserDTO teamUserDTO = (TeamUserDTO) o;
        if (teamUserDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), teamUserDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TeamUserDTO{" +
            "id=" + getId() +
            ", isManager='" + isIsManager() + "'" +
            ", team=" + getTeamId() +
            ", teamDTO=" + getTeam().toString() +
            ", worker=" + getUserId() +
            "}";
    }
}
