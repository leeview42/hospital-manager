package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.WorkerAvailability;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link WorkerAvailability} entity.
 */
public class WorkerAvailabilityDTO implements Serializable {

    private Long id;

    private Instant dateFrom;

    private Instant dateTo;

    private Boolean isAvailable;

    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Instant dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Instant getDateTo() {
        return dateTo;
    }

    public void setDateTo(Instant dateTo) {
        this.dateTo = dateTo;
    }

    public Boolean isIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkerAvailabilityDTO workerAvailabilityDTO = (WorkerAvailabilityDTO) o;
        if (workerAvailabilityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), workerAvailabilityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WorkerAvailabilityDTO{" +
            "id=" + getId() +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateTo='" + getDateTo() + "'" +
            ", isAvailable='" + isIsAvailable() + "'" +
            ", user=" + getUserId() +
            "}";
    }
}
