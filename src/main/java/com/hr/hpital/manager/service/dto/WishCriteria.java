package com.hr.hpital.manager.service.dto;

import com.hr.hpital.manager.domain.Wish;
import com.hr.hpital.manager.domain.enumeration.WishType;
import com.hr.hpital.manager.web.rest.WishResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link Wish} entity. This class is used
 * in {@link WishResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /wishes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WishCriteria implements Serializable, Criteria {
    /**
     * Class for filtering WishType
     */
    public static class WishTypeFilter extends Filter<WishType> {

        public WishTypeFilter() {
        }

        public WishTypeFilter(WishTypeFilter filter) {
            super(filter);
        }

        @Override
        public WishTypeFilter copy() {
            return new WishTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter comment;

    private InstantFilter dateFrom;

    private InstantFilter dateTo;

    private WishTypeFilter wishType;

    private LongFilter userId;

    private LongFilter teamIdFilter;

    public WishCriteria(){
    }

    public WishCriteria(WishCriteria other) {
        if (other != null) {
            this.id = other.id == null ? null : other.id.copy();
            this.comment = other.comment == null ? null : other.comment.copy();
            this.dateFrom = other.dateFrom == null ? null : other.dateFrom.copy();
            this.dateTo = other.dateTo == null ? null : other.dateTo.copy();
            this.wishType = other.wishType == null ? null : other.wishType.copy();
            this.userId = other.userId == null ? null : other.userId.copy();
            this.teamIdFilter = other.teamIdFilter == null ? null : other.teamIdFilter.copy();
        }
    }

    @Override
    public WishCriteria copy() {
        return new WishCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getComment() {
        return comment;
    }

    public void setComment(StringFilter comment) {
        this.comment = comment;
    }

    public InstantFilter getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(InstantFilter dateFrom) {
        this.dateFrom = dateFrom;
    }

    public InstantFilter getDateTo() {
        return dateTo;
    }

    public void setDateTo(InstantFilter dateTo) {
        this.dateTo = dateTo;
    }

    public WishTypeFilter getWishType() {
        return wishType;
    }

    public void setWishType(WishTypeFilter wishType) {
        this.wishType = wishType;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getTeamIdFilter() {
        return teamIdFilter;
    }

    public void setTeamIdFilter(LongFilter teamIdFilter) {
        this.teamIdFilter = teamIdFilter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WishCriteria that = (WishCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(dateFrom, that.dateFrom) &&
                Objects.equals(dateTo, that.dateTo) &&
                Objects.equals(wishType, that.wishType) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(teamIdFilter, that.teamIdFilter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            comment,
            dateFrom,
            dateTo,
            wishType,
            userId,
            teamIdFilter
        );
    }

    @Override
    public String toString() {
        return "WishCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (comment != null ? "comment=" + comment + ", " : "") +
            (dateFrom != null ? "dateFrom=" + dateFrom + ", " : "") +
            (dateTo != null ? "dateTo=" + dateTo + ", " : "") +
            (wishType != null ? "wishType=" + wishType + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (teamIdFilter != null ? "teamId=" + teamIdFilter + ", " : "") +
            "}";
    }

}
