package com.hr.hpital.manager.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UserDependenciesUpdateRequestDTO implements Serializable {

    @NotNull
    private Long userId;

    private Set<Long> dependencyEntityIds = new HashSet<>();

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Long> getDependencyEntityIds() {
        return dependencyEntityIds;
    }

    public void setDependencyEntityIds(Set<Long> dependencyEntityIds) {
        this.dependencyEntityIds = dependencyEntityIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDependenciesUpdateRequestDTO)) return false;

        UserDependenciesUpdateRequestDTO that = (UserDependenciesUpdateRequestDTO) o;

        if (!userId.equals(that.userId)) return false;
        return dependencyEntityIds != null ? dependencyEntityIds.equals(that.dependencyEntityIds) : that.dependencyEntityIds == null;
    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + (dependencyEntityIds != null ? dependencyEntityIds.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserShiftTypesDTO{" +
            "userId=" + userId +
            ", shiftTypeId=" + dependencyEntityIds +
            '}';
    }
}
