package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.time.Duration;
import java.util.Objects;

import com.hr.hpital.manager.domain.Shift;
import com.hr.hpital.manager.web.rest.ShiftResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.RangeFilter;

/**
 * Criteria class for the {@link Shift} entity. This class is used
 * in {@link ShiftResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shifts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShiftCriteria implements Serializable, Criteria {
    /**
     * Class for filtering java.time.Duration
     */
    public static class DurationFilter extends RangeFilter<Duration> {

        public DurationFilter() {
        }

        public DurationFilter(DurationFilter filter) {
            super(filter);
        }

        @Override
        public DurationFilter copy() {
            return new DurationFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter start;

    private InstantFilter end;

    private InstantFilter updateDate;

    private InstantFilter createDate;

    private BooleanFilter generated;

    private LongFilter shiftTypeId;

    private LongFilter createdById;

    private LongFilter updatedById;

    public ShiftCriteria(){
    }

    public ShiftCriteria(ShiftCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.start = other.start == null ? null : other.start.copy();
        this.end = other.end == null ? null : other.end.copy();
        this.updateDate = other.updateDate == null ? null : other.updateDate.copy();
        this.createDate = other.createDate == null ? null : other.createDate.copy();
        this.generated = other.generated == null ? null : other.generated.copy();
        this.shiftTypeId = other.shiftTypeId == null ? null : other.shiftTypeId.copy();
        this.createdById = other.createdById == null ? null : other.createdById.copy();
        this.updatedById = other.updatedById == null ? null : other.updatedById.copy();
    }

    @Override
    public ShiftCriteria copy() {
        return new ShiftCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getStart() {
        return start;
    }

    public void setStart(InstantFilter start) {
        this.start = start;
    }

    public InstantFilter getEnd() {
        return end;
    }

    public void setEnd(InstantFilter end) {
        this.end = end;
    }

    public InstantFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(InstantFilter updateDate) {
        this.updateDate = updateDate;
    }

    public InstantFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(InstantFilter createDate) {
        this.createDate = createDate;
    }

    public BooleanFilter getGenerated() {
        return generated;
    }

    public void setGenerated(BooleanFilter generated) {
        this.generated = generated;
    }

    public LongFilter getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(LongFilter shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    public LongFilter getCreatedById() {
        return createdById;
    }

    public void setCreatedById(LongFilter createdById) {
        this.createdById = createdById;
    }

    public LongFilter getUpdatedById() {
        return updatedById;
    }

    public void setUpdatedById(LongFilter updatedById) {
        this.updatedById = updatedById;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShiftCriteria that = (ShiftCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(start, that.start) &&
            Objects.equals(end, that.end) &&
            Objects.equals(updateDate, that.updateDate) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(generated, that.generated) &&
            Objects.equals(shiftTypeId, that.shiftTypeId) &&
            Objects.equals(createdById, that.createdById) &&
            Objects.equals(updatedById, that.updatedById);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        start,
        end,
        updateDate,
        createDate,
        generated,
        shiftTypeId,
        createdById,
        updatedById
        );
    }

    @Override
    public String toString() {
        return "ShiftCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (start != null ? "start=" + start + ", " : "") +
                (end != null ? "end=" + end + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
                (generated != null ? "generated=" + generated + ", " : "") +
                (shiftTypeId != null ? "shiftTypeId=" + shiftTypeId + ", " : "") +
                (createdById != null ? "createdById=" + createdById + ", " : "") +
                (updatedById != null ? "updatedById=" + updatedById + ", " : "") +
            "}";
    }

}
