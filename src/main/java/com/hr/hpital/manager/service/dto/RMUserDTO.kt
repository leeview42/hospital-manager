package com.hr.hpital.manager.service.dto

open class RMUserDTO {
    var id: Long? = null
    var user: String? = null
    var userName: String? = null
    var status: Boolean? = false
    var uniquePassword: String? = null
    var domainUserAccount: String? = null
    var blocked: Boolean? = false
    var changePassword: Boolean? = null
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RMUserDTO

        if (id != other.id) return false
        if (user != other.user) return false
        if (userName != other.userName) return false
        if (status != other.status) return false
        if (uniquePassword != other.uniquePassword) return false
        if (domainUserAccount != other.domainUserAccount) return false
        if (blocked != other.blocked) return false
        if (changePassword != other.changePassword) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (user?.hashCode() ?: 0)
        result = 31 * result + (userName?.hashCode() ?: 0)
        result = 31 * result + (status?.hashCode() ?: 0)
        result = 31 * result + (uniquePassword?.hashCode() ?: 0)
        result = 31 * result + (domainUserAccount?.hashCode() ?: 0)
        result = 31 * result + (blocked?.hashCode() ?: 0)
        result = 31 * result + (changePassword?.hashCode() ?: 0)
        return result
    }


}
