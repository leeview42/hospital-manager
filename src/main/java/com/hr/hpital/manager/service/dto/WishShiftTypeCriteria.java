package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.WishShiftType;
import com.hr.hpital.manager.web.rest.WishShiftTypeResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link WishShiftType} entity. This class is used
 * in {@link WishShiftTypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /wish-shift-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WishShiftTypeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter wishId;

    private LongFilter shiftTypeId;

    public WishShiftTypeCriteria(){
    }

    public WishShiftTypeCriteria(WishShiftTypeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.wishId = other.wishId == null ? null : other.wishId.copy();
        this.shiftTypeId = other.shiftTypeId == null ? null : other.shiftTypeId.copy();
    }

    @Override
    public WishShiftTypeCriteria copy() {
        return new WishShiftTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getWishId() {
        return wishId;
    }

    public void setWishId(LongFilter wishId) {
        this.wishId = wishId;
    }

    public LongFilter getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(LongFilter shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WishShiftTypeCriteria that = (WishShiftTypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(wishId, that.wishId) &&
            Objects.equals(shiftTypeId, that.shiftTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        wishId,
        shiftTypeId
        );
    }

    @Override
    public String toString() {
        return "WishShiftTypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (wishId != null ? "wishId=" + wishId + ", " : "") +
                (shiftTypeId != null ? "shiftTypeId=" + shiftTypeId + ", " : "") +
            "}";
    }

}
