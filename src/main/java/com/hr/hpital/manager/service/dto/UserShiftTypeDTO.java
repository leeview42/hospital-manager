package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.UserShiftType;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link UserShiftType} entity.
 */
public class UserShiftTypeDTO implements Serializable {

    private Long id;

    private Integer ratioPercentage;


    private Long userId;

    private Long shiftTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRatioPercentage() {
        return ratioPercentage;
    }

    public void setRatioPercentage(Integer ratioPercentage) {
        this.ratioPercentage = ratioPercentage;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(Long shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserShiftTypeDTO userShiftTypeDTO = (UserShiftTypeDTO) o;
        if (userShiftTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userShiftTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserShiftTypeDTO{" +
            "id=" + getId() +
            ", ratioPercentage=" + getRatioPercentage() +
            ", user=" + getUserId() +
            ", shiftType=" + getShiftTypeId() +
            "}";
    }
}
