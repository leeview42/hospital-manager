package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.Team;
import com.hr.hpital.manager.service.dto.TeamDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Team} and its DTO {@link TeamDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class, UserMapper.class, ShiftTypeMapper.class})
public interface TeamMapper extends EntityMapper<TeamDTO, Team> {

    @Mapping(source = "organization.id", target = "organizationId")
    TeamDTO toDto(Team team);

    @Mapping(source = "organizationId", target = "organization")
    Team toEntity(TeamDTO teamDTO);

    default Team fromId(Long id) {
        if (id == null) {
            return null;
        }
        Team team = new Team();
        team.setId(id);
        return team;
    }
}
