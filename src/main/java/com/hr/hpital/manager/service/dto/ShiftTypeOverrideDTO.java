package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.ShiftTypeOverride;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ShiftTypeOverride} entity.
 */
public class ShiftTypeOverrideDTO implements Serializable {

    private Long id;


    private Long shiftTypeId;

    private Long canOverrideId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(Long shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    public Long getCanOverrideId() {
        return canOverrideId;
    }

    public void setCanOverrideId(Long shiftTypeId) {
        this.canOverrideId = shiftTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShiftTypeOverrideDTO shiftTypeOverrideDTO = (ShiftTypeOverrideDTO) o;
        if (shiftTypeOverrideDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shiftTypeOverrideDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShiftTypeOverrideDTO{" +
            "id=" + getId() +
            ", shiftType=" + getShiftTypeId() +
            ", canOverride=" + getCanOverrideId() +
            "}";
    }
}
