package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.repository.DayConfigRepository;
import com.hr.hpital.manager.service.DayConfigService;
import com.hr.hpital.manager.service.DayConfigShiftTypeService;
import com.hr.hpital.manager.service.dto.DayConfigDTO;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeDTO;
import com.hr.hpital.manager.service.mapper.DayConfigMapper;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link DayConfig}.
 */
@Service("dayConfigService")
@Transactional
public class DayConfigServiceImpl implements DayConfigService {

  private final Logger log = LoggerFactory.getLogger(DayConfigServiceImpl.class);

  private final DayConfigRepository dayConfigRepository;

  private final DayConfigMapper dayConfigMapper;

  private final DayConfigShiftTypeService dayConfigShiftTypeService;

  public DayConfigServiceImpl(
      DayConfigRepository dayConfigRepository,
      DayConfigMapper dayConfigMapper,
      DayConfigShiftTypeService dayConfigShiftTypeService) {
    this.dayConfigRepository = dayConfigRepository;
    this.dayConfigMapper = dayConfigMapper;
    this.dayConfigShiftTypeService = dayConfigShiftTypeService;
  }

  /**
   * Save a dayConfig.
   *
   * @param dayConfigDTO
   *          the entity to save.
   * @return the persisted entity.
   */
  @Override
  public DayConfigDTO save(DayConfigDTO dayConfigDTO) {
    log.debug("Request to save DayConfig : {}", dayConfigDTO);
    DayConfig dayConfig = dayConfigMapper.toEntity(dayConfigDTO);
    dayConfig = dayConfigRepository.save(dayConfig);
    List<DayConfigShiftTypeDTO> dayConfigShiftTypesCopy = dayConfigDTO.getDayConfigShiftTypes().stream().map(DayConfigShiftTypeDTO::new).collect(Collectors.toList());
    this.dayConfigShiftTypeService.saveDayConfigShiftTypes(dayConfig, dayConfigShiftTypesCopy);
    return dayConfigMapper.toDto(dayConfig);
  }

  @Override
  public Optional<DayConfigDTO> find(DayConfigDTO dayConfigDTO, Long teamId) {
    return this.dayConfigRepository.findByDateAndTeamId(dayConfigDTO.getDate(), teamId).map(dayConfigMapper::toDto);
  }

  @Override
  public DayConfigDTO saveForWholeMonth(DayConfigDTO dayConfigDTO, List<Integer> weekDays) {
      if (weekDays != null && weekDays.size() > 0) {
          List<LocalDate> dayConfigDates = weekDays.stream()
              .filter(dayIndex -> dayIndex != null && dayIndex >= 0 && dayIndex <= 7)
              .map(dayIndex -> dayConfigDTO.getDate().with(TemporalAdjusters.firstInMonth(DayOfWeek.values()[dayIndex])))
              .map(DateTimeUtil::getOtherWeeksWithSameWeekdayInMonth).flatMap(List::stream).collect(Collectors.toList());
          this.createDayConfigsForDates(dayConfigDTO, dayConfigDates);
      } else {
          this.createDayConfigsForDates(
              dayConfigDTO,
              DateTimeUtil.getOtherWeeksWithSameWeekdayInMonth(dayConfigDTO.getDate()));
      }
    return dayConfigDTO;
  }

  /**
   * Updates a dayConfig.
   *
   * @param dayConfigDTO
   *          the entity to save.
   * @return the persisted entity.
   */
  @Override
  public DayConfigDTO update(DayConfigDTO dayConfigDTO) {
    log.debug("Request to save DayConfig : {}", dayConfigDTO);
    if (dayConfigDTO.getId() != null) {
      DayConfig dayConfig = dayConfigMapper.toEntity(dayConfigDTO);
      dayConfig = dayConfigRepository.save(dayConfig);
      this.dayConfigShiftTypeService.saveDayConfigShiftTypes(dayConfig, dayConfigDTO.getDayConfigShiftTypes());
      return dayConfigMapper.toDto(dayConfig);
    }
    return dayConfigDTO;
  }

  /**
   * Get all the dayConfigs.
   *
   * @param pageable
   *          the pagination information.
   * @return the list of entities.
   */
  @Override
  @Transactional(readOnly = true)
  public Page<DayConfigDTO> findAll(Pageable pageable) {
    log.debug("Request to get all DayConfigs");
    return dayConfigRepository.findAll(pageable).map(dayConfigMapper::toDto);
  }

  /**
   * Return a {@link List} of {@link DayConfigDTO} which matches the time interval from the database.
   *
   * @param fromDate
   *          interval start
   * @param toDate
   *          interval end
   * @param teamId
   * @return the matching entities.
   */
  @Transactional(readOnly = true)
  public List<DayConfigDTO> findForInterval(LocalDate fromDate, LocalDate toDate, Long teamId) {
    log.debug("find by fromDate : {}, toDate: {}", fromDate, toDate);
    return dayConfigRepository.findByDateBetweenAndTeamId(fromDate, toDate, teamId).stream().map(dayConfigMapper::toDto)
        .collect(Collectors.toList());
  }

  @Transactional
  @Override
  public void deleteForInterval(LocalDate fromDate, LocalDate toDate, Long teamId) {
    log.debug("delete by fromDate : {}, toDate: {}", fromDate, toDate);
    dayConfigRepository.deleteByDateBetweenAndTeamId(fromDate, toDate, teamId);
  }

  /**
   * Get one dayConfig by id.
   *
   * @param id
   *          the id of the entity.
   * @return the entity.
   */
  @Override
  @Transactional(readOnly = true)
  public Optional<DayConfigDTO> findOne(Long id) {
    log.debug("Request to get DayConfig : {}", id);
    return dayConfigRepository.findById(id).map(dayConfigMapper::toDto);
  }

  /**
   * Delete the dayConfig by id.
   *
   * @param id
   *          the id of the entity.
   */
  @Override
  public void delete(Long id) {
    log.debug("Request to delete DayConfig : {}", id);
    dayConfigRepository.deleteById(id);
  }

    private void createDayConfigsForDates(DayConfigDTO dto, List<LocalDate> dates) {
        if (dates != null) {
            dto.setId(null);
            dates.stream().forEach(localDate -> {
                this.dayConfigRepository.findByDateAndTeamId(localDate, dto.getTeamId())
                    .ifPresent(dConf -> this.delete(dConf.getId()));
                dto.setDate(localDate);
                dto.setId(null);
                this.save(dto);
            });
        }
    }

}
