package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.web.rest.DayConfigResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link DayConfig} entity. This class is used
 * in {@link DayConfigResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /day-configs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DayConfigCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter date;

    private BooleanFilter isBankHoliday;

    private LongFilter teamIdFilter;

    public DayConfigCriteria(){
    }

    public DayConfigCriteria(DayConfigCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.isBankHoliday = other.isBankHoliday == null ? null : other.isBankHoliday.copy();
    }

    @Override
    public DayConfigCriteria copy() {
        return new DayConfigCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getDate() {
        return date;
    }

    public void setDate(LocalDateFilter date) {
        this.date = date;
    }

    public BooleanFilter getIsBankHoliday() {
        return isBankHoliday;
    }

    public void setIsBankHoliday(BooleanFilter isBankHoliday) {
        this.isBankHoliday = isBankHoliday;
    }

    public LongFilter getTeamIdFilter() {
        return teamIdFilter;
    }

    public void setTeamIdFilter(LongFilter teamIdFilter) {
        this.teamIdFilter = teamIdFilter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DayConfigCriteria that = (DayConfigCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(date, that.date) &&
            Objects.equals(isBankHoliday, that.isBankHoliday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        date,
        isBankHoliday
        );
    }

    @Override
    public String toString() {
        return "DayConfigCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (date != null ? "date=" + date + ", " : "") +
                (isBankHoliday != null ? "isBankHoliday=" + isBankHoliday + ", " : "") +
            "}";
    }

}
