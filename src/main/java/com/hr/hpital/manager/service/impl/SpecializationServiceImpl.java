package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.Specialization;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.repository.SpecializationRepository;
import com.hr.hpital.manager.service.SpecializationService;
import com.hr.hpital.manager.service.dto.SpecializationDTO;
import com.hr.hpital.manager.service.mapper.SpecializationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Specialization}.
 */
@Service
@Transactional
public class SpecializationServiceImpl implements SpecializationService {

    private final Logger log = LoggerFactory.getLogger(SpecializationServiceImpl.class);

    private final SpecializationRepository specializationRepository;

    private final SpecializationMapper specializationMapper;

    public SpecializationServiceImpl(SpecializationRepository specializationRepository, SpecializationMapper specializationMapper) {
        this.specializationRepository = specializationRepository;
        this.specializationMapper = specializationMapper;
    }

    /**
     * Save a specialization.
     *
     * @param specializationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SpecializationDTO save(SpecializationDTO specializationDTO) {
        log.debug("Request to save Specialization : {}", specializationDTO);
        Specialization specialization = specializationMapper.toEntity(specializationDTO);
        specialization = specializationRepository.save(specialization);
        return specializationMapper.toDto(specialization);
    }

    /**
     * Get all the specializations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SpecializationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Specializations");
        return specializationRepository.findAll(pageable)
            .map(specializationMapper::toDto);
    }


    /**
     * Get one specialization by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SpecializationDTO> findOne(Long id) {
        log.debug("Request to get Specialization : {}", id);
        return specializationRepository.findById(id)
            .map(specializationMapper::toDto);
    }

    /**
     * Delete the specialization by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Specialization : {}", id);
        specializationRepository.deleteById(id);
    }

    @Override
    public List<User> findUsersWhoCanWorkSpecialization(Specialization specialization, List<User> possibleWorkers) {
        return possibleWorkers.stream()
            .filter(user -> specialization.getUsers().contains(user))
            .collect(Collectors.toList());
    }
}
