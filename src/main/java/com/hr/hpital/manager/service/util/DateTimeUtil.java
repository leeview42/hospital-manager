package com.hr.hpital.manager.service.util;

import org.springframework.data.util.Pair;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;

public class DateTimeUtil {

    public static final Instant getInstantAtStartOfDay(LocalDate localDate) {
        if (localDate != null) {
            return localDate.atStartOfDay().toInstant(ZoneOffset.UTC);
        }
        return null;
    }

    public static final Instant getInstantAtEndOfDay(LocalDate localDate) {
        if (localDate != null) {
            return localDate.atTime(23, 59, 59).toInstant(ZoneOffset.UTC);
        }
        return null;
    }

    public static final Instant getInstantAtTime(LocalDate localDate, int hour, int minute, int second) {
        if (localDate != null) {
            return localDate.atTime(hour, minute, second).toInstant(ZoneOffset.UTC);
        }
        return null;
    }

    public static final LocalDate getNow() {
        return LocalDate.now(ZoneOffset.UTC);
    }

    public static final LocalDate getDate(Instant instant) {
        return instant.atZone(ZoneOffset.UTC).toLocalDate();
    }

    public static final List<LocalDate> getOtherWeeksWithSameWeekdayInMonth(LocalDate date) {
        Set<LocalDate> dates = new HashSet<>();
        int monthValue = date.getMonthValue();
        LocalDate referenceDate = date.minusWeeks(1l);
        while (referenceDate.getMonthValue() == monthValue) {
            dates.add(referenceDate);
            referenceDate = referenceDate.minusWeeks(1l);
        }
        referenceDate = date.plusWeeks(1l);
        while (referenceDate.getMonthValue() == monthValue) {
            dates.add(referenceDate);
            referenceDate = referenceDate.plusWeeks(1l);
        }
        dates.add(date);
        List<LocalDate> datesList = new ArrayList<>();
        datesList.addAll(dates);
        return datesList;
    }

    public static List<Pair<LocalDate, LocalDate>> getWeekendsInMonth(Integer year, Month month) {
        final List<Pair<LocalDate, LocalDate>> weekendDays = new ArrayList<>();
        LocalDate iterator = LocalDate.of(year, month, 1);
        final LocalDate endOfMonth = LocalDate.of(year, month, iterator.lengthOfMonth());
        while (iterator.isBefore(endOfMonth) || iterator.isEqual(endOfMonth)) {
            if (iterator.getDayOfWeek() == SATURDAY) {
                weekendDays.add(Pair.of(iterator, iterator.plusDays(1)));
                iterator = iterator.plusWeeks(1);
            } else {
                iterator = iterator.plusDays(SATURDAY.getValue() - iterator.getDayOfWeek().getValue());
            }
        }

        return weekendDays;
    }

    public static boolean intersectsWeekend(Instant from, Instant to) {
        final LocalDate fromDate = getDate(from);
        final LocalDate toDate = getDate(to);

        // like Wednesday to Monday, Thursday to Monday, Friday to Wednesday...
        if (fromDate.getDayOfWeek().getValue() > toDate.getDayOfWeek().getValue()) {
            return true;
        }

        // like Monday to Monday, Thursday to Friday...more than 7 days in between
        if (to.toEpochMilli() - from.toEpochMilli() >= 7 * 24 * 60 * 60 * 1000) {
            return true;
        }

        // like Monday to Saturday, Wednesday to SUNDAY, less than 7 days in between
        return toDate.getDayOfWeek().getValue() >= SATURDAY.getValue();
    }

    public static boolean isWeekendDay(LocalDate date) {
        return date.getDayOfWeek() == SATURDAY || date.getDayOfWeek() == SUNDAY;
    }

}
