package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.WishShiftType;
import com.hr.hpital.manager.service.dto.WishShiftTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link WishShiftType} and its DTO {@link WishShiftTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {WishMapper.class, ShiftTypeMapper.class})
public interface WishShiftTypeMapper extends EntityMapper<WishShiftTypeDTO, WishShiftType> {

    @Mapping(source = "wish.id", target = "wishId")
    @Mapping(source = "shiftType.id", target = "shiftTypeId")
    WishShiftTypeDTO toDto(WishShiftType wishShiftType);

    @Mapping(source = "wishId", target = "wish")
    @Mapping(source = "shiftTypeId", target = "shiftType")
    WishShiftType toEntity(WishShiftTypeDTO wishShiftTypeDTO);

    default WishShiftType fromId(Long id) {
        if (id == null) {
            return null;
        }
        WishShiftType wishShiftType = new WishShiftType();
        wishShiftType.setId(id);
        return wishShiftType;
    }
}
