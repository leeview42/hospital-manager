package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.Wish;
import com.hr.hpital.manager.service.dto.WishDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Wish}.
 */
public interface WishService {

    /**
     * Save a wish.
     *
     * @param wishDTO the entity to save.
     * @return the persisted entity.
     */
    WishDTO save(WishDTO wishDTO);

    WishDTO update(WishDTO wishDTO);

    WishDTO updateForWholeMonth(WishDTO wishDTO, List<Integer> weekDays);

    WishDTO saveForWholeMonth(WishDTO wishDTO, List<Integer> weekDays);

    /**
     * Get all the wishes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WishDTO> findAll(Pageable pageable);


    void deleteForInterval(LocalDate fromDate, LocalDate toDate);


    /**
     * Get the "id" wish.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WishDTO> findOne(Long id);

    /**
     * Delete the "id" wish.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    boolean canUserTakeShiftTypeOnDate(User user, ShiftType shiftType, LocalDate localDate);
}
