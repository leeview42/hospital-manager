package com.hr.hpital.manager.service;

public class RequestContext {

    private Long teamId;

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        if (this.teamId == null) {
            this.teamId = teamId;
            this.teamId = teamId != null ? teamId : 0;
        } else {
            throw new RuntimeException("TeamId has been already set");
        }
    }

}
