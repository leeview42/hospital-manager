package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.ShiftWorker_;
import com.hr.hpital.manager.domain.Shift_;
import com.hr.hpital.manager.domain.User_;
import com.hr.hpital.manager.repository.ShiftWorkerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.domain.ShiftWorker;
import com.hr.hpital.manager.service.dto.ShiftWorkerCriteria;
import com.hr.hpital.manager.service.dto.ShiftWorkerDTO;
import com.hr.hpital.manager.service.mapper.ShiftWorkerMapper;

/**
 * Service for executing complex queries for {@link ShiftWorker} entities in the database.
 * The main input is a {@link ShiftWorkerCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShiftWorkerDTO} or a {@link Page} of {@link ShiftWorkerDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShiftWorkerQueryService extends QueryService<ShiftWorker> {

    private final Logger log = LoggerFactory.getLogger(ShiftWorkerQueryService.class);

    private final ShiftWorkerRepository shiftWorkerRepository;

    private final ShiftWorkerMapper shiftWorkerMapper;

    public ShiftWorkerQueryService(ShiftWorkerRepository shiftWorkerRepository, ShiftWorkerMapper shiftWorkerMapper) {
        this.shiftWorkerRepository = shiftWorkerRepository;
        this.shiftWorkerMapper = shiftWorkerMapper;
    }

    /**
     * Return a {@link List} of {@link ShiftWorkerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShiftWorkerDTO> findByCriteria(ShiftWorkerCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShiftWorker> specification = createSpecification(criteria);
        return shiftWorkerMapper.toDto(shiftWorkerRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShiftWorkerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShiftWorkerDTO> findByCriteria(ShiftWorkerCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShiftWorker> specification = createSpecification(criteria);
        return shiftWorkerRepository.findAll(specification, page)
            .map(shiftWorkerMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShiftWorkerCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShiftWorker> specification = createSpecification(criteria);
        return shiftWorkerRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<ShiftWorker> createSpecification(ShiftWorkerCriteria criteria) {
        Specification<ShiftWorker> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShiftWorker_.id));
            }
            if (criteria.getShiftId() != null) {
                specification = specification.and(buildSpecification(criteria.getShiftId(),
                    root -> root.join(ShiftWorker_.shift, JoinType.LEFT).get(Shift_.id)));
            }
            if (criteria.getWorkerId() != null) {
                specification = specification.and(buildSpecification(criteria.getWorkerId(),
                    root -> root.join(ShiftWorker_.worker, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
