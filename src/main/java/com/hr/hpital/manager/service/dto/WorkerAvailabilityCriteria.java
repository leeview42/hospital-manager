package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.WorkerAvailability;
import com.hr.hpital.manager.web.rest.WorkerAvailabilityResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link WorkerAvailability} entity. This class is used
 * in {@link WorkerAvailabilityResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /worker-availabilities?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WorkerAvailabilityCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter dateFrom;

    private InstantFilter dateTo;

    private BooleanFilter isAvailable;

    private LongFilter userId;

    private LongFilter teamIdFilter;

    public WorkerAvailabilityCriteria(){
    }

    public WorkerAvailabilityCriteria(WorkerAvailabilityCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.dateFrom = other.dateFrom == null ? null : other.dateFrom.copy();
        this.dateTo = other.dateTo == null ? null : other.dateTo.copy();
        this.isAvailable = other.isAvailable == null ? null : other.isAvailable.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public WorkerAvailabilityCriteria copy() {
        return new WorkerAvailabilityCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(InstantFilter dateFrom) {
        this.dateFrom = dateFrom;
    }

    public InstantFilter getDateTo() {
        return dateTo;
    }

    public void setDateTo(InstantFilter dateTo) {
        this.dateTo = dateTo;
    }

    public BooleanFilter getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(BooleanFilter isAvailable) {
        this.isAvailable = isAvailable;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getTeamIdFilter() {
        return teamIdFilter;
    }

    public void setTeamIdFilter(LongFilter teamIdFilter) {
        this.teamIdFilter = teamIdFilter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WorkerAvailabilityCriteria that = (WorkerAvailabilityCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(dateFrom, that.dateFrom) &&
            Objects.equals(dateTo, that.dateTo) &&
            Objects.equals(isAvailable, that.isAvailable) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        dateFrom,
        dateTo,
        isAvailable,
        userId
        );
    }

    @Override
    public String toString() {
        return "WorkerAvailabilityCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (dateFrom != null ? "dateFrom=" + dateFrom + ", " : "") +
                (dateTo != null ? "dateTo=" + dateTo + ", " : "") +
                (isAvailable != null ? "isAvailable=" + isAvailable + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
