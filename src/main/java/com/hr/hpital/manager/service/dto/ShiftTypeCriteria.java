package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.time.Duration;
import java.util.Objects;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.web.rest.ShiftTypeResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.RangeFilter;

/**
 * Criteria class for the {@link ShiftType} entity. This class is used
 * in {@link ShiftTypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shift-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShiftTypeCriteria implements Serializable, Criteria {
    /**
     * Class for filtering java.time.Duration
     */
    public static class DurationFilter extends RangeFilter<Duration> {

        public DurationFilter() {
        }

        public DurationFilter(DurationFilter filter) {
            super(filter);
        }

        @Override
        public DurationFilter copy() {
            return new DurationFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter code;

    private BooleanFilter active;

    private StringFilter description;

    private BooleanFilter canRepeat;

    private IntegerFilter startHour;

    private IntegerFilter startMinute;

    private DurationFilter duration;

    private BooleanFilter fillWithRemaining;

    private IntegerFilter noOfWorkers;

    private LongFilter priorityId;

    private BooleanFilter mandatory;

    private LongFilter userId;

    private LongFilter teamIdFilter;

    public ShiftTypeCriteria(){
    }

    public ShiftTypeCriteria(ShiftTypeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.active = other.active == null ? null : other.active.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.canRepeat = other.canRepeat == null ? null : other.canRepeat.copy();
        this.startHour = other.startHour == null ? null : other.startHour.copy();
        this.startMinute = other.startMinute == null ? null : other.startMinute.copy();
        this.duration = other.duration == null ? null : other.duration.copy();
        this.fillWithRemaining = other.fillWithRemaining == null ? null : other.fillWithRemaining.copy();
        this.noOfWorkers = other.noOfWorkers == null ? null : other.noOfWorkers.copy();
        this.priorityId = other.priorityId == null ? null : other.priorityId.copy();
        this.mandatory = other.mandatory == null ? null : other.mandatory.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public ShiftTypeCriteria copy() {
        return new ShiftTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public BooleanFilter getActive() {
        return active;
    }

    public void setActive(BooleanFilter active) {
        this.active = active;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public BooleanFilter getCanRepeat() {
        return canRepeat;
    }

    public void setCanRepeat(BooleanFilter canRepeat) {
        this.canRepeat = canRepeat;
    }

    public IntegerFilter getStartHour() {
        return startHour;
    }

    public void setStartHour(IntegerFilter startHour) {
        this.startHour = startHour;
    }

    public IntegerFilter getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(IntegerFilter startMinute) {
        this.startMinute = startMinute;
    }

    public DurationFilter getDuration() {
        return duration;
    }

    public void setDuration(DurationFilter duration) {
        this.duration = duration;
    }

    public BooleanFilter getFillWithRemaining() {
        return fillWithRemaining;
    }

    public void setFillWithRemaining(BooleanFilter fillWithRemaining) {
        this.fillWithRemaining = fillWithRemaining;
    }

    public BooleanFilter getMandatory() {
        return mandatory;
    }

    public void setMandatory(BooleanFilter mandatory) {
        this.mandatory = mandatory;
    }

    public IntegerFilter getNoOfWorkers() {
        return noOfWorkers;
    }

    public void setNoOfWorkers(IntegerFilter noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public LongFilter getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(LongFilter priorityId) {
        this.priorityId = priorityId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getTeamIdFilter() {
        return teamIdFilter;
    }

    public void setTeamIdFilter(LongFilter teamIdFilter) {
        this.teamIdFilter = teamIdFilter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShiftTypeCriteria that = (ShiftTypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(code, that.code) &&
            Objects.equals(active, that.active) &&
            Objects.equals(description, that.description) &&
            Objects.equals(canRepeat, that.canRepeat) &&
            Objects.equals(startHour, that.startHour) &&
            Objects.equals(startMinute, that.startMinute) &&
            Objects.equals(duration, that.duration) &&
            Objects.equals(fillWithRemaining, that.fillWithRemaining) &&
            Objects.equals(noOfWorkers, that.noOfWorkers) &&
            Objects.equals(priorityId, that.priorityId) &&
            Objects.equals(mandatory, that.mandatory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        code,
        active,
        description,
        canRepeat,
        startHour,
        startMinute,
        duration,
        fillWithRemaining,
        noOfWorkers,
        priorityId,
        mandatory
        );
    }

    @Override
    public String toString() {
        return "ShiftTypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (active != null ? "active=" + active + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (canRepeat != null ? "canRepeat=" + canRepeat + ", " : "") +
                (startHour != null ? "startHour=" + startHour + ", " : "") +
                (startMinute != null ? "startMinute=" + startMinute + ", " : "") +
                (duration != null ? "duration=" + duration + ", " : "") +
                (fillWithRemaining != null ? "fillWithRemaining=" + fillWithRemaining + ", " : "") +
                (noOfWorkers != null ? "noOfWorkers=" + noOfWorkers + ", " : "") +
                (priorityId != null ? "priorityId=" + priorityId + ", " : "") +
                (mandatory != null ? "mandatory" + mandatory + ", " : "") +
            "}";
    }

}
