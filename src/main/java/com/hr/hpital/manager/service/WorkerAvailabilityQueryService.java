package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.Team_;
import com.hr.hpital.manager.domain.User_;
import com.hr.hpital.manager.domain.WorkerAvailability;
import com.hr.hpital.manager.domain.WorkerAvailability_;
import com.hr.hpital.manager.repository.WorkerAvailabilityRepository;
import com.hr.hpital.manager.service.dto.WorkerAvailabilityCriteria;
import com.hr.hpital.manager.service.dto.WorkerAvailabilityDTO;
import com.hr.hpital.manager.service.mapper.WorkerAvailabilityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link WorkerAvailability} entities in the database.
 * The main input is a {@link WorkerAvailabilityCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WorkerAvailabilityDTO} or a {@link Page} of {@link WorkerAvailabilityDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WorkerAvailabilityQueryService extends QueryService<WorkerAvailability> {

    private final Logger log = LoggerFactory.getLogger(WorkerAvailabilityQueryService.class);

    private final WorkerAvailabilityRepository workerAvailabilityRepository;

    private final WorkerAvailabilityMapper workerAvailabilityMapper;

    public WorkerAvailabilityQueryService(WorkerAvailabilityRepository workerAvailabilityRepository, WorkerAvailabilityMapper workerAvailabilityMapper) {
        this.workerAvailabilityRepository = workerAvailabilityRepository;
        this.workerAvailabilityMapper = workerAvailabilityMapper;
    }

    /**
     * Return a {@link List} of {@link WorkerAvailabilityDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WorkerAvailabilityDTO> findByCriteria(WorkerAvailabilityCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<WorkerAvailability> specification = createSpecification(criteria);
        return workerAvailabilityMapper.toDto(workerAvailabilityRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link WorkerAvailabilityDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WorkerAvailabilityDTO> findByCriteria(WorkerAvailabilityCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<WorkerAvailability> specification = createSpecification(criteria);
        return workerAvailabilityRepository.findAll(specification, page)
            .map(workerAvailabilityMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(WorkerAvailabilityCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<WorkerAvailability> specification = createSpecification(criteria);
        return workerAvailabilityRepository.count(specification);
    }

    /**
     * Function to convert WorkerAvailabilityCriteria to a {@link Specification}.
     */
    private Specification<WorkerAvailability> createSpecification(WorkerAvailabilityCriteria criteria) {
        Specification<WorkerAvailability> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WorkerAvailability_.id));
            }
            if (criteria.getDateFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateFrom(), WorkerAvailability_.dateFrom));
            }
            if (criteria.getDateTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateTo(), WorkerAvailability_.dateTo));
            }
            if (criteria.getIsAvailable() != null) {
                specification = specification.and(buildSpecification(criteria.getIsAvailable(), WorkerAvailability_.isAvailable));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(WorkerAvailability_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getTeamIdFilter() != null) {
                specification = specification.and(buildSpecification(criteria.getTeamIdFilter(),
                    root -> root.join(WorkerAvailability_.user, JoinType.INNER).join(User_.teams).get(Team_.id)));
            }

        }
        return specification;
    }
}
