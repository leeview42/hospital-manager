package com.hr.hpital.manager.service.util;

import com.hr.hpital.manager.domain.ShiftType;

import javax.annotation.Nullable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class ShiftUtil {
    public static Instant getShiftStartTime(ShiftType shiftType, LocalDate date) {
        return DateTimeUtil.getInstantAtStartOfDay(date)
            .plus(shiftType.getStartHour().longValue(), ChronoUnit.HOURS)
            .plus(shiftType.getStartMinute().longValue(), ChronoUnit.MINUTES);
    }

    public static Instant getShiftEndTime(ShiftType shiftType, Instant startTime, @Nullable Integer pauseHours) {
        return startTime.plus(shiftType.getDuration()).plus(pauseHours != null ? pauseHours : 0, ChronoUnit.HOURS);
    }
}
