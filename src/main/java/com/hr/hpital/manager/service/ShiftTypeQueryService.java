package com.hr.hpital.manager.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.Priority_;
import com.hr.hpital.manager.domain.ShiftType_;
import com.hr.hpital.manager.domain.Team_;
import com.hr.hpital.manager.domain.User_;
import com.hr.hpital.manager.repository.ShiftTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.service.dto.ShiftTypeCriteria;
import com.hr.hpital.manager.service.dto.ShiftTypeDTO;
import com.hr.hpital.manager.service.mapper.ShiftTypeMapper;

/**
 * Service for executing complex queries for {@link ShiftType} entities in the database.
 * The main input is a {@link ShiftTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShiftTypeDTO} or a {@link Page} of {@link ShiftTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShiftTypeQueryService extends QueryService<ShiftType> {

    private final Logger log = LoggerFactory.getLogger(ShiftTypeQueryService.class);

    private final ShiftTypeRepository shiftTypeRepository;

    private final ShiftTypeMapper shiftTypeMapper;

    public ShiftTypeQueryService(ShiftTypeRepository shiftTypeRepository, ShiftTypeMapper shiftTypeMapper) {
        this.shiftTypeRepository = shiftTypeRepository;
        this.shiftTypeMapper = shiftTypeMapper;
    }

    /**
     * Return a {@link List} of {@link ShiftTypeDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShiftTypeDTO> findByCriteria(ShiftTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShiftType> specification = createSpecification(criteria);
        return shiftTypeMapper.toDto(shiftTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShiftTypeDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShiftTypeDTO> findByCriteria(ShiftTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShiftType> specification = createSpecification(criteria);
        return shiftTypeRepository.findAll(specification, page)
            .map(shiftTypeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<ShiftTypeDTO> findAll() {
        return shiftTypeRepository.findAll()
            .stream().map(shiftTypeMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShiftTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShiftType> specification = createSpecification(criteria);
        return shiftTypeRepository.count(specification);
    }

    /**
     * Function to convert ShiftTypeCriteria to a {@link Specification}.
     */
    private Specification<ShiftType> createSpecification(ShiftTypeCriteria criteria) {
        Specification<ShiftType> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShiftType_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ShiftType_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), ShiftType_.code));
            }
            if (criteria.getActive() != null) {
                specification = specification.and(buildSpecification(criteria.getActive(), ShiftType_.active));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), ShiftType_.description));
            }
            if (criteria.getCanRepeat() != null) {
                specification = specification.and(buildSpecification(criteria.getCanRepeat(), ShiftType_.canRepeat));
            }
            if (criteria.getStartHour() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartHour(), ShiftType_.startHour));
            }
            if (criteria.getStartMinute() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartMinute(), ShiftType_.startMinute));
            }
            if (criteria.getDuration() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDuration(), ShiftType_.duration));
            }
            if (criteria.getFillWithRemaining() != null) {
                specification = specification.and(buildSpecification(criteria.getFillWithRemaining(), ShiftType_.fillWithRemaining));
            }
            if (criteria.getNoOfWorkers() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNoOfWorkers(), ShiftType_.noOfWorkers));
            }
            if (criteria.getPriorityId() != null) {
                specification = specification.and(buildSpecification(criteria.getPriorityId(),
                    root -> root.join(ShiftType_.priority, JoinType.LEFT).get(Priority_.id)));
            }
            if (criteria.getMandatory() != null) {
                specification = specification.and(buildSpecification(criteria.getMandatory(), ShiftType_.mandatory));
            }

            if (criteria.getTeamIdFilter() != null) {
                specification = specification.and(buildSpecification(criteria.getTeamIdFilter(),
                    root -> root.join(ShiftType_.team, JoinType.INNER).get(Team_.id)));
            }
        }
        return specification;
    }
}
