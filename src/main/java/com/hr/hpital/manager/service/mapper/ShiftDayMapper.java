package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.service.dto.ShiftDayDTO;

import com.hr.hpital.manager.domain.ShiftDay;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShiftDay} and its DTO {@link ShiftDayDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShiftTypeMapper.class})
public interface ShiftDayMapper extends EntityMapper<ShiftDayDTO, ShiftDay> {

    @Mapping(source = "shiftType.id", target = "shiftTypeId")
    ShiftDayDTO toDto(ShiftDay shiftDay);

    @Mapping(source = "shiftTypeId", target = "shiftType")
    ShiftDay toEntity(ShiftDayDTO shiftDayDTO);

    default ShiftDay fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShiftDay shiftDay = new ShiftDay();
        shiftDay.setId(id);
        return shiftDay;
    }
}
