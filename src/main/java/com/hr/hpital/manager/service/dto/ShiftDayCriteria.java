package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.ShiftDay;
import com.hr.hpital.manager.web.rest.ShiftDayResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link ShiftDay} entity. This class is used
 * in {@link ShiftDayResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shift-days?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShiftDayCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter dayOfWeek;

    private LongFilter shiftTypeId;

    public ShiftDayCriteria(){
    }

    public ShiftDayCriteria(ShiftDayCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.dayOfWeek = other.dayOfWeek == null ? null : other.dayOfWeek.copy();
        this.shiftTypeId = other.shiftTypeId == null ? null : other.shiftTypeId.copy();
    }

    @Override
    public ShiftDayCriteria copy() {
        return new ShiftDayCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(IntegerFilter dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public LongFilter getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(LongFilter shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShiftDayCriteria that = (ShiftDayCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(dayOfWeek, that.dayOfWeek) &&
            Objects.equals(shiftTypeId, that.shiftTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        dayOfWeek,
        shiftTypeId
        );
    }

    @Override
    public String toString() {
        return "ShiftDayCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (dayOfWeek != null ? "dayOfWeek=" + dayOfWeek + ", " : "") +
                (shiftTypeId != null ? "shiftTypeId=" + shiftTypeId + ", " : "") +
            "}";
    }

}
