package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.config.ApplicationProperties;
import com.hr.hpital.manager.domain.ShiftWorker;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.repository.ShiftWorkerRepository;
import com.hr.hpital.manager.repository.TeamRepository;
import com.hr.hpital.manager.service.ShiftWorkerService;
import com.hr.hpital.manager.service.dto.ShiftWorkerDTO;
import com.hr.hpital.manager.service.mapper.ShiftWorkerMapper;
import com.hr.hpital.manager.service.util.CollectionsUtilKt;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;

import static com.hr.hpital.manager.service.util.DateTimeUtil.getWeekendsInMonth;

/**
 * Service Implementation for managing {@link ShiftWorker}.
 */
@Service
@Transactional
public class ShiftWorkerServiceImpl implements ShiftWorkerService {

    private final Logger log = LoggerFactory.getLogger(ShiftWorkerServiceImpl.class);

    private final ShiftWorkerRepository shiftWorkerRepository;

    private final ShiftWorkerMapper shiftWorkerMapper;

    private TeamRepository teamRepository;

    private final ApplicationProperties applicationProperties;

    public ShiftWorkerServiceImpl(ShiftWorkerRepository shiftWorkerRepository,
                                  ShiftWorkerMapper shiftWorkerMapper,
                                  TeamRepository teamRepository,
                                  ApplicationProperties applicationProperties) {
        this.shiftWorkerRepository = shiftWorkerRepository;
        this.shiftWorkerMapper = shiftWorkerMapper;
        this.teamRepository = teamRepository;
        this.applicationProperties = applicationProperties;
    }

    public void setTeamRepository(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    /**
     * Save a shiftWorker.
     *
     * @param shiftWorkerDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShiftWorkerDTO save(ShiftWorkerDTO shiftWorkerDTO) {
        log.debug("Request to save ShiftWorker : {}", shiftWorkerDTO);
        ShiftWorker shiftWorker = shiftWorkerMapper.toEntity(shiftWorkerDTO);
        shiftWorker = shiftWorkerRepository.save(shiftWorker);
        return shiftWorkerMapper.toDto(shiftWorker);
    }

    /**
     * Get all the shiftWorkers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShiftWorkerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShiftWorkers");
        return shiftWorkerRepository.findAll(pageable)
            .map(shiftWorkerMapper::toDto);
    }

    /**
     * Get one shiftWorker by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShiftWorkerDTO> findOne(Long id) {
        log.debug("Request to get ShiftWorker : {}", id);
        return shiftWorkerRepository.findById(id)
            .map(shiftWorkerMapper::toDto);
    }

    /**
     * Delete the shiftWorker by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShiftWorker : {}", id);
        shiftWorkerRepository.deleteById(id);
    }

    /**
     * Checks if user has already a shift assigned on a date.
     *
     * @param user for which the checking is done
     * @param date on which to check for existing shift
     * @return true in case user is already assigned to a shift on the date, false otherwise
     */
    @Override
    @Transactional(readOnly = true)
    public boolean alreadyHasShiftAssigned(User user, LocalDate date) {
        Instant startTime = DateTimeUtil.getInstantAtStartOfDay(date);
        Instant endTime = DateTimeUtil.getInstantAtEndOfDay(date);
        return 0L != shiftWorkerRepository.countByWorkerAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(user, startTime, endTime);
    }

    /**
     * Checks if user has already a shift assigned in the time interval provided.
     *
     * @param user      for which the checking is done
     * @param startTime the lower bound of the time interval
     * @param endTime   the upper bound of the time interval
     * @return true in case user is already assigned to a shift in the specified time interval false otherwise
     */
    @Override
    @Transactional(readOnly = true)
    public boolean alreadyHasShiftAssigned(User user, Instant startTime, Instant endTime) {
        return 0 != shiftWorkerRepository.countByWorkerAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(user, startTime, endTime);
    }

    /**
     * Filters out those users which already have shifts assigned in the time interval provided.
     *
     * @param users     for which the checking is done
     * @param startTime the lower bound of the time interval
     * @param endTime   the upper bound of the time interval
     * @return available users (those which are not assigned in the time interval)
     */
    @Override
    @Transactional(readOnly = true)
    public List<User> filterUnassignedWorkers(List<User> users, Instant startTime, Instant endTime) {
        if (users.size() == 0) {
            return users;
        }

        final List<User> result = new ArrayList<>(users);
        CollectionsUtilKt.applyInBatches(
            users,
            applicationProperties.getSqlBatchSizeForInConditions(),
            (subList) -> shiftWorkerRepository.findAllByWorkerInAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(subList, startTime, endTime)
                .forEach(shiftWorker -> result.remove(shiftWorker.getWorker()))
        );

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> filterWorkersWithMaximumWorkingWeekendsNotReached(List<User> users, Integer maximumWorkingWeekendsAllowed, Integer year, Month month) {
        if (users.size() == 0) {
            return users;
        }

        if (maximumWorkingWeekendsAllowed == null || maximumWorkingWeekendsAllowed == 0) {
            return Collections.emptyList();
        }

        final Map<User, Integer> usersWorkingWeekends = new HashMap<>();
        getWeekendsInMonth(year, month).forEach(pair -> {
            final Instant start = DateTimeUtil.getInstantAtStartOfDay(pair.getFirst());
            final Instant end = DateTimeUtil.getInstantAtEndOfDay(pair.getSecond());

            CollectionsUtilKt.applyInBatches(
                users,
                applicationProperties.getSqlBatchSizeForInConditions(),
                (subList) ->
                    shiftWorkerRepository.findAllByWorkerInAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(subList, start, end)
                        .forEach(shiftWorker -> {
                            usersWorkingWeekends.computeIfPresent(shiftWorker.getWorker(), (key, oldValue) -> oldValue + 1);
                            usersWorkingWeekends.computeIfAbsent(shiftWorker.getWorker(), key -> 1);
                        })
            );
        });

        return users.stream()
            .filter(user -> !usersWorkingWeekends.containsKey(user) ||  // user has no working weekend this month
                usersWorkingWeekends.get(user) < maximumWorkingWeekendsAllowed) // user has not reached maximum working weekends limit
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public boolean minimumFreeDaysAfterNightShiftsHadPassed(User user, Integer minimumFreeDaysAfterNightShifts, Instant futureShiftStart) {
        Optional<ShiftWorker> shiftWorkerOptional = shiftWorkerRepository.findFirstByWorkerAndShiftEndLessThanEqualOrderByShiftEndDesc(user, futureShiftStart);
        if (shiftWorkerOptional.isEmpty()) {
            return true;
        }

        final LocalDate shiftStartDate = DateTimeUtil.getDate(shiftWorkerOptional.get().getShift().getStart());
        final LocalDate shiftEndDate = DateTimeUtil.getDate(shiftWorkerOptional.get().getShift().getEnd());
        if (shiftStartDate.isEqual(shiftEndDate)) {
            return true; // this shift is a day shift aka starts and ends on the same day
        }

        final LocalDate futureShiftStartDateMinusMinimumFreeDays = DateTimeUtil.getDate(futureShiftStart)
            .minusDays(minimumFreeDaysAfterNightShifts.longValue());

        return futureShiftStartDateMinusMinimumFreeDays.isAfter(shiftEndDate) ||
            futureShiftStartDateMinusMinimumFreeDays.isEqual(shiftEndDate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> filterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(List<User> workers, Integer minimumFreeDaysAfterNightShifts, Instant futureShiftStart) {
        if (workers.size() == 0 || minimumFreeDaysAfterNightShifts == null || minimumFreeDaysAfterNightShifts == 0) {
            return workers;
        }

        return workers.stream()
            .filter(user -> minimumFreeDaysAfterNightShiftsHadPassed(user, minimumFreeDaysAfterNightShifts, futureShiftStart))
            .collect(Collectors.toList());
    }

    @Override
    public boolean checkCanRepeat(User user, LocalDate date) {
        Instant from = DateTimeUtil.getInstantAtStartOfDay(date.minusDays(1L));
        Instant to = DateTimeUtil.getInstantAtStartOfDay(date);
        return shiftWorkerRepository.findByWorkerAndShiftEndLessThanEqualAndShiftEndGreaterThanEqualAndShiftShiftTypeCanRepeatIsFalse(user, from, to).size() == 0;
    }
}
