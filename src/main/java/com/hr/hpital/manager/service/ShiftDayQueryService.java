package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.ShiftDay;
import com.hr.hpital.manager.domain.ShiftDay_;
import com.hr.hpital.manager.domain.ShiftType_;
import com.hr.hpital.manager.repository.ShiftDayRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.service.dto.ShiftDayCriteria;
import com.hr.hpital.manager.service.dto.ShiftDayDTO;
import com.hr.hpital.manager.service.mapper.ShiftDayMapper;

/**
 * Service for executing complex queries for {@link ShiftDay} entities in the database.
 * The main input is a {@link ShiftDayCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShiftDayDTO} or a {@link Page} of {@link ShiftDayDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShiftDayQueryService extends QueryService<ShiftDay> {

    private final Logger log = LoggerFactory.getLogger(ShiftDayQueryService.class);

    private final ShiftDayRepository shiftDayRepository;

    private final ShiftDayMapper shiftDayMapper;

    public ShiftDayQueryService(ShiftDayRepository shiftDayRepository, ShiftDayMapper shiftDayMapper) {
        this.shiftDayRepository = shiftDayRepository;
        this.shiftDayMapper = shiftDayMapper;
    }

    /**
     * Return a {@link List} of {@link ShiftDayDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShiftDayDTO> findByCriteria(ShiftDayCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShiftDay> specification = createSpecification(criteria);
        return shiftDayMapper.toDto(shiftDayRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShiftDayDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShiftDayDTO> findByCriteria(ShiftDayCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShiftDay> specification = createSpecification(criteria);
        return shiftDayRepository.findAll(specification, page)
            .map(shiftDayMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShiftDayCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShiftDay> specification = createSpecification(criteria);
        return shiftDayRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<ShiftDay> createSpecification(ShiftDayCriteria criteria) {
        Specification<ShiftDay> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShiftDay_.id));
            }
            if (criteria.getDayOfWeek() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDayOfWeek(), ShiftDay_.dayOfWeek));
            }
            if (criteria.getShiftTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getShiftTypeId(),
                    root -> root.join(ShiftDay_.shiftType, JoinType.LEFT).get(ShiftType_.id)));
            }
        }
        return specification;
    }
}
