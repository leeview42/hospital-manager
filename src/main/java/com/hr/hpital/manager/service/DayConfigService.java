package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.DayConfigDTO;
import com.hr.hpital.manager.domain.DayConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link DayConfig}.
 */
public interface DayConfigService {

    /**
     * Save a dayConfig.
     *
     * @param dayConfigDTO the entity to save.
     * @return the persisted entity.
     */
    DayConfigDTO save(DayConfigDTO dayConfigDTO);

    Optional<DayConfigDTO> find(DayConfigDTO dayConfigDTO, Long teamId);

    DayConfigDTO saveForWholeMonth(DayConfigDTO dayConfigDTO, List<Integer> weekDays);

    /**
     * Save a dayConfig.
     *
     * @param dayConfigDTO the entity to save.
     * @return the persisted entity.
     */
    DayConfigDTO update(DayConfigDTO dayConfigDTO);

    /**
     * Get all the dayConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DayConfigDTO> findAll(Pageable pageable);

    List<DayConfigDTO> findForInterval(LocalDate fromDate, LocalDate toDate, Long teamId);

    @Transactional()
    void deleteForInterval(LocalDate fromDate, LocalDate toDate, Long teamId);

    /**
     * Get the "id" dayConfig.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DayConfigDTO> findOne(Long id);

    /**
     * Delete the "id" dayConfig.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
