package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.ShiftTypeOverride;
import com.hr.hpital.manager.service.dto.ShiftTypeOverrideDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShiftTypeOverride} and its DTO {@link ShiftTypeOverrideDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShiftTypeMapper.class})
public interface ShiftTypeOverrideMapper extends EntityMapper<ShiftTypeOverrideDTO, ShiftTypeOverride> {

    @Mapping(source = "shiftType.id", target = "shiftTypeId")
    @Mapping(source = "canOverride.id", target = "canOverrideId")
    ShiftTypeOverrideDTO toDto(ShiftTypeOverride shiftTypeOverride);

    @Mapping(source = "shiftTypeId", target = "shiftType")
    @Mapping(source = "canOverrideId", target = "canOverride")
    ShiftTypeOverride toEntity(ShiftTypeOverrideDTO shiftTypeOverrideDTO);

    default ShiftTypeOverride fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShiftTypeOverride shiftTypeOverride = new ShiftTypeOverride();
        shiftTypeOverride.setId(id);
        return shiftTypeOverride;
    }
}
