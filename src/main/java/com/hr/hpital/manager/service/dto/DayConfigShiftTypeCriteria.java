package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.web.rest.DayConfigShiftTypeResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link DayConfigShiftType} entity. This class is used
 * in {@link DayConfigShiftTypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /day-config-shift-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DayConfigShiftTypeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter noOfWorkers;

    private LongFilter dayConfigId;

    private LongFilter shiftTypeId;

    public DayConfigShiftTypeCriteria(){
    }

    public DayConfigShiftTypeCriteria(DayConfigShiftTypeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.noOfWorkers = other.noOfWorkers == null ? null : other.noOfWorkers.copy();
        this.dayConfigId = other.dayConfigId == null ? null : other.dayConfigId.copy();
        this.shiftTypeId = other.shiftTypeId == null ? null : other.shiftTypeId.copy();
    }

    @Override
    public DayConfigShiftTypeCriteria copy() {
        return new DayConfigShiftTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNoOfWorkers() {
        return noOfWorkers;
    }

    public void setNoOfWorkers(IntegerFilter noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public LongFilter getDayConfigId() {
        return dayConfigId;
    }

    public void setDayConfigId(LongFilter dayConfigId) {
        this.dayConfigId = dayConfigId;
    }

    public LongFilter getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(LongFilter shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DayConfigShiftTypeCriteria that = (DayConfigShiftTypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(noOfWorkers, that.noOfWorkers) &&
            Objects.equals(dayConfigId, that.dayConfigId) &&
            Objects.equals(shiftTypeId, that.shiftTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        noOfWorkers,
        dayConfigId,
        shiftTypeId
        );
    }

    @Override
    public String toString() {
        return "DayConfigShiftTypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (noOfWorkers != null ? "noOfWorkers=" + noOfWorkers + ", " : "") +
                (dayConfigId != null ? "dayConfigId=" + dayConfigId + ", " : "") +
                (shiftTypeId != null ? "shiftTypeId=" + shiftTypeId + ", " : "") +
            "}";
    }

}
