package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.TeamUser;
import com.hr.hpital.manager.service.dto.TeamUserDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link TeamUser} and its DTO {@link TeamUserDTO}.
 */
@Mapper(componentModel = "spring", uses = {TeamMapper.class})
public interface TeamUserByUserMapper extends EntityMapper<TeamUserDTO, TeamUser> {

    TeamUserDTO toDto(TeamUser teamUser);

    TeamUser toEntity(TeamUserDTO teamUserDTO);

    default TeamUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        TeamUser teamUser = new TeamUser();
        teamUser.setId(id);
        return teamUser;
    }
}
