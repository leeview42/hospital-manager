package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.WorkerAvailability;
import com.hr.hpital.manager.service.dto.WorkerAvailabilityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link WorkerAvailability} and its DTO {@link WorkerAvailabilityDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface WorkerAvailabilityMapper extends EntityMapper<WorkerAvailabilityDTO, WorkerAvailability> {

    @Mapping(source = "user.id", target = "userId")
    WorkerAvailabilityDTO toDto(WorkerAvailability workerAvailability);

    @Mapping(source = "userId", target = "user")
    WorkerAvailability toEntity(WorkerAvailabilityDTO workerAvailabilityDTO);

    default WorkerAvailability fromId(Long id) {
        if (id == null) {
            return null;
        }
        WorkerAvailability workerAvailability = new WorkerAvailability();
        workerAvailability.setId(id);
        return workerAvailability;
    }
}
