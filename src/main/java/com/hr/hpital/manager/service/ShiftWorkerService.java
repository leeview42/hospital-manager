package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.ShiftWorker;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.service.dto.ShiftWorkerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service Interface for managing {@link ShiftWorker}.
 */
public interface ShiftWorkerService {

    /**
     * Save a shiftWorker.
     *
     * @param shiftWorkerDTO the entity to save.
     * @return the persisted entity.
     */
    ShiftWorkerDTO save(ShiftWorkerDTO shiftWorkerDTO);

    @Transactional(readOnly = true)
    Page<ShiftWorkerDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shiftWorker.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShiftWorkerDTO> findOne(Long id);

    /**
     * Delete the "id" shiftWorker.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Checks if user has already a shift assigned on a date.
     *
     * @param user for which the checking is done
     * @param date on which to check for existing shift
     * @return true in case user is already assigned to a shift on the date, false otherwise
     */
    boolean alreadyHasShiftAssigned(User user, LocalDate date);

    /**
     * Checks if user has already a shift assigned in the time interval provided.
     *
     * @param user      for which the checking is done
     * @param startTime the lower bound of the time interval
     * @param endTime   the upper bound of the time interval
     * @return true in case user is already assigned to a shift in the specified time interval false otherwise
     */
    boolean alreadyHasShiftAssigned(User user, Instant startTime, Instant endTime);

    /**
     * Filters out those users which already have shifts assigned in the time interval provided.
     *
     * @param users     for which the checking is done
     * @param startTime the lower bound of the time interval
     * @param endTime   the upper bound of the time interval
     * @return available users (those which are not assigned in the time interval)
     */
    List<User> filterUnassignedWorkers(List<User> users, Instant startTime, Instant endTime);

    List<User> filterWorkersWithMaximumWorkingWeekendsNotReached(List<User> users, Integer maximumWorkingWeekendsAllowed, Integer year, Month month);

    boolean minimumFreeDaysAfterNightShiftsHadPassed(User user, Integer minimumFreeDaysAfterNightShifts, Instant futureShiftStart);

    List<User> filterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(List<User> workers, Integer minimumFreeDaysAfterNightShifts, Instant futureShiftStart);

    boolean checkCanRepeat(User user, LocalDate date);
}
