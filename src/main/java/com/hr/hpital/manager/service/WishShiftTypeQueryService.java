package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.ShiftType_;
import com.hr.hpital.manager.domain.WishShiftType;
import com.hr.hpital.manager.domain.WishShiftType_;
import com.hr.hpital.manager.domain.Wish_;
import com.hr.hpital.manager.repository.WishShiftTypeRepository;
import com.hr.hpital.manager.service.dto.WishShiftTypeCriteria;
import com.hr.hpital.manager.service.dto.WishShiftTypeDTO;
import com.hr.hpital.manager.service.mapper.WishShiftTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link WishShiftType} entities in the database.
 * The main input is a {@link WishShiftTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WishShiftTypeDTO} or a {@link Page} of {@link WishShiftTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WishShiftTypeQueryService extends QueryService<WishShiftType> {

    private final Logger log = LoggerFactory.getLogger(WishShiftTypeQueryService.class);

    private final WishShiftTypeRepository wishShiftTypeRepository;

    private final WishShiftTypeMapper wishShiftTypeMapper;

    public WishShiftTypeQueryService(WishShiftTypeRepository wishShiftTypeRepository, WishShiftTypeMapper wishShiftTypeMapper) {
        this.wishShiftTypeRepository = wishShiftTypeRepository;
        this.wishShiftTypeMapper = wishShiftTypeMapper;
    }

    /**
     * Return a {@link List} of {@link WishShiftTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WishShiftTypeDTO> findByCriteria(WishShiftTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<WishShiftType> specification = createSpecification(criteria);
        return wishShiftTypeMapper.toDto(wishShiftTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link WishShiftTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WishShiftTypeDTO> findByCriteria(WishShiftTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<WishShiftType> specification = createSpecification(criteria);
        return wishShiftTypeRepository.findAll(specification, page)
            .map(wishShiftTypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(WishShiftTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<WishShiftType> specification = createSpecification(criteria);
        return wishShiftTypeRepository.count(specification);
    }

    /**
     * Function to convert WishShiftTypeCriteria to a {@link Specification}.
     */
    private Specification<WishShiftType> createSpecification(WishShiftTypeCriteria criteria) {
        Specification<WishShiftType> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WishShiftType_.id));
            }
            if (criteria.getWishId() != null) {
                specification = specification.and(buildSpecification(criteria.getWishId(),
                    root -> root.join(WishShiftType_.wish, JoinType.LEFT).get(Wish_.id)));
            }
            if (criteria.getShiftTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getShiftTypeId(),
                    root -> root.join(WishShiftType_.shiftType, JoinType.LEFT).get(ShiftType_.id)));
            }
        }
        return specification;
    }
}
