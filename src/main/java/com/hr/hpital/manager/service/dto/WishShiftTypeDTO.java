package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.WishShiftType;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link WishShiftType} entity.
 */
public class WishShiftTypeDTO implements Serializable {

    private Long id;


    private Long wishId;

    private Long shiftTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWishId() {
        return wishId;
    }

    public void setWishId(Long wishId) {
        this.wishId = wishId;
    }

    public Long getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(Long shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WishShiftTypeDTO wishShiftTypeDTO = (WishShiftTypeDTO) o;
        if (wishShiftTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wishShiftTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WishShiftTypeDTO{" +
            "id=" + getId() +
            ", wish=" + getWishId() +
            ", shiftType=" + getShiftTypeId() +
            "}";
    }
}
