package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.WishShiftTypeDTO;
import com.hr.hpital.manager.domain.WishShiftType;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link WishShiftType}.
 */
public interface WishShiftTypeService {

    /**
     * Save a wishShiftType.
     *
     * @param wishShiftTypeDTO the entity to save.
     * @return the persisted entity.
     */
    WishShiftTypeDTO save(WishShiftTypeDTO wishShiftTypeDTO);

    /**
     * Get all the wishShiftTypes.
     *
     * @return the list of entities.
     */
    List<WishShiftTypeDTO> findAll();


    /**
     * Get the "id" wishShiftType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WishShiftTypeDTO> findOne(Long id);

    /**
     * Delete the "id" wishShiftType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
