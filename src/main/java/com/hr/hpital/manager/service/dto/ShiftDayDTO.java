package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.ShiftDay;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ShiftDay} entity.
 */
public class ShiftDayDTO implements Serializable {

    private Long id;

    private Integer dayOfWeek;


    private Long shiftTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Long getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(Long shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShiftDayDTO shiftDayDTO = (ShiftDayDTO) o;
        if (shiftDayDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shiftDayDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShiftDayDTO{" +
            "id=" + getId() +
            ", dayOfWeek=" + getDayOfWeek() +
            ", shiftType=" + getShiftTypeId() +
            "}";
    }
}
