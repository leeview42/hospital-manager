package com.hr.hpital.manager.service.dto;

import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.enumeration.WishType;
import com.hr.hpital.manager.domain.Wish;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link Wish} entity.
 */
public class WishDTO implements Serializable {

    private Long id;

    private String comment;

    private Instant dateFrom;

    private Instant dateTo;

    @NotNull
    private WishType wishType;

    private LocalDate date;

    private Set<Long> shiftTypes = new HashSet<>();

    private Long userId;

    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Instant dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Instant getDateTo() {
        return dateTo;
    }

    public void setDateTo(Instant dateTo) {
        this.dateTo = dateTo;
    }

    public WishType getWishType() {
        return wishType;
    }

    public void setWishType(WishType wishType) {
        this.wishType = wishType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Long> getShiftTypes() {
        return shiftTypes;
    }

    public void setShiftTypes(Set<Long> shiftTypes) {
        this.shiftTypes = shiftTypes;
    }

    public LocalDate getDate() {
        if (this.date == null && this.dateTo != null) {
            return this.dateTo.atOffset(ZoneOffset.UTC).toLocalDate();
        }
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WishDTO wishDTO = (WishDTO) o;
        if (wishDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wishDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WishDTO{" +
            "id=" + getId() +
            ", comment='" + getComment() + "'" +
            ", dateFrom='" + getDateFrom() + "'" +
            ", dateTo='" + getDateTo() + "'" +
            ", wishType='" + getWishType() + "'" +
            ", user=" + getUserId() +
            "}";
    }
}
