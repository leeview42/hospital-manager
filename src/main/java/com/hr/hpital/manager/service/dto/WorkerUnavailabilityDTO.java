package com.hr.hpital.manager.service.dto;

import com.hr.hpital.manager.domain.WorkerAvailability;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link WorkerAvailability} entity.
 */
public class WorkerUnavailabilityDTO implements Serializable {

    private Long userId;

    private LocalDate startDateInterval;

    private LocalDate stopDateInterval;

    private Set<LocalDate> unavailableDays;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getStartDateInterval() {
        return startDateInterval;
    }

    public void setStartDateInterval(LocalDate startDateInterval) {
        this.startDateInterval = startDateInterval;
    }

    public LocalDate getStopDateInterval() {
        return stopDateInterval;
    }

    public void setStopDateInterval(LocalDate stopDateInterval) {
        this.stopDateInterval = stopDateInterval;
    }

    public Set<LocalDate> getUnavailableDays() {
        return unavailableDays;
    }

    public void setUnavailableDays(Set<LocalDate> unavailableDays) {
        this.unavailableDays = unavailableDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkerUnavailabilityDTO workerAvailabilityDTO = (WorkerUnavailabilityDTO) o;
        if (workerAvailabilityDTO.getUserId() == null || getUserId() == null ||
            workerAvailabilityDTO.getUnavailableDays() == null || getUnavailableDays() == null ||
            workerAvailabilityDTO.getStartDateInterval() == null || getStartDateInterval() == null ||
            workerAvailabilityDTO.getStopDateInterval() == null || getStopDateInterval() == null) {
            return false;
        }
        return Objects.equals(getUserId(), workerAvailabilityDTO.getUserId()) &&
            Objects.equals(getUnavailableDays(), workerAvailabilityDTO.getUnavailableDays()) &&
            Objects.equals(getStartDateInterval(), workerAvailabilityDTO.getStartDateInterval()) &&
            Objects.equals(getStopDateInterval(), workerAvailabilityDTO.getStopDateInterval());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUserId());
    }

    @Override
    public String toString() {
        return "WorkerAvailabilityDTO{" +
            " userId=" + getUserId() +
            ", startDateInterval='" + getStartDateInterval().toString() + "'" +
            ", stopDateInterval='" + getStopDateInterval().toString() + "'" +
            ", unavailableDays='" + getUnavailableDays().toString() + "'" +
            "}";
    }
}
