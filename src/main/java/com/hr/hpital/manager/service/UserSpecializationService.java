package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.UserDependenciesUpdateRequestDTO;
import com.hr.hpital.manager.service.dto.UserSpecializationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hr.hpital.manager.domain.UserSpecialization}.
 */
public interface UserSpecializationService {

    /**
     * Save a userSpecialization.
     *
     * @param userSpecializationDTO the entity to save.
     * @return the persisted entity.
     */
    UserSpecializationDTO save(UserSpecializationDTO userSpecializationDTO);

    /**
     * Get all the userSpecializations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserSpecializationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userSpecialization.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserSpecializationDTO> findOne(Long id);

    /**
     * Delete the "id" userSpecialization.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void saveUserSpecializations(UserDependenciesUpdateRequestDTO userDependenciesUpdateRequestDTO, Long organizationId);
}
