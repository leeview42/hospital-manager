package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.service.dto.ShiftDTO;

import com.hr.hpital.manager.domain.Shift;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import org.mapstruct.*;

import java.time.Instant;
import java.time.LocalDate;

/**
 * Mapper for the entity {@link Shift} and its DTO {@link ShiftDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShiftTypeMapper.class, UserMapper.class, ShiftWorkerMapper.class})
public interface ShiftMapper extends EntityMapper<ShiftDTO, Shift> {

    @Mapping(source = "shiftType.id", target = "shiftTypeId")
    @Mapping(source = "shiftType", target = "shiftType")
    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "updatedBy.id", target = "updatedById")
    @Mapping(source = "start", target = "date")
    ShiftDTO toDto(Shift shift);

    @Mapping(source = "shiftTypeId", target = "shiftType")
    @Mapping(source = "createdById", target = "createdBy")
    @Mapping(source = "updatedById", target = "updatedBy")
    Shift toEntity(ShiftDTO shiftDTO);

    default Shift fromId(Long id) {
        if (id == null) {
            return null;
        }
        Shift shift = new Shift();
        shift.setId(id);
        return shift;
    }

    default LocalDate dateTimeToDate(Instant dateTime) {
        return DateTimeUtil.getDate(dateTime);
    }
}
