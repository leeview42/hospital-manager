package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.ShiftDayDTO;

import com.hr.hpital.manager.domain.ShiftDay;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ShiftDay}.
 */
public interface ShiftDayService {

    /**
     * Save a shiftDay.
     *
     * @param shiftDayDTO the entity to save.
     * @return the persisted entity.
     */
    ShiftDayDTO save(ShiftDayDTO shiftDayDTO);

    /**
     * Get all the shiftDays.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShiftDayDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shiftDay.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShiftDayDTO> findOne(Long id);

    /**
     * Delete the "id" shiftDay.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
