package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.service.WishShiftTypeService;
import com.hr.hpital.manager.domain.WishShiftType;
import com.hr.hpital.manager.repository.WishShiftTypeRepository;
import com.hr.hpital.manager.service.dto.WishShiftTypeDTO;
import com.hr.hpital.manager.service.mapper.WishShiftTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link WishShiftType}.
 */
@Service
@Transactional
public class WishShiftTypeServiceImpl implements WishShiftTypeService {

    private final Logger log = LoggerFactory.getLogger(WishShiftTypeServiceImpl.class);

    private final WishShiftTypeRepository wishShiftTypeRepository;

    private final WishShiftTypeMapper wishShiftTypeMapper;

    public WishShiftTypeServiceImpl(WishShiftTypeRepository wishShiftTypeRepository, WishShiftTypeMapper wishShiftTypeMapper) {
        this.wishShiftTypeRepository = wishShiftTypeRepository;
        this.wishShiftTypeMapper = wishShiftTypeMapper;
    }

    /**
     * Save a wishShiftType.
     *
     * @param wishShiftTypeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WishShiftTypeDTO save(WishShiftTypeDTO wishShiftTypeDTO) {
        log.debug("Request to save WishShiftType : {}", wishShiftTypeDTO);
        WishShiftType wishShiftType = wishShiftTypeMapper.toEntity(wishShiftTypeDTO);
        wishShiftType = wishShiftTypeRepository.save(wishShiftType);
        return wishShiftTypeMapper.toDto(wishShiftType);
    }

    /**
     * Get all the wishShiftTypes.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<WishShiftTypeDTO> findAll() {
        log.debug("Request to get all WishShiftTypes");
        return wishShiftTypeRepository.findAll().stream()
            .map(wishShiftTypeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one wishShiftType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WishShiftTypeDTO> findOne(Long id) {
        log.debug("Request to get WishShiftType : {}", id);
        return wishShiftTypeRepository.findById(id)
            .map(wishShiftTypeMapper::toDto);
    }

    /**
     * Delete the wishShiftType by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WishShiftType : {}", id);
        wishShiftTypeRepository.deleteById(id);
    }
}
