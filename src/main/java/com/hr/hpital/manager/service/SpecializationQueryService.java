package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.domain.Specialization;
import com.hr.hpital.manager.domain.*; // for static metamodels
import com.hr.hpital.manager.repository.SpecializationRepository;
import com.hr.hpital.manager.service.dto.SpecializationCriteria;
import com.hr.hpital.manager.service.dto.SpecializationDTO;
import com.hr.hpital.manager.service.mapper.SpecializationMapper;

/**
 * Service for executing complex queries for {@link Specialization} entities in the database.
 * The main input is a {@link SpecializationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpecializationDTO} or a {@link Page} of {@link SpecializationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpecializationQueryService extends QueryService<Specialization> {

    private final Logger log = LoggerFactory.getLogger(SpecializationQueryService.class);

    private final SpecializationRepository specializationRepository;

    private final SpecializationMapper specializationMapper;

    public SpecializationQueryService(SpecializationRepository specializationRepository, SpecializationMapper specializationMapper) {
        this.specializationRepository = specializationRepository;
        this.specializationMapper = specializationMapper;
    }

    /**
     * Return a {@link List} of {@link SpecializationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpecializationDTO> findByCriteria(SpecializationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Specialization> specification = createSpecification(criteria);
        return specializationMapper.toDto(specializationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SpecializationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpecializationDTO> findByCriteria(SpecializationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Specialization> specification = createSpecification(criteria);
        return specializationRepository.findAll(specification, page)
            .map(specializationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpecializationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Specialization> specification = createSpecification(criteria);
        return specializationRepository.count(specification);
    }

    /**
     * Function to convert SpecializationCriteria to a {@link Specification}.
     */
    private Specification<Specialization> createSpecification(SpecializationCriteria criteria) {
        Specification<Specialization> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Specialization_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Specialization_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Specialization_.description));
            }
            if (criteria.getOrganizationId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrganizationId(),
                    root -> root.join(Specialization_.organization, JoinType.LEFT).get(Organization_.id)));
            }
        }
        return specification;
    }
}
