package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.service.dto.TeamUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TeamUser} and its DTO {@link TeamUserDTO}.
 */
@Mapper(componentModel = "spring", uses = {TeamMapper.class, UserMapper.class})
public interface TeamUserMapper extends EntityMapper<TeamUserDTO, TeamUser> {

    @Mapping(source = "team.id", target = "teamId")
    @Mapping(source = "user.id", target = "userId")
    TeamUserDTO toDto(TeamUser teamUser);

    @Mapping(source = "teamId", target = "team")
    @Mapping(source = "userId", target = "user")
    TeamUser toEntity(TeamUserDTO teamUserDTO);

    default TeamUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        TeamUser teamUser = new TeamUser();
        teamUser.setId(id);
        return teamUser;
    }
}
