package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.domain.DayConfigShiftTypeSpec;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeDTO;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeSpecDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link DayConfigShiftType} and its DTO {@link DayConfigShiftTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {SpecializationMapper.class})
public interface DayConfigShiftTypeSpecMapper extends EntityMapper<DayConfigShiftTypeSpecDTO, DayConfigShiftTypeSpec> {

    @Mapping(source = "specialization.id", target = "specializationId")
    DayConfigShiftTypeSpecDTO toDto(DayConfigShiftTypeSpec dayConfigShiftTypeSpec);

    @Mapping(source = "specializationId", target = "specialization")
    DayConfigShiftTypeSpec toEntity(DayConfigShiftTypeSpecDTO dayConfigShiftTypeSpecDTO);

    default DayConfigShiftTypeSpec fromId(Long id) {
        if (id == null) {
            return null;
        }
        DayConfigShiftTypeSpec dayConfigShiftTypeSpec = new DayConfigShiftTypeSpec();
        dayConfigShiftTypeSpec.setId(id);
        return dayConfigShiftTypeSpec;
    }
}
