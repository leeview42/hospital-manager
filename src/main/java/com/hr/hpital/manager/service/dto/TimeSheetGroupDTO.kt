package com.hr.hpital.manager.service.dto

class TimeSheetGroupDTO {
    var id: Long? = null

    var name: String? = null

    constructor(id: Long?, name: String?) {
        this.id = id
        this.name = name
    }

    constructor()
}
