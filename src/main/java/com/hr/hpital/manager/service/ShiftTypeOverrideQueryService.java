package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.ShiftTypeOverride_;
import com.hr.hpital.manager.domain.ShiftType_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.domain.ShiftTypeOverride;
import com.hr.hpital.manager.repository.ShiftTypeOverrideRepository;
import com.hr.hpital.manager.service.dto.ShiftTypeOverrideCriteria;
import com.hr.hpital.manager.service.dto.ShiftTypeOverrideDTO;
import com.hr.hpital.manager.service.mapper.ShiftTypeOverrideMapper;

/**
 * Service for executing complex queries for {@link ShiftTypeOverride} entities in the database.
 * The main input is a {@link ShiftTypeOverrideCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShiftTypeOverrideDTO} or a {@link Page} of {@link ShiftTypeOverrideDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShiftTypeOverrideQueryService extends QueryService<ShiftTypeOverride> {

    private final Logger log = LoggerFactory.getLogger(ShiftTypeOverrideQueryService.class);

    private final ShiftTypeOverrideRepository shiftTypeOverrideRepository;

    private final ShiftTypeOverrideMapper shiftTypeOverrideMapper;

    public ShiftTypeOverrideQueryService(ShiftTypeOverrideRepository shiftTypeOverrideRepository, ShiftTypeOverrideMapper shiftTypeOverrideMapper) {
        this.shiftTypeOverrideRepository = shiftTypeOverrideRepository;
        this.shiftTypeOverrideMapper = shiftTypeOverrideMapper;
    }

    /**
     * Return a {@link List} of {@link ShiftTypeOverrideDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShiftTypeOverrideDTO> findByCriteria(ShiftTypeOverrideCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShiftTypeOverride> specification = createSpecification(criteria);
        return shiftTypeOverrideMapper.toDto(shiftTypeOverrideRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShiftTypeOverrideDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShiftTypeOverrideDTO> findByCriteria(ShiftTypeOverrideCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShiftTypeOverride> specification = createSpecification(criteria);
        return shiftTypeOverrideRepository.findAll(specification, page)
            .map(shiftTypeOverrideMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShiftTypeOverrideCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShiftTypeOverride> specification = createSpecification(criteria);
        return shiftTypeOverrideRepository.count(specification);
    }

    /**
     * Function to convert ShiftTypeOverrideCriteria to a {@link Specification}.
     */
    private Specification<ShiftTypeOverride> createSpecification(ShiftTypeOverrideCriteria criteria) {
        Specification<ShiftTypeOverride> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShiftTypeOverride_.id));
            }
            if (criteria.getShiftTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getShiftTypeId(),
                    root -> root.join(ShiftTypeOverride_.shiftType, JoinType.LEFT).get(ShiftType_.id)));
            }
            if (criteria.getCanOverrideId() != null) {
                specification = specification.and(buildSpecification(criteria.getCanOverrideId(),
                    root -> root.join(ShiftTypeOverride_.canOverride, JoinType.LEFT).get(ShiftType_.id)));
            }
        }
        return specification;
    }
}
