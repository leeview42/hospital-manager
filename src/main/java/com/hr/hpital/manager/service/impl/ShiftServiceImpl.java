package com.hr.hpital.manager.service.impl;

import com.google.common.collect.Sets;
import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.repository.DayConfigShiftTypeRepository;
import com.hr.hpital.manager.repository.ShiftRepository;
import com.hr.hpital.manager.repository.ShiftTypeRepository;
import com.hr.hpital.manager.repository.UserShiftTypeRepository;
import com.hr.hpital.manager.service.*;
import com.hr.hpital.manager.service.dto.ShiftDTO;
import com.hr.hpital.manager.service.dto.UserDTO;
import com.hr.hpital.manager.service.mapper.ShiftMapper;
import com.hr.hpital.manager.service.mapper.UserMapper;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import com.hr.hpital.manager.web.rest.errors.WorkersAlreadyAssignedException;
import com.hr.hpital.manager.web.rest.errors.WorkersUnavailableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Shift}.
 */
@Service("shiftService")
@Transactional
public class ShiftServiceImpl implements ShiftService {

    private final Logger log = LoggerFactory.getLogger(ShiftServiceImpl.class);

    private final ShiftRepository shiftRepository;

    private final ShiftTypeRepository shiftTypeRepository;

    private final DayConfigShiftTypeService dayConfigShiftTypeService;

    private final DayConfigShiftTypeRepository dayConfigShiftTypeRepository;

    private final WorkerAvailabilityService workerAvailabilityService;

    private UserShiftTypeRepository userShiftTypeRepository;

    private final ShiftMapper shiftMapper;

    private UserService userService;

    private final RequestContext requestContext;

    private final TeamUserService teamUserService;

    private final ShiftWorkerService shiftWorkerService;

    private SpecializationService specializationService;

    private final UserMapper userMapper;

    public ShiftServiceImpl(ShiftRepository shiftRepository,
                            ShiftTypeRepository shiftTypeRepository,
                            DayConfigShiftTypeService dayConfigShiftTypeService,
                            DayConfigShiftTypeRepository dayConfigShiftTypeRepository,
                            WorkerAvailabilityService workerAvailabilityService,
                            UserShiftTypeRepository userShiftTypeRepository,
                            ShiftMapper shiftMapper,
                            UserService userService,
                            RequestContext requestContext,
                            TeamUserService teamUserService,
                            ShiftWorkerService shiftWorkerService,
                            SpecializationService specializationService,
                            UserMapper userMapper) {
        this.shiftRepository = shiftRepository;
        this.shiftTypeRepository = shiftTypeRepository;
        this.dayConfigShiftTypeService = dayConfigShiftTypeService;
        this.dayConfigShiftTypeRepository = dayConfigShiftTypeRepository;
        this.workerAvailabilityService = workerAvailabilityService;
        this.userShiftTypeRepository = userShiftTypeRepository;
        this.shiftMapper = shiftMapper;
        this.userService = userService;
        this.requestContext = requestContext;
        this.teamUserService = teamUserService;
        this.shiftWorkerService = shiftWorkerService;
        this.specializationService = specializationService;
        this.userMapper = userMapper;
    }

    public void setUserShiftTypeRepository(UserShiftTypeRepository userShiftTypeRepository) {
        this.userShiftTypeRepository = userShiftTypeRepository;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setSpecializationService(SpecializationService specializationService) {
        this.specializationService = specializationService;
    }

    /**
     * Save a shift.
     *
     * @param shiftDto the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShiftDTO save(ShiftDTO shiftDto) throws WorkersUnavailableException, WorkersAlreadyAssignedException {
        log.debug("Request to save Shift : {}", shiftDto);
        Shift shift = shiftMapper.toEntity(shiftDto);
        calculateShiftStartEnd(shiftDto.getDate(), shift);
        if (shiftDto.getUsers() != null) {
            Set<User> validUsers = getValidUsersFromDto(shiftDto);
            workersShouldNotBeInVacation(validUsers, shift);
            workersShouldNotBeAssignedToOtherShifts(validUsers, shift);
            shift.setUsers(validUsers);
        }
        shift = shiftRepository.save(shift);
        return shiftMapper.toDto(shift);
    }

    @Override
    public ShiftDTO update(ShiftDTO shiftDto) throws WorkersUnavailableException, WorkersAlreadyAssignedException {
        log.debug("Request to update Shift : {}", shiftDto);
        if (shiftDto.getId() != null) {
            Shift shift = shiftMapper.toEntity(shiftDto);
            calculateShiftStartEnd(shiftDto.getDate(), shift);
            if (shiftDto.getUsers() != null) {
                Shift shiftFromDb = shiftRepository.getOne(shiftDto.getId());
                Set<User> managedUsers = shiftFromDb.getUsers();
                Set<User> validUsersToUpdateShiftWith = getValidUsersFromDto(shiftDto);
                Set<User> newlyAssignedUsers = Sets.difference(validUsersToUpdateShiftWith, managedUsers);

                workersShouldNotBeInVacation(newlyAssignedUsers, shift);
                workersShouldNotBeAssignedToOtherShifts(newlyAssignedUsers, shift);

                // ShiftType is updated so we need to do a validate the already assigned workers
                if (!shiftFromDb.getShiftType().getId().equals(shiftDto.getShiftTypeId())) {
                    Set<User> existentUsers = Sets.intersection(validUsersToUpdateShiftWith, managedUsers);

                    workersShouldNotBeInVacation(existentUsers, shift);
                    workersShouldNotBeAssignedToOtherShifts(existentUsers, shift);
                }

                managedUsers.clear();
                managedUsers.addAll(validUsersToUpdateShiftWith);
                shift.setUsers(managedUsers);
            }
            shift = shiftRepository.save(shift);
            return shiftMapper.toDto(shift);
        }
        return shiftDto;
    }

    @Override
    public Shift create(ShiftType shiftType, LocalDate date, boolean isGenerated) {
        Shift shift = new Shift();
        User systemUser = userService.getUserWithAuthoritiesByLogin("system").orElse(null);
        shift.setCreateDate(Instant.now());
        shift.setCreatedBy(systemUser);
        shift.setStart(
            DateTimeUtil.getInstantAtStartOfDay(date)
                .plus(shiftType.getStartHour(), ChronoUnit.HOURS)
                .plus(shiftType.getStartMinute(), ChronoUnit.MINUTES)
        );
        shift.setEnd(shift.getStart().plus(shiftType.getDuration()));
        shift.setShiftType(shiftType);
        shift.setGenerated(isGenerated);
        shift.setUpdatedBy(systemUser);
        shift.setUpdateDate(Instant.now());
        return shift;
    }

    private void calculateShiftStartEnd(LocalDate date, Shift shift) {
        ShiftType shiftType = shiftTypeRepository.getOne(shift.getShiftType().getId());
        shift.setStart(calculateShiftStart(date, shiftType));
        shift.setEnd(calculateShiftEnd(shift.getStart(), shiftType));
    }

    private Instant calculateShiftStart(LocalDate date, ShiftType shiftType) {
        return DateTimeUtil.getInstantAtStartOfDay(date)
            .plus(shiftType.getStartHour(), ChronoUnit.HOURS)
            .plus(shiftType.getStartMinute(), ChronoUnit.MINUTES);
    }

    private Instant calculateShiftEnd(Instant shiftStart, ShiftType shiftType) {
        return shiftStart.plus(shiftType.getDuration());
    }

    private Set<User> getValidUsersFromDto(ShiftDTO shiftDTO) {
        return shiftDTO.getUsers()
            .stream()
            .map(UserDTO::getId)
            .map(userService::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .filter(user -> teamUserService.userIsTeamMember(user.getId(), requestContext.getTeamId()))
            .collect(Collectors.toSet());
    }

    private void workersShouldNotBeInVacation(Set<User> users, Shift shift) throws WorkersUnavailableException {
        Set<UserDTO> unavailableUsers = users.stream()
            .filter(user -> !workerAvailabilityService.isAvailable(user, shift.getStart(), shift.getEnd()))
            .map(userMapper::userToUserDTO)
            .collect(Collectors.toSet());

        if (unavailableUsers.size() > 0) {
            throw new WorkersUnavailableException(unavailableUsers);
        }
    }

    private void workersShouldNotBeAssignedToOtherShifts(Set<User> users, Shift shift) throws WorkersAlreadyAssignedException {
        Set<UserDTO> alreadyAssignedUsers = users.stream()
            .filter(user -> shiftWorkerService.alreadyHasShiftAssigned(user, shift.getStart(), shift.getEnd()))
            .map(userMapper::userToUserDTO)
            .collect(Collectors.toSet());

        if (alreadyAssignedUsers.size() > 0) {
            throw new WorkersAlreadyAssignedException(alreadyAssignedUsers);
        }
    }

    /**
     * Get all the shifts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShiftDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Shifts");
        return shiftRepository.findAll(pageable)
            .map(shiftMapper::toDto);
    }


    /**
     * Get one shift by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShiftDTO> findOne(Long id) {
        log.debug("Request to get Shift : {}", id);
        return shiftRepository.findById(id)
            .map(shiftMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ShiftDTO> findForInterval(Instant fromDate, Instant toDate, Long teamId) {
        return shiftRepository.findByStartBetweenAndShiftTypeTeamId(fromDate, toDate, teamId)
            .stream()
            .map(shiftMapper::toDto)
            .collect(Collectors.toList());
    }

    /**
     * Delete the shift by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Shift : {}", id);
        shiftRepository.deleteById(id);
    }

    @Override
    public void deleteAndGenerateShiftsForInterval(Instant from, Instant to, Long teamId) {
        deleteGeneratedShiftsForInterval(from, to, teamId);
        generateShiftsForInterval(DateTimeUtil.getDate(from), DateTimeUtil.getDate(to), teamId);
    }

    @Override
    public void deleteGeneratedShiftsForInterval(Instant from, Instant to, Long teamId) {
        this.shiftRepository.deleteByGeneratedIsTrueAndStartGreaterThanEqualAndEndLessThanEqualAndShiftTypeTeamId(from, to, teamId);
    }

    @Override
    public void deleteAllShiftsForInterval(Instant from, Instant to, Long teamId) {
        this.shiftRepository.deleteByStartGreaterThanEqualAndEndLessThanEqualAndShiftTypeTeamId(from, to, teamId);
    }

    @Override
    public void generateShiftsForInterval(LocalDate from, LocalDate to, Long teamId) {
        Set<DayConfigShiftType> dayCfgs = dayConfigShiftTypeRepository.findByDayConfigDateBetweenAndDayConfigTeamId(from, to, teamId);
        if (dayCfgs.size() == 0) {
            return;
        }

        dayCfgs.removeAll(
            generateShifts(dayCfgs, dayConfigShiftType -> dayConfigShiftType.getShiftType().isMandatory())
        );
        dayCfgs.removeAll(
            generateShifts(dayCfgs, dayConfigShiftType -> DateTimeUtil.isWeekendDay(dayConfigShiftType.getDayConfig().getDate()))
        );
        dayCfgs.removeAll(
            generateShifts(dayCfgs, dayConfigShiftType -> dayConfigShiftType.getDayConfig().isIsBankHoliday())
        );
        generateShifts(dayCfgs);
    }

    private Set<DayConfigShiftType> generateShifts(Set<DayConfigShiftType> dayCfgs, Predicate<DayConfigShiftType> filterSpecificCfgsPredicate) {
        Set<DayConfigShiftType> filteredConfigDays = dayConfigShiftTypeService.filter(dayCfgs, filterSpecificCfgsPredicate);
        generateShifts(filteredConfigDays);
        return filteredConfigDays;
    }

    public List<Shift> generateShifts(Set<DayConfigShiftType> dayCfgs) {
        if (dayCfgs.size() == 0) {
            return new ArrayList<>();
        }

        List<Shift> shifts = new ArrayList<>();

        dayCfgs.stream()
            .filter(toPopulate -> toPopulate.getShiftType() != null)
            .sorted(Comparator.comparing(o -> o.getShiftType().getPriority().getCode()))
            .map(this::populateShift)
            .forEach(shifts::add);

        dayCfgs.stream()
            .filter(toPopulate -> toPopulate.getShiftType() == null)
            .map(this::populateShift)
            .forEach(shifts::add);
        return shiftRepository.saveAll(shifts);
    }

    public Shift populateShift(DayConfigShiftType dayConfigShiftType) {
        List<User> possibleWorkers = userShiftTypeRepository.findByShiftType(dayConfigShiftType.getShiftType())
            .stream().map(UserShiftType::getUser).collect(Collectors.toList());

        final Shift shift = create(dayConfigShiftType.getShiftType(), dayConfigShiftType.getDayConfig().getDate(), true);
        if (possibleWorkers.size() == 0) {
            return shift;
        }

        possibleWorkers = filterWorkersByHardRules(possibleWorkers, dayConfigShiftType);
        if (possibleWorkers.size() == 0) {
            return shift;
        }

        if (dayConfigShiftType.getSpecializations().size() == 0) {
            assignWorkersToShift(possibleWorkers, shift, dayConfigShiftType.getNoOfWorkers());
        } else {
            populateShiftBySpecializations(dayConfigShiftType, possibleWorkers, shift);
        }

        return shift;
    }

    public void populateShiftBySpecializations(DayConfigShiftType dayConfigShiftType, List<User> possibleWorkers, Shift shift) {
        for (DayConfigShiftTypeSpec dayCfgShiftTypeSpec : dayConfigShiftType.getSpecializations()) {
            List<User> availableWorkers = specializationService.findUsersWhoCanWorkSpecialization(dayCfgShiftTypeSpec.getSpecialization(), possibleWorkers);
            List<User> assignedWorkers = assignWorkersToShift(availableWorkers, shift, dayCfgShiftTypeSpec.getNoOfWorkers());
            possibleWorkers.removeAll(assignedWorkers);
        }
    }

    public List<User> assignWorkersToShift(List<User> availableWorkers, Shift shift, Integer noOfWorkersNeeded) {
        if (noOfWorkersNeeded == 0) {
            return new ArrayList<>();
        }

        // we found exactly the number of workers needed or less
        if (availableWorkers.size() <= noOfWorkersNeeded) {
            shift.getUsers().addAll(availableWorkers);
            return availableWorkers;
        } else { // we found more workers than needed so we need to randomly assign them, this way nobody will get upset :)
            Collections.shuffle(availableWorkers);
            List<User> assignedUsers = availableWorkers.subList(0, noOfWorkersNeeded);
            shift.getUsers().addAll(assignedUsers);
            return assignedUsers;
        }
    }

    public List<User> filterWorkersByHardRules(List<User> possibleWorkers, DayConfigShiftType dayConfigShiftType) {
        final Instant shiftStart = calculateShiftStart(dayConfigShiftType.getDayConfig().getDate(), dayConfigShiftType.getShiftType());
        final Instant shiftEnd = calculateShiftEnd(shiftStart, dayConfigShiftType.getShiftType());

        possibleWorkers = shiftWorkerService.filterUnassignedWorkers(possibleWorkers, shiftStart, shiftEnd);
        possibleWorkers = workerAvailabilityService.filterAvailableUsers(possibleWorkers, shiftStart, shiftEnd);

        // do the maximum weekends filtering only in case our shift is a weekend shift
        if (DateTimeUtil.intersectsWeekend(shiftEnd, shiftEnd)) {
            possibleWorkers = shiftWorkerService.filterWorkersWithMaximumWorkingWeekendsNotReached(
                possibleWorkers,
                dayConfigShiftType.getShiftType().getTeam().getMaximumWorkingWeekends(),
                dayConfigShiftType.getDayConfig().getDate().getYear(),
                dayConfigShiftType.getDayConfig().getDate().getMonth()
            );
        }

        // do the minimum free days after night shifts only if this shift is not night shift
        // night shift definition: starts in one day, ends in another
        if (DateTimeUtil.getDate(shiftStart).isEqual(DateTimeUtil.getDate(shiftEnd))) {
            possibleWorkers = shiftWorkerService.filterWorkersForWhomMinimumFreeDaysAfterNightShiftsHadPassed(
                possibleWorkers, dayConfigShiftType.getShiftType().getTeam().getMinimumFreeDaysAfterNightShifts(), shiftStart
            );
        }

        return possibleWorkers;
    }
}
