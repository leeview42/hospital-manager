package com.hr.hpital.manager.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hr.hpital.manager.domain.UserSpecialization} entity.
 */
public class UserSpecializationDTO implements Serializable {

    private Long id;


    private Long userId;

    private Long specializationId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(Long specializationId) {
        this.specializationId = specializationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserSpecializationDTO userSpecializationDTO = (UserSpecializationDTO) o;
        if (userSpecializationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userSpecializationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserSpecializationDTO{" +
            "id=" + getId() +
            ", user=" + getUserId() +
            ", specialization=" + getSpecializationId() +
            "}";
    }
}
