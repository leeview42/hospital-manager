package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.ShiftType;

import java.time.Duration;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ShiftType} entity.
 */
public class ShiftTypeDTO implements Serializable {

    private Long id;

    private String name;

    private String code;

    private Boolean active;

    private String description;

    private String displayCode;

    private Boolean canRepeat;

    @Min(value = 0)
    @Max(value = 23)
    private Integer startHour;

    @Min(value = 0)
    @Max(value = 59)
    private Integer startMinute;

    private Duration duration;

    private Long durationMillis;

    private String priorityCode;

    private Boolean fillWithRemaining;

    private Integer noOfWorkers;

    private Long priorityId;

    private Boolean mandatory;

    private Long teamId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

    public Boolean isCanRepeat() {
        return canRepeat;
    }

    public void setCanRepeat(Boolean canRepeat) {
        this.canRepeat = canRepeat;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public void setStartHour(Integer startHour) {
        this.startHour = startHour;
    }

    public Integer getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(Integer startMinute) {
        this.startMinute = startMinute;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
        if (duration != null && durationMillis == null) {
            this.durationMillis = duration.toMillis();
        }
    }

    public Long getDurationMillis() {
        return durationMillis;
    }

    public void setDurationMillis(Long durationMillis) {
        this.durationMillis = durationMillis;
    }

    public Boolean isFillWithRemaining() {
        return fillWithRemaining;
    }

    public void setFillWithRemaining(Boolean fillWithRemaining) {
        this.fillWithRemaining = fillWithRemaining;
    }

    public Integer getNoOfWorkers() {
        return noOfWorkers;
    }

    public void setNoOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) {
        this.priorityCode = priorityCode;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    public Boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShiftTypeDTO shiftTypeDTO = (ShiftTypeDTO) o;
        if (shiftTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shiftTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShiftTypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", active='" + isActive() + "'" +
            ", description='" + getDescription() + "'" +
            ", canRepeat='" + isCanRepeat() + "'" +
            ", startHour=" + getStartHour() +
            ", startMinute=" + getStartMinute() +
            ", duration='" + getDuration() + "'" +
            ", fillWithRemaining='" + isFillWithRemaining() + "'" +
            ", noOfWorkers=" + getNoOfWorkers() +
            ", priority=" + getPriorityId() +
            ", mandatory='" + isMandatory() +
            "}";
    }

    public ShiftTypeDTO(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public ShiftTypeDTO() {
    }
}
