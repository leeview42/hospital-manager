package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.service.dto.ShiftWorkerDTO;

import com.hr.hpital.manager.domain.ShiftWorker;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShiftWorker} and its DTO {@link ShiftWorkerDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShiftMapper.class, UserMapper.class})
public interface ShiftWorkerMapper extends EntityMapper<ShiftWorkerDTO, ShiftWorker> {

    @Mapping(source = "shift.id", target = "shiftId")
    @Mapping(source = "shift", target = "shift")
    @Mapping(source = "worker", target = "worker")
    @Mapping(source = "worker.id", target = "workerId")
    ShiftWorkerDTO toDto(ShiftWorker shiftWorker);

    @Mapping(source = "shiftId", target = "shift")
    @Mapping(source = "workerId", target = "worker")
    ShiftWorker toEntity(ShiftWorkerDTO shiftWorkerDTO);

    default ShiftWorker fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShiftWorker shiftWorker = new ShiftWorker();
        shiftWorker.setId(id);
        return shiftWorker;
    }
}
