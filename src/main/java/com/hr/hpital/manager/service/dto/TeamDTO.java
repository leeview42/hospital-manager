package com.hr.hpital.manager.service.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.hr.hpital.manager.domain.Team} entity.
 */
public class TeamDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    private Long organizationId;

    private Set<UserDTO> users = new HashSet<>();

    private Set<Long> managers = new HashSet<>();

    private Set<Long> members = new HashSet<>();

    private Set<ShiftTypeDTO> shiftTypes = new HashSet<>();

    @Min(0)
    @Max(5)
    private Integer maximumWorkingWeekends;

    @Min(0)
    private Integer minimumFreeDaysAfterNightShifts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }

    public Set<Long> getManagers() {
        return managers;
    }

    public void setManagers(Set<Long> managers) {
        this.managers = managers;
    }

    public Set<Long> getMembers() {
        return members;
    }

    public void setMembers(Set<Long> members) {
        this.members = members;
    }

    public Set<ShiftTypeDTO> getShiftTypes() {
        return shiftTypes;
    }

    public void setShiftTypes(Set<ShiftTypeDTO> shiftTypes) {
        this.shiftTypes = shiftTypes;
    }

    public Integer getMaximumWorkingWeekends() {
        return maximumWorkingWeekends;
    }

    public void setMaximumWorkingWeekends(Integer maximumWorkingWeekends) {
        this.maximumWorkingWeekends = maximumWorkingWeekends;
    }

    public Integer getMinimumFreeDaysAfterNightShifts() {
        return minimumFreeDaysAfterNightShifts;
    }

    public void setMinimumFreeDaysAfterNightShifts(Integer minimumFreeDaysAfterNightShifts) {
        this.minimumFreeDaysAfterNightShifts = minimumFreeDaysAfterNightShifts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TeamDTO teamDTO = (TeamDTO) o;
        if (teamDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), teamDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TeamDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", organization=" + getOrganizationId() +
            "}";
    }
}
