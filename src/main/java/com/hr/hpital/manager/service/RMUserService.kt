package com.hr.hpital.manager.service

import com.hr.hpital.manager.client.RMConnectorClient
import com.hr.hpital.manager.config.RmProperties
import com.hr.hpital.manager.domain.Organization
import com.hr.hpital.manager.domain.Team
import com.hr.hpital.manager.domain.User
import com.hr.hpital.manager.repository.TeamRepository
import com.hr.hpital.manager.repository.UserRepository
import com.hr.hpital.manager.security.AuthoritiesConstants
import com.hr.hpital.manager.service.dto.RMUserDTO
import com.hr.hpital.manager.service.dto.TimeSheetGroupDTO
import com.hr.hpital.manager.service.dto.UserDTO
import com.hr.hpital.manager.service.mapper.UserMapper
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.time.ZonedDateTime


/**
 * Service class for managing rm users.
 */
@Service
@Profile("rm")
@Transactional
class RMUserService(private val userMapper: UserMapper,
                    private val userRepository: UserRepository,
                    private val teamRepository: TeamRepository,
                    private val teamUserService: TeamUserService,
                    private val passwordEncoder: PasswordEncoder,
                    private val rmProperties: RmProperties,
                    private val rmConnectorClient: RMConnectorClient) {
    private val organization = Organization(rmProperties.organizationId)
    private val log = LoggerFactory.getLogger(RMUserService::class.java)

    companion object {
        val defaultAuthorities = setOf(AuthoritiesConstants.USER, AuthoritiesConstants.TEAM_MEMBER);
    }
    private val timeKeeperAuthorities = defaultAuthorities.plus(AuthoritiesConstants.TEAM_MANAGER);

    private final val DEFAULT_LANGUAGE = "ro";

    fun migrateRmUser(rmUserDTO: RMUserDTO) {
        try {
            if (rmUserDTO.user != null && rmUserDTO.uniquePassword != null) {
                userRepository.findOneByLogin(rmUserDTO.user).ifPresentOrElse({
                    var olderThan = ZonedDateTime.now().minusHours(rmProperties.dataExpirationHours!!).toInstant()
                    if (it.migrateDate != null && it.migrateDate.isBefore(olderThan)) {
                        this.updateUser(rmUserDTO, it)
                        this.saveUser(rmUserDTO, it)
                    }
                }, {
                    this.createUser(rmUserDTO);
                })
            }
        } catch (e: Exception) {
            log.error("Could not migrate user with login ${rmUserDTO.user}", e)
        }
    }

    private fun updateUser(rmUser: RMUserDTO, user: User) {
        setFirstAndLastName(rmUser, user)
        if (rmUser.blocked != null && rmUser.status != null) {
            user.activated = !(rmUser.blocked!! && rmUser.status!!)
        }
        user.lastModifiedDate = Instant.now();
    }


    private fun updateUser(rmUser: RMUserDTO, user: UserDTO): UserDTO {
        setFirstAndLastName(rmUser, user)
        if (rmUser.blocked != null && rmUser.status != null) {
            user.isActivated = !(rmUser.blocked!! && rmUser.status!!)
        }
        user.lastModifiedDate = Instant.now();
        return user;
    }

    fun createUser(rmUserDTO: RMUserDTO) {
        val user = UserDTO();
        this.updateUser(rmUserDTO, user);
        user.login = rmUserDTO.user;
        user.createdDate = Instant.now();
        user.organizationId = this.rmProperties.organizationId;
        user.authorities = timeKeeperAuthorities;
        user.langKey = DEFAULT_LANGUAGE;
        user.externalId = rmUserDTO.id
        this.saveUser(rmUserDTO, user);
    }

    private fun saveUser(rmUser: RMUserDTO, userDTO: UserDTO) {
        var user = this.userMapper.userDTOToUser(userDTO);
        this.saveUser(rmUser, user)
    }

    private fun saveUser(rmUser: RMUserDTO, user: User) {
        user.externalId = rmUser.id
        if (rmUser.uniquePassword != null) {
            user.password = passwordEncoder.encode(rmUser.uniquePassword)
        }
        user.migrateDate = Instant.now();
        this.userRepository.save(user);
    }

    private fun setFirstAndLastName(rmUser: RMUserDTO, user: UserDTO) {
        if (rmUser.userName != null) {
            val userNames = listOf(*rmUser.userName!!.split(" ".toRegex()).toTypedArray())
            user.lastName = userNames[0]
            if (userNames.size > 1) {
                user.firstName = userNames.drop(1).joinToString(" ");
            }
        }
    }

    private fun setFirstAndLastName(rmUser: RMUserDTO, user: User) {
        if (rmUser.userName != null) {
            val userNames = listOf(*rmUser.userName!!.split(" ".toRegex()).toTypedArray())
            user.lastName = userNames[0]
            if (userNames.size > 1) {
                user.firstName = userNames.drop(1).joinToString(" ");
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun migrateTimeKeeperUser(userId: Long) {
        this.rmConnectorClient.getConnectorUserById(userId).ifPresentOrElse({
            try {
                this.migrateRmUser(it)
                var timeSheets = this.rmConnectorClient.getTimeSheetGroupsForUser(it.id)
                var managerOpt = userRepository.findOneByLogin(it.user)
                if (managerOpt.isPresent) {
                    this.migrateManagerTeams(managerOpt.get(), timeSheets)
                }
            } catch (e: Exception) {
                log.error("Could not migrate user. Message: ${e.message}", e)
            }
        }, { this.log.warn("Could not find user with id $userId") })
    }

    private fun migrateManagerTeams(manager: User, timeSheets: List<TimeSheetGroupDTO>) {
        timeSheets.forEach {
            var team = this.teamRepository.findByExternalId(it.id).orElse(Team(it.name, it.id, this.organization))
            var olderThan = ZonedDateTime.now().minusHours(rmProperties.dataExpirationHours!!).toInstant()
            if (team.id != null && team.importDate != null && team.importDate.isBefore(olderThan)) {
                team.importDate = Instant.now()
                this.teamRepository.save(team)
                this.migrateTeamUsers(manager, team)
            } else if (team.id == null) {
                team.importDate = Instant.now()
                team = this.teamRepository.save(team)
                this.migrateTeamUsers(manager, team);
            }
            this.teamUserService.makeTeamManager(team.id, manager.id)

        }
    }

    fun migrateTeamUsers(manager: User, team: Team) {
        team.users.clear()
        team.users.add(manager)
        this.rmConnectorClient.getTimeSheetGroupEmployeesForUser(manager.externalId, team.externalId).forEach { it ->
            this.migrateRmUser(it)
            this.userRepository.findOneByLogin(it.user).ifPresent{
                team.users.add(it);
            }
        }
        this.teamRepository.save(team);
    }

}
