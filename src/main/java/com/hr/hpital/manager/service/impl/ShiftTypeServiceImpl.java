package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.service.ShiftTypeService;
import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.repository.ShiftTypeRepository;
import com.hr.hpital.manager.service.dto.ShiftTypeDTO;
import com.hr.hpital.manager.service.mapper.ShiftTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ShiftType}.
 */
@Service("shiftTypeService")
@Transactional
public class ShiftTypeServiceImpl implements ShiftTypeService {

    private final Logger log = LoggerFactory.getLogger(ShiftTypeServiceImpl.class);

    private final ShiftTypeRepository shiftTypeRepository;

    private final ShiftTypeMapper shiftTypeMapper;

    public ShiftTypeServiceImpl(ShiftTypeRepository shiftTypeRepository, ShiftTypeMapper shiftTypeMapper) {
        this.shiftTypeRepository = shiftTypeRepository;
        this.shiftTypeMapper = shiftTypeMapper;
    }

    /**
     * Save a shiftType.
     *
     * @param shiftTypeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShiftTypeDTO save(ShiftTypeDTO shiftTypeDTO) {
        log.debug("Request to save ShiftType : {}", shiftTypeDTO);
        ShiftType shiftType = shiftTypeMapper.toEntity(shiftTypeDTO);
        if (shiftType.getDuration() == null && shiftTypeDTO.getDurationMillis() != null) {
            shiftType.setDuration(Duration.ofMillis(shiftTypeDTO.getDurationMillis()));
        }
        shiftType = shiftTypeRepository.save(shiftType);
        return shiftTypeMapper.toDto(shiftType);
    }

    /**
     * Get all the shiftTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShiftTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShiftTypes");
        return shiftTypeRepository.findAll(pageable)
            .map(shiftTypeMapper::toDto);
    }


    /**
     * Get one shiftType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShiftTypeDTO> findOne(Long id) {
        log.debug("Request to get ShiftType : {}", id);
        return shiftTypeRepository.findById(id)
            .map(shiftTypeMapper::toDto);
    }

    /**
     * Delete the shiftType by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShiftType : {}", id);
        shiftTypeRepository.deleteById(id);
    }
}
