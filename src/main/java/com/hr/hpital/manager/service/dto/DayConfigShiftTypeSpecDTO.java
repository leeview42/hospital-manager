package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.DayConfigShiftType;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link DayConfigShiftType} entity.
 */
public class DayConfigShiftTypeSpecDTO implements Serializable {

    private Long id;

    private Integer noOfWorkers;

    private Long specializationId;

    private SpecializationDTO specialization;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfWorkers() {
        return noOfWorkers;
    }

    public void setNoOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public Long getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(Long specializationId) {
        this.specializationId = specializationId;
    }

    public SpecializationDTO getSpecialization() {
        return specialization;
    }

    public void setSpecialization(SpecializationDTO specialization) {
        this.specialization = specialization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DayConfigShiftTypeSpecDTO that = (DayConfigShiftTypeSpecDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(noOfWorkers, that.noOfWorkers) &&
            Objects.equals(specializationId, that.specializationId) &&
            Objects.equals(specialization, that.specialization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, noOfWorkers, specializationId, specialization);
    }
}
