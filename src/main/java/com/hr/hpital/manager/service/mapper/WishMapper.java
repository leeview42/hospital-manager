package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.service.dto.WishDTO;

import com.hr.hpital.manager.domain.Wish;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Wish} and its DTO {@link WishDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ShiftTypeMapper.class, UserMapper.class})
public interface WishMapper extends EntityMapper<WishDTO, Wish> {

    @Mapping(source = "user.id", target = "userId")
    WishDTO toDto(Wish wish);

    @Mapping(source = "userId", target = "user")
    Wish toEntity(WishDTO wishDTO);

    default Wish fromId(Long id) {
        if (id == null) {
            return null;
        }
        Wish wish = new Wish();
        wish.setId(id);
        return wish;
    }
}
