package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.domain.DayConfigShiftTypeSpec;
import com.hr.hpital.manager.repository.DayConfigShiftTypeSpecRepository;
import com.hr.hpital.manager.service.DayConfigShiftTypeService;
import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.repository.DayConfigShiftTypeRepository;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeDTO;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeSpecDTO;
import com.hr.hpital.manager.service.mapper.DayConfigShiftTypeMapper;
import com.hr.hpital.manager.service.mapper.DayConfigShiftTypeSpecMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Service Implementation for managing {@link DayConfigShiftType}.
 */
@Service
@Transactional
public class DayConfigShiftTypeServiceImpl implements DayConfigShiftTypeService {

  private final Logger log = LoggerFactory.getLogger(DayConfigShiftTypeServiceImpl.class);

  private final DayConfigShiftTypeRepository dayConfigShiftTypeRepository;

  private final DayConfigShiftTypeSpecRepository dayConfigShiftTypeSpecRepository;

  private final DayConfigShiftTypeSpecMapper dayConfigShiftTypeSpecMapper;

  private final DayConfigShiftTypeMapper dayConfigShiftTypeMapper;

  public DayConfigShiftTypeServiceImpl(
      DayConfigShiftTypeRepository dayConfigShiftTypeRepository,
      DayConfigShiftTypeSpecRepository dayConfigShiftTypeSpecRepository, DayConfigShiftTypeSpecMapper dayConfigShiftTypeSpecMapper, DayConfigShiftTypeMapper dayConfigShiftTypeMapper) {
    this.dayConfigShiftTypeRepository = dayConfigShiftTypeRepository;
      this.dayConfigShiftTypeSpecRepository = dayConfigShiftTypeSpecRepository;
      this.dayConfigShiftTypeSpecMapper = dayConfigShiftTypeSpecMapper;
      this.dayConfigShiftTypeMapper = dayConfigShiftTypeMapper;
  }

  /**
   * Save a dayConfigShiftType.
   *
   * @param dayConfigShiftTypeDTO
   *          the entity to save.
   * @return the persisted entity.
   */
  @Override
  public DayConfigShiftTypeDTO save(DayConfigShiftTypeDTO dayConfigShiftTypeDTO) {
    log.debug("Request to save DayConfigShiftType : {}", dayConfigShiftTypeDTO);
    DayConfigShiftType dayConfigShiftType = dayConfigShiftTypeMapper.toEntity(dayConfigShiftTypeDTO);
    dayConfigShiftType = dayConfigShiftTypeRepository.save(dayConfigShiftType);
    return dayConfigShiftTypeMapper.toDto(dayConfigShiftType);
  }

  /**
   * Get all the dayConfigShiftTypes.
   *
   * @param pageable
   *          the pagination information.
   * @return the list of entities.
   */
  @Override
  @Transactional(readOnly = true)
  public Page<DayConfigShiftTypeDTO> findAll(Pageable pageable) {
    log.debug("Request to get all DayConfigShiftTypes");
    return dayConfigShiftTypeRepository.findAll(pageable).map(dayConfigShiftTypeMapper::toDto);
  }

  /**
   * Get one dayConfigShiftType by id.
   *
   * @param id
   *          the id of the entity.
   * @return the entity.
   */
  @Override
  @Transactional(readOnly = true)
  public Optional<DayConfigShiftTypeDTO> findOne(Long id) {
    log.debug("Request to get DayConfigShiftType : {}", id);
    return dayConfigShiftTypeRepository.findById(id).map(dayConfigShiftTypeMapper::toDto);
  }

  /**
   * Delete the dayConfigShiftType by id.
   *
   * @param id
   *          the id of the entity.
   */
  @Override
  public void delete(Long id) {
    log.debug("Request to delete DayConfigShiftType : {}", id);
    dayConfigShiftTypeRepository.deleteById(id);
  }

  @Override
  public void saveDayConfigShiftTypes(DayConfig dayConfig, List<DayConfigShiftTypeDTO> dayConfigShiftTypes) {
      if (dayConfig != null && dayConfig.getId() != null && dayConfigShiftTypes != null) {
          dayConfigShiftTypes.forEach(dayConfigShiftTypeDTO -> {
              Set<DayConfigShiftTypeSpecDTO> specs = dayConfigShiftTypeDTO.getSpecializations();
              dayConfigShiftTypeDTO.setSpecializations(null);
              dayConfigShiftTypeDTO.setDayConfigId(dayConfig.getId());
              DayConfigShiftType dayConfigShiftType = this.dayConfigShiftTypeRepository.save(this.dayConfigShiftTypeMapper.toEntity(dayConfigShiftTypeDTO));
              if (specs!=null && specs.size() > 0) {
                  this.saveDayConfigShiftTypeSpecs(dayConfigShiftType, specs);
              }
          });
      }
  }

  private void saveDayConfigShiftTypeSpecs(DayConfigShiftType dayConfigShiftType, Set<DayConfigShiftTypeSpecDTO> specs) {
      this.dayConfigShiftTypeSpecRepository.saveAll(specs.stream().map(spec -> {
          DayConfigShiftTypeSpec specEntity = this.dayConfigShiftTypeSpecMapper.toEntity(spec);
          specEntity.setDayConfigShiftType(dayConfigShiftType);
          return specEntity;
      }).collect(Collectors.toList()));
  }

    /**
     * Filters a set of DayConfigShiftType's using a predicate.
     *
     * @param dayConfigShiftTypes the set to filter
     * @param predicate           predicate to apply for every element of the set
     * @return a set containing the elements which fulfilled the predicate
     */
    @Override
    public Set<DayConfigShiftType> filter(Set<DayConfigShiftType> dayConfigShiftTypes, Predicate<DayConfigShiftType> predicate) {
        return dayConfigShiftTypes.stream().filter(predicate).collect(Collectors.toSet());
    }
}
