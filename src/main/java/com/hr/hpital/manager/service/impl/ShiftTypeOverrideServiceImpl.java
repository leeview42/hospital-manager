package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.service.ShiftTypeOverrideService;
import com.hr.hpital.manager.domain.ShiftTypeOverride;
import com.hr.hpital.manager.repository.ShiftTypeOverrideRepository;
import com.hr.hpital.manager.service.dto.ShiftTypeOverrideDTO;
import com.hr.hpital.manager.service.mapper.ShiftTypeOverrideMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShiftTypeOverride}.
 */
@Service
@Transactional
public class ShiftTypeOverrideServiceImpl implements ShiftTypeOverrideService {

    private final Logger log = LoggerFactory.getLogger(ShiftTypeOverrideServiceImpl.class);

    private final ShiftTypeOverrideRepository shiftTypeOverrideRepository;

    private final ShiftTypeOverrideMapper shiftTypeOverrideMapper;

    public ShiftTypeOverrideServiceImpl(ShiftTypeOverrideRepository shiftTypeOverrideRepository, ShiftTypeOverrideMapper shiftTypeOverrideMapper) {
        this.shiftTypeOverrideRepository = shiftTypeOverrideRepository;
        this.shiftTypeOverrideMapper = shiftTypeOverrideMapper;
    }

    /**
     * Save a shiftTypeOverride.
     *
     * @param shiftTypeOverrideDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShiftTypeOverrideDTO save(ShiftTypeOverrideDTO shiftTypeOverrideDTO) {
        log.debug("Request to save ShiftTypeOverride : {}", shiftTypeOverrideDTO);
        ShiftTypeOverride shiftTypeOverride = shiftTypeOverrideMapper.toEntity(shiftTypeOverrideDTO);
        shiftTypeOverride = shiftTypeOverrideRepository.save(shiftTypeOverride);
        return shiftTypeOverrideMapper.toDto(shiftTypeOverride);
    }

    /**
     * Get all the shiftTypeOverrides.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShiftTypeOverrideDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShiftTypeOverrides");
        return shiftTypeOverrideRepository.findAll(pageable)
            .map(shiftTypeOverrideMapper::toDto);
    }


    /**
     * Get one shiftTypeOverride by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShiftTypeOverrideDTO> findOne(Long id) {
        log.debug("Request to get ShiftTypeOverride : {}", id);
        return shiftTypeOverrideRepository.findById(id)
            .map(shiftTypeOverrideMapper::toDto);
    }

    /**
     * Delete the shiftTypeOverride by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShiftTypeOverride : {}", id);
        shiftTypeOverrideRepository.deleteById(id);
    }
}
