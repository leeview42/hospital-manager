package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.UserShiftType;
import com.hr.hpital.manager.web.rest.UserDependenciesResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link UserShiftType} entity. This class is used
 * in {@link UserDependenciesResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-shift-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserShiftTypeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter ratioPercentage;

    private LongFilter userId;

    private LongFilter shiftTypeId;

    public UserShiftTypeCriteria(){
    }

    public UserShiftTypeCriteria(UserShiftTypeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.ratioPercentage = other.ratioPercentage == null ? null : other.ratioPercentage.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.shiftTypeId = other.shiftTypeId == null ? null : other.shiftTypeId.copy();
    }

    @Override
    public UserShiftTypeCriteria copy() {
        return new UserShiftTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getRatioPercentage() {
        return ratioPercentage;
    }

    public void setRatioPercentage(IntegerFilter ratioPercentage) {
        this.ratioPercentage = ratioPercentage;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(LongFilter shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserShiftTypeCriteria that = (UserShiftTypeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ratioPercentage, that.ratioPercentage) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(shiftTypeId, that.shiftTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ratioPercentage,
        userId,
        shiftTypeId
        );
    }

    @Override
    public String toString() {
        return "UserShiftTypeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ratioPercentage != null ? "ratioPercentage=" + ratioPercentage + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (shiftTypeId != null ? "shiftTypeId=" + shiftTypeId + ", " : "") +
            "}";
    }

}
