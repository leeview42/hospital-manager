package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.service.TeamService;
import com.hr.hpital.manager.service.UserShiftTypeService;
import com.hr.hpital.manager.domain.UserShiftType;
import com.hr.hpital.manager.repository.UserShiftTypeRepository;
import com.hr.hpital.manager.service.dto.ShiftTypeDTO;
import com.hr.hpital.manager.service.dto.UserShiftTypeDTO;
import com.hr.hpital.manager.service.dto.UserDependenciesUpdateRequestDTO;
import com.hr.hpital.manager.service.mapper.UserShiftTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UserShiftType}.
 */
@Service
@Transactional
public class UserShiftTypeServiceImpl implements UserShiftTypeService {

    private final Logger log = LoggerFactory.getLogger(UserShiftTypeServiceImpl.class);

    private final UserShiftTypeRepository userShiftTypeRepository;

    private final UserShiftTypeMapper userShiftTypeMapper;

    private final TeamService teamService;

    public UserShiftTypeServiceImpl(UserShiftTypeRepository userShiftTypeRepository, UserShiftTypeMapper userShiftTypeMapper, TeamService teamService) {
        this.userShiftTypeRepository = userShiftTypeRepository;
        this.userShiftTypeMapper = userShiftTypeMapper;
        this.teamService = teamService;
    }

    /**
     * Save a userShiftType.
     *
     * @param userShiftTypeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserShiftTypeDTO save(UserShiftTypeDTO userShiftTypeDTO) {
        log.debug("Request to save UserShiftType : {}", userShiftTypeDTO);
        UserShiftType userShiftType = userShiftTypeMapper.toEntity(userShiftTypeDTO);
        userShiftType = userShiftTypeRepository.save(userShiftType);
        return userShiftTypeMapper.toDto(userShiftType);
    }

    /**
     * Get all the userShiftTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserShiftTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserShiftTypes");
        return userShiftTypeRepository.findAll(pageable)
            .map(userShiftTypeMapper::toDto);
    }


    /**
     * Get one userShiftType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserShiftTypeDTO> findOne(Long id) {
        log.debug("Request to get UserShiftType : {}", id);
        return userShiftTypeRepository.findById(id)
            .map(userShiftTypeMapper::toDto);
    }

    /**
     * Delete the userShiftType by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserShiftType : {}", id);
        userShiftTypeRepository.deleteById(id);
    }

    @Override
    public void saveUserShiftTypes(UserDependenciesUpdateRequestDTO userDependenciesUpdateRequestDTO, Long teamId) {
        this.teamService.findOne(teamId).ifPresent(team -> {
            Set<Long> teamShiftTypes = team.getShiftTypes().stream().map(ShiftTypeDTO::getId).collect(Collectors.toSet());
            this.userShiftTypeRepository.deleteByUserIdAndShiftTypeTeamId(userDependenciesUpdateRequestDTO.getUserId(), teamId);
            this.userShiftTypeRepository.saveAll(userDependenciesUpdateRequestDTO.getDependencyEntityIds().stream()
                .filter(teamShiftTypes::contains)
                .map(shiftTypeId -> {
                    User user = new User();
                    user.setId(userDependenciesUpdateRequestDTO.getUserId());
                    ShiftType shiftType = new ShiftType();
                    shiftType.setId(shiftTypeId);
                    UserShiftType userShiftType = new UserShiftType();
                    userShiftType.setShiftType(shiftType);
                    userShiftType.setUser(user);
                    return userShiftType;
                }).collect(Collectors.toSet()));
        });
    }
}
