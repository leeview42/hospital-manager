package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.PriorityDTO;

import com.hr.hpital.manager.domain.Priority;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Priority}.
 */
public interface PriorityService {

    /**
     * Save a priority.
     *
     * @param priorityDTO the entity to save.
     * @return the persisted entity.
     */
    PriorityDTO save(PriorityDTO priorityDTO);

    /**
     * Get all the priorities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PriorityDTO> findAll(Pageable pageable);


    /**
     * Get the "id" priority.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PriorityDTO> findOne(Long id);

    /**
     * Delete the "id" priority.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
