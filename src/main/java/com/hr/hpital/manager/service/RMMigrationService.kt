package com.hr.hpital.manager.service

import com.hr.hpital.manager.client.RMConnectorClient
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.time.Instant
import java.util.*


/**
 * Service class for managing rm users.
 */
@Service
@Profile("rm")
@Transactional
class RMMigrationService(
                         private val rmUserService: RMUserService,
                         private val rmConnectorClient: RMConnectorClient) {
    private val log = LoggerFactory.getLogger(RMMigrationService::class.java)

    fun migrateTimeKeeperUsers() {
        log.info("migration started!")
        var start = Instant.now();
        this.rmConnectorClient.getTimekeeperUserIds(Date()).stream().forEach {
            log.info("Trying to migrate user with external id $it")
            try {
                this.rmUserService.migrateTimeKeeperUser(it)
            } catch (e: Exception) {
                log.error("Could not migrate user.", e)
            }
        }
        var finish = Instant.now();
        var elapsed = Duration.between(start, finish)
        log.info("Import took ${elapsed.toSeconds()} seconds")
    }

}
