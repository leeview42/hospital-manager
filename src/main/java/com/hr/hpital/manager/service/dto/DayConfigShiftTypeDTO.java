package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.domain.ShiftType;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link DayConfigShiftType} entity.
 */
public class DayConfigShiftTypeDTO implements Serializable {

    private Long id;

    private Integer noOfWorkers;

    private Long dayConfigId;

    private Long shiftTypeId;

    private ShiftTypeDTO shiftType;

    private Set<DayConfigShiftTypeSpecDTO> specializations;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfWorkers() {
        return noOfWorkers;
    }

    public void setNoOfWorkers(Integer noOfWorkers) {
        this.noOfWorkers = noOfWorkers;
    }

    public Long getDayConfigId() {
        return dayConfigId;
    }

    public void setDayConfigId(Long dayConfigId) {
        this.dayConfigId = dayConfigId;
    }

    public Long getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(Long shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    public Set<DayConfigShiftTypeSpecDTO> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(Set<DayConfigShiftTypeSpecDTO> specializations) {
        this.specializations = specializations;
    }

    public DayConfigShiftTypeDTO(DayConfigShiftTypeDTO other) {
        this.id = other.id;
        this.noOfWorkers = other.noOfWorkers;
        this.dayConfigId = other.dayConfigId;
        this.shiftTypeId = other.shiftTypeId;
        this.shiftType = other.shiftType;
        this.specializations = other.specializations;
    }

    public DayConfigShiftTypeDTO() {
    }

    public ShiftTypeDTO getShiftType() {
        return shiftType;
    }

    public void setShiftType(ShiftTypeDTO shiftType) {
        this.shiftType = shiftType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DayConfigShiftTypeDTO dayConfigShiftTypeDTO = (DayConfigShiftTypeDTO) o;
        if (dayConfigShiftTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dayConfigShiftTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DayConfigShiftTypeDTO{" +
            "id=" + getId() +
            ", noOfWorkers=" + getNoOfWorkers() +
            ", dayConfig=" + getDayConfigId() +
            ", shiftType=" + getShiftTypeId() +
            "}";
    }

}
