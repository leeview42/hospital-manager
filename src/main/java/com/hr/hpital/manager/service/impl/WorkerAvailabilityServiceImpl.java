package com.hr.hpital.manager.service.impl;

import com.google.common.collect.Sets;
import com.hr.hpital.manager.config.ApplicationProperties;
import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.UserShiftType;
import com.hr.hpital.manager.domain.WorkerAvailability;
import com.hr.hpital.manager.repository.UserRepository;
import com.hr.hpital.manager.repository.UserShiftTypeRepository;
import com.hr.hpital.manager.repository.WorkerAvailabilityRepository;
import com.hr.hpital.manager.service.ShiftWorkerService;
import com.hr.hpital.manager.service.WishService;
import com.hr.hpital.manager.service.WorkerAvailabilityService;
import com.hr.hpital.manager.service.dto.WorkerAvailabilityDTO;
import com.hr.hpital.manager.service.dto.WorkerUnavailabilityDTO;
import com.hr.hpital.manager.service.mapper.UserMapper;
import com.hr.hpital.manager.service.mapper.WorkerAvailabilityMapper;
import com.hr.hpital.manager.service.util.CollectionsUtilKt;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import com.hr.hpital.manager.service.util.ShiftUtil;
import com.hr.hpital.manager.web.rest.errors.WorkersAlreadyAssignedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

import static com.hr.hpital.manager.web.rest.util.ResourceUtil.toLocalDate;

/**
 * Service Implementation for managing {@link WorkerAvailability}.
 */
@Service
@Transactional
public class WorkerAvailabilityServiceImpl implements WorkerAvailabilityService {

    private final Logger log = LoggerFactory.getLogger(WorkerAvailabilityServiceImpl.class);

    private final WorkerAvailabilityRepository workerAvailabilityRepository;

    private final UserRepository userRepository;

    private final WorkerAvailabilityMapper workerAvailabilityMapper;

    private final UserShiftTypeRepository userShiftTypeRepository;

    private final WishService wishService;

    private final ShiftWorkerService shiftWorkerService;

    private final UserMapper userMapper;

    private final ApplicationProperties appProps;

    public WorkerAvailabilityServiceImpl(WorkerAvailabilityRepository workerAvailabilityRepository,
                                         WorkerAvailabilityMapper workerAvailabilityMapper,
                                         UserRepository userRepository,
                                         UserShiftTypeRepository userShiftTypeRepository,
                                         WishService wishService,
                                         ShiftWorkerService shiftWorkerService,
                                         UserMapper userMapper,
                                         ApplicationProperties appProps) {
        this.workerAvailabilityRepository = workerAvailabilityRepository;
        this.workerAvailabilityMapper = workerAvailabilityMapper;
        this.userRepository = userRepository;
        this.userShiftTypeRepository = userShiftTypeRepository;
        this.wishService = wishService;
        this.shiftWorkerService = shiftWorkerService;
        this.userMapper = userMapper;
        this.appProps = appProps;
    }

    /**
     * Save a workerAvailability.
     *
     * @param workerAvailabilityDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WorkerAvailabilityDTO save(WorkerAvailabilityDTO workerAvailabilityDTO) {
        log.debug("Request to save WorkerAvailability : {}", workerAvailabilityDTO);
        WorkerAvailability workerAvailability = workerAvailabilityMapper.toEntity(workerAvailabilityDTO);
        workerAvailability = workerAvailabilityRepository.save(workerAvailability);
        return workerAvailabilityMapper.toDto(workerAvailability);
    }

    /**
     * Get all the workerAvailabilities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WorkerAvailabilityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WorkerAvailabilities");
        return workerAvailabilityRepository.findAll(pageable)
            .map(workerAvailabilityMapper::toDto);
    }


    /**
     * Get one workerAvailability by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WorkerAvailabilityDTO> findOne(Long id) {
        log.debug("Request to get WorkerAvailability : {}", id);
        return workerAvailabilityRepository.findById(id)
            .map(workerAvailabilityMapper::toDto);
    }

    /**
     * Delete the workerAvailability by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WorkerAvailability : {}", id);
        workerAvailabilityRepository.deleteById(id);
    }

    @Override
    public List<User> findAvailableUsersForShiftTypeAndDate(ShiftType shiftType, LocalDate date) {
        if (shiftType != null && date != null) {
            Instant shiftStartTime = ShiftUtil.getShiftStartTime(shiftType, date);
            Instant shiftEndTime = ShiftUtil.getShiftEndTime(shiftType, shiftStartTime, appProps.getMinimumPauseHours());
            List<User> availableUsers = userShiftTypeRepository.findByShiftType(shiftType).stream()
                .map(UserShiftType::getUser)
                .filter(user -> !this.shiftWorkerService.alreadyHasShiftAssigned(user, shiftStartTime, shiftEndTime))
                .filter(user -> this.isAvailable(user, shiftStartTime, shiftEndTime))
                .filter(user -> this.shiftWorkerService.checkCanRepeat(user, date))
                .filter(user -> wishService.canUserTakeShiftTypeOnDate(user, shiftType, date))
                .filter(user -> this.checkSpecialWeekendCondition(user, shiftType, date))
                .collect(Collectors.toList());
            Collections.shuffle(availableUsers);
            return availableUsers;
        }
        return new ArrayList<>();
    }

    /**
     * This method chec
     *
     * @param user
     * @param shiftType
     * @param date
     * @return
     */
    private boolean checkSpecialWeekendCondition(User user, ShiftType shiftType, LocalDate date) {
        if (date.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
            List<WorkerAvailability> workerAvailabilities =
                this.workerAvailabilityRepository.findByUserAndDateFromLessThanAndDateToGreaterThanAndIsAvailableIsFalse(
                    user, DateTimeUtil.getInstantAtEndOfDay(date.plusDays(1)), DateTimeUtil.getInstantAtStartOfDay(date.plusDays(1))
                );

            if (workerAvailabilities.size() > 0) {
                if (shiftType.getStartHour() != null && shiftType.getStartMinute() != null) {
                    LocalTime shiftEndTime = LocalTime.of(shiftType.getStartHour(), shiftType.getStartMinute(), 0);
                    if (shiftType.getDuration() != null) {
                        shiftEndTime = shiftEndTime.plusSeconds(shiftType.getDuration().getSeconds());
                    }
                    LocalTime weekendStartTime = LocalTime.of(this.appProps.getWeekendStartHour(), this.appProps.getWeekendStartMinute());
                    if (weekendStartTime.isBefore(shiftEndTime)) {
                        return false;
                    }
                }
            }

        }
        return true;
    }

    @Override
    public boolean isAvailable(User user, Instant from, Instant to) {
        return 0L == this.workerAvailabilityRepository.countByUserAndDateFromLessThanAndDateToGreaterThanAndIsAvailableIsFalse(user, to, from);
    }

    @Override
    public List<LocalDate> findUnavailableDays(Long userId, Date dateFrom, Date dateTo, Long teamId) {
        return workerAvailabilityRepository
            .findByUserIdAndDateFromBetweenAndIsAvailableIsFalseAndUserTeamsId(
                userId, dateFrom.toInstant(), dateTo.toInstant(), teamId
            )
            .stream()
            .map(workerAvailabilityMapper::toDto)
            .map(dto -> dto.getDateFrom().atOffset(ZoneOffset.UTC).toLocalDate())
            .collect(Collectors.toList());
    }

    @Override
    public void updateWorkerUnavailabilities(WorkerUnavailabilityDTO workerUnavailabilityDTO) throws WorkersAlreadyAssignedException {
        final User user = userRepository.getOne(workerUnavailabilityDTO.getUserId());
        userShotNotBeAssignedToShifts(user, workerUnavailabilityDTO.getUnavailableDays());

        Set<WorkerAvailability> existentWorkerAvailabilitySet =
            workerAvailabilityRepository.findByUserIdAndDateFromBetween(
                workerUnavailabilityDTO.getUserId(),
                DateTimeUtil.getInstantAtStartOfDay(workerUnavailabilityDTO.getStartDateInterval()),
                DateTimeUtil.getInstantAtStartOfDay(workerUnavailabilityDTO.getStopDateInterval())
            );

        Set<LocalDate> unavailableDaysSet = workerUnavailabilityDTO.getUnavailableDays();

        // set existent availabilities to false
        Set<WorkerAvailability> workerAvailabilitiesToSave = new HashSet<>();
        Set<WorkerAvailability> workerAvailabilityToRemoveSet = new HashSet<>();
        existentWorkerAvailabilitySet.stream()
            .filter(workerAvailability -> unavailableDaysSet.contains(toLocalDate(workerAvailability.getDateFrom())))
            .forEach(workerAvailability -> {
                if (workerAvailability.isIsAvailable()) {
                    workerAvailability.setIsAvailable(false);
                    workerAvailabilitiesToSave.add(workerAvailability);
                }

                unavailableDaysSet.remove(toLocalDate(workerAvailability.getDateFrom()));
                workerAvailabilityToRemoveSet.add(workerAvailability);
            });
        existentWorkerAvailabilitySet.removeAll(workerAvailabilityToRemoveSet);
        workerAvailabilityRepository.saveAll(workerAvailabilitiesToSave);

        // remove existent availabilities already marked with false that should be marked with true
        workerAvailabilityRepository.deleteInBatch(existentWorkerAvailabilitySet);

        if (unavailableDaysSet.size() > 0) {
            // create new availabilities marked with false
            workerAvailabilityRepository.saveAll(
                unavailableDaysSet
                    .stream()
                    .map(unavailableDay -> {
                        WorkerAvailability workerAvailability = new WorkerAvailability();
                        workerAvailability.setDateFrom(DateTimeUtil.getInstantAtStartOfDay(unavailableDay));
                        workerAvailability.setDateTo(DateTimeUtil.getInstantAtEndOfDay(unavailableDay));
                        workerAvailability.setUser(user);
                        workerAvailability.setIsAvailable(false);
                        return workerAvailability;
                    })
                    .collect(Collectors.toSet())
            );
        }
    }

    private void userShotNotBeAssignedToShifts(User user, Set<LocalDate> unavailableDays) throws WorkersAlreadyAssignedException {
        Set<LocalDate> shiftDays = unavailableDays.stream()
            .filter(unavailableDay -> shiftWorkerService.alreadyHasShiftAssigned(user, unavailableDay))
            .collect(Collectors.toSet());

        if (shiftDays.size() > 0) {
            throw new WorkersAlreadyAssignedException(Sets.newHashSet(userMapper.userToUserDTO(user)), shiftDays);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> filterAvailableUsers(List<User> users, Instant from, Instant to) {
        if (users.size() == 0) {
            return users;
        }

        final Set<User> unavailableUsers = new HashSet<>();
        CollectionsUtilKt.applyInBatches(
            users,
            appProps.getSqlBatchSizeForInConditions(),
            (subList) -> workerAvailabilityRepository.findAllByUserInAndDateToGreaterThanAndDateFromLessThanAndIsAvailableIsFalse(subList, from, to)
                .forEach(workerAvailability -> unavailableUsers.add(workerAvailability.getUser()))
        );

        return users.stream().filter(user -> !unavailableUsers.contains(user)).collect(Collectors.toList());
    }
}
