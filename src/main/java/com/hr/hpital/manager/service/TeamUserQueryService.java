package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.domain.TeamUser;
import com.hr.hpital.manager.domain.*; // for static metamodels
import com.hr.hpital.manager.repository.TeamUserRepository;
import com.hr.hpital.manager.service.dto.TeamUserCriteria;
import com.hr.hpital.manager.service.dto.TeamUserDTO;
import com.hr.hpital.manager.service.mapper.TeamUserMapper;

/**
 * Service for executing complex queries for {@link TeamUser} entities in the database.
 * The main input is a {@link TeamUserCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TeamUserDTO} or a {@link Page} of {@link TeamUserDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TeamUserQueryService extends QueryService<TeamUser> {

    private final Logger log = LoggerFactory.getLogger(TeamUserQueryService.class);

    private final TeamUserRepository teamUserRepository;

    private final TeamUserMapper teamUserMapper;

    public TeamUserQueryService(TeamUserRepository teamUserRepository, TeamUserMapper teamUserMapper) {
        this.teamUserRepository = teamUserRepository;
        this.teamUserMapper = teamUserMapper;
    }

    /**
     * Return a {@link List} of {@link TeamUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TeamUserDTO> findByCriteria(TeamUserCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TeamUser> specification = createSpecification(criteria);
        return teamUserMapper.toDto(teamUserRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TeamUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TeamUserDTO> findByCriteria(TeamUserCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TeamUser> specification = createSpecification(criteria);
        return teamUserRepository.findAll(specification, page)
            .map(teamUserMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TeamUserCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TeamUser> specification = createSpecification(criteria);
        return teamUserRepository.count(specification);
    }

    /**
     * Function to convert TeamUserCriteria to a {@link Specification}.
     */
    private Specification<TeamUser> createSpecification(TeamUserCriteria criteria) {
        Specification<TeamUser> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TeamUser_.id));
            }
            if (criteria.getIsManager() != null) {
                specification = specification.and(buildSpecification(criteria.getIsManager(), TeamUser_.isManager));
            }
            if (criteria.getTeamId() != null) {
                specification = specification.and(buildSpecification(criteria.getTeamId(),
                    root -> root.join(TeamUser_.team, JoinType.LEFT).get(Team_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(TeamUser_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
