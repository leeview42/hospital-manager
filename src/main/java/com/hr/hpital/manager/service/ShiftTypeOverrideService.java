package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.ShiftTypeOverrideDTO;

import com.hr.hpital.manager.domain.ShiftTypeOverride;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ShiftTypeOverride}.
 */
public interface ShiftTypeOverrideService {

    /**
     * Save a shiftTypeOverride.
     *
     * @param shiftTypeOverrideDTO the entity to save.
     * @return the persisted entity.
     */
    ShiftTypeOverrideDTO save(ShiftTypeOverrideDTO shiftTypeOverrideDTO);

    /**
     * Get all the shiftTypeOverrides.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShiftTypeOverrideDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shiftTypeOverride.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShiftTypeOverrideDTO> findOne(Long id);

    /**
     * Delete the "id" shiftTypeOverride.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
