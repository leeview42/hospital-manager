package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.Team;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.repository.TeamRepository;
import com.hr.hpital.manager.repository.UserRepository;
import com.hr.hpital.manager.service.OrganizationService;
import com.hr.hpital.manager.service.TeamQueryService;
import com.hr.hpital.manager.service.TeamService;
import com.hr.hpital.manager.service.TeamUserService;
import com.hr.hpital.manager.service.dto.TeamCriteria;
import com.hr.hpital.manager.service.dto.TeamDTO;
import com.hr.hpital.manager.service.mapper.TeamMapper;
import com.hr.hpital.manager.service.mapper.UserMapper;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.web.rest.util.ResourceUtil;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Team}.
 */
@Service("teamService")
@Transactional
public class TeamServiceImpl implements TeamService {

    private final Logger log = LoggerFactory.getLogger(TeamServiceImpl.class);

    private final TeamRepository teamRepository;

    private final TeamMapper teamMapper;

    private final UserRepository userRepository;

    private final OrganizationService organizationService;

    private final TeamQueryService teamQueryService;

    private final UserMapper userMapper;

    private final TeamUserService teamUserService;

    public TeamServiceImpl(TeamRepository teamRepository,
                           TeamMapper teamMapper,
                           UserRepository userRepository,
                           OrganizationService organizationService, TeamQueryService teamQueryService, UserMapper userMapper, TeamUserService teamUserService) {
        this.teamRepository = teamRepository;
        this.teamMapper = teamMapper;
        this.userRepository = userRepository;
        this.organizationService = organizationService;
        this.teamQueryService = teamQueryService;
        this.userMapper = userMapper;
        this.teamUserService = teamUserService;
    }

    /**
     * Save a team.
     *
     * @param teamDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TeamDTO save(TeamDTO teamDTO) {
        log.debug("Request to save Team : {}", teamDTO);
        putOrganizationId(teamDTO);
        Team team = teamMapper.toEntity(teamDTO);
        team.setUsers(this.mapUsers(teamDTO));
        team = teamRepository.save(team);
        teamUserService.updateTeamManagers(team.getId(), teamDTO.getManagers());
        return teamMapper.toDto(team);
    }

    /**
     * Update a team.
     *
     * @param teamDTO the entity to update.
     * @return the persisted entity.
     */
    @Override
    public TeamDTO update(TeamDTO teamDTO) {
        log.debug("Request to update Team : {}", teamDTO);
        putOrganizationId(teamDTO);
        Set<User> managedUsersInTeam = teamRepository.findById(teamDTO.getId())
            .orElseThrow(() -> new BadRequestAlertException("Invalid id", "team", "idNotExists"))
            .getUsers();
        managedUsersInTeam.clear();
        managedUsersInTeam.addAll(this.mapUsers(teamDTO));
        Team team = teamMapper.toEntity(teamDTO);
        team.setUsers(managedUsersInTeam);
        team = teamRepository.save(team);
        this.teamUserService.updateTeamManagers(team.getId(), teamDTO.getManagers());
        return teamMapper.toDto(team);
    }

    private void putOrganizationId(TeamDTO teamDTO) {
        organizationService
            .findOneByUserLogin()
            .ifPresent(organizationDTO -> teamDTO.setOrganizationId(organizationDTO.getId()));
    }

    /**
     * Get all the teams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TeamDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Teams");
        return teamRepository.findAll(pageable)
            .map(teamMapper::toDto);
    }


    /**
     * Get one team by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TeamDTO> findOne(Long id) {
        log.debug("Request to get Team : {}", id);
        return teamRepository.findById(id)
            .map(teamMapper::toDto).map(teamUserService::mapTeamMembers);
    }

    /**
     * Delete the team by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Team : {}", id);
        teamRepository.deleteById(id);
    }

    private Set<User> mapUsers(TeamDTO teamDTO) {
        Long orgId = ResourceUtil.getCurrentUserOrgId();
        Set<User> users = new HashSet<>();
        if (orgId != null) {
            if (teamDTO.getManagers() != null) {
                users.addAll(teamDTO.getManagers().stream().map(userId -> userRepository.findByIdAndOrganizationId(userId, orgId))
                    .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
            }
            if (teamDTO.getMembers() != null) {
                users.addAll(teamDTO.getMembers().stream().map(userId -> userRepository.findByIdAndOrganizationId(userId, orgId))
                    .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
            }
        }
        return users;
    }

    @Override
    public Long countByIdAndOrganizationId(Long id, Long organizationId) {
        TeamCriteria criteria = new TeamCriteria();
        LongFilter organizationFilter = new LongFilter();
        organizationFilter.setEquals(organizationId);
        LongFilter idFilter = new LongFilter();
        idFilter.setEquals(id);
        criteria.setOrganizationId(organizationFilter);
        criteria.setId(idFilter);
        return this.teamQueryService.countByCriteria(criteria);
    }
}
