package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.Wish;
import com.hr.hpital.manager.domain.enumeration.WishType;
import com.hr.hpital.manager.repository.ShiftTypeRepository;
import com.hr.hpital.manager.repository.WishRepository;
import com.hr.hpital.manager.service.WishService;
import com.hr.hpital.manager.service.dto.WishDTO;
import com.hr.hpital.manager.service.mapper.WishMapper;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Wish}.
 */
@Service
@Transactional
public class WishServiceImpl implements WishService {

    private final Logger log = LoggerFactory.getLogger(WishServiceImpl.class);

    private final WishRepository wishRepository;

    private final ShiftTypeRepository shiftTypeRepository;

    private final WishMapper wishMapper;

    public WishServiceImpl(WishRepository wishRepository, ShiftTypeRepository shiftTypeRepository, WishMapper wishMapper) {
        this.wishRepository = wishRepository;
        this.shiftTypeRepository = shiftTypeRepository;
        this.wishMapper = wishMapper;
    }

    /**
     * Save a wish.
     *
     * @param wishDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WishDTO save(WishDTO wishDTO) {
        log.debug("Request to save Wish : {}", wishDTO);
        convertDtoDateToInterval(wishDTO);
        Wish wish = wishMapper.toEntity(wishDTO);
        wish = wishRepository.save(wish);
        return wishMapper.toDto(wish);
    }

    @Override
    public WishDTO saveForWholeMonth(WishDTO wishDTO, List<Integer> weekDays) {
        WishDTO response = this.save(wishDTO);
        Long dayConfigId = response.getId();
        this.saveOrUpdateWishForDates(response, weekDays);
        response.setId(dayConfigId);
        return response;
    }

    private void saveOrUpdateWishForDates(WishDTO wishDTO, List<Integer> weekDays) {
        if (weekDays != null && weekDays.size() > 0) {
            List<LocalDate> wishDates = weekDays.stream()
                .filter(dayIndex -> dayIndex != null && dayIndex >= 0 && dayIndex <= 7)
                .map(dayIndex -> wishDTO.getDate().with(TemporalAdjusters.firstInMonth(DayOfWeek.values()[dayIndex])))
                .map(DateTimeUtil::getOtherWeeksWithSameWeekdayInMonth).flatMap(List::stream)
                .collect(Collectors.toList());
            this.createOrUpdateWishForDates(wishDTO, wishDates);
        } else {
            this.createOrUpdateWishForDates(wishDTO, DateTimeUtil.getOtherWeeksWithSameWeekdayInMonth(wishDTO.getDate()));
        }
    }

    private void createOrUpdateWishForDates(WishDTO dto, List<LocalDate> dates) {
        if (dates != null) {
            dates.stream().forEach(localDate -> {
                Wish wish = this.wishRepository.findByUserIdAndDateFromGreaterThanEqualAndDateToLessThanEqual(dto.getUserId(), DateTimeUtil.getInstantAtStartOfDay(localDate), DateTimeUtil.getInstantAtEndOfDay(localDate)).orElse(new Wish());
                dto.setDate(localDate);
                if (wish.getId() == null) {
                    dto.setId(null);
                    this.save(dto);
                } else {
                    dto.setId(wish.getId());
                    this.update(dto);
                }
            });
        }
    }

    @Override
    public WishDTO update(WishDTO wishDTO) {
        log.debug("Request to update wish : {}", wishDTO);
        if (wishDTO.getId() != null) {
            this.convertDtoDateToInterval(wishDTO);
            Set<ShiftType> managedShiftTypes = wishRepository.findById(wishDTO.getId()).get().getShiftTypes();
            managedShiftTypes.clear();
            wishDTO.getShiftTypes()
                .stream()
                .map(shiftTypeRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(managedShiftTypes::add);
            Wish wish = wishMapper.toEntity(wishDTO);
            wish.setShiftTypes(managedShiftTypes);
            wish = wishRepository.save(wish);
            return wishMapper.toDto(wish);
        }
        return wishDTO;
    }

    @Override
    public WishDTO updateForWholeMonth(WishDTO wishDTO, List<Integer> weekDays) {
        WishDTO savedWishDTO = this.update(wishDTO);
        savedWishDTO.setShiftTypes(wishDTO.getShiftTypes());
        this.saveOrUpdateWishForDates(savedWishDTO, weekDays);
        return wishDTO;
    }

    /**
     * Get all the wishes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WishDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Wishes");
        return wishRepository.findAll(pageable)
            .map(wishMapper::toDto);
    }

    /**
     * Get one wish by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */

    @Override
    @Transactional(readOnly = true)
    public Optional<WishDTO> findOne(Long id) {
        log.debug("Request to get Wish : {}", id);
        return wishRepository.findById(id)
            .map(wishMapper::toDto);
    }

    /**
     * Delete the wish by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Wish : {}", id);
        wishRepository.deleteById(id);
    }

    @Override
    public boolean canUserTakeShiftTypeOnDate(User user, ShiftType shiftType, LocalDate localDate) {
        Objects.requireNonNull(user, "User cannot be null.");
        Objects.requireNonNull(shiftType, "Shift type cannot be null.");
        Objects.requireNonNull(localDate, "Date cannot be null.");
        List<Wish> userWishes = this.wishRepository.findByUserAndDateFromLessThanEqualAndDateToGreaterThanEqual(user, DateTimeUtil.getInstantAtStartOfDay(localDate), DateTimeUtil.getInstantAtEndOfDay(localDate));
        Optional optional = userWishes
            .stream()
            .filter(wish -> WishType.EXCEPT.equals(wish.getWishType()))
            .map(Wish::getShiftTypes)
            .filter(shiftTypes -> shiftTypes.contains(shiftType))
            .findFirst();
        if (optional.isPresent()) {
            return false;
        }
        Optional<Wish> wishOpt = userWishes
            .stream()
            .filter(wish -> WishType.ONLY.equals(wish.getWishType())).findFirst();
        if (wishOpt.isPresent()) {
            return wishOpt.get().getShiftTypes().contains(shiftType);
        }
        return true;
    }


    private void convertDtoDateToInterval(WishDTO dto) {
        dto.setDateFrom(DateTimeUtil.getInstantAtStartOfDay(dto.getDate()));
        dto.setDateTo(DateTimeUtil.getInstantAtEndOfDay(dto.getDate()));
    }

    @Transactional
    @Override
    public void deleteForInterval(LocalDate fromDate, LocalDate toDate) {
        log.debug("delete by fromDate : {}, toDate: {}", fromDate, toDate);
        wishRepository.deleteByDateFromGreaterThanEqualAndDateToLessThanEqual(DateTimeUtil.getInstantAtStartOfDay(fromDate), DateTimeUtil.getInstantAtEndOfDay(toDate));
    }

}
