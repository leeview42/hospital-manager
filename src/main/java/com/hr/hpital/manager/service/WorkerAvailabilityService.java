package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.service.dto.WorkerAvailabilityDTO;
import com.hr.hpital.manager.service.dto.WorkerUnavailabilityDTO;
import com.hr.hpital.manager.domain.WorkerAvailability;
import com.hr.hpital.manager.web.rest.errors.WorkersAlreadyAssignedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link WorkerAvailability}.
 */
public interface WorkerAvailabilityService {

    /**
     * Save a workerAvailability.
     *
     * @param workerAvailabilityDTO the entity to save.
     * @return the persisted entity.
     */
    WorkerAvailabilityDTO save(WorkerAvailabilityDTO workerAvailabilityDTO);

    /**
     * Get all the workerAvailabilities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WorkerAvailabilityDTO> findAll(Pageable pageable);


    /**
     * Get the "id" workerAvailability.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WorkerAvailabilityDTO> findOne(Long id);

    /**
     * Delete the "id" workerAvailability.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<User> findAvailableUsersForShiftTypeAndDate(ShiftType shiftType, LocalDate date);

    List<LocalDate> findUnavailableDays(Long userId, Date dateFrom, Date dateTo, Long teamId);

    void updateWorkerUnavailabilities(WorkerUnavailabilityDTO workerUnavailabilityDTO) throws WorkersAlreadyAssignedException;

    boolean isAvailable(User user, Instant from, Instant to);

    List<User> filterAvailableUsers(List<User> users, Instant from, Instant to);
}
