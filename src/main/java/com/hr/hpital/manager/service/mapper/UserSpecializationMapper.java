package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.service.dto.UserSpecializationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserSpecialization} and its DTO {@link UserSpecializationDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, SpecializationMapper.class})
public interface UserSpecializationMapper extends EntityMapper<UserSpecializationDTO, UserSpecialization> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "specialization.id", target = "specializationId")
    UserSpecializationDTO toDto(UserSpecialization userSpecialization);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "specializationId", target = "specialization")
    UserSpecialization toEntity(UserSpecializationDTO userSpecializationDTO);

    default UserSpecialization fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserSpecialization userSpecialization = new UserSpecialization();
        userSpecialization.setId(id);
        return userSpecialization;
    }
}
