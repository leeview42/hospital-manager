package com.hr.hpital.manager.service.dto

class TimeSheetGroupEmployeeDTO: RMUserDTO() {
    var workHoursPerDay: Integer? = null

    var locationName: String? = null

    var costCenter: String? = null

    var hireDate: String? = null

    var leaveDate: String? = null

    var isShift: Boolean? = false

}
