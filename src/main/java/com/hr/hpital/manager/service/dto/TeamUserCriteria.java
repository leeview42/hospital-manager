package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link com.hr.hpital.manager.domain.TeamUser} entity. This class is used
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /team-users?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TeamUserCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private BooleanFilter isManager;

    private LongFilter teamId;

    private LongFilter userId;

    public TeamUserCriteria(){
    }

    public TeamUserCriteria(TeamUserCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.isManager = other.isManager == null ? null : other.isManager.copy();
        this.teamId = other.teamId == null ? null : other.teamId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public TeamUserCriteria copy() {
        return new TeamUserCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public BooleanFilter getIsManager() {
        return isManager;
    }

    public void setIsManager(BooleanFilter isManager) {
        this.isManager = isManager;
    }

    public LongFilter getTeamId() {
        return teamId;
    }

    public void setTeamId(LongFilter teamId) {
        this.teamId = teamId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TeamUserCriteria that = (TeamUserCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(isManager, that.isManager) &&
            Objects.equals(teamId, that.teamId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        isManager,
        teamId,
        userId
        );
    }

    @Override
    public String toString() {
        return "TeamUserCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (isManager != null ? "isManager=" + isManager + ", " : "") +
                (teamId != null ? "teamId=" + teamId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
