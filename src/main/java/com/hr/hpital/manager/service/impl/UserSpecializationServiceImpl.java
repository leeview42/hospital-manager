package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.service.OrganizationService;
import com.hr.hpital.manager.service.UserSpecializationService;
import com.hr.hpital.manager.repository.UserSpecializationRepository;
import com.hr.hpital.manager.service.dto.SpecializationDTO;
import com.hr.hpital.manager.service.dto.UserDependenciesUpdateRequestDTO;
import com.hr.hpital.manager.service.dto.UserSpecializationDTO;
import com.hr.hpital.manager.service.mapper.UserSpecializationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UserSpecialization}.
 */
@Service
@Transactional
public class UserSpecializationServiceImpl implements UserSpecializationService {

    private final Logger log = LoggerFactory.getLogger(UserSpecializationServiceImpl.class);

    private final UserSpecializationRepository userSpecializationRepository;

    private final UserSpecializationMapper userSpecializationMapper;

    private final OrganizationService organizationService;

    public UserSpecializationServiceImpl(UserSpecializationRepository userSpecializationRepository, UserSpecializationMapper userSpecializationMapper, OrganizationService organizationService) {
        this.userSpecializationRepository = userSpecializationRepository;
        this.userSpecializationMapper = userSpecializationMapper;
        this.organizationService = organizationService;
    }

    /**
     * Save a userSpecialization.
     *
     * @param userSpecializationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserSpecializationDTO save(UserSpecializationDTO userSpecializationDTO) {
        log.debug("Request to save UserSpecialization : {}", userSpecializationDTO);
        UserSpecialization userSpecialization = userSpecializationMapper.toEntity(userSpecializationDTO);
        userSpecialization = userSpecializationRepository.save(userSpecialization);
        return userSpecializationMapper.toDto(userSpecialization);
    }

    /**
     * Get all the userSpecializations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserSpecializationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserSpecializations");
        return userSpecializationRepository.findAll(pageable)
            .map(userSpecializationMapper::toDto);
    }


    /**
     * Get one userSpecialization by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserSpecializationDTO> findOne(Long id) {
        log.debug("Request to get UserSpecialization : {}", id);
        return userSpecializationRepository.findById(id)
            .map(userSpecializationMapper::toDto);
    }

    /**
     * Delete the userSpecialization by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserSpecialization : {}", id);
        userSpecializationRepository.deleteById(id);
    }

    @Override
    public void saveUserSpecializations(UserDependenciesUpdateRequestDTO userDependenciesUpdateRequestDTO, Long organizationId) {
        this.organizationService.findOne(organizationId).ifPresent(organization -> {
            Set<Long> organizationSpecializations = organization.getSpecializations().stream().map(SpecializationDTO::getId).collect(Collectors.toSet());
            this.userSpecializationRepository.deleteByUserId(userDependenciesUpdateRequestDTO.getUserId());
            this.userSpecializationRepository.saveAll(userDependenciesUpdateRequestDTO.getDependencyEntityIds().stream()
                .filter(organizationSpecializations::contains)
                .map(specializationId -> {
                    User user = new User();
                    user.setId(userDependenciesUpdateRequestDTO.getUserId());
                    Specialization specialization = new Specialization();
                    specialization.setId(specializationId);
                    UserSpecialization userSpecialization = new UserSpecialization();
                    userSpecialization.setSpecialization(specialization);
                    userSpecialization.setUser(user);
                    return userSpecialization;
                }).collect(Collectors.toSet()));
        });
    }
}
