package com.hr.hpital.manager.service.util

import com.hr.hpital.manager.domain.ShiftType
import java.time.Instant
import java.time.LocalDate
import java.time.temporal.ChronoUnit


fun getShiftStartTime(shiftType: ShiftType, date: LocalDate): Instant {
    return DateTimeUtil.getInstantAtStartOfDay(date)
        .plus(shiftType.startHour.toLong(), ChronoUnit.HOURS)
        .plus(shiftType.startMinute.toLong(), ChronoUnit.MINUTES)
}

fun getShiftEndTime(shiftType: ShiftType, startTime: Instant, pauseHours: Int = 0): Instant {
    return startTime.plus(shiftType.duration).plus(pauseHours.toLong(), ChronoUnit.HOURS)
}
