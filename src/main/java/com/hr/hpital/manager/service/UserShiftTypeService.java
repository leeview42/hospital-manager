package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.UserShiftTypeDTO;

import com.hr.hpital.manager.domain.UserShiftType;
import com.hr.hpital.manager.service.dto.UserDependenciesUpdateRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link UserShiftType}.
 */
public interface UserShiftTypeService {

    /**
     * Save a userShiftType.
     *
     * @param userShiftTypeDTO the entity to save.
     * @return the persisted entity.
     */
    UserShiftTypeDTO save(UserShiftTypeDTO userShiftTypeDTO);

    /**
     * Get all the userShiftTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserShiftTypeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userShiftType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserShiftTypeDTO> findOne(Long id);

    /**
     * Delete the "id" userShiftType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void saveUserShiftTypes(UserDependenciesUpdateRequestDTO userDependenciesUpdateRequestDTO, Long teamId);
}
