package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.domain.TeamUser;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.repository.TeamUserRepository;
import com.hr.hpital.manager.service.TeamUserQueryService;
import com.hr.hpital.manager.service.TeamUserService;
import com.hr.hpital.manager.service.dto.TeamDTO;
import com.hr.hpital.manager.service.dto.TeamUserCriteria;
import com.hr.hpital.manager.service.dto.TeamUserDTO;
import com.hr.hpital.manager.service.mapper.TeamUserMapper;
import io.github.jhipster.service.filter.BooleanFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.hr.hpital.manager.web.rest.util.ResourceUtil.createTeamUserCriteria;

/**
 * Service Implementation for managing {@link TeamUser}.
 */
@Service("teamUserService")
@Transactional
public class TeamUserServiceImpl implements TeamUserService {

    private final Logger log = LoggerFactory.getLogger(TeamUserServiceImpl.class);

    private final TeamUserRepository teamUserRepository;

    private final TeamUserMapper teamUserMapper;

    private final TeamUserQueryService teamUserQueryService;


    public TeamUserServiceImpl(TeamUserRepository teamUserRepository,
                               TeamUserMapper teamUserMapper,
                               TeamUserQueryService teamUserQueryService) {
        this.teamUserRepository = teamUserRepository;
        this.teamUserMapper = teamUserMapper;
        this.teamUserQueryService = teamUserQueryService;
    }

    /**
     * Save a teamUser.
     *
     * @param teamUserDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TeamUserDTO save(TeamUserDTO teamUserDTO) {
        log.debug("Request to save TeamUser : {}", teamUserDTO);
        TeamUser teamUser = teamUserMapper.toEntity(teamUserDTO);
        teamUser = teamUserRepository.save(teamUser);
        return teamUserMapper.toDto(teamUser);
    }

    /**
     * Get all the teamUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TeamUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TeamUsers");
        return teamUserRepository.findAll(pageable)
            .map(teamUserMapper::toDto);
    }

    /**
     * Get one teamUser by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TeamUserDTO> findOne(Long id) {
        log.debug("Request to get TeamUser : {}", id);
        return teamUserRepository.findById(id)
            .map(teamUserMapper::toDto);
    }

    /**
     * Delete the teamUser by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TeamUser : {}", id);
        teamUserRepository.deleteById(id);
    }

    @Override
    public void updateTeamManagers(Long teamId, Set<Long> managerIds) {
        if (teamId != null) {
            Set<TeamUser> teamUsers = this.teamUserRepository.findAllByTeamId(teamId).stream().map(teamUser -> {
                teamUser.setIsManager(false);
                return teamUser;
            }).collect(Collectors.toSet());
            this.teamUserRepository.saveAll(teamUsers);
            if (managerIds != null && managerIds.size() > 0) {
                this.teamUserRepository.saveAll(teamUsers.stream().filter(teamUser -> managerIds.contains(teamUser.getUser().getId())).map(teamUser -> {
                    teamUser.setIsManager(true);
                    return teamUser;
                }).collect(Collectors.toSet()));
            }
        }
    }

    @Override
    public void makeTeamManager(Long teamId, Long managerId) {
        this.teamUserRepository.findByTeamIdAndUserId(teamId, managerId).ifPresent(teamUser -> {teamUser.setIsManager(true); this.teamUserRepository.save(teamUser);});
    }

    @Override
    public Set<Long> getTeamMembers(Long teamId) {
        Set<Long> members = new HashSet<>();
        if (teamId != null) {
            return this.teamUserRepository.findAllByTeamIdAndIsManagerIsFalse(teamId).stream().map(TeamUser::getUser).map(User::getId).collect(Collectors.toSet());
        }
        return members;
    }

    @Override
    public Set<Long> getTeamManagers(Long teamId) {
        Set<Long> members = new HashSet<>();
        if (teamId != null) {
            return this.teamUserRepository.findAllByTeamIdAndIsManagerIsTrue(teamId).stream().map(TeamUser::getUser).map(User::getId).collect(Collectors.toSet());
        }
        return members;
    }

    @Override
    public TeamDTO mapTeamMembers(TeamDTO teamDTO) {
        if (teamDTO != null && teamDTO.getId() != null) {
            teamDTO.setManagers(this.getTeamManagers(teamDTO.getId()));
            teamDTO.setMembers(this.getTeamMembers(teamDTO.getId()));
        }
        return teamDTO;
    }

    /**
     * Checks if a user is member of a team
     *
     * @param userId user's id
     * @param teamId team's id
     * @return true in case it user is part of the team, false otherwise
     */
    @Override
    @Transactional(readOnly = true)
    public boolean userIsTeamMember(Long userId, Long teamId) {
        TeamUserCriteria criteria = createTeamUserCriteria(userId, teamId);
        return teamUserQueryService.countByCriteria(criteria) > 0;
    }

    /**
     * Checks if a user is member of a team and is also one of the team's managers
     *
     * @param userId user's id
     * @param teamId team's id
     * @return true in case it user is part of the team and is manager, false otherwise
     */
    @Override
    @Transactional(readOnly = true)
    public boolean userIsTeamManager(Long userId, Long teamId) {
        TeamUserCriteria criteria = createTeamUserCriteria(userId, teamId);
        BooleanFilter managerFilter = new BooleanFilter();
        managerFilter.setEquals(Boolean.TRUE);
        criteria.setIsManager(managerFilter);
        return teamUserQueryService.countByCriteria(criteria) > 0;
    }
}
