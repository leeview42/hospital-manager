package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.Priority;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link Priority} entity.
 */
public class PriorityDTO implements Serializable {

    private Long id;

    private String code;

    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PriorityDTO priorityDTO = (PriorityDTO) o;
        if (priorityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), priorityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PriorityDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
