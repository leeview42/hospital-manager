package com.hr.hpital.manager.service.impl;

import com.hr.hpital.manager.service.ShiftDayService;
import com.hr.hpital.manager.domain.ShiftDay;
import com.hr.hpital.manager.repository.ShiftDayRepository;
import com.hr.hpital.manager.service.dto.ShiftDayDTO;
import com.hr.hpital.manager.service.mapper.ShiftDayMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ShiftDay}.
 */
@Service
@Transactional
public class ShiftDayServiceImpl implements ShiftDayService {

    private final Logger log = LoggerFactory.getLogger(ShiftDayServiceImpl.class);

    private final ShiftDayRepository shiftDayRepository;

    private final ShiftDayMapper shiftDayMapper;

    public ShiftDayServiceImpl(ShiftDayRepository shiftDayRepository, ShiftDayMapper shiftDayMapper) {
        this.shiftDayRepository = shiftDayRepository;
        this.shiftDayMapper = shiftDayMapper;
    }

    /**
     * Save a shiftDay.
     *
     * @param shiftDayDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShiftDayDTO save(ShiftDayDTO shiftDayDTO) {
        log.debug("Request to save ShiftDay : {}", shiftDayDTO);
        ShiftDay shiftDay = shiftDayMapper.toEntity(shiftDayDTO);
        shiftDay = shiftDayRepository.save(shiftDay);
        return shiftDayMapper.toDto(shiftDay);
    }

    /**
     * Get all the shiftDays.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShiftDayDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShiftDays");
        return shiftDayRepository.findAll(pageable)
            .map(shiftDayMapper::toDto);
    }


    /**
     * Get one shiftDay by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShiftDayDTO> findOne(Long id) {
        log.debug("Request to get ShiftDay : {}", id);
        return shiftDayRepository.findById(id)
            .map(shiftDayMapper::toDto);
    }

    /**
     * Delete the shiftDay by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShiftDay : {}", id);
        shiftDayRepository.deleteById(id);
    }
}
