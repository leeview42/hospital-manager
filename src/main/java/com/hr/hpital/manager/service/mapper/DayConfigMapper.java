package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.service.dto.DayConfigDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link DayConfig} and its DTO {@link DayConfigDTO}.
 */
@Mapper(componentModel = "spring", uses = {DayConfigShiftTypeMapper.class, TeamMapper.class})
public interface DayConfigMapper extends EntityMapper<DayConfigDTO, DayConfig> {

    @Mapping(source = "team.id", target = "teamId")
    DayConfigDTO toDto(DayConfig shiftType);

    @Mapping(source = "teamId", target = "team")
    DayConfig toEntity(DayConfigDTO shiftTypeDTO);

    default DayConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        DayConfig dayConfig = new DayConfig();
        dayConfig.setId(id);
        return dayConfig;
    }
}
