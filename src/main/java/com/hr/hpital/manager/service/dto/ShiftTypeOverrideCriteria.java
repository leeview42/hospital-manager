package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.ShiftTypeOverride;
import com.hr.hpital.manager.web.rest.ShiftTypeOverrideResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link ShiftTypeOverride} entity. This class is used
 * in {@link ShiftTypeOverrideResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shift-type-overrides?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShiftTypeOverrideCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter shiftTypeId;

    private LongFilter canOverrideId;

    public ShiftTypeOverrideCriteria(){
    }

    public ShiftTypeOverrideCriteria(ShiftTypeOverrideCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.shiftTypeId = other.shiftTypeId == null ? null : other.shiftTypeId.copy();
        this.canOverrideId = other.canOverrideId == null ? null : other.canOverrideId.copy();
    }

    @Override
    public ShiftTypeOverrideCriteria copy() {
        return new ShiftTypeOverrideCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(LongFilter shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    public LongFilter getCanOverrideId() {
        return canOverrideId;
    }

    public void setCanOverrideId(LongFilter canOverrideId) {
        this.canOverrideId = canOverrideId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShiftTypeOverrideCriteria that = (ShiftTypeOverrideCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(shiftTypeId, that.shiftTypeId) &&
            Objects.equals(canOverrideId, that.canOverrideId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        shiftTypeId,
        canOverrideId
        );
    }

    @Override
    public String toString() {
        return "ShiftTypeOverrideCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (shiftTypeId != null ? "shiftTypeId=" + shiftTypeId + ", " : "") +
                (canOverrideId != null ? "canOverrideId=" + canOverrideId + ", " : "") +
            "}";
    }

}
