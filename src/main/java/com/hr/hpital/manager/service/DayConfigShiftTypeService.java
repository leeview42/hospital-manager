package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.service.dto.DayConfigDTO;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Service Interface for managing {@link DayConfigShiftType}.
 */
public interface DayConfigShiftTypeService {

    /**
     * Save a dayConfigShiftType.
     *
     * @param dayConfigShiftTypeDTO the entity to save.
     * @return the persisted entity.
     */
    DayConfigShiftTypeDTO save(DayConfigShiftTypeDTO dayConfigShiftTypeDTO);

    /**
     * Get all the dayConfigShiftTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DayConfigShiftTypeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dayConfigShiftType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DayConfigShiftTypeDTO> findOne(Long id);

    /**
     * Delete the "id" dayConfigShiftType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void saveDayConfigShiftTypes(DayConfig dayConfig, List<DayConfigShiftTypeDTO> dayConfigShiftTypes);

    /**
     * Filters a set of DayConfigShiftType's using a predicate.
     *
     * @param dayConfigShiftTypes the set to filter
     * @param predicate predicate to apply for every element of the set
     * @return a set containing the elements which fulfilled the predicate
     */
    Set<DayConfigShiftType> filter(Set<DayConfigShiftType> dayConfigShiftTypes, Predicate<DayConfigShiftType> predicate);

}
