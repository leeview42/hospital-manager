package com.hr.hpital.manager.service.util

import java.util.function.Consumer

fun <U> applyInBatches(list: List<U>, batchSize: Int, consumer: Consumer<List<U>>) {
    for (index in list.indices step batchSize) {
        consumer.accept(list.subList(index, if (index + batchSize < list.size) index + batchSize else list.size))
    }
}

fun <U> splitInBatches(list: List<U>, batchSize: Int): List<List<U>> {
    var result: List<List<U>> = arrayListOf()
    for (index in list.indices step batchSize) {
        result + list.subList(index, if (index + batchSize < list.size) index + batchSize else list.size)
    }
    return result
}
