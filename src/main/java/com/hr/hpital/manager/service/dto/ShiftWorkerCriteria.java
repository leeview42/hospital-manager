package com.hr.hpital.manager.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.hr.hpital.manager.domain.ShiftWorker;
import com.hr.hpital.manager.web.rest.ShiftWorkerResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;

/**
 * Criteria class for the {@link ShiftWorker} entity. This class is used
 * in {@link ShiftWorkerResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shift-workers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShiftWorkerCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter shiftId;

    private LongFilter workerId;

    public ShiftWorkerCriteria(){
    }

    public ShiftWorkerCriteria(ShiftWorkerCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.shiftId = other.shiftId == null ? null : other.shiftId.copy();
        this.workerId = other.workerId == null ? null : other.workerId.copy();
    }

    @Override
    public ShiftWorkerCriteria copy() {
        return new ShiftWorkerCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getShiftId() {
        return shiftId;
    }

    public void setShiftId(LongFilter shiftId) {
        this.shiftId = shiftId;
    }

    public LongFilter getWorkerId() {
        return workerId;
    }

    public void setWorkerId(LongFilter workerId) {
        this.workerId = workerId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShiftWorkerCriteria that = (ShiftWorkerCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(shiftId, that.shiftId) &&
            Objects.equals(workerId, that.workerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        shiftId,
        workerId
        );
    }

    @Override
    public String toString() {
        return "ShiftWorkerCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (shiftId != null ? "shiftId=" + shiftId + ", " : "") +
                (workerId != null ? "workerId=" + workerId + ", " : "") +
            "}";
    }

}
