package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.domain.DayConfigShiftType_;
import com.hr.hpital.manager.domain.DayConfig_;
import com.hr.hpital.manager.domain.ShiftType_;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeCriteria;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeDTO;
import com.hr.hpital.manager.service.mapper.DayConfigShiftTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.repository.DayConfigShiftTypeRepository;

/**
 * Service for executing complex queries for {@link DayConfigShiftType} entities in the database.
 * The main input is a {@link DayConfigShiftTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DayConfigShiftTypeDTO} or a {@link Page} of {@link DayConfigShiftTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DayConfigShiftTypeQueryService extends QueryService<DayConfigShiftType> {

    private final Logger log = LoggerFactory.getLogger(DayConfigShiftTypeQueryService.class);

    private final DayConfigShiftTypeRepository dayConfigShiftTypeRepository;

    private final DayConfigShiftTypeMapper dayConfigShiftTypeMapper;

    public DayConfigShiftTypeQueryService(DayConfigShiftTypeRepository dayConfigShiftTypeRepository, DayConfigShiftTypeMapper dayConfigShiftTypeMapper) {
        this.dayConfigShiftTypeRepository = dayConfigShiftTypeRepository;
        this.dayConfigShiftTypeMapper = dayConfigShiftTypeMapper;
    }

    /**
     * Return a {@link List} of {@link DayConfigShiftTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DayConfigShiftTypeDTO> findByCriteria(DayConfigShiftTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DayConfigShiftType> specification = createSpecification(criteria);
        return dayConfigShiftTypeMapper.toDto(dayConfigShiftTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DayConfigShiftTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DayConfigShiftTypeDTO> findByCriteria(DayConfigShiftTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DayConfigShiftType> specification = createSpecification(criteria);
        return dayConfigShiftTypeRepository.findAll(specification, page)
            .map(dayConfigShiftTypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DayConfigShiftTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DayConfigShiftType> specification = createSpecification(criteria);
        return dayConfigShiftTypeRepository.count(specification);
    }

    /**
     * Function to convert DayConfigShiftTypeCriteria to a {@link Specification}.
     */
    private Specification<DayConfigShiftType> createSpecification(DayConfigShiftTypeCriteria criteria) {
        Specification<DayConfigShiftType> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DayConfigShiftType_.id));
            }
            if (criteria.getNoOfWorkers() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNoOfWorkers(), DayConfigShiftType_.noOfWorkers));
            }
            if (criteria.getDayConfigId() != null) {
                specification = specification.and(buildSpecification(criteria.getDayConfigId(),
                    root -> root.join(DayConfigShiftType_.dayConfig, JoinType.LEFT).get(DayConfig_.id)));
            }
            if (criteria.getShiftTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getShiftTypeId(),
                    root -> root.join(DayConfigShiftType_.shiftType, JoinType.LEFT).get(ShiftType_.id)));
            }
        }
        return specification;
    }
}
