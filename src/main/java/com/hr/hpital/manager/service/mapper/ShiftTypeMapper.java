package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.service.dto.ShiftTypeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.Duration;

/**
 * Mapper for the entity {@link ShiftType} and its DTO {@link ShiftTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {PriorityMapper.class, TeamMapper.class})
public interface ShiftTypeMapper extends EntityMapper<ShiftTypeDTO, ShiftType> {

    @Mapping(source = "priority.id", target = "priorityId")
    @Mapping(source = "priority.code", target = "priorityCode")
    @Mapping(source = "team.id", target = "teamId")
    @Mapping(source = "duration", target = "durationMillis")
    ShiftTypeDTO toDto(ShiftType shiftType);

    @Mapping(source = "priorityId", target = "priority")
    @Mapping(source = "teamId", target = "team")
    ShiftType toEntity(ShiftTypeDTO shiftTypeDTO);

    default ShiftType fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShiftType shiftType = new ShiftType();
        shiftType.setId(id);
        return shiftType;
    }

    default Long toId(ShiftType shiftType) {
        if (shiftType == null || shiftType.getId() == null) {
            return null;
        }
        return shiftType.getId();
    }

    default Long durationToMillis(Duration duration) {
        return duration != null ? duration.toMillis() : null;
    }
}
