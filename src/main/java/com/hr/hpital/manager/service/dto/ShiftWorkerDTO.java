package com.hr.hpital.manager.service.dto;
import com.hr.hpital.manager.domain.ShiftWorker;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ShiftWorker} entity.
 */
public class ShiftWorkerDTO implements Serializable {

    private Long id;


    private Long shiftId;

    private Long workerId;

    private UserDTO worker;

    private ShiftDTO shift;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShiftId() {
        return shiftId;
    }

    public void setShiftId(Long shiftId) {
        this.shiftId = shiftId;
    }

    public Long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Long userId) {
        this.workerId = userId;
    }

    public UserDTO getWorker() {
        return worker;
    }

    public void setWorker(UserDTO worker) {
        this.worker = worker;
    }

    public ShiftDTO getShift() {
        return shift;
    }

    public void setShift(ShiftDTO shift) {
        this.shift = shift;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShiftWorkerDTO shiftWorkerDTO = (ShiftWorkerDTO) o;
        if (shiftWorkerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shiftWorkerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShiftWorkerDTO{" +
            "id=" + getId() +
            ", shift=" + getShiftId() +
            ", worker=" + getWorkerId() +
            "}";
    }
}
