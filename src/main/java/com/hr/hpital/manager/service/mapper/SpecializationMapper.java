package com.hr.hpital.manager.service.mapper;

import com.hr.hpital.manager.domain.*;
import com.hr.hpital.manager.service.dto.SpecializationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Specialization} and its DTO {@link SpecializationDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganizationMapper.class})
public interface SpecializationMapper extends EntityMapper<SpecializationDTO, Specialization> {

    @Mapping(source = "organization.id", target = "organizationId")
    @Mapping(source = "organization.name", target = "organizationName")
    SpecializationDTO toDto(Specialization specialization);

    @Mapping(source = "organizationId", target = "organization")
    Specialization toEntity(SpecializationDTO specializationDTO);

    default Specialization fromId(Long id) {
        if (id == null) {
            return null;
        }
        Specialization specialization = new Specialization();
        specialization.setId(id);
        return specialization;
    }
}
