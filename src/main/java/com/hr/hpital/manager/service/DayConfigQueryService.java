package com.hr.hpital.manager.service;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.domain.DayConfig_;
import com.hr.hpital.manager.domain.ShiftType_;
import com.hr.hpital.manager.domain.Team_;
import com.hr.hpital.manager.repository.DayConfigRepository;
import com.hr.hpital.manager.service.dto.DayConfigCriteria;
import com.hr.hpital.manager.service.dto.DayConfigDTO;
import com.hr.hpital.manager.service.mapper.DayConfigMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link DayConfig} entities in the database.
 * The main input is a {@link DayConfigCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DayConfigDTO} or a {@link Page} of {@link DayConfigDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DayConfigQueryService extends QueryService<DayConfig> {

    private final Logger log = LoggerFactory.getLogger(DayConfigQueryService.class);

    private final DayConfigRepository dayConfigRepository;

    private final DayConfigMapper dayConfigMapper;

    public DayConfigQueryService(DayConfigRepository dayConfigRepository, DayConfigMapper dayConfigMapper) {
        this.dayConfigRepository = dayConfigRepository;
        this.dayConfigMapper = dayConfigMapper;
    }

    /**
     * Return a {@link List} of {@link DayConfigDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DayConfigDTO> findByCriteria(DayConfigCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DayConfig> specification = createSpecification(criteria);
        return dayConfigMapper.toDto(dayConfigRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DayConfigDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DayConfigDTO> findByCriteria(DayConfigCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DayConfig> specification = createSpecification(criteria);
        return dayConfigRepository.findAll(specification, page)
            .map(dayConfigMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DayConfigCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DayConfig> specification = createSpecification(criteria);
        return dayConfigRepository.count(specification);
    }

    /**
     * Function to convert DayConfigCriteria to a {@link Specification}.
     */
    private Specification<DayConfig> createSpecification(DayConfigCriteria criteria) {
        Specification<DayConfig> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), DayConfig_.id));
            }
            if (criteria.getDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDate(), DayConfig_.date));
            }
            if (criteria.getIsBankHoliday() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBankHoliday(), DayConfig_.isBankHoliday));
            }
            if (criteria.getTeamIdFilter() != null) {
                specification = specification.and(buildSpecification(criteria.getTeamIdFilter(),
                    root -> root.join(DayConfig_.team, JoinType.INNER).get(Team_.id)));
            }
        }
        return specification;
    }
}
