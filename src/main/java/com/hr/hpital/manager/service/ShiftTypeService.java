package com.hr.hpital.manager.service;

import com.hr.hpital.manager.service.dto.ShiftTypeDTO;

import com.hr.hpital.manager.domain.ShiftType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ShiftType}.
 */
public interface ShiftTypeService {

    /**
     * Save a shiftType.
     *
     * @param shiftTypeDTO the entity to save.
     * @return the persisted entity.
     */
    ShiftTypeDTO save(ShiftTypeDTO shiftTypeDTO);

    /**
     * Get all the shiftTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShiftTypeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shiftType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShiftTypeDTO> findOne(Long id);

    /**
     * Delete the "id" shiftType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
