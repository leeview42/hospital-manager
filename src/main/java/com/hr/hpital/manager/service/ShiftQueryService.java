package com.hr.hpital.manager.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.hr.hpital.manager.domain.Shift;
import com.hr.hpital.manager.domain.ShiftType_;
import com.hr.hpital.manager.domain.Shift_;
import com.hr.hpital.manager.domain.User_;
import com.hr.hpital.manager.repository.ShiftRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hr.hpital.manager.service.dto.ShiftCriteria;
import com.hr.hpital.manager.service.dto.ShiftDTO;
import com.hr.hpital.manager.service.mapper.ShiftMapper;

/**
 * Service for executing complex queries for {@link Shift} entities in the database.
 * The main input is a {@link ShiftCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShiftDTO} or a {@link Page} of {@link ShiftDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShiftQueryService extends QueryService<Shift> {

    private final Logger log = LoggerFactory.getLogger(ShiftQueryService.class);

    private final ShiftRepository shiftRepository;

    private final ShiftMapper shiftMapper;

    public ShiftQueryService(ShiftRepository shiftRepository, ShiftMapper shiftMapper) {
        this.shiftRepository = shiftRepository;
        this.shiftMapper = shiftMapper;
    }

    /**
     * Return a {@link List} of {@link ShiftDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShiftDTO> findByCriteria(ShiftCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Shift> specification = createSpecification(criteria);
        return shiftMapper.toDto(shiftRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShiftDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShiftDTO> findByCriteria(ShiftCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Shift> specification = createSpecification(criteria);
        return shiftRepository.findAll(specification, page)
            .map(shiftMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShiftCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Shift> specification = createSpecification(criteria);
        return shiftRepository.count(specification);
    }

    /**
     * Function to convert ShiftCriteria to a {@link Specification}.
     */
    private Specification<Shift> createSpecification(ShiftCriteria criteria) {
        Specification<Shift> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Shift_.id));
            }
            if (criteria.getStart() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStart(), Shift_.start));
            }
            if (criteria.getEnd() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEnd(), Shift_.end));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), Shift_.updateDate));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), Shift_.createDate));
            }
            if (criteria.getGenerated() != null) {
                specification = specification.and(buildSpecification(criteria.getGenerated(), Shift_.generated));
            }
            if (criteria.getShiftTypeId() != null) {
                specification = specification.and(buildSpecification(criteria.getShiftTypeId(),
                    root -> root.join(Shift_.shiftType, JoinType.LEFT).get(ShiftType_.id)));
            }
            if (criteria.getCreatedById() != null) {
                specification = specification.and(buildSpecification(criteria.getCreatedById(),
                    root -> root.join(Shift_.createdBy, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getUpdatedById() != null) {
                specification = specification.and(buildSpecification(criteria.getUpdatedById(),
                    root -> root.join(Shift_.updatedBy, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
