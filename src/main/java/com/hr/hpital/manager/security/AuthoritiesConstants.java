package com.hr.hpital.manager.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String TEAM_MANAGER = "ROLE_TEAM_MANAGER";

    public static final String ORG_MANAGER = "ROLE_ORG_MANAGER";

    public static final String TEAM_MEMBER = "ROLE_TEAM_MEMBER";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
