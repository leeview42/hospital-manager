package com.hr.hpital.manager.security;

import com.hr.hpital.manager.service.dto.UserDTO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class DomainUserDetails implements UserDetails {

    private final String username;

    private final String password;

    private final UserDTO user;

    private final List<GrantedAuthority> authorities;

    public DomainUserDetails(String username, String password, UserDTO user) {
        this.username = username;
        this.password = password;
        this.user = user;
        this.authorities = user.getAuthorities().stream()
            .map(authority -> new SimpleGrantedAuthority(authority))
            .collect(Collectors.toList());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.user.isActivated();
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.user.isActivated();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.user.isActivated();
    }

    @Override
    public boolean isEnabled() {
        return this.user.isActivated();
    }
}
