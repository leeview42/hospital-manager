package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.domain.DayConfig;
import com.hr.hpital.manager.service.DayConfigQueryService;
import com.hr.hpital.manager.service.DayConfigService;
import com.hr.hpital.manager.service.dto.DayConfigCriteria;
import com.hr.hpital.manager.service.dto.DayConfigDTO;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.hr.hpital.manager.web.rest.util.ResourceUtil.createLongFilter;

/**
 * REST controller for managing {@link DayConfig}.
 */
@RestController
@RequestMapping("/api")
public class DayConfigResource {

    private static final String ENTITY_NAME = "dayConfig";
    private final Logger log = LoggerFactory.getLogger(DayConfigResource.class);
    private final DayConfigService dayConfigService;
    private final DayConfigQueryService dayConfigQueryService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public DayConfigResource(DayConfigService dayConfigService, DayConfigQueryService dayConfigQueryService) {
        this.dayConfigService = dayConfigService;
        this.dayConfigQueryService = dayConfigQueryService;
    }

    /**
     * {@code POST  /day-configs} : Create a new dayConfig.
     *
     * @param dayConfigDTO the dayConfigDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dayConfigDTO, or with status {@code 400 (Bad Request)} if the dayConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/team-management/day-configs")
    public ResponseEntity<DayConfigDTO> createDayConfig(@RequestBody @Valid DayConfigDTO dayConfigDTO, @RequestParam("createWholeMonth") Boolean createWholeMonth, @RequestParam("weekDays") List<Integer> weekDays, @RequestParam("teamId") Long teamId) throws URISyntaxException {
        log.debug("REST request to save DayConfig : {}", dayConfigDTO);
        if (dayConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new dayConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (dayConfigService.find(dayConfigDTO, teamId).isPresent()) {
            throw new BadRequestAlertException("A day config for this date already exists!", ENTITY_NAME, "dayconfigAlreadyExists");
        }
        DayConfigDTO result = null;
        dayConfigDTO.setTeamId(teamId);
        if (!createWholeMonth) {
            result = dayConfigService.save(dayConfigDTO);
        } else {
            result = dayConfigService.saveForWholeMonth(dayConfigDTO, weekDays);
        }
        return ResponseEntity.created(new URI("/api/day-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getDate().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /day-configs} : Updates an existing dayConfig.
     *
     * @param dayConfigDTO the dayConfigDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayConfigDTO,
     * or with status {@code 400 (Bad Request)} if the dayConfigDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dayConfigDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/team-management/day-configs")
    public ResponseEntity<DayConfigDTO> updateDayConfig(@RequestBody @Valid DayConfigDTO dayConfigDTO, @RequestParam("createWholeMonth") Boolean createWholeMonth, @RequestParam("weekDays") List<Integer> weekDays, @RequestParam("teamId") Long teamId) throws URISyntaxException {
        log.debug("REST request to update DayConfig : {}", dayConfigDTO);
        if (dayConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Optional<DayConfigDTO> countryDTOOpt = dayConfigService.find(dayConfigDTO, teamId);
        if (countryDTOOpt.isPresent() && !countryDTOOpt.get().getId().equals(dayConfigDTO.getId())) {
            throw new BadRequestAlertException("A day config for this date already exists!", ENTITY_NAME, "dayconfigAlreadyExists");
        }
        DayConfigDTO result = null;
        dayConfigDTO.setTeamId(teamId);
        if (createWholeMonth) {
            result = dayConfigService.saveForWholeMonth(dayConfigDTO, weekDays);
        } else {
            dayConfigService.delete(dayConfigDTO.getId());
            result = dayConfigService.save(dayConfigDTO);
        }
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getDate().toString()))
            .body(result);
    }

    /**
     * {@code GET  /day-configs} : get all the dayConfigs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dayConfigs in body.
     */
    @GetMapping("/team-member/day-configs")
    public ResponseEntity<List<DayConfigDTO>> getAllDayConfigs(DayConfigCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get DayConfigs by criteria: {}", criteria);
        criteria.setTeamIdFilter(createLongFilter(teamId));
        Page<DayConfigDTO> page = dayConfigQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /day-configs/interval} : get all the dayConfigs for the specified interval of time.
     *
     * @param fromDate interval start
     * @param toDate   interval end
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dayConfigs in body.
     */
    @GetMapping("/team-member/day-configs/interval")
    public ResponseEntity<List<DayConfigDTO>> getAllDayConfigsForInterval(@RequestParam(name = "fromDate")
                                                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                              LocalDate fromDate, @RequestParam(name = "toDate")
                                                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                              LocalDate toDate, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get DayConfigs by fromDate: {}, toDate: {}", fromDate, toDate);

        if (toDate.isBefore(fromDate)) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok().body(this.dayConfigService.findForInterval(fromDate, toDate, teamId));
    }

    @DeleteMapping("/team-management/day-configs/interval")
    public ResponseEntity deleteAllDayConfigsForInterval(@RequestParam(name = "fromDate")
                                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                             LocalDate fromDate, @RequestParam(name = "toDate")
                                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                             LocalDate toDate, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get DayConfigs by fromDate: {}, toDate: {}", fromDate, toDate);

        if (toDate.isBefore(fromDate)) {
            return ResponseEntity.noContent().build();
        }
        this.dayConfigService.deleteForInterval(fromDate, toDate, teamId);
        return ResponseEntity.ok().build();
    }

    /**
     * {@code GET  /day-configs/count} : count all the dayConfigs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/team-member/day-configs/count")
    public ResponseEntity<Long> countDayConfigs(DayConfigCriteria criteria, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to count DayConfigs by criteria: {}", criteria);
        criteria.setTeamIdFilter(createLongFilter(teamId));
        return ResponseEntity.ok().body(dayConfigQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /day-configs/:id} : get the "id" dayConfig.
     *
     * @param id the id of the dayConfigDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dayConfigDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/team-member/day-configs/{id}")
    @PostAuthorize("#teamId == returnObject.body.teamId")
    public ResponseEntity<DayConfigDTO> getDayConfig(@PathVariable Long id, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get DayConfig : {}", id);
        Optional<DayConfigDTO> dayConfigDTO = dayConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dayConfigDTO);
    }

    @DeleteMapping("/team-management/day-configs/{id}")
    @PreAuthorize("#teamId == @dayConfigService.findOne(#id).get().getTeamId()")
    public ResponseEntity<Void> deleteDayConfig(@PathVariable Long id, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to delete DayConfig : {}", id);
        dayConfigService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

}
