package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.domain.WishShiftType;
import com.hr.hpital.manager.service.WishShiftTypeService;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.service.dto.WishShiftTypeDTO;
import com.hr.hpital.manager.service.dto.WishShiftTypeCriteria;
import com.hr.hpital.manager.service.WishShiftTypeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link WishShiftType}.
 */
@RestController
@RequestMapping("/api")
public class WishShiftTypeResource {

    private final Logger log = LoggerFactory.getLogger(WishShiftTypeResource.class);

    private static final String ENTITY_NAME = "wishShiftType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WishShiftTypeService wishShiftTypeService;

    private final WishShiftTypeQueryService wishShiftTypeQueryService;

    public WishShiftTypeResource(WishShiftTypeService wishShiftTypeService, WishShiftTypeQueryService wishShiftTypeQueryService) {
        this.wishShiftTypeService = wishShiftTypeService;
        this.wishShiftTypeQueryService = wishShiftTypeQueryService;
    }

    /**
     * {@code POST  /wish-shift-types} : Create a new wishShiftType.
     *
     * @param wishShiftTypeDTO the wishShiftTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new wishShiftTypeDTO, or with status {@code 400 (Bad Request)} if the wishShiftType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wish-shift-types")
    public ResponseEntity<WishShiftTypeDTO> createWishShiftType(@RequestBody WishShiftTypeDTO wishShiftTypeDTO) throws URISyntaxException {
        log.debug("REST request to save WishShiftType : {}", wishShiftTypeDTO);
        if (wishShiftTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new wishShiftType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WishShiftTypeDTO result = wishShiftTypeService.save(wishShiftTypeDTO);
        return ResponseEntity.created(new URI("/api/wish-shift-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /wish-shift-types} : Updates an existing wishShiftType.
     *
     * @param wishShiftTypeDTO the wishShiftTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated wishShiftTypeDTO,
     * or with status {@code 400 (Bad Request)} if the wishShiftTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the wishShiftTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wish-shift-types")
    public ResponseEntity<WishShiftTypeDTO> updateWishShiftType(@RequestBody WishShiftTypeDTO wishShiftTypeDTO) throws URISyntaxException {
        log.debug("REST request to update WishShiftType : {}", wishShiftTypeDTO);
        if (wishShiftTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WishShiftTypeDTO result = wishShiftTypeService.save(wishShiftTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, wishShiftTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /wish-shift-types} : get all the wishShiftTypes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wishShiftTypes in body.
     */
    @GetMapping("/wish-shift-types")
    public ResponseEntity<List<WishShiftTypeDTO>> getAllWishShiftTypes(WishShiftTypeCriteria criteria) {
        log.debug("REST request to get WishShiftTypes by criteria: {}", criteria);
        List<WishShiftTypeDTO> entityList = wishShiftTypeQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /wish-shift-types/count} : count all the wishShiftTypes.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/wish-shift-types/count")
    public ResponseEntity<Long> countWishShiftTypes(WishShiftTypeCriteria criteria) {
        log.debug("REST request to count WishShiftTypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(wishShiftTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /wish-shift-types/:id} : get the "id" wishShiftType.
     *
     * @param id the id of the wishShiftTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the wishShiftTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wish-shift-types/{id}")
    public ResponseEntity<WishShiftTypeDTO> getWishShiftType(@PathVariable Long id) {
        log.debug("REST request to get WishShiftType : {}", id);
        Optional<WishShiftTypeDTO> wishShiftTypeDTO = wishShiftTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wishShiftTypeDTO);
    }

    /**
     * {@code DELETE  /wish-shift-types/:id} : delete the "id" wishShiftType.
     *
     * @param id the id of the wishShiftTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wish-shift-types/{id}")
    public ResponseEntity<Void> deleteWishShiftType(@PathVariable Long id) {
        log.debug("REST request to delete WishShiftType : {}", id);
        wishShiftTypeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
