package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.service.ShiftTypeQueryService;
import com.hr.hpital.manager.service.ShiftTypeService;
import com.hr.hpital.manager.service.dto.ShiftTypeCriteria;
import com.hr.hpital.manager.service.dto.ShiftTypeDTO;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.hr.hpital.manager.web.rest.util.ResourceUtil.createLongFilter;

/**
 * REST controller for managing {@link ShiftType}.
 */
@RestController
@RequestMapping("/api")
public class ShiftTypeResource {

    private final Logger log = LoggerFactory.getLogger(ShiftTypeResource.class);

    private static final String ENTITY_NAME = "shiftType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShiftTypeService shiftTypeService;

    private final ShiftTypeQueryService shiftTypeQueryService;

    public ShiftTypeResource(ShiftTypeService shiftTypeService, ShiftTypeQueryService shiftTypeQueryService) {
        this.shiftTypeService = shiftTypeService;
        this.shiftTypeQueryService = shiftTypeQueryService;
    }

    /**
     * {@code POST  /shift-types} : Create a new shiftType.
     *
     * @param shiftTypeDTO the shiftTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shiftTypeDTO, or with status {@code 400 (Bad Request)} if the shiftType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/team-management/shift-types")
    public ResponseEntity<ShiftTypeDTO> createShiftType(@RequestBody ShiftTypeDTO shiftTypeDTO, @RequestParam("teamId") Long teamId) throws URISyntaxException {
        log.debug("REST request to save ShiftType : {}", shiftTypeDTO);
        if (shiftTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new shiftType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        shiftTypeDTO.setTeamId(teamId);
        ShiftTypeDTO result = shiftTypeService.save(shiftTypeDTO);
        return ResponseEntity.created(new URI("/api/shift-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shift-types} : Updates an existing shiftType.
     *
     * @param shiftTypeDTO the shiftTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shiftTypeDTO,
     * or with status {@code 400 (Bad Request)} if the shiftTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shiftTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/team-management/shift-types")
    public ResponseEntity<ShiftTypeDTO> updateShiftType(@RequestBody ShiftTypeDTO shiftTypeDTO, @RequestParam("teamId") Long teamId) throws URISyntaxException {
        log.debug("REST request to update ShiftType : {}", shiftTypeDTO);
        if (shiftTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        shiftTypeDTO.setTeamId(teamId);
        ShiftTypeDTO result = shiftTypeService.save(shiftTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shiftTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shift-types} : get all the shiftTypes.
     *
     * @param pageable    the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder  a {@link UriComponentsBuilder} URI builder.
     * @param criteria    the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shiftTypes in body.
     */
    @GetMapping("/team-member/shift-types")
    public ResponseEntity<List<ShiftTypeDTO>> getAllShiftTypes(ShiftTypeCriteria criteria,
                                                               Pageable pageable,
                                                               @RequestParam MultiValueMap<String, String> queryParams,
                                                               UriComponentsBuilder uriBuilder,
                                                               @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get ShiftTypes by criteria: {}", criteria);
        criteria.setTeamIdFilter(createLongFilter(teamId));
        Page<ShiftTypeDTO> page = shiftTypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/team-member/shift-types/all")
    public ResponseEntity<List<ShiftTypeDTO>> getAll() {
        return ResponseEntity.ok().body(shiftTypeQueryService.findAll());
    }

    /**
    * {@code GET  /shift-types/count} : count all the shiftTypes.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/team-member/shift-types/count")
    public ResponseEntity<Long> countShiftTypes(ShiftTypeCriteria criteria) {
        log.debug("REST request to count ShiftTypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(shiftTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shift-types/:id} : get the "id" shiftType.
     *
     * @param id the id of the shiftTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shiftTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/team-member/shift-types/{id}")
    @PostAuthorize("#teamId == returnObject.body.teamId")
    public ResponseEntity<ShiftTypeDTO> getShiftType(@PathVariable("id") Long id, @RequestParam(value = "teamId", required = false) Long teamId) {
        log.debug("REST request to get ShiftType : {}", id);
        Optional<ShiftTypeDTO> shiftTypeDTO = shiftTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shiftTypeDTO);
    }
}
