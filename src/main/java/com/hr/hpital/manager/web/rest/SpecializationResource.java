package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.security.AuthoritiesConstants;
import com.hr.hpital.manager.service.SpecializationService;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.service.dto.SpecializationDTO;
import com.hr.hpital.manager.service.dto.SpecializationCriteria;
import com.hr.hpital.manager.service.SpecializationQueryService;

import com.hr.hpital.manager.web.rest.util.ResourceUtil;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hr.hpital.manager.domain.Specialization}.
 */
@RestController
@RequestMapping("/api")
public class SpecializationResource {

    private final Logger log = LoggerFactory.getLogger(SpecializationResource.class);

    private static final String ENTITY_NAME = "specialization";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpecializationService specializationService;

    private final SpecializationQueryService specializationQueryService;

    public SpecializationResource(SpecializationService specializationService, SpecializationQueryService specializationQueryService) {
        this.specializationService = specializationService;
        this.specializationQueryService = specializationQueryService;
    }

    /**
     * {@code POST  /specializations} : Create a new specialization.
     *
     * @param specializationDTO the specializationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new specializationDTO, or with status {@code 400 (Bad Request)} if the specialization has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/specializations")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<SpecializationDTO> createSpecialization(@Valid @RequestBody SpecializationDTO specializationDTO) throws URISyntaxException {
        log.debug("REST request to save Specialization : {}", specializationDTO);
        if (specializationDTO.getId() != null) {
            throw new BadRequestAlertException("A new specialization cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpecializationDTO result = specializationService.save(specializationDTO);
        return ResponseEntity.created(new URI("/api/specializations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /specializations} : Updates an existing specialization.
     *
     * @param specializationDTO the specializationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated specializationDTO,
     * or with status {@code 400 (Bad Request)} if the specializationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the specializationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/specializations")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<SpecializationDTO> updateSpecialization(@Valid @RequestBody SpecializationDTO specializationDTO) throws URISyntaxException {
        log.debug("REST request to update Specialization : {}", specializationDTO);
        if (specializationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SpecializationDTO result = specializationService.save(specializationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, specializationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /specializations} : get all the specializations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of specializations in body.
     */
    @GetMapping("/specializations")
    public ResponseEntity<List<SpecializationDTO>> getAllSpecializations(SpecializationCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Specializations by criteria: {}", criteria);
        criteria.setOrganizationId(ResourceUtil.createCurrentUserOrgFilter(true));
        Page<SpecializationDTO> page = specializationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /specializations/count} : count all the specializations.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/specializations/count")
    public ResponseEntity<Long> countSpecializations(SpecializationCriteria criteria) {
        log.debug("REST request to count Specializations by criteria: {}", criteria);
        criteria.setOrganizationId(ResourceUtil.createCurrentUserOrgFilter(true));
        return ResponseEntity.ok().body(specializationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /specializations/:id} : get the "id" specialization.
     *
     * @param id the id of the specializationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the specializationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/specializations/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<SpecializationDTO> getSpecialization(@PathVariable Long id) {
        log.debug("REST request to get Specialization : {}", id);
        Optional<SpecializationDTO> specializationDTO = specializationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(specializationDTO);
    }

    /**
     * {@code DELETE  /specializations/:id} : delete the "id" specialization.
     *
     * @param id the id of the specializationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/specializations/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteSpecialization(@PathVariable Long id) {
        log.debug("REST request to delete Specialization : {}", id);
        specializationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
