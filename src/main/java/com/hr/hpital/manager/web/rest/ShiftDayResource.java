package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.service.ShiftDayService;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.service.dto.ShiftDayDTO;
import com.hr.hpital.manager.service.dto.ShiftDayCriteria;
import com.hr.hpital.manager.service.ShiftDayQueryService;

import com.hr.hpital.manager.domain.ShiftDay;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ShiftDay}.
 */
@RestController
@RequestMapping("/api")
public class ShiftDayResource {

    private final Logger log = LoggerFactory.getLogger(ShiftDayResource.class);

    private static final String ENTITY_NAME = "shiftDay";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShiftDayService shiftDayService;

    private final ShiftDayQueryService shiftDayQueryService;

    public ShiftDayResource(ShiftDayService shiftDayService, ShiftDayQueryService shiftDayQueryService) {
        this.shiftDayService = shiftDayService;
        this.shiftDayQueryService = shiftDayQueryService;
    }

    /**
     * {@code POST  /shift-days} : Create a new shiftDay.
     *
     * @param shiftDayDTO the shiftDayDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shiftDayDTO, or with status {@code 400 (Bad Request)} if the shiftDay has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shift-days")
    public ResponseEntity<ShiftDayDTO> createShiftDay(@RequestBody ShiftDayDTO shiftDayDTO) throws URISyntaxException {
        log.debug("REST request to save ShiftDay : {}", shiftDayDTO);
        if (shiftDayDTO.getId() != null) {
            throw new BadRequestAlertException("A new shiftDay cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShiftDayDTO result = shiftDayService.save(shiftDayDTO);
        return ResponseEntity.created(new URI("/api/shift-days/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shift-days} : Updates an existing shiftDay.
     *
     * @param shiftDayDTO the shiftDayDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shiftDayDTO,
     * or with status {@code 400 (Bad Request)} if the shiftDayDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shiftDayDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shift-days")
    public ResponseEntity<ShiftDayDTO> updateShiftDay(@RequestBody ShiftDayDTO shiftDayDTO) throws URISyntaxException {
        log.debug("REST request to update ShiftDay : {}", shiftDayDTO);
        if (shiftDayDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShiftDayDTO result = shiftDayService.save(shiftDayDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shiftDayDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shift-days} : get all the shiftDays.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shiftDays in body.
     */
    @GetMapping("/shift-days")
    public ResponseEntity<List<ShiftDayDTO>> getAllShiftDays(ShiftDayCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ShiftDays by criteria: {}", criteria);
        Page<ShiftDayDTO> page = shiftDayQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /shift-days/count} : count all the shiftDays.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/shift-days/count")
    public ResponseEntity<Long> countShiftDays(ShiftDayCriteria criteria) {
        log.debug("REST request to count ShiftDays by criteria: {}", criteria);
        return ResponseEntity.ok().body(shiftDayQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shift-days/:id} : get the "id" shiftDay.
     *
     * @param id the id of the shiftDayDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shiftDayDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shift-days/{id}")
    public ResponseEntity<ShiftDayDTO> getShiftDay(@PathVariable Long id) {
        log.debug("REST request to get ShiftDay : {}", id);
        Optional<ShiftDayDTO> shiftDayDTO = shiftDayService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shiftDayDTO);
    }

    /**
     * {@code DELETE  /shift-days/:id} : delete the "id" shiftDay.
     *
     * @param id the id of the shiftDayDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shift-days/{id}")
    public ResponseEntity<Void> deleteShiftDay(@PathVariable Long id) {
        log.debug("REST request to delete ShiftDay : {}", id);
        shiftDayService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
