package com.hr.hpital.manager.web.rest.interceptor;

import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.service.RequestContext;
import com.hr.hpital.manager.service.TeamUserService;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TeamMemberInterceptor implements HandlerInterceptor {

    private final TeamUserService teamUserService;

    private final RequestContext requestContext;

    public TeamMemberInterceptor(TeamUserService teamUserService, RequestContext requestContext) {
        this.teamUserService = teamUserService;
        this.requestContext = requestContext;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getDetails() != null && auth.getDetails() instanceof User) {
            User currentUser = (User) auth.getDetails();
            return teamUserService.userIsTeamMember(currentUser.getId(), requestContext.getTeamId());
        }
        throw new AccessDeniedException("Access is denied!");
    }

}
