package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.domain.WorkerAvailability;
import com.hr.hpital.manager.service.TeamUserService;
import com.hr.hpital.manager.service.WorkerAvailabilityQueryService;
import com.hr.hpital.manager.service.WorkerAvailabilityService;
import com.hr.hpital.manager.service.dto.WorkerAvailabilityCriteria;
import com.hr.hpital.manager.service.dto.WorkerAvailabilityDTO;
import com.hr.hpital.manager.service.dto.WorkerUnavailabilityDTO;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.web.rest.errors.ClientRequestAlertException;
import com.hr.hpital.manager.web.rest.errors.WorkersAlreadyAssignedException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.zalando.problem.Status;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.*;

import static com.hr.hpital.manager.web.rest.util.ResourceUtil.createLongFilter;

/**
 * REST controller for managing {@link WorkerAvailability}.
 */
@RestController
@RequestMapping("/api")
public class WorkerAvailabilityResource {

    private final Logger log = LoggerFactory.getLogger(WorkerAvailabilityResource.class);

    private static final String ENTITY_NAME = "workerAvailability";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WorkerAvailabilityService workerAvailabilityService;

    private final WorkerAvailabilityQueryService workerAvailabilityQueryService;

    private final TeamUserService teamUserService;

    public WorkerAvailabilityResource(WorkerAvailabilityService workerAvailabilityService,
                                      WorkerAvailabilityQueryService workerAvailabilityQueryService,
                                      TeamUserService teamUserService) {
        this.workerAvailabilityService = workerAvailabilityService;
        this.workerAvailabilityQueryService = workerAvailabilityQueryService;
        this.teamUserService = teamUserService;
    }

    /**
     * {@code POST  /worker-availabilities} : Create a new workerAvailability.
     *
     * @param workerAvailabilityDTO the workerAvailabilityDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new workerAvailabilityDTO,
     * or with status {@code 400 (Bad Request)} if the workerAvailability has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/team-management/worker-availabilities")
    public ResponseEntity<WorkerAvailabilityDTO> createWorkerAvailability(@RequestBody WorkerAvailabilityDTO workerAvailabilityDTO) throws URISyntaxException {
        log.debug("REST request to save WorkerAvailability : {}", workerAvailabilityDTO);
        if (workerAvailabilityDTO.getId() != null) {
            throw new BadRequestAlertException("A new workerAvailability cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WorkerAvailabilityDTO result = workerAvailabilityService.save(workerAvailabilityDTO);
        return ResponseEntity.created(new URI("/api/worker-availabilities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /worker-availabilities} : Updates the unavailable days of a worker
     *
     * @param workerUnavailabilityDTO user's data needed to be marked available/unavailable
     * @param teamId                  teamId for which the user needs to be marked as available/unavailable
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and empty body if it succeeds
     * or with status {@code 400 (BAD_REQUEST)} in case user is already assigned to shifts in some of the days
     * @throws ClientRequestAlertException in case no days are provided with the request
     */
    @PutMapping("/team-management/worker-availabilities/unavailableDays")
    @PreAuthorize(" #workerUnavailabilityDTO.userId != null && " +
        " @teamUserService.userIsTeamMember(#workerUnavailabilityDTO.userId, #teamId) ")
    public ResponseEntity<Void> updateWorkerUnavailabilities(@RequestBody WorkerUnavailabilityDTO workerUnavailabilityDTO,
                                                             @RequestParam("teamId") Long teamId) {
        log.debug("REST request to update WorkerUnavailabilityDTO : {}", workerUnavailabilityDTO);
        if (workerUnavailabilityDTO.getUnavailableDays() == null) {
            throw new ClientRequestAlertException(Status.FORBIDDEN, "No days had been provided", "missingunavailabledays");
        }

        try {
            workerAvailabilityService.updateWorkerUnavailabilities(workerUnavailabilityDTO);
        } catch (WorkersAlreadyAssignedException e) {
            Map<String, Object> params = new HashMap<>();
            params.put("workers", e.getWorkers());
            params.put("assignedDays", e.getAssignedDays());
            throw new ClientRequestAlertException(Status.BAD_REQUEST, "Workers already assigned to other shifts",
                params, "workersalreadyassignedondays");
        }

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * {@code PUT  /worker-availabilities} : Updates an existing workerAvailability.
     *
     * @param workerAvailabilityDTO the workerAvailabilityDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated workerAvailabilityDTO,
     * or with status {@code 400 (Bad Request)} if the workerAvailabilityDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the workerAvailabilityDTO couldn't be updated.
     */
    @PutMapping("/team-management/worker-availabilities")
    public ResponseEntity<WorkerAvailabilityDTO> updateWorkerAvailability(@RequestBody WorkerAvailabilityDTO workerAvailabilityDTO) {
        log.debug("REST request to update WorkerAvailability : {}", workerAvailabilityDTO);
        if (workerAvailabilityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WorkerAvailabilityDTO result = workerAvailabilityService.save(workerAvailabilityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, workerAvailabilityDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /worker-availabilities/unavailableDays} : get all the unavailable days of the user
     *
     * @param userId
     * @param dateFrom
     * @param dateTo
     * @return
     */
    @GetMapping("/team-member/worker-availabilities/unavailableDays")
    public ResponseEntity<List<LocalDate>> getAllWorkerAvailabilities(@RequestParam(name = "userId") Long userId,
                                                                      @RequestParam(name = "dateFrom")
                                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                          Date dateFrom,
                                                                      @RequestParam(name = "dateTo")
                                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                          Date dateTo, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get WorkerAvailabilities by UserId: {}, dateFrom: {}, dateTo: {}", userId, dateFrom, dateTo);

        if (dateFrom.before(dateTo)) {
            return ResponseEntity.ok().body(workerAvailabilityService.findUnavailableDays(userId, dateFrom, dateTo, teamId));
        }

        return ResponseEntity.noContent().build();
    }

    /**
     * {@code GET  /worker-availabilities} : get all the workerAvailabilities.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of workerAvailabilities in body.
     */
    @GetMapping("/team-member/worker-availabilities")
    public ResponseEntity<List<WorkerAvailabilityDTO>> getAllWorkerAvailabilities(WorkerAvailabilityCriteria criteria,
                                                                                  Pageable pageable,
                                                                                  @RequestParam MultiValueMap<String, String> queryParams,
                                                                                  UriComponentsBuilder uriBuilder,
                                                                                  @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get WorkerAvailabilities by criteria: {}", criteria);
        criteria.setTeamIdFilter(createLongFilter(teamId));
        Page<WorkerAvailabilityDTO> page = workerAvailabilityQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /worker-availabilities/count} : count all the workerAvailabilities.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/team-member/worker-availabilities/count")
    public ResponseEntity<Long> countWorkerAvailabilities(WorkerAvailabilityCriteria criteria, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to count WorkerAvailabilities by criteria: {}", criteria);
        criteria.setTeamIdFilter(createLongFilter(teamId));
        return ResponseEntity.ok().body(workerAvailabilityQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /worker-availabilities/:id} : get the "id" workerAvailability.
     *
     * @param id the id of the workerAvailabilityDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the workerAvailabilityDTO,
     * or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/team-member/worker-availabilities/{id}")
    public ResponseEntity<WorkerAvailabilityDTO> getWorkerAvailability(@PathVariable Long id) {
        log.debug("REST request to get WorkerAvailability : {}", id);
        Optional<WorkerAvailabilityDTO> workerAvailabilityDTO = workerAvailabilityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(workerAvailabilityDTO);
    }

    /**
     * {@code DELETE  /worker-availabilities/:id} : delete the "id" workerAvailability.
     *
     * @param id the id of the workerAvailabilityDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/team-management/worker-availabilities/{id}")
    public ResponseEntity<Void> deleteWorkerAvailability(@PathVariable Long id) {
        log.debug("REST request to delete WorkerAvailability : {}", id);
        workerAvailabilityService.delete(id);
        return ResponseEntity.noContent().headers(
            HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())
        ).build();
    }
}
