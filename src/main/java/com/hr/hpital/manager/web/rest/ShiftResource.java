package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.domain.Shift;
import com.hr.hpital.manager.service.ShiftQueryService;
import com.hr.hpital.manager.service.ShiftService;
import com.hr.hpital.manager.service.ShiftTypeService;
import com.hr.hpital.manager.service.UserService;
import com.hr.hpital.manager.service.dto.ShiftCriteria;
import com.hr.hpital.manager.service.dto.ShiftDTO;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.web.rest.errors.ClientRequestAlertException;
import com.hr.hpital.manager.web.rest.errors.WorkersAlreadyAssignedException;
import com.hr.hpital.manager.web.rest.errors.WorkersUnavailableException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.zalando.problem.Status;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Shift}.
 */
@RestController
@RequestMapping("/api")
public class ShiftResource {

    private final Logger log = LoggerFactory.getLogger(ShiftResource.class);

    private static final String ENTITY_NAME = "shift";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShiftService shiftService;

    private final ShiftTypeService shiftTypeService;

    private final ShiftQueryService shiftQueryService;

    private final UserService userService;

    public ShiftResource(ShiftService shiftService,
                         ShiftTypeService shiftTypeService,
                         ShiftQueryService shiftQueryService,
                         UserService userService) {
        this.shiftService = shiftService;
        this.shiftTypeService = shiftTypeService;
        this.shiftQueryService = shiftQueryService;
        this.userService = userService;
    }

    /**
     * {@code POST  /shifts} : Create a new shift.
     *
     * @param shiftDTO the shiftDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shiftDTO,
     * or with status {@code 400 (Bad Request)} if the shift has already an ID or if the some workers are unavailable
     * due to holidays or other shift already assigned at the time, in this case worker names are provided to the client.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/team-management/shifts")
    @PreAuthorize(
        " #shiftDTO.shiftTypeId != null && " +
            "@shiftTypeService.findOne(#shiftDTO.shiftTypeId).get().getTeamId() == #teamId && " +
            "authentication.details.organization != null "
    )
    public ResponseEntity<ShiftDTO> createShift(@Valid @RequestBody ShiftDTO shiftDTO,
                                                @RequestParam("teamId") Long teamId) throws URISyntaxException {
        log.debug("REST request to save Shift : {}", shiftDTO);
        if (shiftDTO.getId() != null) {
            throw new BadRequestAlertException("A new shift cannot already have an ID", ENTITY_NAME, "idexists");
        }
        userService.getUserWithAuthorities().ifPresent(user -> {
            shiftDTO.setCreatedById(user.getId());
            shiftDTO.setUpdatedById(user.getId());
        });
        shiftDTO.setCreateDate(Instant.now());
        shiftDTO.setUpdateDate(Instant.now());
        ShiftDTO result;

        try {
            result = shiftService.save(shiftDTO);
        } catch (WorkersAlreadyAssignedException e) {
            throw new ClientRequestAlertException(Status.BAD_REQUEST, "Workers already assigned to other shifts",
                Collections.singletonMap("workers", e.getWorkers()), "workersalreadyassigned");
        } catch (WorkersUnavailableException e) {
            throw new ClientRequestAlertException(Status.BAD_REQUEST, "Workers are unavailable",
                Collections.singletonMap("workers", e.getWorkers()), "workersunavailable");
        }

        return ResponseEntity.created(new URI("/api/shifts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shifts} : Updates an existing shift.
     *
     * @param shiftDTO the shiftDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shiftDTO,
     * or with status {@code 400 (Bad Request)} if the shiftDTO is not valid or if the some workers are unavailable
     * due to holidays or other shift already assigned at the time, in this case worker names are provided to the client,
     * or with status {@code 500 (Internal Server Error)} if the shiftDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/team-management/shifts")
    @PreAuthorize(" #shiftDTO.shiftTypeId != null && authentication.details.organization != null ")
    public ResponseEntity<ShiftDTO> updateShift(@Valid @RequestBody ShiftDTO shiftDTO,
                                                @RequestParam("teamId") Long teamId) {
        log.debug("REST request to update Shift : {}", shiftDTO);
        if (shiftDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        userService.getUserWithAuthorities().ifPresent(user -> {
            shiftDTO.setUpdatedById(user.getId());
        });
        shiftDTO.setUpdateDate(Instant.now());
        shiftDTO.setGenerated(false);
        ShiftDTO result;

        try {
            result = shiftService.update(shiftDTO);
        } catch (WorkersAlreadyAssignedException e) {
            throw new ClientRequestAlertException(Status.BAD_REQUEST, "Workers already assigned to other shifts",
                Collections.singletonMap("workers", e.getWorkers()), "workersalreadyassigned");
        } catch (WorkersUnavailableException e) {
            throw new ClientRequestAlertException(Status.BAD_REQUEST, "Workers are unavailable",
                Collections.singletonMap("workers", e.getWorkers()), "workersunavailable");
        }

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shiftDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shifts} : get all the shifts.
     *
     * @param pageable    the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder  a {@link UriComponentsBuilder} URI builder.
     * @param criteria    the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shifts in body.
     */
    @GetMapping("/team-member/shifts")
    public ResponseEntity<List<ShiftDTO>> getAllShifts(ShiftCriteria criteria, Pageable pageable,
                                                       @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Shifts by criteria: {}", criteria);
        Page<ShiftDTO> page = shiftQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shifts/count} : count all the shifts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/team-member/shifts/count")
    public ResponseEntity<Long> countShifts(ShiftCriteria criteria) {
        log.debug("REST request to count Shifts by criteria: {}", criteria);
        return ResponseEntity.ok().body(shiftQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shifts/:id} : get the "id" shift.
     *
     * @param id the id of the shiftDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shiftDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/team-member/shifts/{id}")
    public ResponseEntity<ShiftDTO> getShift(@PathVariable Long id) {
        log.debug("REST request to get Shift : {}", id);
        Optional<ShiftDTO> shiftDTO = shiftService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shiftDTO);
    }

    @GetMapping("/team-member/shifts/interval")
    public ResponseEntity<List<ShiftDTO>> getForInterval(@RequestParam(name = "fromDate")
                                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                             LocalDate fromDate, @RequestParam(name = "toDate")
                                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                             LocalDate toDate,
                                                         @RequestParam("teamId") Long teamId) {
        if (toDate.isBefore(fromDate)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok().body(this.shiftService.findForInterval(DateTimeUtil.getInstantAtStartOfDay(fromDate), DateTimeUtil.getInstantAtEndOfDay(toDate), teamId));
    }

    @DeleteMapping("/team-management/shifts/interval")
    public ResponseEntity deleteForInterval(@RequestParam(name = "fromDate")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                LocalDate fromDate, @RequestParam(name = "toDate")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                LocalDate toDate, @RequestParam("teamId") Long teamId) {
        if (toDate.isBefore(fromDate)) {
            return ResponseEntity.noContent().build();
        }
        this.shiftService.deleteAllShiftsForInterval(DateTimeUtil.getInstantAtStartOfDay(fromDate), DateTimeUtil.getInstantAtEndOfDay(toDate), teamId);
        return ResponseEntity.ok().build();
    }

    /**
     * {@code DELETE  /shifts/:id} : delete the "id" shift.
     *
     * @param id the id of the shiftDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/team-management/shifts/{id}")
    @PreAuthorize("#teamId == @shiftService.findOne(#id).get().getShiftType().getTeamId()")
    public ResponseEntity<Void> deleteShift(@PathVariable Long id, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to delete Shift : {}", id);
        shiftService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
