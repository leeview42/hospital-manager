package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.service.ShiftService;
import com.hr.hpital.manager.service.ShiftWorkerQueryService;
import com.hr.hpital.manager.service.ShiftWorkerService;
import com.hr.hpital.manager.service.dto.IntervalRequestDTO;
import com.hr.hpital.manager.service.dto.ShiftWorkerCriteria;
import com.hr.hpital.manager.service.dto.ShiftWorkerDTO;
import com.hr.hpital.manager.service.util.DateTimeUtil;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.domain.ShiftWorker;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ShiftWorker}.
 */
@RestController
@RequestMapping("/api/team-management")
public class ShiftWorkerResource {

    private static final String ENTITY_NAME = "shiftWorker";
    private final Logger log = LoggerFactory.getLogger(ShiftWorkerResource.class);
    private final ShiftWorkerService shiftWorkerService;
    private final ShiftWorkerQueryService shiftWorkerQueryService;
    private final ShiftService shiftService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ShiftWorkerResource(ShiftWorkerService shiftWorkerService, ShiftWorkerQueryService shiftWorkerQueryService, ShiftService shiftService) {
        this.shiftWorkerService = shiftWorkerService;
        this.shiftWorkerQueryService = shiftWorkerQueryService;
        this.shiftService = shiftService;
    }

    /**
     * {@code POST  /shift-workers} : Create a new shiftWorker.
     *
     * @param shiftWorkerDTO the shiftWorkerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shiftWorkerDTO, or with status {@code 400 (Bad Request)} if the shiftWorker has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shift-workers")
    public ResponseEntity<ShiftWorkerDTO> createShiftWorker(@RequestBody ShiftWorkerDTO shiftWorkerDTO) throws URISyntaxException {
        log.debug("REST request to save ShiftWorker : {}", shiftWorkerDTO);
        if (shiftWorkerDTO.getId() != null) {
            throw new BadRequestAlertException("A new shiftWorker cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShiftWorkerDTO result = shiftWorkerService.save(shiftWorkerDTO);
        return ResponseEntity.created(new URI("/api/shift-workers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/shift-workers/generate")
    public ResponseEntity<Void> generate(@RequestBody IntervalRequestDTO shiftWorkerDTO, @RequestParam("teamId") Long teamId) throws URISyntaxException {
        if (shiftWorkerDTO.getFromDate() != null && shiftWorkerDTO.getToDate() != null && shiftWorkerDTO.getToDate().isAfter(shiftWorkerDTO.getFromDate())) {
            this.shiftService.deleteAndGenerateShiftsForInterval(
                DateTimeUtil.getInstantAtStartOfDay(shiftWorkerDTO.getFromDate()),
                DateTimeUtil.getInstantAtEndOfDay(shiftWorkerDTO.getToDate()),
                teamId
            );
        }
        return ResponseEntity.ok().build();
    }

    /**
     * {@code PUT  /shift-workers} : Updates an existing shiftWorker.
     *
     * @param shiftWorkerDTO the shiftWorkerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shiftWorkerDTO,
     * or with status {@code 400 (Bad Request)} if the shiftWorkerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shiftWorkerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shift-workers")
    public ResponseEntity<ShiftWorkerDTO> updateShiftWorker(ShiftWorkerDTO shiftWorkerDTO) throws URISyntaxException {
        log.debug("REST request to update ShiftWorker : {}", shiftWorkerDTO);
        if (shiftWorkerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShiftWorkerDTO result = shiftWorkerService.save(shiftWorkerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shiftWorkerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shift-workers} : get all the shiftWorkers.
     *
     * @param pageable    the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder  a {@link UriComponentsBuilder} URI builder.
     * @param criteria    the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shiftWorkers in body.
     */
    @GetMapping("/shift-workers")
    public ResponseEntity<List<ShiftWorkerDTO>> getAllShiftWorkers(ShiftWorkerCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ShiftWorkers by criteria: {}", criteria);
        Page<ShiftWorkerDTO> page = shiftWorkerQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shift-workers/count} : count all the shiftWorkers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/shift-workers/count")
    public ResponseEntity<Long> countShiftWorkers(ShiftWorkerCriteria criteria) {
        log.debug("REST request to count ShiftWorkers by criteria: {}", criteria);
        return ResponseEntity.ok().body(shiftWorkerQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shift-workers/:id} : get the "id" shiftWorker.
     *
     * @param id the id of the shiftWorkerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shiftWorkerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shift-workers/{id}")
    public ResponseEntity<ShiftWorkerDTO> getShiftWorker(@PathVariable Long id) {
        log.debug("REST request to get ShiftWorker : {}", id);
        Optional<ShiftWorkerDTO> shiftWorkerDTO = shiftWorkerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shiftWorkerDTO);
    }

    /**
     * {@code DELETE  /shift-workers/:id} : delete the "id" shiftWorker.
     *
     * @param id the id of the shiftWorkerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shift-workers/{id}")
    public ResponseEntity<Void> deleteShiftWorker(@PathVariable Long id) {
        log.debug("REST request to delete ShiftWorker : {}", id);
        shiftWorkerService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
