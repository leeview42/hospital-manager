package com.hr.hpital.manager.web.rest.util;

import com.hr.hpital.manager.domain.Authority;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.security.AuthoritiesConstants;
import com.hr.hpital.manager.security.SecurityUtils;
import com.hr.hpital.manager.service.dto.TeamUserCriteria;
import io.github.jhipster.service.filter.LongFilter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

public class ResourceUtil {

    public static LongFilter createCurrentUserOrgFilter() {
        return createCurrentUserOrgFilter(SecurityUtils.getCurrentUser());
    }

    public static LongFilter createCurrentUserOrgFilter(Boolean checkAdmin) {
        if (checkAdmin) {
            Optional<User> userOpt = SecurityUtils.getCurrentUser();
            if (userOpt.isPresent()) {
                User user = userOpt.get();
                if (user.getAuthorities().stream().map(Authority::getName).anyMatch(AuthoritiesConstants.ADMIN::equalsIgnoreCase)) {
                    return new LongFilter();
                }
            }
        }
        return createCurrentUserOrgFilter();
    }

    public static LongFilter createCurrentUserOrgFilter(Optional<User> userOpt) {
        LongFilter orgFilter = new LongFilter();
        orgFilter.setEquals(-1l);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            if (user.getOrganization() != null) {
                orgFilter.setEquals(user.getOrganization().getId());
            }
        }
        return orgFilter;
    }

    public static final LongFilter createCurrentUserLongFilter() {
        LongFilter longFilter = new LongFilter();
        longFilter.setEquals(-1l);
        Optional<User> userOpt = SecurityUtils.getCurrentUser();
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            longFilter.setEquals(user.getId());
        }
        return longFilter;
    }

    public static final LongFilter createLongFilter(Long actual) {
        LongFilter longFilter = new LongFilter();
        longFilter.setEquals(-1l);
        if (actual != null) {
            longFilter.setEquals(actual);
        }
        return longFilter;
    }

    public static final Long getCurrentUserOrgId() {
        Long orgId = null;
        Optional<User> userOpt = SecurityUtils.getCurrentUser();
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            if (user.getOrganization() != null) {
                orgId = user.getOrganization().getId();
            }
        }
        return orgId;
    }

    public static TeamUserCriteria createTeamUserCriteria(Long userId, Long teamId) {
        LongFilter userFilter = new LongFilter();
        userFilter.setEquals(-1l);
        LongFilter teamFilter = new LongFilter();
        teamFilter.setEquals(-1l);
        if (teamId != null && userId != null) {
            userFilter.setEquals(userId);
            teamFilter.setEquals(teamId);
        }
        TeamUserCriteria teamUserCriteria = new TeamUserCriteria();
        teamUserCriteria.setTeamId(teamFilter);
        teamUserCriteria.setUserId(userFilter);
        return teamUserCriteria;
    }

    public static LocalDate toLocalDate(Instant instant) {
        return instant.atOffset(ZoneOffset.UTC).toLocalDate();
    }

    public static LocalDateTime toLocalDateTime(Instant instant) {
        return instant.atOffset(ZoneOffset.UTC).toLocalDateTime();
    }

}
