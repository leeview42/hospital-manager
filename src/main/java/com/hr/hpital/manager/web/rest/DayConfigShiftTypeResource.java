package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.domain.DayConfigShiftType;
import com.hr.hpital.manager.service.DayConfigShiftTypeService;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeDTO;
import com.hr.hpital.manager.service.dto.DayConfigShiftTypeCriteria;
import com.hr.hpital.manager.service.DayConfigShiftTypeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link DayConfigShiftType}.
 */
@RestController
@RequestMapping("/api")
public class DayConfigShiftTypeResource {

    private final Logger log = LoggerFactory.getLogger(DayConfigShiftTypeResource.class);

    private static final String ENTITY_NAME = "dayConfigShiftType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DayConfigShiftTypeService dayConfigShiftTypeService;

    private final DayConfigShiftTypeQueryService dayConfigShiftTypeQueryService;

    public DayConfigShiftTypeResource(DayConfigShiftTypeService dayConfigShiftTypeService, DayConfigShiftTypeQueryService dayConfigShiftTypeQueryService) {
        this.dayConfigShiftTypeService = dayConfigShiftTypeService;
        this.dayConfigShiftTypeQueryService = dayConfigShiftTypeQueryService;
    }

    /**
     * {@code POST  /day-config-shift-types} : Create a new dayConfigShiftType.
     *
     * @param dayConfigShiftTypeDTO the dayConfigShiftTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dayConfigShiftTypeDTO, or with status {@code 400 (Bad Request)} if the dayConfigShiftType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/day-config-shift-types")
    public ResponseEntity<DayConfigShiftTypeDTO> createDayConfigShiftType(@RequestBody DayConfigShiftTypeDTO dayConfigShiftTypeDTO) throws URISyntaxException {
        log.debug("REST request to save DayConfigShiftType : {}", dayConfigShiftTypeDTO);
        if (dayConfigShiftTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new dayConfigShiftType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DayConfigShiftTypeDTO result = dayConfigShiftTypeService.save(dayConfigShiftTypeDTO);
        return ResponseEntity.created(new URI("/api/day-config-shift-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /day-config-shift-types} : Updates an existing dayConfigShiftType.
     *
     * @param dayConfigShiftTypeDTO the dayConfigShiftTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayConfigShiftTypeDTO,
     * or with status {@code 400 (Bad Request)} if the dayConfigShiftTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dayConfigShiftTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/day-config-shift-types")
    public ResponseEntity<DayConfigShiftTypeDTO> updateDayConfigShiftType(@RequestBody DayConfigShiftTypeDTO dayConfigShiftTypeDTO) throws URISyntaxException {
        log.debug("REST request to update DayConfigShiftType : {}", dayConfigShiftTypeDTO);
        if (dayConfigShiftTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DayConfigShiftTypeDTO result = dayConfigShiftTypeService.save(dayConfigShiftTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dayConfigShiftTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /day-config-shift-types} : get all the dayConfigShiftTypes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dayConfigShiftTypes in body.
     */
    @GetMapping("/day-config-shift-types")
    public ResponseEntity<List<DayConfigShiftTypeDTO>> getAllDayConfigShiftTypes(DayConfigShiftTypeCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get DayConfigShiftTypes by criteria: {}", criteria);
        Page<DayConfigShiftTypeDTO> page = dayConfigShiftTypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /day-config-shift-types/count} : count all the dayConfigShiftTypes.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/day-config-shift-types/count")
    public ResponseEntity<Long> countDayConfigShiftTypes(DayConfigShiftTypeCriteria criteria) {
        log.debug("REST request to count DayConfigShiftTypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(dayConfigShiftTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /day-config-shift-types/:id} : get the "id" dayConfigShiftType.
     *
     * @param id the id of the dayConfigShiftTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dayConfigShiftTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/day-config-shift-types/{id}")
    public ResponseEntity<DayConfigShiftTypeDTO> getDayConfigShiftType(@PathVariable Long id) {
        log.debug("REST request to get DayConfigShiftType : {}", id);
        Optional<DayConfigShiftTypeDTO> dayConfigShiftTypeDTO = dayConfigShiftTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dayConfigShiftTypeDTO);
    }

    /**
     * {@code DELETE  /day-config-shift-types/:id} : delete the "id" dayConfigShiftType.
     *
     * @param id the id of the dayConfigShiftTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/day-config-shift-types/{id}")
    public ResponseEntity<Void> deleteDayConfigShiftType(@PathVariable Long id) {
        log.debug("REST request to delete DayConfigShiftType : {}", id);
        dayConfigShiftTypeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
