package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.domain.Wish;
import com.hr.hpital.manager.service.WishQueryService;
import com.hr.hpital.manager.service.WishService;
import com.hr.hpital.manager.service.dto.WishCriteria;
import com.hr.hpital.manager.service.dto.WishDTO;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.hr.hpital.manager.web.rest.util.ResourceUtil.createLongFilter;

/**
 * REST controller for managing {@link Wish}.
 */
@RestController
@RequestMapping("/api/team-management")
public class WishResource {

    private final Logger log = LoggerFactory.getLogger(WishResource.class);

    private static final String ENTITY_NAME = "wish";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WishService wishService;

    private final WishQueryService wishQueryService;

    public WishResource(WishService wishService, WishQueryService wishQueryService) {
        this.wishService = wishService;
        this.wishQueryService = wishQueryService;
    }

    /**
     * {@code POST  /wishes} : Create a new wish.
     *
     * @param wishDTO the wishDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new wishDTO, or with status {@code 400 (Bad Request)} if the wish has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wishes")
    public ResponseEntity<WishDTO> createWish(@Valid @RequestBody WishDTO wishDTO,
                                              @RequestParam("createWholeMonth") Boolean createWholeMonth,
                                              @RequestParam("weekDays") List<Integer> weekDays) throws URISyntaxException {
        log.debug("REST request to save Wish : {}", wishDTO);
        if (wishDTO.getId() != null) {
            throw new BadRequestAlertException("A new wish cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WishDTO result = null;
        if (!createWholeMonth) {
            result = wishService.save(wishDTO);
        } else {
            result = wishService.saveForWholeMonth(wishDTO, weekDays);
        }
        return ResponseEntity.created(new URI("/api/wishes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /wishes} : Updates an existing wish.
     *
     * @param wishDTO the wishDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated wishDTO,
     * or with status {@code 400 (Bad Request)} if the wishDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the wishDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wishes")
    public ResponseEntity<WishDTO> updateWish(@Valid @RequestBody WishDTO wishDTO,
                                              @RequestParam("createWholeMonth") Boolean createWholeMonth,
                                              @RequestParam("weekDays") List<Integer> weekDays) {
        log.debug("REST request to update Wish : {}", wishDTO);
        if (wishDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WishDTO result = null;
        if (createWholeMonth) {
            result = wishService.updateForWholeMonth(wishDTO, weekDays);
        } else {
            wishService.update(wishDTO);
        }

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, wishDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /wishes} : get all the wishes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wishes in body.
     */
    @GetMapping("/wishes")
    public ResponseEntity<List<WishDTO>> getAllWishes(WishCriteria criteria,
                                                      Pageable pageable,
                                                      @RequestParam MultiValueMap<String, String> queryParams,
                                                      UriComponentsBuilder uriBuilder,
                                                      @RequestParam("teamId") Long teamId) {
        log.debug("REST request to get Wishes by criteria: {}", criteria);
        criteria.setTeamIdFilter(createLongFilter(teamId));
        Page<WishDTO> page = wishQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /wishes/count} : count all the wishes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/wishes/count")
    public ResponseEntity<Long> countWishes(WishCriteria criteria, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to count Wishes by criteria: {}", criteria);
        criteria.setTeamIdFilter(createLongFilter(teamId));
        return ResponseEntity.ok().body(wishQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /wishes/:id} : get the "id" wish.
     *
     * @param id the id of the wishDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the wishDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wishes/{id}")
    public ResponseEntity<WishDTO> getWish(@PathVariable Long id) {
        log.debug("REST request to get Wish : {}", id);
        Optional<WishDTO> wishDTO = wishService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wishDTO);
    }

    /**
     * {@code DELETE  /wishes/:id} : delete the "id" wish.
     *
     * @param id the id of the wishDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wishes/{id}")
    public ResponseEntity<Void> deleteWish(@PathVariable Long id) {
        log.debug("REST request to delete Wish : {}", id);
        wishService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @DeleteMapping("/wishes/interval")
    public ResponseEntity deleteAllWishesForInterval(@RequestParam(name = "fromDate")
                                                     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                         LocalDate fromDate, @RequestParam(name = "toDate")
                                                     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                         LocalDate toDate) {
        log.debug("REST request to get Wishes by fromDate: {}, toDate: {}", fromDate, toDate);

        if (toDate.isBefore(fromDate)) {
            return ResponseEntity.noContent().build();
        }
        this.wishService.deleteForInterval(fromDate, toDate);
        return ResponseEntity.ok().build();
    }
}
