/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hr.hpital.manager.web.rest.vm;
