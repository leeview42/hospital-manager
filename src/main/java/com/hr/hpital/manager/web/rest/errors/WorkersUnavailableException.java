package com.hr.hpital.manager.web.rest.errors;

import com.hr.hpital.manager.service.dto.UserDTO;

import java.util.Set;

public class WorkersUnavailableException extends Exception {

    private final Set<UserDTO> workers;

    public WorkersUnavailableException(Set<UserDTO> workers) {
        super();
        this.workers = workers;
    }

    public WorkersUnavailableException(Set<UserDTO> workers, Throwable throwable) {
        super(throwable);
        this.workers = workers;
    }

    public Set<UserDTO> getWorkers() {
        return workers;
    }
}
