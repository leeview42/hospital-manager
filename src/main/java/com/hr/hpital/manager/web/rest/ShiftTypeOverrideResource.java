package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.service.ShiftTypeOverrideService;
import com.hr.hpital.manager.web.rest.errors.BadRequestAlertException;
import com.hr.hpital.manager.service.dto.ShiftTypeOverrideDTO;
import com.hr.hpital.manager.service.dto.ShiftTypeOverrideCriteria;
import com.hr.hpital.manager.service.ShiftTypeOverrideQueryService;

import com.hr.hpital.manager.domain.ShiftTypeOverride;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ShiftTypeOverride}.
 */
@RestController
@RequestMapping("/api")
public class ShiftTypeOverrideResource {

    private final Logger log = LoggerFactory.getLogger(ShiftTypeOverrideResource.class);

    private static final String ENTITY_NAME = "shiftTypeOverride";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShiftTypeOverrideService shiftTypeOverrideService;

    private final ShiftTypeOverrideQueryService shiftTypeOverrideQueryService;

    public ShiftTypeOverrideResource(ShiftTypeOverrideService shiftTypeOverrideService, ShiftTypeOverrideQueryService shiftTypeOverrideQueryService) {
        this.shiftTypeOverrideService = shiftTypeOverrideService;
        this.shiftTypeOverrideQueryService = shiftTypeOverrideQueryService;
    }

    /**
     * {@code POST  /shift-type-overrides} : Create a new shiftTypeOverride.
     *
     * @param shiftTypeOverrideDTO the shiftTypeOverrideDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shiftTypeOverrideDTO, or with status {@code 400 (Bad Request)} if the shiftTypeOverride has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shift-type-overrides")
    public ResponseEntity<ShiftTypeOverrideDTO> createShiftTypeOverride(@RequestBody ShiftTypeOverrideDTO shiftTypeOverrideDTO) throws URISyntaxException {
        log.debug("REST request to save ShiftTypeOverride : {}", shiftTypeOverrideDTO);
        if (shiftTypeOverrideDTO.getId() != null) {
            throw new BadRequestAlertException("A new shiftTypeOverride cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShiftTypeOverrideDTO result = shiftTypeOverrideService.save(shiftTypeOverrideDTO);
        return ResponseEntity.created(new URI("/api/shift-type-overrides/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shift-type-overrides} : Updates an existing shiftTypeOverride.
     *
     * @param shiftTypeOverrideDTO the shiftTypeOverrideDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shiftTypeOverrideDTO,
     * or with status {@code 400 (Bad Request)} if the shiftTypeOverrideDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shiftTypeOverrideDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shift-type-overrides")
    public ResponseEntity<ShiftTypeOverrideDTO> updateShiftTypeOverride(@RequestBody ShiftTypeOverrideDTO shiftTypeOverrideDTO) throws URISyntaxException {
        log.debug("REST request to update ShiftTypeOverride : {}", shiftTypeOverrideDTO);
        if (shiftTypeOverrideDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShiftTypeOverrideDTO result = shiftTypeOverrideService.save(shiftTypeOverrideDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shiftTypeOverrideDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shift-type-overrides} : get all the shiftTypeOverrides.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shiftTypeOverrides in body.
     */
    @GetMapping("/shift-type-overrides")
    public ResponseEntity<List<ShiftTypeOverrideDTO>> getAllShiftTypeOverrides(ShiftTypeOverrideCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ShiftTypeOverrides by criteria: {}", criteria);
        Page<ShiftTypeOverrideDTO> page = shiftTypeOverrideQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /shift-type-overrides/count} : count all the shiftTypeOverrides.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/shift-type-overrides/count")
    public ResponseEntity<Long> countShiftTypeOverrides(ShiftTypeOverrideCriteria criteria) {
        log.debug("REST request to count ShiftTypeOverrides by criteria: {}", criteria);
        return ResponseEntity.ok().body(shiftTypeOverrideQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shift-type-overrides/:id} : get the "id" shiftTypeOverride.
     *
     * @param id the id of the shiftTypeOverrideDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shiftTypeOverrideDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shift-type-overrides/{id}")
    public ResponseEntity<ShiftTypeOverrideDTO> getShiftTypeOverride(@PathVariable Long id) {
        log.debug("REST request to get ShiftTypeOverride : {}", id);
        Optional<ShiftTypeOverrideDTO> shiftTypeOverrideDTO = shiftTypeOverrideService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shiftTypeOverrideDTO);
    }

    /**
     * {@code DELETE  /shift-type-overrides/:id} : delete the "id" shiftTypeOverride.
     *
     * @param id the id of the shiftTypeOverrideDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shift-type-overrides/{id}")
    public ResponseEntity<Void> deleteShiftTypeOverride(@PathVariable Long id) {
        log.debug("REST request to delete ShiftTypeOverride : {}", id);
        shiftTypeOverrideService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
