package com.hr.hpital.manager.web.rest.interceptor;

import com.hr.hpital.manager.service.RequestContext;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class TeamIdInterceptor implements HandlerInterceptor {

    private final String teamIdKey = "teamId";

    private final RequestContext requestContext;

    public TeamIdInterceptor(RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final Map<String, String[]> parametersMap = request.getParameterMap();
        if (parametersMap.containsKey(teamIdKey)) {
            requestContext.setTeamId(Long.valueOf(parametersMap.get(teamIdKey)[0]));
        } else {
            throw new AccessDeniedException("Access is denied!");
        }
        return true;
    }

}
