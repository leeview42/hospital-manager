package com.hr.hpital.manager.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class ClientRequestAlertException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    private final Map<String, Object> params;

    private final String errorKey;

    public ClientRequestAlertException(Status status, String defaultMessage, String errorKey) {
        this(status, ErrorConstants.DEFAULT_TYPE, defaultMessage, null, errorKey);
    }

    public ClientRequestAlertException(Status status, String defaultMessage, Map<String, Object> params, String errorKey) {
        this(status, ErrorConstants.DEFAULT_TYPE, defaultMessage, params, errorKey);
    }

    public ClientRequestAlertException(Status status, URI type, String defaultMessage, Map<String, Object> params, String errorKey) {
        super(type, defaultMessage, status, null, null, null, getAlertParameters(params, errorKey));
        this.params = params;
        this.errorKey = errorKey;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public String getErrorKey() {
        return errorKey;
    }

    private static Map<String, Object> getAlertParameters(Map<String, Object> params, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        if (params != null) {
            parameters.put("params", params);
        }
        return parameters;
    }
}
