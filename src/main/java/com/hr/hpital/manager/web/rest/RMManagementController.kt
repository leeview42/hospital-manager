package com.hr.hpital.manager.web.rest

import com.hr.hpital.manager.service.RMMigrationService
import com.hr.hpital.manager.service.RMUserService
import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import kotlin.concurrent.thread

@Profile("rm")
@RestController
@RequestMapping("/management")
class RMManagementController(private val rmMigrationService: RMMigrationService) {
    @GetMapping("/triggerTimekeeperImport")
    public fun triggerTimekeeperImport(): String {
        thread(start = true) {
            this.rmMigrationService.migrateTimeKeeperUsers();
        }
        return "Migration started";
    }
}
