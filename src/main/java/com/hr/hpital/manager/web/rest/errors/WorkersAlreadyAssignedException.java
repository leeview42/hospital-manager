package com.hr.hpital.manager.web.rest.errors;

import com.hr.hpital.manager.service.dto.UserDTO;

import java.time.LocalDate;
import java.util.Set;

public class WorkersAlreadyAssignedException extends Exception {

    final private Set<UserDTO> workers;
    private Set<LocalDate> assignedDays;

    public WorkersAlreadyAssignedException(Set<UserDTO> workers) {
        super();
        this.workers = workers;
    }

    public WorkersAlreadyAssignedException(Set<UserDTO> workers, Set<LocalDate> assignedDays) {
        super();
        this.workers = workers;
        this.assignedDays = assignedDays;
    }

    public WorkersAlreadyAssignedException(Set<UserDTO> workers, Throwable throwable) {
        super(throwable);
        this.workers = workers;
    }

    public WorkersAlreadyAssignedException(Set<UserDTO> workers, Set<LocalDate> assignedDays, Throwable throwable) {
        super(throwable);
        this.workers = workers;
        this.assignedDays = assignedDays;
    }

    public Set<UserDTO> getWorkers() {
        return workers;
    }

    public Set<LocalDate> getAssignedDays() {
        return assignedDays;
    }
}
