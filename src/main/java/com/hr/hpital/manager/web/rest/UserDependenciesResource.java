package com.hr.hpital.manager.web.rest;

import com.hr.hpital.manager.service.UserShiftTypeService;
import com.hr.hpital.manager.service.UserSpecializationService;
import com.hr.hpital.manager.service.dto.UserDependenciesUpdateRequestDTO;
import com.hr.hpital.manager.service.UserShiftTypeQueryService;

import com.hr.hpital.manager.domain.UserShiftType;
import com.hr.hpital.manager.web.rest.util.ResourceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing {@link UserShiftType}.
 */
@RestController
@RequestMapping("/api/team-management/user-dependencies")
public class UserDependenciesResource {

    private final Logger log = LoggerFactory.getLogger(UserDependenciesResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserShiftTypeService userShiftTypeService;

    private final UserShiftTypeQueryService userShiftTypeQueryService;

    private final UserSpecializationService userSpecializationService;

    public UserDependenciesResource(UserShiftTypeService userShiftTypeService, UserShiftTypeQueryService userShiftTypeQueryService, UserSpecializationService userSpecializationService) {
        this.userShiftTypeService = userShiftTypeService;
        this.userShiftTypeQueryService = userShiftTypeQueryService;
        this.userSpecializationService = userSpecializationService;
    }

    @PostMapping("/user-shift-types")
    public ResponseEntity updateUserShiftType(@RequestBody UserDependenciesUpdateRequestDTO userDependenciesUpdateRequestDTO, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to update UserShiftTypes : {}", userDependenciesUpdateRequestDTO);
        userShiftTypeService.saveUserShiftTypes(userDependenciesUpdateRequestDTO, teamId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/user-specializations")
    public ResponseEntity updateUserSpecializations(@RequestBody UserDependenciesUpdateRequestDTO userDependenciesUpdateRequestDTO, @RequestParam("teamId") Long teamId) {
        log.debug("REST request to update User Specializations : {}", userDependenciesUpdateRequestDTO);
        userSpecializationService.saveUserSpecializations(userDependenciesUpdateRequestDTO, ResourceUtil.getCurrentUserOrgId());
        return ResponseEntity.ok().build();
    }

}
