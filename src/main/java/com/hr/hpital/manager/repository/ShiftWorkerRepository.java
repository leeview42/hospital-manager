package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.ShiftWorker;
import com.hr.hpital.manager.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ShiftWorker entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShiftWorkerRepository extends JpaRepository<ShiftWorker, Long>, JpaSpecificationExecutor<ShiftWorker> {

    @Query("select shiftWorker from ShiftWorker shiftWorker where shiftWorker.worker.login = ?#{principal.username}")
    List<ShiftWorker> findByWorkerIsCurrentUser();

    List<ShiftWorker> findByWorkerAndShiftEndLessThanEqualAndShiftEndGreaterThanEqualAndShiftShiftTypeCanRepeatIsFalse(User user, Instant from, Instant to);

    // counts ShiftWorker entities of a worker which have intersection with [from, to] interval provided in params
    Long countByWorkerAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(User user, Instant from, Instant to);

    List<ShiftWorker> findAllByWorkerInAndShiftEndGreaterThanEqualAndShiftStartLessThanEqual(Collection<User> user, Instant from, Instant to);

    Optional<ShiftWorker> findFirstByWorkerAndShiftEndLessThanEqualOrderByShiftEndDesc(User user, Instant date);
}
