package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.DayConfigShiftType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;


/**
 * Spring Data  repository for the DayConfigShiftType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayConfigShiftTypeRepository extends JpaRepository<DayConfigShiftType, Long>, JpaSpecificationExecutor<DayConfigShiftType> {

    List<DayConfigShiftType> findByDayConfigId(Long dayConfigId);

    void deleteByDayConfigId(Long dayConfigId);

    Set<DayConfigShiftType> findByDayConfigDateBetweenAndDayConfigTeamId(LocalDate from, LocalDate to, Long teamId);

}
