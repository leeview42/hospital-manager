package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.TeamUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the TeamUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TeamUserRepository extends JpaRepository<TeamUser, Long>, JpaSpecificationExecutor<TeamUser> {

    @Query("select teamUser from TeamUser teamUser where teamUser.user.login = ?#{principal.username}")
    Set<TeamUser> findByUserIsCurrentUser();

    Set<TeamUser> findAllByTeamId(Long teamId);

    Optional<TeamUser> findByTeamIdAndUserId(Long teamId, Long userId);

    Set<TeamUser> findAllByTeamIdAndIsManagerIsFalse(Long teamId);

    Set<TeamUser> findAllByTeamIdAndIsManagerIsTrue(Long teamId);

}
