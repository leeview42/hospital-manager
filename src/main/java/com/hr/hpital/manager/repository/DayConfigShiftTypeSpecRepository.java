package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.DayConfigShiftTypeSpec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DayConfigShiftTypeSpec entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayConfigShiftTypeSpecRepository extends JpaRepository<DayConfigShiftTypeSpec, Long>, JpaSpecificationExecutor<DayConfigShiftTypeSpec> {

}
