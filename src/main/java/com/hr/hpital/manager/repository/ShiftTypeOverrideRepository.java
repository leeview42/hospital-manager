package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.ShiftTypeOverride;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ShiftTypeOverride entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShiftTypeOverrideRepository extends JpaRepository<ShiftTypeOverride, Long>, JpaSpecificationExecutor<ShiftTypeOverride> {

    Optional<ShiftTypeOverride> findFirstByShiftTypeOrCanOverride(ShiftType shiftType, ShiftType canOverride);

}
