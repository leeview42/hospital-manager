package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.Priority;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Priority entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PriorityRepository extends JpaRepository<Priority, Long>, JpaSpecificationExecutor<Priority> {

}
