package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.WishShiftType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WishShiftType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WishShiftTypeRepository extends JpaRepository<WishShiftType, Long>, JpaSpecificationExecutor<WishShiftType> {

}
