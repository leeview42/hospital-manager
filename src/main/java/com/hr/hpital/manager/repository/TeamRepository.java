package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.Team;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Team entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TeamRepository extends JpaRepository<Team, Long>, JpaSpecificationExecutor<Team> {

    public Optional<Team> findByExternalId(Long externalId);

}
