package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.Shift;
import com.hr.hpital.manager.domain.ShiftType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the Shift entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShiftRepository extends JpaRepository<Shift, Long>, JpaSpecificationExecutor<Shift> {

    @Query("select shift from Shift shift where shift.createdBy.login = ?#{principal.username}")
    List<Shift> findByCreatedByIsCurrentUser();

    @Query("select shift from Shift shift where shift.updatedBy.login = ?#{principal.username}")
    List<Shift> findByUpdatedByIsCurrentUser();

    List<Shift> findByStartBetweenAndShiftTypeTeamId(Instant dateFrom, Instant dateTo, Long teamId);

    void deleteByGeneratedIsTrueAndStartGreaterThanEqualAndEndLessThanEqualAndShiftTypeTeamId(Instant from, Instant to, Long teamId);

    void deleteByStartGreaterThanEqualAndEndLessThanEqualAndShiftTypeTeamId(Instant from, Instant to, Long teamId);
}
