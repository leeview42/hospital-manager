package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.Wish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Wish entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WishRepository extends JpaRepository<Wish, Long>, JpaSpecificationExecutor<Wish> {

    @Query("select wish from Wish wish where wish.user.login = ?#{principal.username}")
    List<Wish> findByUserIsCurrentUser();

    List<Wish> findByUserAndDateFromLessThanEqualAndDateToGreaterThanEqual(User user, Instant fromDate, Instant toDate);

    void deleteByDateFromGreaterThanEqualAndDateToLessThanEqual(Instant dateFrom, Instant dateTo);

    Optional<Wish> findByUserIdAndDateFromGreaterThanEqualAndDateToLessThanEqual(Long userId, Instant dateFrom, Instant dateTo);
}
