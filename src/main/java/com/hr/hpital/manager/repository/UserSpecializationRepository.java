package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.UserSpecialization;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the UserSpecialization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserSpecializationRepository extends JpaRepository<UserSpecialization, Long>, JpaSpecificationExecutor<UserSpecialization> {

    @Query("select userSpecialization from UserSpecialization userSpecialization where userSpecialization.user.login = ?#{principal.username}")
    List<UserSpecialization> findByUserIsCurrentUser();

    void deleteByUserId(Long userId);
}
