package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.ShiftType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ShiftType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShiftTypeRepository extends JpaRepository<ShiftType, Long>, JpaSpecificationExecutor<ShiftType> {

}
