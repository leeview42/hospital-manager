package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.ShiftDay;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ShiftDay entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShiftDayRepository extends JpaRepository<ShiftDay, Long>, JpaSpecificationExecutor<ShiftDay> {

}
