package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.DayConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the DayConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayConfigRepository extends JpaRepository<DayConfig, Long>, JpaSpecificationExecutor<DayConfig> {

    List<DayConfig> findByDateBetweenAndTeamId(LocalDate from, LocalDate to, Long teamId);

    void deleteByDateBetweenAndTeamId(LocalDate from, LocalDate to, Long teamId);

    Optional<DayConfig> findByDateAndTeamId(LocalDate date, Long teamId);

}
