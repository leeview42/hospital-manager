package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.ShiftType;
import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.UserShiftType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the UserShiftType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserShiftTypeRepository extends JpaRepository<UserShiftType, Long>, JpaSpecificationExecutor<UserShiftType> {

    @Query("select userShiftType from UserShiftType userShiftType where userShiftType.user.login = ?#{principal.username}")
    List<UserShiftType> findByUserIsCurrentUser();

    List<UserShiftType> findByShiftType(ShiftType shiftType);

    List<UserShiftType> deleteByUserIdAndShiftTypeTeamId(Long userId, Long teamId);

    Optional<UserShiftType> findFirstByUserAndShiftType(User user, ShiftType shiftType);

}
