package com.hr.hpital.manager.repository;

import com.hr.hpital.manager.domain.User;
import com.hr.hpital.manager.domain.WorkerAvailability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * Spring Data  repository for the WorkerAvailability entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WorkerAvailabilityRepository extends JpaRepository<WorkerAvailability, Long>, JpaSpecificationExecutor<WorkerAvailability> {

    @Query("select workerAvailability from WorkerAvailability workerAvailability where workerAvailability.user.login = ?#{principal.username}")
    List<WorkerAvailability> findByUserIsCurrentUser();

    // counts WorkingAvailability entities of user which intersect [dateFrom, dateTo] parameters interval
    Long countByUserAndDateFromLessThanAndDateToGreaterThanAndIsAvailableIsFalse(User user, Instant dateTo, Instant dateFrom);

    // selects all WorkingAvailability entities of users which intersect [dateFrom, dateTo] parameters interval
    List<WorkerAvailability> findAllByUserInAndDateToGreaterThanAndDateFromLessThanAndIsAvailableIsFalse(List<User> users, Instant dateFrom,  Instant dateTo);

    // returns WorkingAvailability entities of user which intersect [dateFrom, dateTo] parameters interval
    List<WorkerAvailability> findByUserAndDateFromLessThanAndDateToGreaterThanAndIsAvailableIsFalse(User user, Instant dateTo, Instant dateFrom);

    /**
     * Get WorkerAvailability in interval [dateFrom, dateTo]
     *
     * @param userId   user id for which we need to find its unavailable days
     * @param dateFrom
     * @param dateTo
     * @return
     */
    Set<WorkerAvailability> findByUserIdAndDateFromBetween(Long userId, Instant dateFrom, Instant dateTo);

    /**
     * Get WorkerAvailability for when the user is not available in interval [dateFrom, dateTo]
     *
     * @param userId   user id for which we need to find its unavailable days
     * @param dateFrom
     * @param dateTo
     * @param teamId
     * @return
     */
    List<WorkerAvailability> findByUserIdAndDateFromBetweenAndIsAvailableIsFalseAndUserTeamsId(Long userId, Instant dateFrom, Instant dateTo, Long teamId);
}
