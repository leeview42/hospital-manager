entity Shift {
	date LocalDate required,
    startDate Instant,
    endDate Instant,
    generated Boolean,
	updateDate Instant,
	createDate Instant
}

entity ShiftType {
	name String,
    code String,
    active Boolean,
    displayCode String,
    description String,
    canRepeat Boolean,
  	startHour Integer min(0) max(23),
    startMinute Integer min(0) max(59),
    duration Duration,
    fillWithRemaining Boolean,
    noOfWorkers Integer,
    mandatory Boolean
}

entity ShiftTypeOverride {

}

entity DayConfig {
	date LocalDate,
    isBankHoliday Boolean,
}

entity DayConfigShiftType {
	noOfWorkers Integer
}

entity DayConfigShiftTypeSpec {
	noOfWorkers Integer
}

entity WorkerAvailability {
	dateFrom Instant,
    dateTo Instant,
    isAvailable Boolean
}

enum WishType {
    ONLY, EXCEPT
}

entity Wish {
    comment String,
    dateFrom Instant,
    dateTo Instant,
    wishType WishType required
}

entity UserShiftType {
	ratioPercentage Integer
}

entity ShiftDay {
	dayOfWeek Integer
}

entity ShiftWorker {
}

entity WishShiftType {

}

entity Priority {
    level Integer,
    description String,
}

entity Team {
    name String,
    description String
}

entity TeamUser {
    isManager Boolean
}

entity Organization {
    name String,
    description String,
    hasSpecializations Boolean,
}

entity Specialization {
	name String required,
    description String
}

entity UserSpecialization {

}

relationship ManyToOne {
	Shift{shiftType} to ShiftType
    ShiftType{priority} to Priority
    ShiftType{team} to Team
    ShiftWorker{shift} to Shift
    ShiftWorker{worker} to User
    TeamUser{team} to Team
    TeamUser{user} to User
    Shift{createdBy} to User
    Shift{updatedBy} to User
    ShiftDay{shiftType} to ShiftType
    Wish{user} to User
    UserShiftType{user} to User
    UserShiftType{shiftType} to ShiftType
    DayConfigShiftType{dayConfig} to DayConfig
    DayConfigShiftType{shiftType} to ShiftType
    DayConfigShiftTypeSpec{dayConfigShiftType} to DayConfigShiftType
    DayConfigShiftTypeSpec{specialization} to Specialization
    WishShiftType{shiftType} to ShiftType
    WishShiftType{wish} to Wish
    WorkerAvailability{user} to User
    ShiftTypeOverride{shiftType} to ShiftType
    ShiftTypeOverride{canOverride} to ShiftType
    Team{organization} to Organization
    // User{organization} to Organization
    Specialization{organization} to Organization
    DayConfig{team} to Team
    UserSpecialization{user} to User
    UserSpecialization{specialization} to Specialization
}

// Set pagination options
paginate * with pagination

// Use Data Transfer Objects (DTO)
dto * with mapstruct

// Set service options to all except few
service all with serviceImpl

filter *
